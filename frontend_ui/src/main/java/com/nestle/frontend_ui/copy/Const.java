package com.nestle.frontend_ui.copy;

import android.os.Environment;

/**
 * Created by solon on 2018/3/31.
 */

public class Const {
    public static final String SD_FILES_NAME = "" ; //sdcard dir name
    public static final String DATA_FILES_NAME = "" ; //data dir name


    public static final int SDCARD_NO = 100 ;
    public static final int SDCARD_FILE_COPE_SUCCESS = 101 ;
    public static final int SDCARD_FILE_COPE_FAILED = 102 ;

    /**
     * get root dir
     * @return
     */
    public static String getDirAdress() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+ DATA_FILES_NAME;
    }
}
