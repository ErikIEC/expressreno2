package com.nestle.frontend_ui.copy;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by solon on 2018/3/31.
 */

public class SdcardHelper {

    public static final String TAG = SdcardHelper.class.getSimpleName();
    private volatile static SdcardHelper ourInstance = null;

    public static SdcardHelper getInstance() {
        if (ourInstance == null) {
            synchronized (SdcardHelper.class) {
                if (ourInstance == null) {
                    ourInstance = new SdcardHelper();
                }
                return ourInstance;
            }
        }
        return ourInstance;
    }

    private SdcardHelper() {
        SdcardUtils.createDir(Const.DATA_FILES_NAME);
    }

    /**
     * cope file
     */
    public void copyFileSdcardToData() {
        List<String> sdList = SdcardUtils.getExtSDCardPath();

        if (sdList.size() == 2) {
            List<String> sdcardFilePaths = SdcardUtils.getListFilePath(sdList.get(1) + Const.SD_FILES_NAME) ;

            for(String sdcardFilePath : sdcardFilePaths) {
                String tartgetFileString = Const.getDirAdress() + "/" + SdcardUtils.getFileName(sdcardFilePath) + SdcardUtils.getFilePrefix(sdcardFilePath);
                if (!SdcardUtils.fileIsExist(tartgetFileString)) {
                    File fromFile = new File(sdcardFilePath);
                    File toFile = new File(tartgetFileString);

                    try {
                        SdcardUtils.copyFile(fromFile, toFile);
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }

        } else {
            // no sdcard

        }
    }

}
