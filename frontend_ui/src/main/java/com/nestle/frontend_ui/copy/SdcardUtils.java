package com.nestle.frontend_ui.copy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by solon on 2018/3/31.
 */

public class SdcardUtils {

    /**
     * Get the sdcard path （>1 ==> sdcard  1 ==> no sdcard）
     *
     * @return
     */
    public static List<String> getExtSDCardPath() {

        List<String> lResult = new ArrayList<String>();
        try {
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec("mount");
            InputStream is = proc.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("extsd")) {
                    String[] arr = line.split(" ");
                    String path = arr[1];
                    File file = new File(path);
                    if (file.isDirectory()) {
                        lResult.add(path);
                    }
                }
            }
            isr.close();
        } catch (Exception e) {

        }
        return lResult;
    }

    /**
     * create dirs
     * @param filePath
     */
    public static void createDir(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /**
     * file exists
     *
     * @param fileName
     * @return
     */
    public static boolean fileIsExist(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return true;
        }
        return false;
    }

    /**
     * Get the file address under the directory
     *
     * @param strDirAdress
     * @return
     */
    public static ArrayList<String> getListFilePath(String strDirAdress) {
        ArrayList<String> it = new ArrayList<String>();

        File f = new File(strDirAdress + "/");
        File[] files = f.listFiles();
        if (files == null) {
            return it;
        }

        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            it.add(file.getPath());
        }
        return it;
    }

    /**
     * copy file
     *
     * @param sourceFile
     * @param targetFile
     * @throws IOException
     */
    public static void copyFile(File sourceFile, File targetFile) throws IOException {

        FileInputStream input = new FileInputStream(sourceFile);
        BufferedInputStream inBuff = new BufferedInputStream(input);


        FileOutputStream output = new FileOutputStream(targetFile);
        BufferedOutputStream outBuff = new BufferedOutputStream(output);


        byte[] b = new byte[1024 * 5];
        int len;
        while ((len = inBuff.read(b)) != -1) {
            outBuff.write(b, 0, len);
        }


        outBuff.flush();


        inBuff.close();
        outBuff.close();
        output.close();
        input.close();
    }

    /**
     * get filename
     * @param filePath
     * @return
     */
    public static String getFileName(String filePath) {
        String[] strarray = filePath.split("[/]");

        if (strarray.length == 0) {
            return "";
        }
        String end = strarray[strarray.length - 1]
                .substring(0, strarray[strarray.length - 1].lastIndexOf(".")).toLowerCase();

        return end;
    }


    /**
     * Get the file suffix
     * @param filePath
     * @return
     */
    public static String getFilePrefix(String filePath) {
        String prefix=filePath.substring(filePath.lastIndexOf(".")+1);
        return prefix ;
    }

}
