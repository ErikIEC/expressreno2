--function generateForeignStrings()
delete from string where language_id!=1;

insert into string (id, category_id, language_id, text)
select id, category_id, 2, text||" (FR)" from string
where language_id=1;

insert into string (id, category_id, language_id, text)
select id, category_id, 3, text||" (ES)" from string
where language_id=1;
