PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE `language` (
  `id` INT NOT NULL,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`id`));
INSERT INTO "language" VALUES(1,'English');
INSERT INTO "language" VALUES(2,'French');
INSERT INTO "language" VALUES(3,'Spanish');
CREATE TABLE `category` (
  `category_id` INT NOT NULL,
  `category_name` INT NOT NULL,
  PRIMARY KEY (`category_id`),

  CONSTRAINT `name`
    FOREIGN KEY (`category_name`)
    REFERENCES `string` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
INSERT INTO "category" VALUES(1,1);
INSERT INTO "category" VALUES(2,2);
CREATE TABLE `product` (
  `product_id` INT NOT NULL,
  `category_id` INT NOT NULL,
  `art_id` INT NOT NULL,
  `product_type` INT NOT NULL,
  `product_name` INT NOT NULL,
  `color` INT NOT NULL,
  `ratio` TEXT NOT NULL,
  `brix` TEXT NOT NULL,
  `settings` BLOB NULL,
  PRIMARY KEY (`product_id`),


  CONSTRAINT `category_id`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`category_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `name`
    FOREIGN KEY (`product_name`)
    REFERENCES `string` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
INSERT INTO "product" VALUES(1,1,1,1,1,0,'1+2','12.3',X'ACED000573720039636F6D2E6E6573746C652E6261636B656E642E70726F647563742E7374727563747572652E4E6573746C6550726F6475637453657474696E674554C8F9FEC3C25102000749000B627574746F6E5365747570440009737065656446696C6C440009737065656450756D70490009737465707350756D7049000A737465707353746172745B000F74696D65436F6E63656E74726174657400025B495B000C74696D6544697370656E736571007E000178700000000040A7700000000000408F4000000000000000025800000191757200025B494DBA602676EAB2A50200007870000000030000000100000002000000037571007E000300000003000000020000000300000004');
INSERT INTO "product" VALUES(2,1,2,1,2,0,'1+3','12.4',X'ACED000573720039636F6D2E6E6573746C652E6261636B656E642E70726F647563742E7374727563747572652E4E6573746C6550726F6475637453657474696E674554C8F9FEC3C25102000749000B627574746F6E5365747570440009737065656446696C6C440009737065656450756D70490009737465707350756D7049000A737465707353746172745B000F74696D65436F6E63656E74726174657400025B495B000C74696D6544697370656E736571007E0001787000000000407F400000000000407F400000000000000004B000000320757200025B494DBA602676EAB2A50200007870000000030000000100000002000000037571007E000300000003000000020000000300000004');
INSERT INTO "product" VALUES(3,2,3,1,3,0,'1+4','12.5',X'ACED000573720039636F6D2E6E6573746C652E6261636B656E642E70726F647563742E7374727563747572652E4E6573746C6550726F6475637453657474696E674554C8F9FEC3C25102000749000B627574746F6E5365747570440009737065656446696C6C440009737065656450756D70490009737465707350756D7049000A737465707353746172745B000F74696D65436F6E63656E74726174657400025B495B000C74696D6544697370656E736571007E0001787000000000409F3F33333333334082F000000000000000025800000193757200025B494DBA602676EAB2A50200007870000000030000000100000002000000037571007E000300000003000000020000000300000004');
INSERT INTO "product" VALUES(4,2,4,1,4,0,'1+5','12.6',X'ACED000573720039636F6D2E6E6573746C652E6261636B656E642E70726F647563742E7374727563747572652E4E6573746C6550726F6475637453657474696E674554C8F9FEC3C25102000749000B627574746F6E5365747570440009737065656446696C6C440009737065656450756D70490009737465707350756D7049000A737465707353746172745B000F74696D65436F6E63656E74726174657400025B495B000C74696D6544697370656E736571007E0001787000000000409F3F33333333334082F000000000000000025800000194757200025B494DBA602676EAB2A50200007870000000030000000100000002000000037571007E000300000003000000020000000300000004');
CREATE TABLE `sale` (
  `hour` INT NOT NULL,
  `product_id` INT NOT NULL,
  `dispense_time` INT NOT NULL,
  PRIMARY KEY (`hour`, `product_id`),

  CONSTRAINT `product_id`
    FOREIGN KEY (`product_id`)
    REFERENCES `product` (`product_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
INSERT INTO "sale" VALUES(408397,1,1000);
INSERT INTO "sale" VALUES(408397,2,2000);
INSERT INTO "sale" VALUES(408397,3,2500);
INSERT INTO "sale" VALUES(408397,4,3000);
CREATE TABLE `fault_code` (
  `fault_code_id` INT NOT NULL,
  `video_id` INT NOT NULL,
  `fault_name` INT NOT NULL,
  `fault_description` INT NOT NULL,
  PRIMARY KEY (`fault_code_id`),


  CONSTRAINT `name`
    FOREIGN KEY (`fault_name`)
    REFERENCES `string` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `description`
    FOREIGN KEY (`fault_description`)
    REFERENCES `string` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
INSERT INTO "fault_code" VALUES(1,1,1,1);
INSERT INTO "fault_code" VALUES(2,1,2,2);
INSERT INTO "fault_code" VALUES(3,1,3,3);
INSERT INTO "fault_code" VALUES(4,1,4,4);
INSERT INTO "fault_code" VALUES(5,2,5,5);
INSERT INTO "fault_code" VALUES(6,3,6,6);
INSERT INTO "fault_code" VALUES(7,4,7,7);
INSERT INTO "fault_code" VALUES(8,5,8,8);
INSERT INTO "fault_code" VALUES(9,6,9,9);
INSERT INTO "fault_code" VALUES(10,7,10,10);
INSERT INTO "fault_code" VALUES(11,8,11,11);
CREATE TABLE product_setting (
    "product_id" INT NOT NULL,
    "settings" BLOB
);
CREATE TABLE `note` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `timestamp` INT NOT NULL,
  `note` TEXT NOT NULL
  );
INSERT INTO "note" VALUES(1,1470229200,'Note 1');
INSERT INTO "note" VALUES(2,1470229800,'Note 2');
INSERT INTO "note" VALUES(3,1470230000,'Note 3');
INSERT INTO "note" VALUES(4,1470240000,'Note 4');
ANALYZE sqlite_master;
CREATE TABLE `fault` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `fault_code` INT NOT NULL,
  `timestamp` INT NOT NULL,

  CONSTRAINT `fault_code`
    FOREIGN KEY (`fault_code`)
    REFERENCES `fault_code` (`fault_code_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
INSERT INTO "fault" VALUES(1,1,1470246402);
CREATE TABLE "product_setting_blob_source" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "speed_fill" REAL NOT NULL,
    "speed_pump" REAL NOT NULL,
    "steps_start" INTEGER NOT NULL,
    "steps_pump" INTEGER NOT NULL,
    "dispense_small" INTEGER NOT NULL,
    "dispense_medium" INTEGER NOT NULL,
    "dispense_large" INTEGER NOT NULL,
    "concentrate_small" INTEGER NOT NULL,
    "concentrate_medium" INTEGER NOT NULL,
    "concentrate_large" INTEGER NOT NULL,
    "buttton_setup" INTEGER NOT NULL
);
INSERT INTO "product_setting_blob_source" VALUES(1,3000.0,1000.0,401,600,2,3,4,1,2,3,0);
INSERT INTO "product_setting_blob_source" VALUES(2,500.0,500.0,800,1200,2,3,4,1,2,3,0);
INSERT INTO "product_setting_blob_source" VALUES(3,1999.8,606.0,403,600,2,3,4,1,2,3,0);
INSERT INTO "product_setting_blob_source" VALUES(4,1999.8,606.0,404,600,2,3,4,1,2,3,0);
CREATE TABLE string (
    "id" INT NOT NULL,
    "category_id" INT NOT NULL,
    "language_id" INT NOT NULL,
    "text" TEXT NOT NULL,
    primary key ( id, category_id, language_id)
);
INSERT INTO "string" VALUES(1,2,1,'Dispense Solenoid 1 Fault');
INSERT INTO "string" VALUES(2,2,1,'Dispense Solenoid 2 Fault');
INSERT INTO "string" VALUES(3,2,1,'Dispense Solenoid 3 Fault');
INSERT INTO "string" VALUES(4,2,1,'Dispense Solenoid 4 Fault');
INSERT INTO "string" VALUES(5,2,1,'Concentrate Temperature Fault');
INSERT INTO "string" VALUES(6,2,1,'Low Voltage Fault');
INSERT INTO "string" VALUES(7,2,1,'Refrigeration Fault');
INSERT INTO "string" VALUES(8,2,1,'Water Temperature Fault');
INSERT INTO "string" VALUES(9,2,1,'Refrigeration Switching Fault');
INSERT INTO "string" VALUES(10,2,1,'Slow Cooling Fault');
INSERT INTO "string" VALUES(11,2,1,'Concentrate Over-temperature Fault');
INSERT INTO "string" VALUES(1,3,1,'Description of Dispense Solenoid 1 Fault ');
INSERT INTO "string" VALUES(2,3,1,'Description of Dispense Solenoid 2 Fault');
INSERT INTO "string" VALUES(3,3,1,'Description of Dispense Solenoid 3 Fault');
INSERT INTO "string" VALUES(4,3,1,'Description of Dispense Solenoid 4 Fault');
INSERT INTO "string" VALUES(5,3,1,'Concentrate Temperature Fault Description');
INSERT INTO "string" VALUES(6,3,1,'Low Voltage Fault Description');
INSERT INTO "string" VALUES(7,3,1,'Refrigeration Fault Description');
INSERT INTO "string" VALUES(8,3,1,'Water Temperature Fault Description');
INSERT INTO "string" VALUES(9,3,1,'Refrigeration Switching Fault Description');
INSERT INTO "string" VALUES(10,3,1,'Slow Cooling Fault Description');
INSERT INTO "string" VALUES(11,3,1,'Concentrate Over-temperature Fault Description');
INSERT INTO "string" VALUES(1,4,1,'Juice Category');
INSERT INTO "string" VALUES(2,4,1,'Tea Category');
INSERT INTO "string" VALUES(1,5,1,'Apple Juice');
INSERT INTO "string" VALUES(2,5,1,'Orange Juice');
INSERT INTO "string" VALUES(3,5,1,'Green Tea');
INSERT INTO "string" VALUES(4,5,1,'Mint Tea');
INSERT INTO "string" VALUES(1,2,2,'Dispense Solenoid 1 Fault (FR)');
INSERT INTO "string" VALUES(2,2,2,'Dispense Solenoid 2 Fault (FR)');
INSERT INTO "string" VALUES(3,2,2,'Dispense Solenoid 3 Fault (FR)');
INSERT INTO "string" VALUES(4,2,2,'Dispense Solenoid 4 Fault (FR)');
INSERT INTO "string" VALUES(5,2,2,'Concentrate Temperature Fault (FR)');
INSERT INTO "string" VALUES(6,2,2,'Low Voltage Fault (FR)');
INSERT INTO "string" VALUES(7,2,2,'Refrigeration Fault (FR)');
INSERT INTO "string" VALUES(8,2,2,'Water Temperature Fault (FR)');
INSERT INTO "string" VALUES(9,2,2,'Refrigeration Switching Fault (FR)');
INSERT INTO "string" VALUES(10,2,2,'Slow Cooling Fault (FR)');
INSERT INTO "string" VALUES(11,2,2,'Concentrate Over-temperature Fault (FR)');
INSERT INTO "string" VALUES(1,3,2,'Description of Dispense Solenoid 1 Fault  (FR)');
INSERT INTO "string" VALUES(2,3,2,'Description of Dispense Solenoid 2 Fault (FR)');
INSERT INTO "string" VALUES(3,3,2,'Description of Dispense Solenoid 3 Fault (FR)');
INSERT INTO "string" VALUES(4,3,2,'Description of Dispense Solenoid 4 Fault (FR)');
INSERT INTO "string" VALUES(5,3,2,'Concentrate Temperature Fault Description (FR)');
INSERT INTO "string" VALUES(6,3,2,'Low Voltage Fault Description (FR)');
INSERT INTO "string" VALUES(7,3,2,'Refrigeration Fault Description (FR)');
INSERT INTO "string" VALUES(8,3,2,'Water Temperature Fault Description (FR)');
INSERT INTO "string" VALUES(9,3,2,'Refrigeration Switching Fault Description (FR)');
INSERT INTO "string" VALUES(10,3,2,'Slow Cooling Fault Description (FR)');
INSERT INTO "string" VALUES(11,3,2,'Concentrate Over-temperature Fault Description (FR)');
INSERT INTO "string" VALUES(1,4,2,'Juice Category (FR)');
INSERT INTO "string" VALUES(2,4,2,'Tea Category (FR)');
INSERT INTO "string" VALUES(1,5,2,'Apple Juice (FR)');
INSERT INTO "string" VALUES(2,5,2,'Orange Juice (FR)');
INSERT INTO "string" VALUES(3,5,2,'Green Tea (FR)');
INSERT INTO "string" VALUES(4,5,2,'Mint Tea (FR)');
INSERT INTO "string" VALUES(1,2,3,'Dispense Solenoid 1 Fault (ES)');
INSERT INTO "string" VALUES(2,2,3,'Dispense Solenoid 2 Fault (ES)');
INSERT INTO "string" VALUES(3,2,3,'Dispense Solenoid 3 Fault (ES)');
INSERT INTO "string" VALUES(4,2,3,'Dispense Solenoid 4 Fault (ES)');
INSERT INTO "string" VALUES(5,2,3,'Concentrate Temperature Fault (ES)');
INSERT INTO "string" VALUES(6,2,3,'Low Voltage Fault (ES)');
INSERT INTO "string" VALUES(7,2,3,'Refrigeration Fault (ES)');
INSERT INTO "string" VALUES(8,2,3,'Water Temperature Fault (ES)');
INSERT INTO "string" VALUES(9,2,3,'Refrigeration Switching Fault (ES)');
INSERT INTO "string" VALUES(10,2,3,'Slow Cooling Fault (ES)');
INSERT INTO "string" VALUES(11,2,3,'Concentrate Over-temperature Fault (ES)');
INSERT INTO "string" VALUES(1,3,3,'Description of Dispense Solenoid 1 Fault  (ES)');
INSERT INTO "string" VALUES(2,3,3,'Description of Dispense Solenoid 2 Fault (ES)');
INSERT INTO "string" VALUES(3,3,3,'Description of Dispense Solenoid 3 Fault (ES)');
INSERT INTO "string" VALUES(4,3,3,'Description of Dispense Solenoid 4 Fault (ES)');
INSERT INTO "string" VALUES(5,3,3,'Concentrate Temperature Fault Description (ES)');
INSERT INTO "string" VALUES(6,3,3,'Low Voltage Fault Description (ES)');
INSERT INTO "string" VALUES(7,3,3,'Refrigeration Fault Description (ES)');
INSERT INTO "string" VALUES(8,3,3,'Water Temperature Fault Description (ES)');
INSERT INTO "string" VALUES(9,3,3,'Refrigeration Switching Fault Description (ES)');
INSERT INTO "string" VALUES(10,3,3,'Slow Cooling Fault Description (ES)');
INSERT INTO "string" VALUES(11,3,3,'Concentrate Over-temperature Fault Description (ES)');
INSERT INTO "string" VALUES(1,4,3,'Juice Category (ES)');
INSERT INTO "string" VALUES(2,4,3,'Tea Category (ES)');
INSERT INTO "string" VALUES(1,5,3,'Apple Juice (ES)');
INSERT INTO "string" VALUES(2,5,3,'Orange Juice (ES)');
INSERT INTO "string" VALUES(3,5,3,'Green Tea (ES)');
INSERT INTO "string" VALUES(4,5,3,'Mint Tea (ES)');
INSERT INTO "string" VALUES(100,100,100,'Foo!');
INSERT INTO "string" VALUES(1,1,1,'English!');
INSERT INTO "string" VALUES(2,1,1,'Français!');
INSERT INTO "string" VALUES(3,1,1,'Español!');
INSERT INTO "string" VALUES(4,1,1,'Home!');
INSERT INTO "string" VALUES(5,1,1,'Back!');
INSERT INTO "string" VALUES(6,1,1,'Okay!');
INSERT INTO "string" VALUES(7,1,1,'Invalid!');
INSERT INTO "string" VALUES(8,1,1,'Clear!');
INSERT INTO "string" VALUES(9,1,1,'PIN!');
INSERT INTO "string" VALUES(10,1,1,'Product
Selection!');
INSERT INTO "string" VALUES(11,1,1,'Set
Portions!');
INSERT INTO "string" VALUES(12,1,1,'Rinse!');
INSERT INTO "string" VALUES(13,1,1,'Dispenser
Lock!');
INSERT INTO "string" VALUES(14,1,1,'Tutorials!');
INSERT INTO "string" VALUES(15,1,1,'System
Data!');
INSERT INTO "string" VALUES(16,1,1,'Dispense Status!');
INSERT INTO "string" VALUES(17,6,1,'Ready!');
INSERT INTO "string" VALUES(18,6,1,'Not Ready!');
INSERT INTO "string" VALUES(19,6,1,'Water
Temperature!');
INSERT INTO "string" VALUES(20,6,1,'Concentrate
Temperature!');
INSERT INTO "string" VALUES(21,1,1,'Service Center 1-800-367-5813!');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('note',4);
INSERT INTO "sqlite_sequence" VALUES('fault',2);
INSERT INTO "sqlite_sequence" VALUES('product_setting_blob_source',4);
CREATE UNIQUE INDEX product_product_id on product(product_id);
CREATE UNIQUE INDEX product_setting_product_id on product_setting(product_id);
COMMIT;
