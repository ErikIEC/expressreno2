package com.nestle.backend.product.structure;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseSerializer
{
	public static byte[] serialize(Object input)
	{
		ByteArrayOutputStream outputSteam = new ByteArrayOutputStream();
		ObjectOutput outputObject = null;
		byte[] result=null;

		try
		{
			outputObject = new ObjectOutputStream(outputSteam);
			outputObject.writeObject(input);
			result = outputSteam.toByteArray();
			outputObject.close();
			outputSteam.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}

	public static Object deserialize(byte[] array)
	{
		ByteArrayInputStream inputByteStream = new ByteArrayInputStream(array);
		ObjectInput objectInput = null;
		Object result = null;

		try
		{
			objectInput = new ObjectInputStream(inputByteStream);
			result = objectInput.readObject();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return result;
	}
}

