package com.nestle.backend.product.structure;

import java.io.Serializable;

/**
 * Created by jason on 8/3/16.
 */
public class NestleProductSetting implements Serializable
{
	public double speedFill, speedPump;
	public int stepsStart, stepsPump, buttonSetup, timeDispense[], timeConcentrate[];
}

