/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setting_serializer;

import com.nestle.backend.product.structure.DatabaseSerializer;
import com.nestle.backend.product.structure.NestleProductSetting;

import java.sql.*;

/**
 *
 * @author jason
 */
public class SettingSerializer
{

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws Exception
	{
		System.out.println("Nestle Express Product Setting Row to serialized settings BLOB converter utility.");

		if (args.length!=1)
		{
			System.out.println("Wrong number of arguments received");
			System.out.println("Syntax: <Program Name> /Path/to/database.db");
			return;
		}

		String location="jdbc:sqlite:"+args[0];
		System.out.println("Opening Sqlite3 database at "+location);

		// TODO code application logic here
		Connection connection= DriverManager.getConnection(location);
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.

		System.out.println("select * from product_setting_blob_source");
		ResultSet result = statement.executeQuery("select * from product_setting_blob_source");
		while(result.next())
		{
			// read the result set
			NestleProductSetting setting = new NestleProductSetting();
			setting.timeDispense = new int[3];
			setting.timeConcentrate = new int[3];

			setting.speedFill = result.getDouble("speed_fill");
			setting.speedPump = result.getDouble("speed_pump");
			setting.stepsStart = result.getInt("steps_start");
			setting.stepsPump = result.getInt("steps_pump");
			setting.timeDispense[0] = result.getInt("dispense_small");
			setting.timeDispense[1] = result.getInt("dispense_medium");
			setting.timeDispense[2] = result.getInt("dispense_large");
			setting.timeConcentrate[0] = result.getInt("concentrate_small");
			setting.timeConcentrate[1] = result.getInt("concentrate_medium");
			setting.timeConcentrate[2] = result.getInt("concentrate_large");
			setting.buttonSetup = result.getInt("buttton_setup");

			byte[] array= DatabaseSerializer.serialize(setting);
			int id=result.getInt("id");
			System.out.println("update table `product` where id = " + result.getInt("id") + " with BLOB of ="+array.length+" bytes");

			PreparedStatement prepared=connection.prepareStatement(
					"update product "+
					"set settings=? "+
					"where product_id=?");
			prepared.setBytes(1, array);
			prepared.setInt(2, id);

			prepared.execute();
		}

		connection.close();
		System.out.println("Database closed");
	}
	
}
