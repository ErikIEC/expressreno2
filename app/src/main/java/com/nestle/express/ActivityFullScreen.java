package com.nestle.express;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


/**
 * Created by jason on 7/28/16.
 */
public class ActivityFullScreen extends Activity
{
	DevicePolicyManager mDpm;
	ActivityManager activityManager;
	//TODO : This runnable is always executed, never destroyed. Even when the Activity is destroyed, the runnable is still running
	Runnable runSetFullScreen = new Runnable()
	{
		@Override
		public void run()
		{
			setFullscreen();
			handler.postDelayed(runSetFullScreen, 1000);
		}
	};

	Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		mDpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		handler.postDelayed(runSetFullScreen, 1000);

		if (!mDpm.isLockTaskPermitted(this.getPackageName()))
		{
			Context context = getApplicationContext();
			Toast.makeText(context, "Must Be Device Admin", Toast.LENGTH_LONG);
		}
	}

	void setFullscreen()
	{
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
						| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
						| View.SYSTEM_UI_FLAG_LOW_PROFILE
						| View.SYSTEM_UI_FLAG_FULLSCREEN
						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	void activateScreenPinning()
	{
		Log.d("PINNING", this.getPackageName());


		/*List<ActivityManager.RunningTaskInfo> services = activityManager.getRunningTasks(Integer.MAX_VALUE);

		boolean inForeground = false;

		if (services.get(0).topActivity.getPackageName().toString().equalsIgnoreCase(getPackageName().toString()))
			inForeground=true;

		if (inForeground)
		{*/
		//ComponentName mDeviceAdminSample = new ComponentName(this, ReceiverAdmin.class);

		if (mDpm.isLockTaskPermitted(this.getPackageName()) && !activityManager.isInLockTaskMode())
		{
			Log.d("PINNING", "ALLOWED");
			startLockTask();
		}
		else
			Log.d("PINNING", "NOT ALLOWED");
		//}
	}

	void deactivateScreenPinning()
	{
		//Causes security exception because the Updater is the device owner, not this app.
		//as a result the only way to unlock is to forcibly quit with System.exit(1)
		if (mDpm.isLockTaskPermitted(this.getPackageName()) && activityManager.isInLockTaskMode())
			stopLockTask();
	}

	@Override
	public void onWindowFocusChanged(boolean focus)
	{
		super.onWindowFocusChanged(focus);

		if (focus)
		{
			activateScreenPinning();
			setFullscreen();
		}
	}

	@Override
	public void onBackPressed()
	{
		//if (getFragmentManager().getBackStackEntryCount() > 0 )
		//	getFragmentManager().popBackStack();
		//else
		//	super.onBackPressed();
	}
}
