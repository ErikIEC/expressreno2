package com.nestle.express.test;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.nestle.express.communications.protocol_layer.events.EventDispense;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.RinseRequestCallback;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.R;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by jason on 8/5/16.
 */
public class ActivityTestBackend extends Activity
{


	ExpressApp mApp;
	ApplicationManager mAppManager;
	MachineManager mMachineManager;
	ProductManager mProductManager;
	NestleProduct mProduct;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_backend);

		mApp=(ExpressApp) getApplication();
		mProductManager=mApp.getProductManager();
		mMachineManager=mApp.getMachineManager();
		mAppManager=mApp.getApplicationManager();
		mAppManager.setOnRinseCallback(mRinseCallback);
		mProduct=mProductManager.getSelectedProduct(ProductSlot.SLOT1);

		ApplicationManager.FRONTEND_STATE state=mAppManager.getFrontendState();

		if (state== ApplicationManager.FRONTEND_STATE.SET_PORTIONS)
			Log.d("TEST", "Set Portions");

		//Time in milliseconds
		mProduct.setTimeSmall(1000);
		mProduct.setTimeMedium(2000);
		mProduct.setTimeLarge(3000);
		mProduct.setTimeConcentrate(1500);

		mProductManager.updateSelectedProductSetting(mProduct);
	}


	final ProductSlot mSlot=ProductSlot.SLOT1;
	@Override
	protected void onResume()
	{
		super.onResume();

		RinseStatus status=mAppManager.getRinseStatus();
		handleRinseStatus(mSlot, status);
	}

	RinseRequestCallback mRinseCallback = new RinseRequestCallback()
	{
		@Override
		public void onRinse(RinseStatus status)
		{
			handleRinseStatus(mSlot, status);
		}
	};

	void handleRinseStatus(ProductSlot slot, RinseStatus status)
	{
		RinseStatus.RINSE_STATUS rinseMode=status.getRinseStatus(slot);

		if (rinseMode== RinseStatus.RINSE_STATUS.MANUAL_RINSE)
			Log.d("RINSE", "Manual Rinse");
		else if (rinseMode==RinseStatus.RINSE_STATUS.DAILY_RINSE)
			Log.d("RINSE", "Daily Rinse");
		else if (rinseMode==RinseStatus.RINSE_STATUS.ALLERGEN_RINSE)
			Log.d("RINSE", "Allergen Rinse");
	}




	public void onSmall(View v)
	{
		ExpressApp mApp= (ExpressApp) getApplication();
		ProductManager mProductManager = mApp.getProductManager();
		MachineManager mMachine = mApp.getMachineManager();
		NestleProduct mProduct = mProductManager.getSelectedProduct(ProductSlot.SLOT1);

		mMachine.presizedDispenseStart(mProduct, EventDispense.Mode.MODE_NORMAL,
				MachineManager.ProductSize.SMALL, mRunnable);
	}

	public void onMedium(View v)
	{
		ExpressApp mApp= (ExpressApp) getApplication();
		ProductManager mProductManager = mApp.getProductManager();
		MachineManager mMachine = mApp.getMachineManager();
		NestleProduct mProduct = mProductManager.getSelectedProduct(ProductSlot.SLOT1);

		mMachine.presizedDispenseStart(mProduct, EventDispense.Mode.MODE_NORMAL,
				MachineManager.ProductSize.MEDIUM, mRunnable);
	}

	public void onLarge(View v)
	{
		ExpressApp mApp= (ExpressApp) getApplication();
		ProductManager mProductManager = mApp.getProductManager();
		MachineManager mMachine = mApp.getMachineManager();
		NestleProduct mProduct = mProductManager.getSelectedProduct(ProductSlot.SLOT1);

		mMachine.presizedDispenseStart(mProduct, EventDispense.Mode.MODE_NORMAL,
				MachineManager.ProductSize.LARGE, mRunnable);
	}

	public void onConcentrate(View v)
	{
		ExpressApp mApp= (ExpressApp) getApplication();
		ProductManager mProductManager = mApp.getProductManager();
		MachineManager mMachine = mApp.getMachineManager();
		NestleProduct mProduct = mProductManager.getSelectedProduct(ProductSlot.SLOT1);

		mMachine.presizedDispenseStart(mProduct, EventDispense.Mode.MODE_CONCENTRATE,
				MachineManager.ProductSize.CONCENTRATE, mRunnable);

	}

	Runnable mRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			Toast.makeText(getApplicationContext(),(String) "Finish",
					Toast.LENGTH_SHORT).show();
		}
	};

	public void onDaily(View v)
	{
		mMachineManager.rinseStart(mProduct, RinseStatus.RINSE_STATUS.DAILY_RINSE, mRunnable);
	}

	public void onAllergen(View v)
	{
		mMachineManager.rinseStart(mProduct, RinseStatus.RINSE_STATUS.ALLERGEN_RINSE, mRunnable);
	}


	public void onManual(View v)
	{
		mMachineManager.rinseStart(mProduct, RinseStatus.RINSE_STATUS.MANUAL_RINSE, null);
	}

	public void stopRinse(View v)
	{
		mMachineManager.rinseEnd(mProduct);
	}

	public void stopDispense(View v)
	{
		mMachineManager.dispenseEnd(mProduct);
	}
	public void startDispense(View v)
	{
		mMachineManager.dispenseStart(mProduct, EventDispense.Mode.MODE_NORMAL);
	}

	public void quit(View v)
	{
		finish();
	}
}
