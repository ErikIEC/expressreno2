/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.iec_backend.machine.structure;

/**
 *
 * @author jason
 */
public class TemperatureHistory
{
	int secondCounter, minuteCounter;
	//For previous 60 seconds (readings, one per second)
	MinMaxHistory mPastMinuteWater1, mPastMinuteWater2, mPastMinuteConcentrate;
	final int NUMBER_SECONDS=60;
	//For previous 48 hours
	MinMaxHistory mPastWater1, mPastWater2, mPastConcentrate;
	final int NUMBER_PAST=48*60;

	public TemperatureHistory()
	{
		mPastMinuteWater1=new MinMaxHistory(NUMBER_SECONDS);
		mPastMinuteWater2=new MinMaxHistory(NUMBER_SECONDS);
		mPastMinuteConcentrate=new MinMaxHistory(NUMBER_SECONDS);

		mPastWater1=new MinMaxHistory(NUMBER_PAST);
		mPastWater2=new MinMaxHistory(NUMBER_PAST);
		mPastConcentrate=new MinMaxHistory(NUMBER_PAST);
	}

	public void addTemperatures(double water1, double water2, double concentrate)
	{
		mPastMinuteWater1.add(water1);
		mPastMinuteWater2.add(water2);
		mPastMinuteConcentrate.add(concentrate);

		secondCounter++;
		if (secondCounter>=NUMBER_SECONDS)
		{
			secondCounter=0;
			mPastWater1.add(mPastMinuteWater1.getMinAndMax());
			mPastWater2.add(mPastMinuteWater2.getMinAndMax());
			mPastConcentrate.add(mPastMinuteConcentrate.getMinAndMax());
		}
	}

	public MinMaxRow getRangeWater1()
	{
		return mPastWater1.getMinAndMax();
	}

	public MinMaxRow getRangeWater2()
	{
		return mPastWater2.getMinAndMax();
	}

	public MinMaxRow getRangeConcentrate()
	{
		return mPastConcentrate.getMinAndMax();
	}
}