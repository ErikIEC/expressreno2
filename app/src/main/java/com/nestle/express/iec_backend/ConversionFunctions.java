package com.nestle.express.iec_backend;

import com.nestle.express.iec_backend.application.ApplicationManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by jason on 1/4/17.
 */
public class ConversionFunctions
{
	public static double convertFtoC(double f)
	{
		double celsius=(f-32)*5/9;
		return celsius;
	}

	public static double convertOztoMl(double oz)
	{
		double ml=29.5735*oz;
		return ml;
	}

	public static String formatTemperature(ApplicationManager appManager, double degreesF)
	{
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		DecimalFormat format = new DecimalFormat("#0.0", otherSymbols);

		if (appManager.getTempUnits()== ApplicationManager.UNITS.IMPERIAL)
			return format.format(degreesF)+"°F";

		double celsius=ConversionFunctions.convertFtoC(degreesF);
		return format.format(celsius)+"°C";
	}

	public static String formatVolume(ApplicationManager appManager, double ounce)
	{
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		DecimalFormat format = new DecimalFormat("#0.0#", otherSymbols);

		if (appManager.getTempUnits()== ApplicationManager.UNITS.IMPERIAL)
			return format.format(ounce)+" oz";

		double celsius=ConversionFunctions.convertOztoMl(ounce);
		return format.format(celsius)+" ml";
	}
}
