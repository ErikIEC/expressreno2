package com.nestle.express.iec_backend.machine;

import android.content.Context;
import android.support.annotation.StringRes;

import com.nestle.express.communications.protocol_layer.events.EventStatusResponse;
import com.nestle.express.iec_backend.R;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.ArrayList;

/**
 * Created by jason on 12/21/16.
 */
public class FaultMatrix
{
	final String TAG="FaultMatrix";
	public static final int FAULT_F1_CH1=0;
	public static final int FAULT_F1_CH2=1;
	public static final int FAULT_F1_CH3=2;
	public static final int FAULT_F1_CH4=3;
	public static final int FAULT_F1_CH5=4;
	public static final int FAULT_F2=5;
	public static final int FAULT_F3_CH1_1=6;
	public static final int FAULT_F3_CH1_2=7;
	public static final int FAULT_F3_CH1_3=8;
	public static final int FAULT_F3_CH1_4=9;
	public static final int FAULT_F3_CH1_5=10;
	public static final int FAULT_F3_CH2_1=11;
	public static final int FAULT_F3_CH2_2=12;
	public static final int FAULT_F3_CH2_3=13;
	public static final int FAULT_F3_CH2_4=14;
	public static final int FAULT_F3_CH2_5=15;
	public static final int FAULT_F3_CH3_1=16;
	public static final int FAULT_F3_CH3_2=17;
	public static final int FAULT_F3_CH3_3=18;
	public static final int FAULT_F3_CH3_4=19;
	public static final int FAULT_F3_CH3_5=20;
	public static final int FAULT_F3_CH4_1=21;
	public static final int FAULT_F3_CH4_2=22;
	public static final int FAULT_F3_CH4_3=23;
	public static final int FAULT_F3_CH4_4=24;
	public static final int FAULT_F3_CH4_5=25;
	public static final int FAULT_F4_1=26;
	public static final int FAULT_F4_2=27;
	public static final int FAULT_F5_1=28;
	public static final int FAULT_F5_2=29;
	public static final int FAULT_F6_1=30;
	public static final int FAULT_F6_2=31;
    public static final int FAULT_F6_3=32;
	public static final int FAULT_F7=33;
	public static final int FAULT_F8=34;
	public static final int FAULT_F8_2=35;
	public static final int FAULT_MAX=36;


	public static final int CHANNEL_MAX=5;

	boolean[] mActiveFaults=new boolean[FAULT_MAX];
	boolean[] mPreviousFaults=new boolean[FAULT_MAX];
	boolean[] mIsNewFault=new boolean[FAULT_MAX];

	boolean[] mActiveWarning=new boolean[FAULT_MAX];
	boolean[] mPreviousWarning=new boolean[FAULT_MAX];
	boolean[] mIsNewWarning=new boolean[FAULT_MAX];

	String[] mFaultText=new String[FAULT_MAX];
	String mFaultMessages;

	Runnable mOnFaultCallback;
	OnNewFaultCallback mOnNewFaultCallback;
    OnNewWarningCallback mOnNewWarningCallback;
	boolean[] mSlotFaultStatus =new boolean[CHANNEL_MAX];
	boolean[] mSlotWarnStatus =new boolean[CHANNEL_MAX];
	Context mContext;

	public FaultMatrix(Context context)
	{
		mContext=context;
	}

	void updateState(EventStatusResponse response)
	{
		clearActiveFaults();

		//F1 and F3
		handleMotor(0, response.getMotor1());
		handleMotor(1, response.getMotor2());
		handleMotor(2, response.getMotor3());
		handleMotor(3, response.getMotor4());

		//F2
		if (response.isFaultConcentrate())
		{
			activateFault(FAULT_F2);
			mFaultText[FAULT_F2]+=getString(R.string.fault_f2);
		}

		//F4
		if (response.isFlagWaterSolErr())
		{
			activateFault(FAULT_F4_1);
			mFaultText[FAULT_F4_1]+=getString(R.string.fault_f4_1);
		}
		if (response.isFlagConcSolErr())
		{
			activateFault(FAULT_F4_2);
			mFaultText[FAULT_F4_2]+=getString(R.string.fault_f4_2);
		}

		//F5
		if (response.isFaultWater1())
		{
			activateFault(FAULT_F5_1);
			mFaultText[FAULT_F5_1]+=getString(R.string.fault_f5_1);
		}
		if (response.isFaultWater2())
		{
			activateFault(FAULT_F5_2);
			mFaultText[FAULT_F5_2]+=getString(R.string.fault_f5_2);
		}

		//F6
		/*if (response.isFlagCompErr())
		{
			activateFault(FAULT_F6_1);
			mFaultText[FAULT_F6_1]+=getString(R.string.fault_f6_1);
		}
		if (response.isFlagCompFanErr())
		{
			activateFault(FAULT_F6_2);
			mFaultText[FAULT_F6_2]+=getString(R.string.fault_f6_2);
		}*/
        if(response.isFlagCabFanError())
        {
            activateFault(FAULT_F6_3);
            mFaultText[FAULT_F6_3]+=getString(R.string.fault_f6_3);
        }

		//F7
		if (response.isFlagCoolErr())
		{
			//activateFault(FAULT_F7);
            activateWarning(FAULT_F7);
			mFaultText[FAULT_F7]+=getString(R.string.fault_f7_2);
		}

		//F8
		if (response.isFlagOvertempErr())
		{
			activateFault(FAULT_F8);
			mFaultText[FAULT_F8]+=getString(R.string.fault_f8);
		}


		computeSlotStatus(0);
		computeSlotStatus(1);
		computeSlotStatus(2);
		computeSlotStatus(3);
		computeSlotStatus(4);

		checkFaultChange();

		generateFaultMessages();
		//Log.d(TAG, mFaultMessages);
	}

	/*Fault F1 per motor (Water/Dispense Solenoid Fault)
	  Fault F3 per motor
	  	Home position
	  	Overcurrent
	  	Undervoltage
	  	Thermal Warning
	  	Thermal Shutdown
	*/
	void handleMotor(int channel, EventStatusResponse.MotorFaults faults)
	{
		int naturalChannel=channel+1;
		if (faults.isF1())
		{
			int faultIndex=FAULT_F1_CH1+channel;
			activateFault(faultIndex);
			mFaultText[faultIndex]+=getSlotString("F1", naturalChannel)+getString(R.string.fault_f1);
		}

		if (faults.isF3())
		{
			int offset=5*channel;

			if (faults.homeSenseError)
			{
				int faultIndex=FAULT_F3_CH1_1+offset;
				activateFault(faultIndex);
				mFaultText[faultIndex]+=getSlotString("F3", naturalChannel)+getString(R.string.fault_f3_1);
			}

			if (faults.stepperOCD)
			{
				int faultIndex=FAULT_F3_CH1_2+offset;
				activateFault(faultIndex);
				mFaultText[faultIndex] += getSlotString("F3", naturalChannel) + getString(R.string.fault_f3_2);
			}
			if (faults.stepperUVLO)
			{
				int faultIndex=FAULT_F3_CH1_3+offset;
				activateFault(faultIndex);
				mFaultText[faultIndex] += getSlotString("F3", naturalChannel) + getString(R.string.fault_f3_3);
			}

			if (faults.stepperTHWRN)
			{
				int faultIndex=FAULT_F3_CH1_4+offset;
				//activateFault(faultIndex);
                activateWarning(faultIndex);
				mFaultText[faultIndex] += getSlotString("F3", naturalChannel) + getString(R.string.fault_f3_4);
			}
			if (faults.stepperTHSD)
			{
				int faultIndex=FAULT_F3_CH1_5+offset;
				activateFault(faultIndex);
				mFaultText[faultIndex] += getSlotString("F3", naturalChannel) + getString(R.string.fault_f3_5);
			}
		}
	}

	void activateFault(int number)
	{
		mActiveFaults[number]=true;
	}

	void activateWarning(int number)
	{
		mActiveWarning[number]=true;
	}

	public void clearActiveFaults()
	{
		for(int i=0; i<FAULT_MAX; i++)
		{
			mPreviousFaults[i]=mActiveFaults[i];
			mActiveFaults[i]=false;
			mFaultText[i]="";

            mPreviousWarning[i]=mActiveWarning[i];
            mActiveWarning[i]=false;
        }
	}

	void checkFaultChange()
	{
		boolean isChanged=false;
		boolean isNew=false;
		boolean isNewWarning=false;

		for(int i=0; i<FAULT_MAX; i++)
		{
			mIsNewFault[i]=false;
            mIsNewWarning[i]=false;

			if (mPreviousFaults[i]!=mActiveFaults[i])
				isChanged=true;

			if (mPreviousFaults[i]!=mActiveFaults[i] && mActiveFaults[i])
			{
				mIsNewFault[i]=true;
				isNew=true;
			}

            if (mPreviousWarning[i]!=mActiveWarning[i] && mActiveWarning[i])
            {
                mIsNewWarning[i]=true;
                isNewWarning=true;
            }
        }

		if (isChanged)
			callOnFaultCallback();
		if (isNew)
			callOnNewFaultCallback();
        if (isNewWarning)
            callOnNewWarningCallback();
	}

	void generateFaultMessages()
	{
		mFaultMessages="";

		for(String s:mFaultText)
		{
			if (s.length()>0)
				mFaultMessages+=s+"\n";
		}
	}

	String getString(@StringRes int id)
	{
		return mContext.getString(id);
	}

	static String getSlotString(Context context, int channel)
	{
		return context.getString(R.string.slot)+" "+channel+" ";
	}

	String getSlotString(String fCode, int channel)
	{
		return fCode+", "+getString(R.string.slot)+" "+channel+": ";
	}

	void computeSlotStatus(int channel)
	{
		mSlotFaultStatus[channel]=false;
        mSlotWarnStatus[channel]=false;
		//Fault F8 is no longer ignored

		if (isFault(FAULT_F1_CH1, channel))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F2))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F3_CH1_1, channel))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F3_CH1_2, channel))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F3_CH1_3, channel))
			mSlotFaultStatus[channel]=true;
		//if (isFault(FAULT_F3_CH1_4, channel))
            //mSlotFaultStatus[channel] = true;
        if (isWarning(FAULT_F3_CH1_4, channel))
            mSlotWarnStatus[channel]=true;
        if (isFault(FAULT_F3_CH1_5, channel))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F4_1))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F4_2))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F5_1))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F5_2))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F6_1))
			mSlotFaultStatus[channel]=true;
		if (isFault(FAULT_F6_2))
			mSlotFaultStatus[channel]=true;
        if (isFault(FAULT_F6_3))
            mSlotFaultStatus[channel]=true;
        //if (isFault(FAULT_F7))
        //    mSlotFaultStatus[channel] = true;
        if (isWarning(FAULT_F7))
            mSlotWarnStatus[channel]=true;
        if (isFault(FAULT_F8)) {
            mSlotFaultStatus[channel] = true;
        }
	}

	boolean isFault(int index)
	{
		return mActiveFaults[index];
	}

	boolean isFault(int index, int channel)
	{
		return mActiveFaults[index+(channel*5)] ;
	}

    boolean isWarning(int index)
    {
        return mActiveWarning[index];
    }

    boolean isWarning(int index, int channel)
    {
        return mActiveWarning[index+(channel*5)] ;
    }

    //--

	public boolean getSlotFaultStatus(ProductSlot slot)
	{
		return mSlotFaultStatus[slot.ordinal()];
	}

	public boolean getSlotWarnStatus(ProductSlot slot)
	{
		return mSlotWarnStatus[slot.ordinal()];
	}

	public void setOnFaultCallback(Runnable callback)
	{
		mOnFaultCallback=callback;

		if (mOnFaultNeedsCalled)
			callOnFaultCallback();
	}

	public void setOnNewFaultCallback(OnNewFaultCallback callback)
	{
		mOnNewFaultCallback=callback;
	}

    public void setOnNewWarningCallback(OnNewWarningCallback callback)
    {
        mOnNewWarningCallback=callback;
    }

    boolean mOnFaultNeedsCalled =false;
	public void callOnFaultCallback()
	{
		mOnFaultNeedsCalled =true;

		if (mOnFaultCallback!=null)
		{
			mOnFaultCallback.run();
			mOnFaultNeedsCalled=false;
		}
	}

	public void callOnNewFaultCallback()
	{
		if (mOnNewFaultCallback!=null)
			mOnNewFaultCallback.run(mIsNewFault);
	}

    public void callOnNewWarningCallback()
    {
        if (mOnNewWarningCallback!=null)
            mOnNewWarningCallback.run(mIsNewWarning);
    }

    public static String decode(Context context, int faultCode)
	{
		switch (faultCode)
		{
		case 0: return getSlotString(context, 1)+context.getString(R.string.fault_f1);
		case 1: return getSlotString(context, 2)+context.getString(R.string.fault_f1);
		case 2: return getSlotString(context, 3)+context.getString(R.string.fault_f1);
		case 3: return getSlotString(context, 4)+context.getString(R.string.fault_f1);
		case 4: return context.getString(R.string.fault_f1_5); 
		case 5: return context.getString(R.string.fault_f2); 
		case 6: return getSlotString(context, 1)+context.getString(R.string.fault_f3_1);
		case 7: return getSlotString(context, 1)+context.getString(R.string.fault_f3_2);
		case 8: return getSlotString(context, 1)+context.getString(R.string.fault_f3_3);
		case 9: return getSlotString(context, 1)+context.getString(R.string.fault_f3_4);
		case 10: return getSlotString(context, 1)+context.getString(R.string.fault_f3_5);
		case 11: return getSlotString(context, 2)+context.getString(R.string.fault_f3_1);
		case 12: return getSlotString(context, 2)+context.getString(R.string.fault_f3_2);
		case 13: return getSlotString(context, 2)+context.getString(R.string.fault_f3_3);
		case 14: return getSlotString(context, 2)+context.getString(R.string.fault_f3_4);
		case 15: return getSlotString(context, 2)+context.getString(R.string.fault_f3_5);
		case 16: return getSlotString(context, 3)+context.getString(R.string.fault_f3_1);
		case 17: return getSlotString(context, 3)+context.getString(R.string.fault_f3_2);
		case 18: return getSlotString(context, 3)+context.getString(R.string.fault_f3_3);
		case 19: return getSlotString(context, 3)+context.getString(R.string.fault_f3_4);
		case 20: return getSlotString(context, 3)+context.getString(R.string.fault_f3_5);
		case 21: return getSlotString(context, 4)+context.getString(R.string.fault_f3_1);
		case 22: return getSlotString(context, 4)+context.getString(R.string.fault_f3_2);
		case 23: return getSlotString(context, 4)+context.getString(R.string.fault_f3_3);
		case 24: return getSlotString(context, 4)+context.getString(R.string.fault_f3_4);
		case 25: return getSlotString(context, 4)+context.getString(R.string.fault_f3_5);
		case 26: return context.getString(R.string.fault_f4_1); 
		case 27: return context.getString(R.string.fault_f4_2); 
		case 28: return context.getString(R.string.fault_f5_1); 
		case 29: return context.getString(R.string.fault_f5_2); 
		case 30: return context.getString(R.string.fault_f6_1); 
		case 31: return context.getString(R.string.fault_f6_2);
        case 32: return context.getString(R.string.fault_f6_3);
		case 33: return context.getString(R.string.fault_f7_1);
        case 34: return context.getString(R.string.fault_f8);
        }
		return "No Fault";
	}

	public static String decodeFNumber(int id)
	{
		switch (id)
		{
			case 0: return "F1";
			case 1: return "F1";
			case 2: return "F1";
			case 3: return "F1";
			case 4: return "F1";
			case 5: return "F2";
			case 6: return "F3";
			case 7: return "F3";
			case 8: return "F3";
			case 9: return "F3";
			case 10: return "F3";
			case 11: return "F3";
			case 12: return "F3";
			case 13: return "F3";
			case 14: return "F3";
			case 15: return "F3";
			case 16: return "F3";
			case 17: return "F3";
			case 18: return "F3";
			case 19: return "F3";
			case 20: return "F3";
			case 21: return "F3";
			case 22: return "F3";
			case 23: return "F3";
			case 24: return "F3";
			case 25: return "F3";
			case 26: return "F4";
			case 27: return "F4";
			case 28: return "F5";
			case 29: return "F5";
			case 30: return "F6";
			case 31: return "F6";
			case 32: return "F6";
			case 33: return "F7";
            case 34: return "F8";
            case 35: return "F8";
        }
		return "";
	}

	public enum SLOT___
	{
		SLOT1,
		SLOT2,
		SLOT3,
		SLOT4,
		SLOT_WATER_NOZZLE
	}

	public interface OnNewFaultCallback
	{
		void run(boolean[] newFaults);
	}

    public interface OnNewWarningCallback{
        void run(boolean[] newWarnings);
    }

	public ArrayList<DatabaseFault> getActiveFaults()
	{
		ArrayList<DatabaseFault> faultList=new ArrayList<>();

		for(int i=0; i<FAULT_MAX; i++)
		{
			if (mActiveFaults[i]||mActiveWarning[i])
			{
				DatabaseFault activeFault=new DatabaseFault();
				activeFault.timestamp=System.currentTimeMillis();
				activeFault.fault_code=i;
				faultList.add(activeFault);
			}
		}

		return faultList;
	}

}
