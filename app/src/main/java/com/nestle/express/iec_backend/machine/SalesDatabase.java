package com.nestle.express.iec_backend.machine;

import android.os.Handler;
import android.util.Log;

import com.nestle.express.iec_backend.database.DatabaseDriverV1;
import com.nestle.express.iec_backend.machine.structure.RowSale;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.DatabaseSale;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

/**
 * Created by jason on 7/1/16.
 *
 * See documentation in databases.odt
 */
public class SalesDatabase
{
	DatabaseDriverV1 mDatabase;
	ProductManager mProductManager;
	int mToday;
	int mProductIds[]=new int[4];
	int mDispenseTimes[]=new int[4];

	public SalesDatabase(DatabaseDriverV1 db, ProductManager product)
	{
		mDatabase=db;
		mProductManager=product;

		updateToday();
		clearAndLoad();
	}

	void addSale(int channel)
	{
		DatabaseSale sale=new DatabaseSale();

		sale.unixDayNumber=mToday;
		sale.productId=mProductIds[channel];
		sale.dispenseTime=mDispenseTimes[channel];

		Log.d("SALESDB", "Save "+channel+", day: "+sale.unixDayNumber+" product: "+sale.productId+" time: "+sale.dispenseTime);

		mDatabase.addSale(sale);
	}

	public void save()
	{
		addSale(0);
		addSale(1);
		addSale(2);
		addSale(3);

		updateToday();
		clearAndLoad();
	}

	final long DAILY_MS=60*60*24*1000;
	void updateToday()
	{
		mToday=(int) (System.currentTimeMillis()/DAILY_MS);
	}

	public String getDate(long timestamp){
        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(timestamp*DAILY_MS);
        String date = mydate.get(Calendar.MONTH)+"/"+mydate.get(Calendar.DAY_OF_MONTH)+"/"+mydate.get(Calendar.YEAR);
        return date;
    }

	void loadProductId(ProductSlot channel)
	{
		mProductIds[channel.ordinal()]=mProductManager.getSelectedProduct(channel).getId();
	}

	void loadDispenseTime(int channel)
	{
		mDispenseTimes[channel]=mDatabase.getSaleDispenseTime(mProductIds[channel], mToday);
		Log.d("SALESDB", "Load "+channel+", product: "+mProductIds[channel]+" time: "+mDispenseTimes[channel]);
	}

	//Load today's sale volume (if any) for each slot;
	public void clearAndLoad()
	{
		mDatabase.truncateSales(mToday);

		loadProductId(ProductSlot.SLOT1);
		loadProductId(ProductSlot.SLOT2);
		loadProductId(ProductSlot.SLOT3);
		loadProductId(ProductSlot.SLOT4);

		loadDispenseTime(0);
		loadDispenseTime(1);
		loadDispenseTime(2);
		loadDispenseTime(3);

        repostSave();
	}

	void addSale(int slot, int timeMilliseconds)
	{
		mDispenseTimes[slot]+=timeMilliseconds;
	}

	public RowSale[] getRealSales()
	{
        //return mDatabase.getSales().toArray(new RowSale[0]);
        ArrayList<DatabaseSale> saleDbArray;
        saleDbArray = mDatabase.getSales();

		if(saleDbArray == null)
            return null;

        int arraySize = saleDbArray.size();

        RowSale[] sales = new RowSale[arraySize];
        int i=0;
        String ratio;
        for (DatabaseSale saleDb : saleDbArray)
        {
            if (saleDb!=null) {
                sales[i]=null;
                sales[i]=new RowSale();
                sales[i].unixDayNumber = saleDb.unixDayNumber;
                sales[i].productId = saleDb.productId;
                sales[i].volume = (double)saleDb.dispenseTime;

				ratio = mDatabase.getProduct(saleDb.productId).ratio;
				if(ratio.equals("4:1"))
					sales[i].ratio = 4.0;
				else if(ratio.equals("5:1"))
					sales[i].ratio = 5.0;
				else if(ratio.equals("11:1"))
					sales[i].ratio = 11.0;
                else if(ratio.equals("3:1"))
                    sales[i].ratio = 3.0;
                else
					sales[i].ratio = 4.5;

               i++;
            }
        }

        return sales;
    }

	public RowSale[] getSales()
	{
		long seed = System.nanoTime();

		//10000700, 10000701, 10000702, 10000703
		final int numberProducts=10;
		final int numberSaleDays=30*9;
		int arraySize=numberSaleDays*numberProducts;
		RowSale[] sales=new RowSale[arraySize];
		final long millisecondsPerDay=24*60*60*1000;

		int currentDay= (int) (System.currentTimeMillis() / millisecondsPerDay);
		int startDay=currentDay-numberSaleDays;

		Random rand=new Random(seed);
		for(int i=0; i<arraySize; i++)
		{
			sales[i]=null;
			if ((rand.nextInt()%13)==0)
				continue;

			sales[i]=new RowSale();
			sales[i].unixDayNumber=startDay+i%numberSaleDays;
			sales[i].productId=10000700+i/numberSaleDays;

			double a=2*3.14/14*i;
			double b=2*3.14/32*i;
			sales[i].volume=((Math.sin(a)+1)+(Math.cos(b)+1))*100;
		}

		ArrayList<RowSale> saleArray=new ArrayList<>();
		for (RowSale sale : sales)
		{
			if (sale!=null)
				saleArray.add(sale);
		}
		Collections.shuffle(saleArray);

		return saleArray.toArray(new RowSale[0]);
	}

	void repostSave()
	{
		mHandler.removeCallbacks(mUpdateRunnable);
		mHandler.postDelayed(mUpdateRunnable, UPDATE_TIME);
	}

	Handler mHandler = new Handler();
	final int UPDATE_TIME=1000*60*15;
	Runnable mUpdateRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			save();
			updateToday();
			clearAndLoad();
		}
	};
}
