package com.nestle.express.iec_backend.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;
import com.nestle.express.iec_backend.product.structure.DatabaseNote;
import com.nestle.express.iec_backend.product.structure.DatabaseProduct;
import com.nestle.express.iec_backend.product.structure.DatabaseProductId;
import com.nestle.express.iec_backend.product.structure.DatabaseProductSetting;
import com.nestle.express.iec_backend.product.structure.DatabaseSale;
import com.nestle.express.iec_backend.product.structure.NestleProduct;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 *
 * This class returns error strings ready for display.
 * Errors are not translated because the database is
 * where the translations are stored.
 *
 * DBv1 Error 1: Database file corrupt or does not exist
 * DBv1 Error 2: Database write error (SD Card Inserted?)
 *
 */
public class DatabaseDriverV1 implements DatabaseProvider
{
	final String TAG="DatabaseDriverV1";
	final String ERROR_OPEN="DBv1 Error 1: Database file corrupt or does not exist";
	final String ERROR_EXEC="DBv1 Error 2: Database operation failed (SD Card Inserted?)";

	String mError="DBv1: No Error Triggered";

	int mLanguage=1; //English is the default language
	SQLiteDatabase mDb=null;
	String mDatabasePath=null;

	//High level functions

	/* Expects path to be
	 * 1) /path/to/directory (isFilePath=False)
	 * 2) /path/to/database.db (isFilePath=True)
	 *
	 * No trailing slash
	 */
	public DatabaseDriverV1(boolean isFilePath, String path) throws FileNotFoundException
	{
		if (isFilePath)
			mDatabasePath=path;
		else
			mDatabasePath=path+"/database_v1.db";

		File f=new File(path);

		if (!f.exists())
			throw new FileNotFoundException("Path "+path+" does not exist.");
	}

	public void open() throws SQLiteException
	{
		try
		{
			mDb=SQLiteDatabase.openDatabase(mDatabasePath, null, SQLiteDatabase.OPEN_READWRITE);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_OPEN;
			throw e;
		}

		initializeDatabase();
	}

	void initializeDatabase() throws SQLiteException
	{
		//truncateSales();
		generateProductSettings();
	}

	public void close()
	{
		mDb.close();
	}

	//the rest

	public String getError()
	{
		return mError;
	}

	public void setLanguage(DatabaseLanguage language)
	{
		mLanguage=language.getId();
	}

	public String getString(int category, int id)
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select text from string " +
					"where ( language_id=1 AND category_id=? AND id=?);",
					new String[] {Integer.toString(category), Integer.toString(id)} );

			if (!cursor.moveToFirst())
			{
				cursor.close();
				return null;
			}

			String str=cursor.getString(cursor.getColumnIndex("text"));

			cursor.close();
			return str;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}


	public int[] getVersion()
	{
		try
		{
			Cursor cursor = mDb.rawQuery(
					"select version.majorversion, version.minorversion from version"
					, null );

			if (!cursor.moveToFirst())
			{
				cursor.close();
				return null;
			}

			int[] version=new int[2];

			while (!cursor.isAfterLast())
			{
				//assume last version entry in the database is the current version
				version[0] = (cursor.getInt(cursor.getColumnIndex("majorversion")));
				version[1] = (cursor.getInt(cursor.getColumnIndex("minorversion")));
				cursor.moveToNext();
			}

			cursor.close();

			return version;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public ArrayList<DatabaseLanguage> getLanguages()
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select * from language;", null);

			return DatabaseLanguage.createArrayFromCursor(cursor);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public DatabaseLanguage getLanguage(String isoCode)
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select * from language where iso_code=?;",
					new String[] {isoCode} );

			if (!cursor.moveToFirst())
			{
				cursor.close();
				throw new SQLiteException("Error, No Language found for iso_code="+isoCode);
			}

			DatabaseLanguage language=DatabaseLanguage.createFromCursor(cursor);
			cursor.close();

			return language;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;

			DatabaseLanguage english=new DatabaseLanguage();
			english.isoCode="en";
			english.name="Engilsh";
			english.id=1;

			return english;
		}
	}

	//Sales
	public boolean addSale(DatabaseSale sale)
	{
		try
		{
			mDb.execSQL("delete from sale where  unixDayNumber=? and product_id=?;",
					new Object [] {sale.unixDayNumber, sale.productId});
			mDb.execSQL("insert into sale (unixDayNumber, product_id, volume) " +
					"values (" +
					"    ?," +
					"    ?, ? " +
					");", new Object [] {sale.unixDayNumber, sale.productId, sale.dispenseTime}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public boolean clearSale()
	{
		Log.d("DB", "clearSale");

		try
		{
			mDb.execSQL("delete from sale;");
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public double getSaleVolume(int unixDayNumber, NestleProduct product)
	{
		int id=product.getId();
		try
		{
			Cursor cursor = mDb.rawQuery("select * from sale where product_id=? and unixDayNumber=?;"
					, new String[] {Integer.toString(id), Integer.toString(unixDayNumber)});

			DatabaseSale sale=DatabaseSale.createFromCursor(cursor);
			cursor.close();

			if (sale!=null)
				return sale.dispenseTime;

			return 0;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return 0;
		}
	}

	public int getSaleDispenseTime(int productId, int unixDayNumber)
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select * from sale"+
					" where unixDayNumber=? and product_id=?;",
					new String[] {Integer.toString(unixDayNumber),
							Integer.toString(productId)});

			DatabaseSale sale=null;
			if (cursor.moveToFirst())
			{
				sale=DatabaseSale.createFromCursor(cursor);
				cursor.close();
			}

			if (sale==null)
				return 0;

			return sale.dispenseTime;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return 0;
		}
	}

	public ArrayList<DatabaseSale> getSales()
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select * from sale;", null);

			return DatabaseSale.createArrayFromCursor(cursor);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public void truncateSales(int today) throws SQLiteException
	{
		int truncateDay=today-180;

		try
		{
			mDb.execSQL(
					//delete sales older than 180 days old
					"delete from sale " +
					"where unixDayNumber < ?; "
					, new Object[] {truncateDay}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			throw e;
		}
	}

	public void generateProductSettings()
	{
		try
		{
			mDb.execSQL(
					//create missing settings from defaults
					"insert into product_setting (product_id, speed_fill,\n" +
					"    speed_pump, steps_start, steps_pump, dispense_small,\n" +
					"    dispense_medium, dispense_large,\n" +
					"    dispense_concentrate, button_setup, mode, post_rinse_volume) \n" +
					"select product_id, speed_fill,\n" +
					"    speed_pump, steps_start, steps_pump, dispense_small,\n" +
					"    dispense_medium, dispense_large,\n" +
					"    dispense_concentrate, button_setup, mode, post_rinse_volume  from product \n" +
					"where product.product_id in \n" +
					"( \n" +
					"    select product_id from product \n" +
					"    where not product.product_id \n" +
					"    In \n" +
					"    ( \n" +
					"        select product_setting.product_id \n" +
					"        from product_setting \n" +
					"    ) \n" +
					");"
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			throw e;
		}
	}

	//notes

	public boolean addNote(DatabaseNote note)
	{
		try
		{
			mDb.execSQL("Insert into note (timestamp, note)\n" +
					"values ( strftime('%s','now'), ?)", new Object [] {note.note}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public boolean clearNote()
	{
		try
		{
			mDb.execSQL("delete from note;");
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}


	public ArrayList<DatabaseNote> getNotes()
	{
		try
		{
			Cursor cursor = mDb.rawQuery("select * from note;", null);

			return DatabaseNote.createArrayFromCursor(cursor);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	// faults

	public boolean addFault(DatabaseFault fault)
	{
		try
		{
			mDb.execSQL("insert into fault (timestamp, fault_code)\n" +
					"values ( ?, ?)", new Object [] {fault.timestamp, fault.fault_code}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public boolean clearFault()
	{
		try
		{
			mDb.execSQL("delete from fault;");
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public ArrayList<DatabaseFault> getFaults()
	{
		try
		{
			Cursor cursor = mDb.rawQuery(
					"select fault.timestamp, fault.fault_code from fault order by timestamp desc"
					, null );

			return DatabaseFault.createArrayFromCursor(cursor);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}


	public boolean truncateFaults()
	{
		//truncate after 30 days
		final long DAILY_MS=60*60*24*1000;
		long truncateTime=System.currentTimeMillis()-DAILY_MS*30;
		try
		{
			mDb.execSQL(
					" DELETE FROM `fault` \n" +
					" WHERE id NOT IN ( \n" +
					"   SELECT id \n" +
					"   FROM ( \n" +
					"     SELECT id \n" +
					"     FROM `fault` \n" +
					"     ORDER BY id DESC \n" +
					"     LIMIT 100 \n" +
					"   ) \n" +
					" );"
			);

			mDb.execSQL(
					//delete faults older than 30 days old
					"delete from fault " +
					"where timestamp < ?; "
					, new Object [] {truncateTime}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	// product
	public int[] getProductIds()
	{
		try
		{
			Cursor cursor = mDb.rawQuery(
					"select " +
					"    product.product_id " +
					"from product;"
					, null );

			ArrayList<DatabaseProductId> ids=DatabaseProductId.createArrayFromCursor(cursor);

			if (ids==null)
				return null;

			int size=ids.size();
			int[] array=new int[size];

			for(int i=0; i<size; i++)
				array[i]=ids.get(i).id;

			return array;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public ArrayList<DatabaseDisplayProduct> getDisplayProducts()
	{
		try
		{
			Cursor cursor = mDb.rawQuery(
					"select distinct " +
					"    product.product_id, " +
					"    sn.text as product_string, sc.text as category_string, sb.text as brand_string " +
					"from product " +
					"inner join category as cat " +
					"    on cat.category_id=product.category_id " +
					"inner join string as sn " +
					"    on sn.id=product.product_name " +
					"    and sn.category_id=5 " +
					"    and sn.language_id=? " +
					"inner join string as sc " +
					"    on sc.id=cat.category_name " +
					"    and sc.category_id=4 " +
					"    and sc.language_id=? " +
					"inner join string as sb " +
					"    on sb.id=product.brand " +
					"    and sb.category_id=6 " +
					"    and sb.language_id=? " +
					"order by sc.text, sb.text, product_string "
					, new String[] { Integer.toString(mLanguage), Integer.toString(mLanguage), Integer.toString(mLanguage)} );

			return DatabaseDisplayProduct.createArrayFromCursor(cursor);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public DatabaseDisplayProduct getDisplayProduct(int id)
	{
		try
		{
			Cursor cursor = mDb.rawQuery(
					"select distinct" +
					"    product.product_id, " +
					"    sn.text as product_string, sc.text as category_string, " +
					"    sb.text as brand_string " +
					"from product " +
					"inner join category as cat " +
					"    on cat.category_id=product.category_id " +
					"inner join string as sn " +
					"    on sn.id=product.product_name " +
					"    and sn.category_id=5 " +
					"    and sn.language_id=? " +
					"inner join string as sc " +
					"    on sc.id=cat.category_name " +
					"    and sc.category_id=4 " +
					"    and sc.language_id=? " +
					"inner join string as sb " +
					"    on sb.id=product.brand " +
					"    and sb.category_id=6 " +
					"    and sb.language_id=? " +
					"where product_id=? "
					, new String[] { Integer.toString(mLanguage), Integer.toString(mLanguage), Integer.toString(mLanguage),
					Integer.toString(id)});

			if (!cursor.moveToFirst())
			{
				cursor.close();
				return null;
			}

			DatabaseDisplayProduct product=DatabaseDisplayProduct.createFromCursor(cursor);
			cursor.close();
			return product;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public DatabaseProduct getProduct(int id)
	{
		try
		{
			Cursor product_cursor = mDb.rawQuery(
					"select " +
					"    product.*, " +
					"    sn.text as product_string, sc.text as category_string, " +
					"    sb.text as brand_string " +
					"from product " +
					"inner join category as cat " +
					"    on cat.category_id=product.category_id " +
					"inner join string as sn " +
					"    on sn.id=product.product_name " +
					"    and sn.category_id=5 " +
					"    and sn.language_id=? " +
					"inner join string as sc " +
					"    on sc.id=cat.category_name " +
					"    and sc.category_id=4 " +
					"    and sc.language_id=? "+
					"inner join string as sb " +
					"    on sb.id=product.brand " +
					"    and sb.category_id=6 " +
					"    and sb.language_id=? "+
					"where product.product_id=?;"
					, new String[] { Integer.toString(mLanguage), Integer.toString(mLanguage),
							Integer.toString(mLanguage),
							Integer.toString(id)} );

			Cursor setting_cursor = mDb.rawQuery(
					"select " +
							"   settings.* " +
							"from product " +
							"inner join product_setting as settings " +
							"    on product.product_id=settings.product_id " +
							"where product.product_id=?;"
					, new String[] { Integer.toString(id)} );

			if (!product_cursor.moveToFirst())
			{
				product_cursor.close();
				setting_cursor.close();
				return null;
			}

			if (!setting_cursor.moveToFirst())
			{
				product_cursor.close();
				setting_cursor.close();
				return null;
			}

			DatabaseProduct product=DatabaseProduct.createFromCursor(product_cursor, setting_cursor);

			return product;
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return null;
		}
	}

	public boolean updateSetting(NestleProduct product)
	{
		DatabaseProductSetting setting=product.getSetting().getDatabaseSetting();

		try
		{
			mDb.execSQL(
					"update product_setting " +
					"set speed_fill=?, " +
					"speed_pump=?, steps_start=?, steps_pump=?,\n" +
					"dispense_small=?, dispense_medium=?, dispense_large=?,\n" +
					"dispense_concentrate=?, button_setup=?, mode=?, post_rinse_volume=? " +
					"where product_id=?;",
					new Object [] {
							setting.speedFill,
							setting.speedPump,
							setting.stepsStart,
							setting.stepsPump,
							setting.timeDispenseSmall,
							setting.timeDispenseMedium,
							setting.timeDispenseLarge,
							setting.timeDispenseConcentrate,
							setting.buttonSetup,
							setting.mode,
							setting.postRinseVolume, product.getId()}
			);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			return false;
		}

		return true;
	}

	public void updateProducts(String path) throws Exception
	{
		String script="attach '"+path+"' as up; " +
				" " +
				"delete from string where category_id=4; " +
				"delete from string where category_id=5; " +
				"delete from string where category_id=6;  " +
				" " +
				"insert into string (id, category_id, language_id, text) " +
				"select id, category_id, language_id, text from up.string where " +
				"up.string.category_id=4 or up.string.category_id=5 or up.string.category_id=6; " +
				" " +
				"delete from category; " +
				"insert into category (category_id, category_name) " +
				"select category_id, category_name from up.category; " +
				" " +
				"delete from product; " +
				"insert into product (product_id, category_id, art_id, product_type, product_name, color, ratio, brix, speed_fill, speed_pump, steps_start, steps_pump, dispense_small, dispense_medium, dispense_large, dispense_concentrate, button_setup, mode, post_rinse_volume, brand) " +
				"select product_id, category_id, art_id, product_type, product_name, color, ratio, brix, speed_fill, speed_pump, steps_start, steps_pump, dispense_small, dispense_medium, dispense_large, dispense_concentrate, button_setup, mode, post_rinse_volume, brand from up.product;";
		try
		{
			String[] queries = script.split(";");
			for(String query : queries)
				mDb.execSQL(query);
		}
		catch (SQLiteException e)
		{
			e.printStackTrace();
			mError=ERROR_EXEC;
			throw new Exception(e.getMessage());
		}

	}
}
