package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 7/12/16.
 *
 */
public class DatabaseDisplayProduct extends DatabaseRowParent
{
	public int id;
	public String name;
	public String category;
	public String displayName;
	public String brand;


	public static DatabaseDisplayProduct createFromCursor(Cursor cursor)
	{
		DatabaseDisplayProduct display=new DatabaseDisplayProduct();

		display.id=getIntByName(cursor, "product_id");
		display.name=getStringByName(cursor, "product_string");
		display.category=getStringByName(cursor, "category_string");
		display.brand=getStringByName(cursor, "brand_string");
		display.displayName=display.category+" - "+display.name;

		return display;
	}

	public static ArrayList<DatabaseDisplayProduct> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
			return null;

		ArrayList<DatabaseDisplayProduct> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}

	public String toString()
	{
		if (id>0)
			return String.format("%s", category+" - "+brand+": "+name+" ("+id+")");
			//return String.format("%20s", category) + " - " + String.format("%20s", brand) + ": "+ name+" ("+id+")";
		return name;
	}

	public boolean equals(Object value)
	{
		if (value==this)	return true;
		if ( !(value instanceof DatabaseDisplayProduct) ) return false;
		DatabaseDisplayProduct product=(DatabaseDisplayProduct) value;

		if (product.id==id)
			return true;
		return false;
	}
}
