package com.nestle.express.iec_backend.database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Created by jason on 7/1/16.
 *
 * The purpose of this class is the read and write
 * settings of types: string, float, int, and int-array
 * from text files. These values are used for configuring the
 * application.
 *
 * This is the low level "driver" which is explicitly
 * decoupled from the rest of the design. It is to
 * contain only minimal logic in order to allow for
 * different configuration file formats to be used
 * without modification of the rest of the application.
 *
 */

public class ConfigDatabaseDriver
{
	final String TAG="ConfigDatabaseDriver";

	String mPath;
	String mComment;
	Properties mConfig;
	FileInputStream mInput;
	FileOutputStream mOutput;
	File mFile;

	void log(String message)
	{
		System.out.println(TAG+"/"+message);
	}

	public void initializeDatabase(String path, String comment)
	{
		mComment=comment;
		mPath=path;
		mConfig = new Properties();

		mFile=new File(path);

		try
		{
			if (!mFile.exists())
			{
				if (!mFile.createNewFile())
					log("Could not open or create machine.config file!");
			}
		}
		catch (IOException ex)
		{
			log("Could not open or create machine.config file!");
			ex.printStackTrace();
		}

		load();
	}

	public void load()
	{
		try
		{
			mInput= new FileInputStream(mFile);
			mConfig.load(mInput);

			try
			{
				mInput.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void save()
	{
		try
		{
			mOutput= new FileOutputStream(mFile);
			mConfig.store(mOutput, mComment);

			try
			{
				mOutput.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void clear()
	{
		mConfig.clear();
	}

	public void writeString(String key, String value)
	{
		mConfig.setProperty(key, value);
	}

	public String readString(String key, String defaultValue)
	{
		return mConfig.getProperty(key, defaultValue);
	}

	public void writeDouble(String key, double value)
	{
		String str=Double.toString(value);
		mConfig.setProperty(key, str);
	}

	public double readDouble(String key, double defaultValue)
	{
		String defaultString=Double.toString(defaultValue);

		String str=mConfig.getProperty(key, defaultString);

		double result=defaultValue;

		try
		{
			result=Double.valueOf(str);
		}
		catch (NumberFormatException e)
		{
			log("On key " + key + " got invaild data `" + str + "` returning default");
		}

		return result;
	}

	public void writeInt(String key, int value)
	{
		String str=Integer.toString(value);
		mConfig.setProperty(key, str);
	}

	public int readInt(String key, int defaultValue)
	{
		String defaultString=Integer.toString(defaultValue);
		String value=mConfig.getProperty(key, defaultString);

		int result=defaultValue;

		try
		{
			result=Integer.valueOf(value);
		}
		catch (NumberFormatException e)
		{
			log("On key " + key + " got invaild data `" + value + "` returning default");
		}

		return result;
	}

	public void writeIntArray(String key, int values[])
	{
		String str="";

		for(int i : values)
		{
			if (str.length()>0)
				str+=",";

			str+=Integer.toString(i);
		}

		mConfig.setProperty(key, str);
	}

	public int[] readIntArray(String key, int defaultValue, int size)
	{
		int[] defaultArray=new int[size];
		int[] array=new int[size];

		for(int i=0; i<size; i++)
		{
			array[i]=defaultValue;
			defaultArray[i]=defaultValue;
		}

		String valueString=mConfig.getProperty(key, "");

		if (valueString.equals(""))
			return defaultArray;

		List<String> items = Arrays.asList(valueString.split("\\s*,\\s*"));

		int i=0;
		for(String s : items)
		{
			try
			{
				array[i]=Integer.valueOf(s);
			}
			catch (NumberFormatException e)
			{
				log("On key " + key + " got invaild data `" + s + "` returning default");
				return defaultArray;
			}

			if (i<size)
				i++;
			else
			{
				log("Read: Key " + key + " in file " + mPath + " has more than " + Integer.toString(size)+" items.");
				break;
			}
		}

		return array;
	}
}
