package com.nestle.express.iec_backend.machine;

import com.nestle.express.iec_backend.database.ConfigDatabaseParent;

/**
 * Created by jason on 7/1/16.
 *
 * See documentation in configuration_files.odt
 *
 * Provides a setter/getter interface to write to
 * the machine configuration database
 *
 * Settings			Type	Range
 * 	water1			double	25 to 50 F
 * 	water2			double	25 to 50 F
 * 	concentrate		double	35 to 50 F
 */
public class MachineConfigDatabase extends ConfigDatabaseParent
{
	final String COMMENT="Machine Configuration - Driver Board Settings";
	final String FILENAME="/machine.config";

	final String KEY_WATER1="water1";
	final String KEY_WATER2="water2";
	final String KEY_CONCENTRATE="concentrate";
	final String KEY_LOCKOUT="lockout";

	final double DEFAULT_WATER1=42;
	final double DEFAULT_WATER2=40;
	final double DEFAULT_CONCENTRATE=42;
	final int DEFAULT_LOCKOUT=0;

	final double UPPER_WATER1=50;
	final double UPPER_WATER2=50;
	final double UPPER_CONCENTRATE=50;

	final double LOWER_WATER1=25;
	final double LOWER_WATER2=25;
	final double LOWER_CONCENTRATE=35;

	public MachineConfigDatabase(String directory)
	{
		initialize(directory, FILENAME, COMMENT);
	}

	public MachineConfigDatabase(String directory, String filename)
	{
		initialize(directory, filename, COMMENT);
	}

	public double getWater1()
	{
		return getSetting(KEY_WATER1, DEFAULT_WATER1, LOWER_WATER1, UPPER_WATER1);
	}

	public double getWater2()
	{
		return getSetting(KEY_WATER2, DEFAULT_WATER2, LOWER_WATER2, UPPER_WATER2);
	}

	public double getConcentrate()
	{
		return getSetting(KEY_CONCENTRATE, DEFAULT_CONCENTRATE, LOWER_CONCENTRATE, UPPER_CONCENTRATE);
	}

	public boolean getLockout()
	{
		int lockout=getSetting(KEY_LOCKOUT, DEFAULT_LOCKOUT);
		if(lockout==0)
			return false;
		return true;
	}

	public void setWater1(double value)
	{
		setSetting(KEY_WATER1, value, LOWER_WATER1, UPPER_WATER1);
	}

	public void setWater2(double value)
	{
		setSetting(KEY_WATER2, value, LOWER_WATER2, UPPER_WATER2);
	}

	public void setConcentrate(double value)
	{
		setSetting(KEY_CONCENTRATE, value, LOWER_CONCENTRATE, UPPER_CONCENTRATE);
	}

	public void setLockout(boolean value)
	{
		if (value)
			setSetting(KEY_LOCKOUT, 1);
		else
			setSetting(KEY_LOCKOUT, 0);
	}
}
