package com.nestle.express.iec_backend.product;

import android.util.Log;

import com.nestle.express.iec_backend.database.DatabaseDriverV1;
import com.nestle.express.iec_backend.machine.FaultMatrix;
import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductArt;
import com.nestle.express.iec_backend.product.structure.ProductRow;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.iec_backend.product.structure.SelectionIndexes;
import com.nestle.express.iec_backend.product.structure.SelectionMatrix;

import java.util.ArrayList;

/**
 * Created by jason on 7/12/16.
 * <p>
 * Base directory
 * Resources
 * ValidProductIds
 */
public class ProductManager {
    final String TAG = "ProductManager";
    final String mProductsDirectory = "/art/products";
    private String mDirectory;
    DatabaseLanguage mDefaultLanguage;

    ProductConfigDatabase mConfig;
    DatabaseDriverV1 mDatabase;
    NestleProduct[] mProducts;
    NestleProduct mProduct1, mProduct2, mProduct3, mProduct4;
    NestleProduct.ProductStatus mStatus1, mStatus2, mStatus3, mStatus4;
    NestleProduct.ProductStatus[] mProductsStatus;

    public ProductManager(DatabaseLanguage language, String directory, DatabaseDriverV1 database) {
        mDirectory = directory;
        mDefaultLanguage = language;
        mDatabase = database;

        int[] validIds = mDatabase.getProductIds();

        mConfig = new ProductConfigDatabase(directory, validIds);

        mProduct1=initializeProduct(ProductSlot.SLOT1, mDefaultLanguage.getIsoCode());
        mProduct2=initializeProduct(ProductSlot.SLOT2, mDefaultLanguage.getIsoCode());
        mProduct3=initializeProduct(ProductSlot.SLOT3, mDefaultLanguage.getIsoCode());
        mProduct4=initializeProduct(ProductSlot.SLOT4, mDefaultLanguage.getIsoCode());

        mProducts = new NestleProduct[]{mProduct1, mProduct2, mProduct3, mProduct4};

        mStatus1 = mProduct1.getStatus();
        mStatus2 = mProduct2.getStatus();
        mStatus3 = mProduct3.getStatus();
        mStatus4 = mProduct4.getStatus();
        mProductsStatus = new NestleProduct.ProductStatus[]{mStatus1, mStatus2, mStatus3, mStatus4};
    }

	public void updateValidIDs()
	{
		mConfig.setValidProductIDs(mDatabase.getProductIds());
	}

    public void setLanguage(DatabaseLanguage language) {
        mDefaultLanguage = language;
    }

    public DatabaseLanguage getLanguage() {return mDefaultLanguage;}

    public void open() {
        mDatabase.open();
        mConfig.load();
    }

    public void save() {
        mConfig.save();
    }

    public void close() {
        mConfig.save();
    }

    public NestleProduct getProductById(int id)
	{
		NestleProduct product=new NestleProduct(mDatabase.getProduct(id));
		return product;
	}

    public NestleProduct getSelectedProduct(ProductSlot slot) {
        return getSelectedProduct(slot, mDefaultLanguage);
    }

    public NestleProduct getSelectedProduct(ProductSlot slot, DatabaseLanguage language) {
        return getSelectedProduct(slot, language.getIsoCode());
    }

    public NestleProduct initializeProduct(ProductSlot slot, String language) {
        int id = mConfig.getSelectedId(slot);

        if (id < 0)
            return null;

        NestleProduct product = new NestleProduct(mDatabase.getProduct(id));
        product.setSlot(slot);

        if (product == null) {
            return null;
        }

        //TEST: Comment out the line below
        product.setProductArt(new ProductArt(product, mDirectory + mProductsDirectory, mDefaultLanguage.isoCode, language));

		if (mFaultMatrix!=null)
		{
			if (mFaultMatrix.getSlotFaultStatus(slot))
				product.setProductStatus(NestleProduct.ProductStatus.FAULT);
		}

        return product;
    }

    public NestleProduct getSelectedProduct(ProductSlot slot, String language) {
        /*int id = mConfig.getSelectedId(slot);

        if (id < 0)
            return null;

        NestleProduct product = new NestleProduct(mDatabase.getProduct(id));
        product.setSlot(slot);

        if (product == null) {
            return null;
        }

        //TEST: Comment out the line below
        product.setProductArt(new ProductArt(product, mDirectory + mProductsDirectory, mDefaultLanguage.isoCode, language));

        if (mFaultMatrix!=null)
        {
            if (mFaultMatrix.getSlotFaultStatus(slot))
                product.setProductStatus(NestleProduct.ProductStatus.FAULT);
        }

        return product;*/

        if (mProducts[slot.ordinal()] == null) {
            return null;
        }

        mProducts[slot.ordinal()].setProductArt(new ProductArt(mProducts[slot.ordinal()], mDirectory + mProductsDirectory, mDefaultLanguage.isoCode, language));

        if (mFaultMatrix!=null)
        {
            if (mFaultMatrix.getSlotFaultStatus(slot))
                mProducts[slot.ordinal()].setProductStatus(NestleProduct.ProductStatus.FAULT);
            else if(mProducts[slot.ordinal()].getStatus() == NestleProduct.ProductStatus.FAULT) {
                mProducts[slot.ordinal()].setProductStatus(mProductsStatus[slot.ordinal()]);        // return to state before fault
            }
        }
        else if(mProducts[slot.ordinal()].getStatus() == NestleProduct.ProductStatus.FAULT)
            mProducts[slot.ordinal()].setProductStatus(mProductsStatus[slot.ordinal()]);            // return to state before fault

        return mProducts[slot.ordinal()];
    }

    public void setProductSlotStatus(ProductSlot slot, NestleProduct.ProductStatus status){
        if(status == NestleProduct.ProductStatus.FAULT)                                 // if setting fault
            mProductsStatus[slot.ordinal()] = mProducts[slot.ordinal()].getStatus();    // save previous state

        mProducts[slot.ordinal()].setProductStatus(status);
    }

    //Update the setting associated with the product.
    public void updateSelectedProductSetting(NestleProduct product) {
		Log.e(TAG, "update "+product.getSlot()+" "+product.getId());
        int id = mConfig.getSelectedId(product.getSlot());

        if (id < 0)
            Log.e(TAG, "Cannot update product that does not exist");

        mDatabase.updateSetting(product);
    }

    public ArrayList<DatabaseDisplayProduct> getDisplayProducts() {
		ArrayList<DatabaseDisplayProduct> allProducts=new ArrayList<>();
        ArrayList<DatabaseDisplayProduct> databaseProducts=mDatabase.getDisplayProducts();
        DatabaseDisplayProduct noProduct=new DatabaseDisplayProduct();
		noProduct.id=-1;
		noProduct.category="";
		noProduct.name="No Product";
		noProduct.displayName="No Product";

		allProducts.add(noProduct);
		allProducts.addAll(databaseProducts);

		return allProducts;
    }

    public ArrayList<DatabaseDisplayProduct> getOnlyDisplayProducts() {
        ArrayList<DatabaseDisplayProduct> allProducts=new ArrayList<>();
        ArrayList<DatabaseDisplayProduct> databaseProducts=mDatabase.getDisplayProducts();

        allProducts.addAll(databaseProducts);

        return allProducts;
    }

    public SelectionIndexes getSelectedIndex() {
        return new SelectionIndexes(mConfig.getSelectedIndexes());
    }

    public void setSelectionIndex(SelectionIndexes selectionIndex) {
        mConfig.setSelectedIndexes(selectionIndex.getElements());
		mConfig.save();

        mProducts[0]=initializeProduct(ProductSlot.SLOT1, mDefaultLanguage.getIsoCode());
        mProducts[1]=initializeProduct(ProductSlot.SLOT2, mDefaultLanguage.getIsoCode());
        mProducts[2]=initializeProduct(ProductSlot.SLOT3, mDefaultLanguage.getIsoCode());
        mProducts[3]=initializeProduct(ProductSlot.SLOT4, mDefaultLanguage.getIsoCode());
    }

    public SelectionMatrix getSelectionMatrix() {
        int[] ids = mConfig.getSelectionMatrix();


        SelectionMatrix matrix = new SelectionMatrix();
        for (int i = 0; i < ids.length; i++) {
            DatabaseDisplayProduct product = mDatabase.getDisplayProduct(ids[i]);

            if (product == null) {
                product = new DatabaseDisplayProduct();
                product.id = -1;
                product.name = "No Product";
                product.category = "No Category";
                product.displayName = "No Category - No Product";
            }

            matrix.setElement(i, product);
        }

        return matrix;
    }

    public void setSelectionMatrix(SelectionMatrix selectionMatrix) {
        mConfig.setSelectionMatrix(selectionMatrix.getElementIds());
    }

	public void updateSelectionMatrix(ProductSlot slot, ProductRow row, DatabaseDisplayProduct product) {

		SelectionMatrix matrix=getSelectionMatrix();
		matrix.setElement(slot, row, product);

		mConfig.setSelectionMatrix(matrix.getElementIds());
        mConfig.save();
	}

	FaultMatrix mFaultMatrix;
	public void setFaultMatrix(FaultMatrix faults)
	{
		mFaultMatrix=faults;
	}
}
