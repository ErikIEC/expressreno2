package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseSale extends DatabaseRowParent
{
	public int unixDayNumber;
	public int productId;
	public int dispenseTime;

	public static DatabaseSale createFromCursor(Cursor cursor)
	{
		DatabaseSale sale=new DatabaseSale();

		sale.productId=getIntByName(cursor, "product_id");
		sale.unixDayNumber =getIntByName(cursor, "unixDayNumber");
		sale.dispenseTime =getIntByName(cursor, "volume");

		return sale;
	}

	public static ArrayList<DatabaseSale> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
		{
			cursor.close();
			return null;
		}

		ArrayList<DatabaseSale> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}
}
