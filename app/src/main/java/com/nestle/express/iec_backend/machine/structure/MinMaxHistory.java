/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.iec_backend.machine.structure;

/**
 *
 * @author jason
 */
public class MinMaxHistory
{
	int index;
	MinMaxRow history[];
	boolean populated[];
	public MinMaxHistory(int length)
	{
		history=new MinMaxRow[length];
		populated=new boolean[length];
		
		history[0]=new MinMaxRow(0);
	}
	
	public void add(double d)
	{
		populated[index]=true;
		history[index]=new MinMaxRow(d);
		
		index++;
		if(index>=history.length)
			index=0;
	}
	
	public void add(MinMaxRow row)
	{
		populated[index]=true;
		history[index]=row;
		
		index++;
		if(index>=history.length)
			index=0;
	}
	
	public double getMax()
	{
		double max=history[0].max;
		for (int i=0; i<history.length; i++)
		{
			if (populated[i] && history[i].max>max)
				max=history[i].max;
		}
		return max;
	}
	
	public double getMin()
	{
		double min=history[0].min;
		for (int i=0; i<history.length; i++)
		{
			if (populated[i] && history[i].min<min)
				min=history[i].min;
		}
		return min;
	}
	
	public MinMaxRow getMinAndMax()
	{
		double min=getMin();
		double max=getMax();
		
		return new MinMaxRow(min, max);
	}
}
