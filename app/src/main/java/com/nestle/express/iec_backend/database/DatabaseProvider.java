package com.nestle.express.iec_backend.database;

/**
 * Created by jason on 8/8/16.
 */
public interface DatabaseProvider
{
	String getString(int category, int id);
}
