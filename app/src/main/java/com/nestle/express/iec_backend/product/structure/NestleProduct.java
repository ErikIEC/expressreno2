package com.nestle.express.iec_backend.product.structure;

/**
 * Created by jason on 7/7/16.
 *
 *  Type	Name			Access
 *  int		id				r
 *	String	name			r
 *	int		category id		r
 *	String	category name	r
 *	String	ratio			r
 *	String	brix			r
 *	double	speedFill		rw
 *	double	speedPump		rw
 *	int		stepsStart		rw
 *	int		stepsPump		rw
 *	String	filenameBanner	r
 *	--
 *	Drawable	imageBanner	rw
 */
public class NestleProduct
{
	ProductSlot mSlot=null;
	DatabaseProduct mProduct;
	NestleProductSetting mSetting;
	NestleProductSetting mDefaultSetting;
	ProductArt mProductArt =null;
	ProductStatus mStatus=ProductStatus.DISPENSE;


	public NestleProduct()
	{
		mProduct=new DatabaseProduct();
		mSetting=new NestleProductSetting(new DatabaseProductSetting());
		mDefaultSetting=new NestleProductSetting(new DatabaseProductSetting());
	}

	public NestleProduct(DatabaseProduct product)
	{
		mProduct=product;
		mSetting=new NestleProductSetting(mProduct.setting);
		mDefaultSetting=new NestleProductSetting(mProduct.defaultSetting);
	}

	public void resetSetting()
	{
		mSetting=mDefaultSetting;
	}

	public void setSlot(ProductSlot slot)
	{
		mSlot=slot;
	}

	public void setProduct(DatabaseProduct product)
	{
		mProduct=product;
	}

	public void setSetting(NestleProductSetting setting)
	{
		mSetting=setting;
	}

	public void setSpeedFill(double value)
	{
		mSetting.setSpeedFill(value);
	}

	public void setSpeedPump(double value)
	{
		mSetting.setSpeedPump(value);
	}

	public void setStepsStart(int value)
	{
		mSetting.setStepsStart(value);
	}

	public void setStepsPump(int value)
	{
		mSetting.setStepsPump(value);
	}

	public void setProductArt(ProductArt art)
	{
		mProductArt =art;
	}

	public void setProductStatus(ProductStatus status)
	{
		mStatus=status;
	}


	public ProductSlot getSlot()
	{
		return mSlot;
	}

	public DatabaseProduct getProduct()
	{
		return mProduct;
	}

	public NestleProductSetting getSetting()
	{
		return mSetting;
	}

	public NestleProductSetting getDefaultSetting()
	{
		return mDefaultSetting;
	}

	public int getId()
	{
		return mProduct.product_id;
	}

	public String getName()
	{
		return mProduct.name;
	}

	public String getFullName()
	{
		//mProduct.category+" - "+
		return mProduct.name+" ("+mProduct.product_id+")";
	}

	public String getCategoryName()
	{
		return mProduct.category;
	}

	public String getRatioText()
	{
		return mProduct.ratio;
	}

	public String getBrixText()
	{
		return mProduct.brix;
	}

	public double getSpeedFill()
	{
		return mSetting.getSpeedFill();
	}

	public double getSpeedPump()
	{
		return mSetting.getSpeedPump();
	}

	public double getDefaultSpeedPump(){
		return mDefaultSetting.getSpeedPump();
	}

	public int getStepsStart()
	{
		return mSetting.getStepsStart();
	}

	public int getStepsPump()
	{
		return mSetting.getStepsPump();
	}

	public NestleProductButtonSetup getButtonSetup()
	{
		return mSetting.getEnabledButtons();
	}

	public ProductArt getProductArt()
	{
		return mProductArt;
	}

	public ProductStatus getStatus()
	{
		return mStatus;
	}

	public int getTimeSmall()
	{
		return mSetting.getTimeDispenseSmall();
	}

	public int getTimeMedium()
	{
		return mSetting.getTimeDispenseMedium();
	}

	public int getTimeLarge()
	{
		return mSetting.getTimeDispenseLarge();
	}

	public int getTimeConcentrate()
	{
		return mSetting.getTimeDispenseConcentrate();
	}

	public void setTimeSmall(int time)
	{
		mSetting.setTimeDispenseSmall(time);
	}

	public void setTimeMedium(int time)
	{
		mSetting.setTimeDispenseMedium(time);
	}
	public void setTimeLarge(int time)
	{
		mSetting.setTimeDispenseLarge(time);
	}
	public void setTimeConcentrate(int time)
	{
		mSetting.setTimeDispenseConcentrate(time);
	}
	public void setPostRinseVolume(double volume)
	{
		mSetting.setPostRinseVolume(volume);
	}

	public double getPostRinseVolume()
	{
		return mSetting.getPostRinseVolume();
	}

	public void setButtonSetup(NestleProductButtonSetup setup)
	{
		mSetting.setEnabledButtons(setup);
	}

	public enum ProductStatus
	{
		DISPENSE,
		RINSE,
		FAULT,
		SOLD_OUT
	}

}