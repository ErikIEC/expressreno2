package com.nestle.express.iec_backend.machine.structure;

/**
 * Created by jason on 7/8/16.
 *
	 Non-Fatal Faults
	 F1	Dispense Solenoid Short or Open
	 F2	Concentrate Thermistor Out of Range
	 F3	Low Voltage to Machine (removed from spec)

	 Fatal Faults

	 F4	Refrigeration Valve Relay Short or Open
	 F5	Water 1 or Water 2 Thermistor Out of Range
	 F6	Refrigeration Switching Fault
	 F7	Slow Cooling
	 F8	Over Temperature
 */
public class FaultStruct
{
	//non-fatal
	boolean[] f1_solenoid = new boolean[4]; //blocks dispense slot
	boolean f2_concentrateTemperature;

	//fatal, error screen
	boolean f4_refrigerationRelay;
	boolean f5_waterThermistor;
	boolean f6_refrigerationSwitching;
	boolean f7_slowCooling;
	boolean f8_overTemperature;

	public boolean equals(FaultStruct fault)
	{
		for(int i=0; i<4; i++)
		{
			if (f1_solenoid[i]!=fault.f1_solenoid[i])
				return false;
		}

		return	(f2_concentrateTemperature == fault.f2_concentrateTemperature) &&
				(f4_refrigerationRelay == fault.f4_refrigerationRelay) &&
				(f5_waterThermistor == fault.f5_waterThermistor) &&
				(f6_refrigerationSwitching == fault.f6_refrigerationSwitching) &&
				(f7_slowCooling == fault.f7_slowCooling) &&
				(f8_overTemperature == fault.f8_overTemperature);
	}
}
