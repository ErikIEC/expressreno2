package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseProductId extends DatabaseRowParent
{
	public int id;


	public static DatabaseProductId createFromCursor(Cursor cursor)
	{
		DatabaseProductId dbId=new DatabaseProductId();

		dbId.id=getIntByName(cursor, "product_id");

		return dbId;
	}

	public static ArrayList<DatabaseProductId> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
		{
			cursor.close();
			return null;
		}

		ArrayList<DatabaseProductId> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}
}
