package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseFault extends DatabaseRowParent
{
	public long timestamp;
	public int fault_code;

	public static DatabaseFault createFromCursor(Cursor cursor)
	{
		DatabaseFault fault=new DatabaseFault();

		fault.timestamp=getLongByName(cursor, "timestamp");
		fault.fault_code=getIntByName(cursor, "fault_code");

		return fault;
	}

	public static ArrayList<DatabaseFault> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
		{
			cursor.close();
			return null;
		}

		ArrayList<DatabaseFault> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}
}
