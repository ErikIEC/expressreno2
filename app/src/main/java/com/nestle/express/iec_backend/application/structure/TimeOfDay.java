package com.nestle.express.iec_backend.application.structure;

/**
 * Created by jason on 7/8/16.
 */
public class TimeOfDay
{
	public TimeOfDay()
	{

	}

	public TimeOfDay(int minutes)
	{
		hour=minutes/60;
		minute=minutes%60;
	}

	public int getMinutes()
	{
		int minutes=hour*60+minute;
		return minutes;
	}

	//24 unixDayNumber format
	public int hour;
	public int minute;

	public boolean equals(Object object)
	{
		if (object==this)
			return true;

		if (object==null)
			return false;

		if (object instanceof TimeOfDay)
		{
			TimeOfDay time=(TimeOfDay) object;

			return (hour==time.hour) && (minute==time.minute);
		}

		return false;
	}
}
