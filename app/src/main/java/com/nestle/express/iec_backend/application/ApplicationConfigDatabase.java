package com.nestle.express.iec_backend.application;

import com.nestle.express.iec_backend.database.ConfigDatabaseParent;
import com.nestle.express.iec_backend.application.structure.TimeOfDay;

/**
 * Created by jason on 7/1/16.
 *
 * See documentation in configuration_files.odt
 *
 * Provides a setter/getter interface to write to
 * the application configuration database
 */
public class ApplicationConfigDatabase extends ConfigDatabaseParent
{
	final String COMMENT="Application Configuration - User Settings";
	final String FILENAME="/application.config";

	final String KEY_LANGUAGE="language";
	final String KEY_USE_METRIC_DISPLAY_UNITS="use_metric_units";
	final String KEY_TECHNICIAN_PIN="technician_pin";
	final String KEY_OPERATOR_PIN="operator_pin";
	final String KEY_TIMEOUT_IDLE="timeout_idle";
	final String KEY_TIMEOUT_MANUAL_RINSE="timeout_manual_rinse";
	final String KEY_TIMEOUT_PORTION_SET="timeout_portion_set";
	final String KEY_TIMEOUT_SETTINGS_MENU="timeout_settings_menu";
	final String KEY_RINSE_MODE="rinse_mode";
	final String KEY_RINSE_TIMES="rinse_times";
	final String KEY_RINSE_TIME1="rinse_time1";
	final String KEY_RINSE_TIME2="rinse_time2";
	final String SUFFIX_HOURS="_hours";
	final String SUFFIX_MINUTES="_minutes";
	final String KEY_POST_RINSE_VOLUME="post_rinse_volume";

	final String DEFAULT_LANGUAGE="en";
	final int DEFAULT_USE_METRIC_DISPLAY_UNITS=0;
	final String DEFAULT_TECHNICIAN_PIN="1234";
	final String DEFAULT_OPERATOR_PIN="7777";
	final int DEFAULT_TIMEOUT_IDLE=300;
	final int DEFAULT_TIMEOUT_MANUAL_RINSE=300;
	final int DEFAULT_TIMEOUT_PORTION_SET=300;
	final int DEFAULT_TIMEOUT_SETTINGS_MENU=300;
	final int DEFAULT_RINSE_MODE=0;
	final int DEFAULT_RINSE_TIMES=0;
	final int DEFAULT_RINSE_TIME1_HOURS=8;
	final int DEFAULT_RINSE_TIME1_MINUTES=0;
	final int DEFAULT_RINSE_TIME2_HOURS=0;
	final int DEFAULT_RINSE_TIME2_MINUTES=0;
	final double DEFAULT_POST_RINSE_VOLUME=0;

	final int LOWER_USE_METRIC_DISPLAY_UNITS=0;
	final int LOWER_TIMEOUT_IDLE=30;
	final int LOWER_TIMEOUT_MANUAL_RINSE=30;
	final int LOWER_TIMEOUT_PORTION_SET=2;
	final int LOWER_TIMEOUT_SETTINGS_MENU=30;
	final int LOWER_RINSE_MODE=0;
	final int LOWER_RINSE_TIMES=0;
	final int LOWER_HOURS=0;
	final int LOWER_MINUTES=0;
	final double LOWER_POST_RINSE_VOLUME=0;

	final int UPPER_USE_METRIC_DISPLAY_UNITS=1;
	final int UPPER_TIMEOUT_IDLE=300;
	final int UPPER_TIMEOUT_MANUAL_RINSE=300;
	final int UPPER_TIMEOUT_PORTION_SET=300;
	final int UPPER_TIMEOUT_SETTINGS_MENU=600;
	final int UPPER_RINSE_MODE=1;
	final int UPPER_RINSE_TIMES=2;
	final int UPPER_HOURS=23;
	final int UPPER_MINUTES=59;
	final double UPPER_POST_RINSE_VOLUME=5;

	public ApplicationConfigDatabase(String directory)
	{
		initialize(directory, FILENAME, COMMENT);
	}

	public ApplicationConfigDatabase(String directory, String filename)
	{
		initialize(directory, filename, COMMENT);
	}

	public String getLanguage()
	{
		String language=getSetting(KEY_LANGUAGE, "en");

		return language;
	}

	public int getMetricDisplayUnits()
	{
		return getSetting(KEY_USE_METRIC_DISPLAY_UNITS, DEFAULT_USE_METRIC_DISPLAY_UNITS,
				LOWER_USE_METRIC_DISPLAY_UNITS, UPPER_USE_METRIC_DISPLAY_UNITS);
	}

	//needs validation logic
	public String getTechnicianPin()
	{
		return getSetting(KEY_TECHNICIAN_PIN, DEFAULT_TECHNICIAN_PIN);
	}

	//needs validation logic
	public String getOperatorPin()
	{
		return getSetting(KEY_OPERATOR_PIN, DEFAULT_OPERATOR_PIN);
	}

	public int getTimeoutIdle()
	{
		return getSetting(KEY_TIMEOUT_IDLE, DEFAULT_TIMEOUT_IDLE,
				LOWER_TIMEOUT_IDLE, UPPER_TIMEOUT_IDLE);
	}

	public int getTimeoutManualRinse()
	{
		return getSetting(KEY_TIMEOUT_MANUAL_RINSE, DEFAULT_TIMEOUT_MANUAL_RINSE,
				LOWER_TIMEOUT_MANUAL_RINSE, UPPER_TIMEOUT_MANUAL_RINSE);
	}

	public int getTimeoutPortionSet()
	{
		return getSetting(KEY_TIMEOUT_PORTION_SET, DEFAULT_TIMEOUT_PORTION_SET,
				LOWER_TIMEOUT_PORTION_SET, UPPER_TIMEOUT_PORTION_SET);
	}

	public int getTimeoutSettingsMenu()
	{
		return getSetting(KEY_TIMEOUT_SETTINGS_MENU, DEFAULT_TIMEOUT_SETTINGS_MENU,
				LOWER_TIMEOUT_SETTINGS_MENU, UPPER_TIMEOUT_SETTINGS_MENU);
	}

	public RINSE_MODE getRinseMode()
	{
		RINSE_MODE mode;
		int setting= getSetting(KEY_RINSE_MODE, DEFAULT_RINSE_MODE,
				LOWER_RINSE_MODE, UPPER_RINSE_MODE);

		if (setting==1)
			return RINSE_MODE.MANUAL;
		else
			return RINSE_MODE.AUTOMATIC;
	}

	public RINSE_TIMES getRinseTimes()
	{
		int value= getSetting(KEY_RINSE_TIMES, DEFAULT_RINSE_TIMES,
				LOWER_RINSE_TIMES, UPPER_RINSE_TIMES);

		if (value==2)
			return RINSE_TIMES.TWICE_DAILY;
		else if (value==1)
			return RINSE_TIMES.ONCE_DAILY;
		else
			return RINSE_TIMES.OFF;
	}

	TimeOfDay getTimeSetting(String prefix, int defaultHour, int defaultMinute)
	{
		TimeOfDay time=new TimeOfDay();

		time.hour=getSetting(prefix+SUFFIX_HOURS, defaultHour,
				LOWER_HOURS, UPPER_HOURS);
		time.minute=getSetting(prefix+SUFFIX_MINUTES, defaultMinute,
				LOWER_MINUTES, UPPER_MINUTES);

		return time;
	}

	void setTimeSetting(String prefix, TimeOfDay time)
	{
		setSetting(prefix+SUFFIX_HOURS, time.hour, LOWER_HOURS, UPPER_HOURS);
		setSetting(prefix+SUFFIX_MINUTES, time.minute, LOWER_MINUTES, UPPER_MINUTES);
	}

	//should return calendar object instead
	public TimeOfDay getRinseTime1()
	{
		return getTimeSetting(KEY_RINSE_TIME1, DEFAULT_RINSE_TIME1_HOURS, DEFAULT_RINSE_TIME1_MINUTES);
	}

	//should return calendar object
	public TimeOfDay getRinseTime2()
	{
		return getTimeSetting(KEY_RINSE_TIME2, DEFAULT_RINSE_TIME2_HOURS, DEFAULT_RINSE_TIME2_MINUTES);
	}

	public double getPostRinseVolume()
	{
		return getSetting(KEY_POST_RINSE_VOLUME, DEFAULT_POST_RINSE_VOLUME, LOWER_POST_RINSE_VOLUME,UPPER_POST_RINSE_VOLUME);
	}


	public void setLanguage(String language)
	{
		setSetting(KEY_LANGUAGE, language);
	}

	public void setUseMetricDisplayUnits(int value)
	{
		setSetting(KEY_USE_METRIC_DISPLAY_UNITS, value,
				LOWER_USE_METRIC_DISPLAY_UNITS, UPPER_USE_METRIC_DISPLAY_UNITS);
	}

	//needs validation logic
	public void setTechnicianPin(String pin)
	{
		setSetting(KEY_TECHNICIAN_PIN, pin);
	}

	//needs validation logic
	public void setOperatorPin(String pin)
	{
		setSetting(KEY_OPERATOR_PIN, pin);
	}

	public void setTimeoutIdle(int seconds)
	{
		setSetting(KEY_TIMEOUT_IDLE, seconds, LOWER_TIMEOUT_IDLE, UPPER_TIMEOUT_IDLE);
	}

	public void setTimeoutManualRinse(int seconds)
	{
		setSetting(KEY_TIMEOUT_MANUAL_RINSE, seconds,
				LOWER_TIMEOUT_MANUAL_RINSE, UPPER_TIMEOUT_MANUAL_RINSE);
	}

	public void setTimeoutPortionSet(int seconds)
	{
		setSetting(KEY_TIMEOUT_PORTION_SET, seconds,
				LOWER_TIMEOUT_PORTION_SET, UPPER_TIMEOUT_PORTION_SET);
	}

	public void setTimeoutSettingsMenu(int seconds)
	{
		setSetting(KEY_TIMEOUT_SETTINGS_MENU, seconds,
				LOWER_TIMEOUT_SETTINGS_MENU, UPPER_TIMEOUT_SETTINGS_MENU);
	}

	/* Set
		final String KEY_RINSE_MODE="rinse_mode";
		final String KEY_RINSE_TIMES="rinse_times";
		final String KEY_RINSE_TIME1="rinse_time1";
		final String KEY_RINSE_TIME2="rinse_time2";
	 */

	public void setRinseMode(RINSE_MODE mode)
	{
		if (mode==RINSE_MODE.MANUAL)
			setSetting(KEY_RINSE_MODE, 1); //manual
		else
			setSetting(KEY_RINSE_MODE, 0); //automatic
	}

	public void setRinseTimes(RINSE_TIMES times)
	{
		if (times==RINSE_TIMES.TWICE_DAILY)
			setSetting(KEY_RINSE_TIMES, 2);
		else if (times==RINSE_TIMES.ONCE_DAILY)
			setSetting(KEY_RINSE_TIMES, 1);
		else
			setSetting(KEY_RINSE_TIMES, 0); //off
	}

	public void setRinseTime1(TimeOfDay time)
	{
		setTimeSetting(KEY_RINSE_TIME1, time);
	}

	public void setRinseTime2(TimeOfDay time)
	{
		setTimeSetting(KEY_RINSE_TIME2, time);
	}

	public enum LANGUAGE
	{
		ENGLISH,
		SPANISH,
		FRENCH
	}

	public enum RINSE_MODE
	{
		AUTOMATIC,
		MANUAL
	}

	public enum RINSE_TIMES
	{
		OFF,
		ONCE_DAILY,
		TWICE_DAILY
	}
}
