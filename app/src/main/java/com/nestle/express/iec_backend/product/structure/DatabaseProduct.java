package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseProduct extends DatabaseRowParent
{
	public int product_id;
	public int art_id;
	public int product_type;
	public int color;
	public String brix;
	public String ratio;
	public String name;
	public String brand;
	public String category;
	public DatabaseProductSetting setting, defaultSetting;

	public static DatabaseProduct createFromCursor(Cursor product_cursor, Cursor setting_cursor)
	{
		DatabaseProduct product=new DatabaseProduct();

		product.product_id=getIntByName(product_cursor, "product_id");
		product.art_id=getIntByName(product_cursor, "art_id");
		product.product_type=getIntByName(product_cursor, "product_type");
		product.color=getIntByName(product_cursor, "color");
		product.brix=getStringByName(product_cursor, "brix");
		product.ratio=getStringByName(product_cursor, "ratio");
		product.name=getStringByName(product_cursor, "product_string");
		product.category=getStringByName(product_cursor, "category_string");
		product.brand=getStringByName(product_cursor, "brand_string");
		product.defaultSetting=DatabaseProductSetting.createFromCursor(product_cursor);
		product.setting=DatabaseProductSetting.createFromCursor(setting_cursor);

		product_cursor.close();
		setting_cursor.close();

		return product;
	}
/*
	public static ArrayList<DatabaseProduct> createArrayFromCursor(Cursor product_cursor, Cursor setting_cursor)
	{
		if (!product_cursor.moveToFirst())
			return null;

		if (!setting_cursor.moveToFirst())
			return null;

		ArrayList<DatabaseProduct> sales=new ArrayList<>();

		while (!product_cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		return sales;
	}*/
}
