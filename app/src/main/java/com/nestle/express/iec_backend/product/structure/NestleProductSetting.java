package com.nestle.express.iec_backend.product.structure;

import java.io.Serializable;

/**
 * Created by jason on 8/3/16.
 * A wrapper class, interfacing the database primitive to the application.
 */



public class NestleProductSetting
{
	DatabaseProductSetting mSetting;

	NestleProductSetting(DatabaseProductSetting setting)
	{
		mSetting=setting;
	}

	public DatabaseProductSetting getDatabaseSetting()
	{
		return mSetting;
	}
	
	public int getId()
	{
		return mSetting.id;
	}

	public void setId(int id)
	{
		mSetting.id = id;
	}

	public double getSpeedFill()
	{
		return mSetting.speedFill;
	}

	public void setSpeedFill(double speedFill)
	{
		mSetting.speedFill = speedFill;
	}

	public double getSpeedPump()
	{
		return mSetting.speedPump;
	}

	public void setSpeedPump(double speedPump)
	{
		mSetting.speedPump = speedPump;
	}

	public int getStepsStart()
	{
		return mSetting.stepsStart;
	}

	public void setStepsStart(int stepsStart)
	{
		mSetting.stepsStart = stepsStart;
	}

	public int getStepsPump()
	{
		return mSetting.stepsPump;
	}

	public void setStepsPump(int stepsPump)
	{
		mSetting.stepsPump = stepsPump;
	}

	public int getTimeDispenseSmall()
	{
		return mSetting.timeDispenseSmall;
	}

	public void setTimeDispenseSmall(int timeDispenseSmall)
	{
		mSetting.timeDispenseSmall = timeDispenseSmall;
	}

	public int getTimeDispenseMedium()
	{
		return mSetting.timeDispenseMedium;
	}

	public void setTimeDispenseMedium(int timeDispenseMedium)
	{
		mSetting.timeDispenseMedium = timeDispenseMedium;
	}

	public int getTimeDispenseLarge()
	{
		return mSetting.timeDispenseLarge;
	}

	public void setTimeDispenseLarge(int timeDispenseLarge)
	{
		mSetting.timeDispenseLarge = timeDispenseLarge;
	}

	public int getTimeDispenseConcentrate()
	{
		return mSetting.timeDispenseConcentrate;
	}

	public void setTimeDispenseConcentrate(int timeDispenseConcentrate)
	{
		mSetting.timeDispenseConcentrate = timeDispenseConcentrate;
	}

	public NestleProductButtonSetup getEnabledButtons()
	{
		return new NestleProductButtonSetup(mSetting.buttonSetup);
	}

	public void setEnabledButtons(NestleProductButtonSetup buttonSetup)
	{
		mSetting.buttonSetup = buttonSetup.getDatabaseValue();
	}

	public void setPostRinseVolume(double volume)
	{
		mSetting.postRinseVolume=volume;
	}

	public double getPostRinseVolume()
	{
		return mSetting.postRinseVolume;
	}
}
