package com.nestle.express.iec_backend.product.structure;

import android.util.Log;

/**
 * Created by jason on 7/12/16.
 *
 */
public class SelectionMatrix
{
	final String TAG="SelectionMatrix";
	//4x4 matrix
	DatabaseDisplayProduct[][] mProducts;

	public SelectionMatrix()
	{
		//create array of nulls
		mProducts=new DatabaseDisplayProduct[4][4];

		for(int i=0;i<4; i++)
			for(int j=0; j<4; j++)
				mProducts[i][j]=null;
	}

	boolean isOutOfBounds(int i)
	{
		if (i<0 || i>3)
			return true;
		return  false;
	}

	int limitIndex(int index)
	{
		if (index<0)
			index=0;
		if (index>3)
			index=3;

		return index;
	}

	public void setElement(ProductSlot slot, ProductRow row, DatabaseDisplayProduct product)
	{
		int slotNumber=slot.ordinal();
		int rowNumber=row.ordinal();

		mProducts[slotNumber][rowNumber]=product;
	}

	public void setElement(int index, DatabaseDisplayProduct product)
	{
		if (index<0 || index>15)
		{
			Log.e(TAG, "setElement index out of bounds");
			return;
		}

		int slot=index/4;
		int row=index%4;

		mProducts[slot][row]=product;
	}

	public void setElement(int slot, int row, DatabaseDisplayProduct product)
	{
		if (isOutOfBounds(slot) || isOutOfBounds(row))
		{
			Log.e(TAG, "setElement index out of bounds");
			return;
		}

		mProducts[slot][row]=product;
	}

	public DatabaseDisplayProduct getElement(ProductSlot slot, ProductRow row)
	{
		int slotNumber=slot.ordinal();
		int rowNumber=row.ordinal();

		return mProducts[slotNumber][rowNumber];
	}

	public DatabaseDisplayProduct getElement(int slot, int row)
	{
		if (isOutOfBounds(slot) || isOutOfBounds(row))
		{
			Log.e(TAG, "getElement index out of bounds");
			return null;
		}

		return mProducts[slot][row];
	}

	public int[] getElementIds()
	{
		int[] elementIds=new int [16];

		for(int i=0;i<4; i++)
			for(int j=0; j<4; j++)
				elementIds[i*4+j]=mProducts[i][j].id;

		return elementIds;
	}
}
