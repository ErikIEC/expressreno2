package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseLanguage extends DatabaseRowParent
{
	public int id;
	public String isoCode;//Fixme the isoCode is not fetch from the database, wasn't said the mapping would be done with the isoCode in the database ?
	public String name;


	public static DatabaseLanguage createFromCursor(Cursor cursor)
	{
		DatabaseLanguage language=new DatabaseLanguage();

		language.id=getIntByName(cursor, "id");
		language.name=getStringByName(cursor, "name");
		language.isoCode=getStringByName(cursor, "iso_code");

		return language;
	}

	public static ArrayList<DatabaseLanguage> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
		{
			cursor.close();
			return null;
		}

		ArrayList<DatabaseLanguage> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}

	public int getId()
	{
		return id;
	}

	public String getIsoCode()
	{
		return isoCode;
	}

	public String getName()
	{
		return name;
	}
}
