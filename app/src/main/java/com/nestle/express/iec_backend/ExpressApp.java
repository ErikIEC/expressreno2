package com.nestle.express.iec_backend;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.util.Log;

import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.database.ArtDatabase;
import com.nestle.express.iec_backend.database.DatabaseDriverV1;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.machine.SalesDatabase;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.communications.device_layer.DeviceFile;
import com.nestle.express.communications.device_layer.DeviceLayer;
import com.nestle.express.communications.physical_layer.PhysicalLayer;
import com.nestle.express.communications.physical_layer.PhysicalNestleSerial;
import com.nestle.express.communications.protocol_layer.ProtocolLayer;
import com.nestle.express.communications.protocol_layer.ProtocolNestle;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;

import java.util.Locale;
import java.io.FileNotFoundException;

/**
 * Created by jason on 6/3/16.
 */
public class ExpressApp extends Application
{

	public static final String DEFAULT_LOCALE = "DEFAULT_LOCALE";
	final String TAG="ExpressApp";
	String mFailureMessage="";
	STARTUP_FAILURE_CAUSE mFailureMode=STARTUP_FAILURE_CAUSE.NO_ERROR;
	static DatabaseLanguage mLanguage = new DatabaseLanguage();

	final String USBDirectory="mnt/media_rw/udisk";
	final String directory="/mnt/media_rw/extsd/v1";
	//final String directory = "/sdcard/nestle";    //For device
	final String serialPort="/dev/ttymxc1";
	String mVersion="None";

	//Dependencies of Machine Manager
	DeviceLayer mDevice;
	PhysicalLayer mPhysical;
	ProtocolLayer mProtocol;

	DatabaseDriverV1 mDatabase;
	ProductManager mProductManager;
	MachineManager mMachineManager;
	ApplicationManager mApplicationManager;
	SalesDatabase mSalesDatabase;
	ArtDatabase mArtDatabase;

	boolean isCommunicationsActive;
	private static Context app;

	public static Context getApp() {
		return app;
	}


	@Override
	public void onTerminate()
	{
		super.onTerminate();
		mDatabase.close();
		mProductManager.close();
		mApplicationManager.close();
		mMachineManager.close();
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		app = this;
		
		if (!SDCardUtilities.isSDCardPresent())
		{
			Log.e(TAG, "SD Card Directory Not Found");
			mFailureMode=STARTUP_FAILURE_CAUSE.SD_CARD_NOT_FOUND;
			return;
		}

		try
		{
			mDatabase = new DatabaseDriverV1(false, directory);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			mFailureMode=STARTUP_FAILURE_CAUSE.PRODUCT_DATABASE_NOT_FOUND;
			mFailureMessage=e.getMessage();
			return;
		}

		try
		{
			mDatabase.open();
		}
		catch (SQLiteException e)
		{
			mFailureMode=STARTUP_FAILURE_CAUSE.PRODUCT_DATABASE_FORMAT;
			mFailureMessage=e.getMessage();
			return;
		}

		try
		{
			mArtDatabase=new ArtDatabase(directory, "en");
			mArtDatabase.checkArt(getLanguageByIsoCode("en"), mDatabase.getProductIds());
			mArtDatabase.checkArt(getLanguageByIsoCode("fr"), mDatabase.getProductIds());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			mFailureMode=STARTUP_FAILURE_CAUSE.ART_DATABASE_ERROR;
			mFailureMessage=e.getMessage();
			//return;
		}

		mApplicationManager=new ApplicationManager(directory);
		mLanguage=mDatabase.getLanguage(mApplicationManager.getLanguageIsoCode());

		mDevice=new DeviceFile(serialPort);
		mPhysical=new PhysicalNestleSerial(mDevice);
		mProtocol=new ProtocolNestle(mPhysical);
		mProtocol.open();

		setLocale(getApplicationContext(), new Locale(mLanguage.getIsoCode()));
		SharedPreferences.Editor prefsEdit = PreferenceManager.getDefaultSharedPreferences(this).edit();
		prefsEdit.putString(DEFAULT_LOCALE, mLanguage.getIsoCode());
		prefsEdit.commit();

		mProductManager=new ProductManager(mLanguage, directory, mDatabase);
		mProductManager.open();


		mSalesDatabase=new SalesDatabase(mDatabase, mProductManager);
		mMachineManager=new MachineManager(mDatabase, mProductManager, mSalesDatabase, mApplicationManager, this ,directory, mProtocol);


		setSelectedLanguage(mLanguage);
		mHandler.postDelayed(mCheckCommunications, 5000);
	}

	public ProductManager getProductManager()
	{
		return mProductManager;
	}

	public MachineManager getMachineManager()
	{
		return mMachineManager;
	}

	public DatabaseDriverV1 getDatabase()
	{
		return mDatabase;
	}

	public ApplicationManager getApplicationManager()
	{
		return mApplicationManager;
	}

	public SalesDatabase getSalesDatabase()
	{
		return mSalesDatabase;
	}

	public ArtDatabase getArtDatabase()
	{
		return mArtDatabase;
	}

	public void setSelectedLanguage(DatabaseLanguage language)
	{
		mLanguage=language;
		mApplicationManager.setLanguage(language);
		mProductManager.setLanguage(language);
		mDatabase.setLanguage(language);
	}

	public DatabaseLanguage getSelectedLanguage()
	{
		return mLanguage;
	}

	public DatabaseLanguage getLanguageByIsoCode(String isoCode)
	{
		return mDatabase.getLanguage(isoCode);
	}

	public static void setLocale(Context context, @NonNull Locale locale) {
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
    }

	public enum STARTUP_FAILURE_CAUSE
	{
		NO_ERROR,
		SD_CARD_NOT_FOUND,
		PRODUCT_DATABASE_NOT_FOUND,
		PRODUCT_DATABASE_FORMAT,
		ART_DATABASE_ERROR,
		NO_COMMUNICATIONS,
		UPDATE_AVAILABLE
	}

	public STARTUP_FAILURE_CAUSE getStartupFailureCase()
	{
		return mFailureMode;
	}

	public String getFailureMessage()
	{
		return mFailureMessage;
	}

	public String getSDCardDirectory()
	{
		return directory;
	}

	public String getUSBDirectory()
	{
		return USBDirectory;
	}
	//TODO :  This runnable is always executed, never destroyed. getStartupFailureCase is only used in ActivityBase and ActivityStartup.
	//It would be better to have this code moved to the specific activity and destroyed when we dont need it again.
	// As it is now we have an useless process in the UIThread every 5 seconds
	Handler mHandler = new Handler();
	Runnable mCheckCommunications = new Runnable()
	{
		@Override
		public void run()
		{
			if (mMachineManager.getAndClearReceivedCommunications()==false)
			{
				isCommunicationsActive=false;

				//if (mFailureMode==STARTUP_FAILURE_CAUSE.NO_ERROR)
					mFailureMode=STARTUP_FAILURE_CAUSE.NO_COMMUNICATIONS;
			}
			else
				isCommunicationsActive=true;

			mHandler.postDelayed(this, 5000);
		}
	};

	public void closeDatabase()
	{
		mDatabase.close();
	}

	public void openDatabase()
	{
		mDatabase.open();
	}

	public void setVersion(String version)
	{
		if (version!=null && !version.toLowerCase().contains("none"))
			mVersion=version;
	}

	public String getVersion()
	{
		return mVersion;
	}
}
