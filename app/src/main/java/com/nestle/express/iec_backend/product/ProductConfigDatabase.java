package com.nestle.express.iec_backend.product;

import com.nestle.express.iec_backend.database.ConfigDatabaseParent;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by jason on 7/5/16.
 *
 * See documentation in configuration_files.odt
 *
 * Provides a setter/getter interface to write to
 * the product configuration database
 *
 * Settings			Type			Range		Description
 * 	selected		int-array[4]	0-3			Which product in product selection matrix is chosen
 * 	selection		int-array[4*4]	see [1]		Product selection matrix consisting of database keys
 *
 * 	[1] value must be in mValidProductIDs which is supplied at initialization
 *
 * Note: the database primary key of -1 (or less) is reserved for the error "Product does not exist"
 * and is not considered a valid product ID even if it is passed to this class at initialization.
 */

public class ProductConfigDatabase extends ConfigDatabaseParent
{
	final String COMMENT="Product Configuration - Selected Products and Product Selection Matrix";
	final String FILENAME="product.config";

	final String KEY_SELECTED_INDEX="selected_index";
	final String KEY_SELECTION_MATRIX="selection_matrix";

	final int DEFAULT_SELECTED=0;
	final int DEFAULT_MATRIX=-1;
	final int LOWER_SELECTED=0;
	final int UPPER_SELECTED=3;
	final int SIZE_SELECTED=4;
	final int SIZE_MATRIX=16;


	int mValidProductIDs[];


	public ProductConfigDatabase(String directory, int validProductIDs[])
	{
		initialize(directory, FILENAME, COMMENT);

		mValidProductIDs=validProductIDs;
	}

	public ProductConfigDatabase(String directory, String filename, int validProductIDs[])
	{
		initialize(directory, filename, COMMENT);

		mValidProductIDs=validProductIDs;
	}

	public void setValidProductIDs(int[] validIDs)
	{
		mValidProductIDs=validIDs;
	}

	public boolean keyIsValid(int key)
	{
		if (key==-1)
			return false;

		for(int i : mValidProductIDs)
		{
			if (key==i)
				return true;
		}

		return false;
	}

	public int[] sanitizeMatrix(int[] matrix)
	{
		int[] newMatrix=matrix.clone();

		for(int i=0; i<newMatrix.length; i++)
		{
			int value=newMatrix[i];

			if (!keyIsValid(value))
				newMatrix[i]=-1;
		}

		return newMatrix;
	}

	public int[] getSelectedIndexes()
	{
		return getSetting(KEY_SELECTED_INDEX, DEFAULT_SELECTED, SIZE_SELECTED, LOWER_SELECTED, UPPER_SELECTED);
	}

	public int getSelectedId(ProductSlot slot)
	{
		int ids[]= getSelectedIDs();

		if (slot==ProductSlot.SLOT4)
			return ids[3];
		else if (slot==ProductSlot.SLOT3)
			return ids[2];
		else if (slot==ProductSlot.SLOT2)
			return ids[1];
		else //SLOT1
			return ids[0];
	}

	public int[] getSelectedIDs()
	{
		int[] indexes=getSelectedIndexes();
		int[] matrix=getSelectionMatrix();

		int[] ids=new int[SIZE_SELECTED];

		for(int i=0; i<ids.length; i++)
		{
			ids[i]=matrix[SIZE_SELECTED*i+indexes[i]];
		}

		return ids;
	}

	public int[] getSelectionMatrix()
	{
		int matrix[]=getSetting(KEY_SELECTION_MATRIX, DEFAULT_MATRIX, SIZE_MATRIX);

		return sanitizeMatrix(matrix);
	}

	public void setSelectedIndexes(int selected[])
	{
		setSetting(KEY_SELECTED_INDEX, selected, LOWER_SELECTED, UPPER_SELECTED);
	}

	public void setSelectionMatrix(int matrix[])
	{
		int sanitizedMatrix[] = sanitizeMatrix(matrix);

		setSetting(KEY_SELECTION_MATRIX, sanitizedMatrix);
	}

}
