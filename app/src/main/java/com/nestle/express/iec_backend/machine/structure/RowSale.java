package com.nestle.express.iec_backend.machine.structure;

/**
 * Created by jason on 7/7/16.
 */
public class RowSale
{
	public int unixDayNumber; //unix timestamp
	public int productId;
	public double ratio;
	public double volume; // K*RPM*DispenseTime=Volume
}
