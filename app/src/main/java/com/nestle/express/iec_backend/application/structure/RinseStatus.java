package com.nestle.express.iec_backend.application.structure;

import android.util.Log;

import com.nestle.express.iec_backend.application.ApplicationConfigDatabase;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jason on 1/6/17.
 */
public class RinseStatus
{
	RinseStatus self;
	final long DAILY_MILLISECONDS=1000 * 60 * 60 * 24;
	ApplicationConfigDatabase mAppConfigDb;
	Timer mDailyRinseTimer1=new Timer(), mDailyRinseTimer2=new Timer();

	boolean mChanged[]=new boolean[4];
	RINSE_STATUS mStatus[]=new RINSE_STATUS[4];

	public RinseStatus(ApplicationConfigDatabase appConfigDb)
	{
		mAppConfigDb=appConfigDb;
		setAll(RINSE_STATUS.NONE);
		updateRinseTimes();
		self=this;
	}

	public void updateRinseTimes()
	{
		Log.d("TIMER", "-----------------");
		Log.d("TIMER", "NOW: "+new Date(System.currentTimeMillis()).toString());
		mDailyRinseTimer1.cancel();
		mDailyRinseTimer2.cancel();

		mDailyRinseTimer1=new Timer();
		mDailyRinseTimer2=new Timer();

		ApplicationConfigDatabase.RINSE_TIMES times=mAppConfigDb.getRinseTimes();
		Log.d("TIMER", "MODE: "+times);

		if (times== ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY ||
				times== ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY)
		{

			TimeOfDay rinse1=mAppConfigDb.getRinseTime1();
			Calendar date1 = Calendar.getInstance();

			date1.set(Calendar.HOUR_OF_DAY, rinse1.hour);
			date1.set(Calendar.MINUTE, rinse1.minute);
			date1.set(Calendar.SECOND, 0);
			date1.set(Calendar.MILLISECOND, 0);

			long time=date1.getTime().getTime();
			if (time<System.currentTimeMillis())
				time+=DAILY_MILLISECONDS;


			mDailyRinseTimer1.schedule(
					new DailyRinseTimer(),
					new Date(time),
					DAILY_MILLISECONDS
			);
		}

		if (times== ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY)
		{
			TimeOfDay rinse2=mAppConfigDb.getRinseTime2();
			Calendar date2 = Calendar.getInstance();

			date2.set(Calendar.HOUR_OF_DAY, rinse2.hour);
			date2.set(Calendar.MINUTE, rinse2.minute);
			date2.set(Calendar.SECOND, 0);
			date2.set(Calendar.MILLISECOND, 0);

			long time=date2.getTime().getTime();
			if (time<System.currentTimeMillis())
				time+=DAILY_MILLISECONDS;

			mDailyRinseTimer2.schedule(
					new DailyRinseTimer(),
					new Date(time),
					DAILY_MILLISECONDS
			);
		}
	}

	public RINSE_STATUS getRinseStatus(ProductSlot slot)
	{
		return mStatus[slot.ordinal()];
	}

	public void setRinseStatus(ProductSlot slot, RINSE_STATUS status)
	{
		mChanged[slot.ordinal()]=true;
		mStatus[slot.ordinal()]=status;
	}

	public void makeManual()
	{
		setAll(RINSE_STATUS.MANUAL_RINSE);
	}

	public void makeDaily()
	{
		setAll(RINSE_STATUS.DAILY_RINSE);
	}

	public void makeNone()
	{
		setAll(RINSE_STATUS.NONE);
	}

	void setAll(RINSE_STATUS status)
	{
		mStatus[0]=status;
		mStatus[1]=status;
		mStatus[2]=status;
		mStatus[3]=status;

		mChanged[0]=true;
		mChanged[1]=true;
		mChanged[2]=true;
		mChanged[3]=true;
	}

	public enum RINSE_STATUS
	{
		NONE,
		MANUAL_RINSE,
		DAILY_RINSE,
		ALLERGEN_RINSE
	}

	RinseRequestCallback mRinseCallback;
	public void setRinseCallback(RinseRequestCallback callback)
	{
		mRinseCallback=callback;
	}

	public void clearChangeStatus()
	{
		mChanged[0]=false;
		mChanged[1]=false;
		mChanged[2]=false;
		mChanged[3]=false;
	}

	public void runCallbackIfNeeded()
	{
		boolean run=false;
		for(boolean changed : mChanged)
		{
			if (changed)
				run=true;
		}

		if (run)
			runCallback();

	}

	void runCallback()
	{
		Log.d("RinseStatus", "Callback");
		if (mRinseCallback!=null)
			mRinseCallback.onRinse(this);
	}

	class DailyRinseTimer extends TimerTask
	{
		public void run()
		{
			makeDaily();
			Log.d("DailyRinse", "TimerTask");
			runCallback();
		}
	}
}
