package com.nestle.express.iec_backend.product.structure;

/**
 * Created by jason on 12/13/16.
 */
public class NestleProductButtonSetup
{
	//Which buttons are enabled
	private int mDatabaseValue=0;
	public boolean small;
	public boolean medium;
	public boolean large;

	public boolean isConcentrate()
	{
		return concentrate;
	}

	public void setConcentrate(boolean concentrate)
	{
		this.concentrate = concentrate;
	}

	public boolean isSmall()
	{
		return small;
	}

	public void setSmall(boolean small)
	{
		this.small = small;
	}

	public boolean isMedium()
	{
		return medium;
	}

	public void setMedium(boolean medium)
	{
		this.medium = medium;
	}

	public boolean isLarge()
	{
		return large;
	}

	public void setLarge(boolean large)
	{
		this.large = large;
	}

	public boolean concentrate;
	/*
		If Small, Medium, and large are disabled. -> Push to pour only.
		else -> show enabled presized dispense buttons

		If concentrate enabled -> show concentrate dispense button
	 */

	/*
		Database Storage format
		Bit 0 - Small
		Bit 1 - Medium
		Bit 2 - Large
		Bit 3 - Concentrate
	 */
	public NestleProductButtonSetup(int databaseValue)
	{
		mDatabaseValue=databaseValue;

		if ((mDatabaseValue&0x1)!=0)	small=true;
		if ((mDatabaseValue&0x2)!=0)	medium=true;
		if ((mDatabaseValue&0x4)!=0)	large=true;
		if ((mDatabaseValue&0x8)!=0)	concentrate=true;
	}

	public int getDatabaseValue()
	{
		mDatabaseValue=0;
		if (small)			mDatabaseValue|=0x1;
		if (medium)			mDatabaseValue|=0x2;
		if (large)			mDatabaseValue|=0x4;
		if (concentrate)	mDatabaseValue|=0x8;

		return mDatabaseValue;
	}
}
