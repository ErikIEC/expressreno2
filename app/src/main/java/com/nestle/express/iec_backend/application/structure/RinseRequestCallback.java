package com.nestle.express.iec_backend.application.structure;

/**
 * Created by jason on 1/6/17.
 */
public interface RinseRequestCallback
{
	void onRinse(RinseStatus status);
}
