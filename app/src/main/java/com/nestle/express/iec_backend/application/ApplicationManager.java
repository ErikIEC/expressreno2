package com.nestle.express.iec_backend.application;

import com.nestle.express.iec_backend.application.structure.RinseRequestCallback;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.application.structure.TimeOfDay;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;

/**
 * Created by jason on 12/20/16.
 */
public class ApplicationManager
{
	ApplicationConfigDatabase mAppConfigDb;
	FRONTEND_STATE mFrontendState=FRONTEND_STATE.DEFAULT;
	UNITS tempUnits = UNITS.IMPERIAL;

	public ApplicationManager(String baseDirectory)
	{
		mAppConfigDb=new ApplicationConfigDatabase(baseDirectory);
		mRinseStatus = new RinseStatus(mAppConfigDb);
	}

	public void save()
	{
		mAppConfigDb.save();
	}

	public void close()
	{
		save();
	}

	public UNITS getUnits()
	{
		if (mAppConfigDb.getMetricDisplayUnits()==1)
			return UNITS.METRIC;
		return UNITS.IMPERIAL;
	}

	public void setUnits(UNITS units)
	{
		int useMetric=0;
		if (units==UNITS.METRIC)
			useMetric=1;

		mAppConfigDb.setUseMetricDisplayUnits(useMetric);
		mAppConfigDb.save();
	}

	/* Add to handle displaying temporary units under technician screen language change */
	public void setTempUnits(UNITS units)
	{
		tempUnits = units;
	}

	/* Add to handle displaying temporary units under technician screen language change */
	public UNITS getTempUnits()
	{
		return (tempUnits);
	}

	public String getLanguageIsoCode()
	{
		return mAppConfigDb.getLanguage();
	}

	public void setLanguage(DatabaseLanguage language)
	{
		mAppConfigDb.setLanguage(language.getIsoCode());
		mAppConfigDb.save();
	}

	public int getTimeoutIdle()
	{
		return mAppConfigDb.getTimeoutIdle();
	}

	public int getTimeoutSettingsMenu()
	{
		return mAppConfigDb.getTimeoutSettingsMenu();
	}

	public int getTimeoutPortionSet()
	{
		return mAppConfigDb.getTimeoutPortionSet();
	}

	public int getTimeoutRinse()
	{
		return mAppConfigDb.getTimeoutManualRinse();
	}

	public TimeOfDay getRinseTime(RINSE_TIME time)
	{
		if (time==RINSE_TIME.RINSE_TIME1)
			return mAppConfigDb.getRinseTime1();
		else
			return mAppConfigDb.getRinseTime2();
	}

	public ApplicationConfigDatabase.RINSE_MODE getRinseMode()
	{
		return mAppConfigDb.getRinseMode();
	}

	public ApplicationConfigDatabase.RINSE_TIMES getRinseTimes()
	{
		return mAppConfigDb.getRinseTimes();
	}


	public void setTimeoutIdle(int seconds)
	{
		mAppConfigDb.setTimeoutIdle(seconds);
	}

	public void setTimeoutTechnician(int seconds)
	{
		mAppConfigDb.setTimeoutSettingsMenu(seconds);
	}

	public void setTimeoutPortionSet(int seconds)
	{
		mAppConfigDb.setTimeoutPortionSet(seconds);
	}

	public void setTimeoutRinse(int seconds)
	{
		mAppConfigDb.setTimeoutManualRinse(seconds);
	}

	public void setRinseTime(RINSE_TIME time, TimeOfDay value)
	{
		if (time==RINSE_TIME.RINSE_TIME1)
			mAppConfigDb.setRinseTime1(value);
		else
			mAppConfigDb.setRinseTime2(value);

		mRinseStatus.updateRinseTimes();
	}

	public void setRinseMode(ApplicationConfigDatabase.RINSE_MODE value)
	{
		mAppConfigDb.setRinseMode(value);
	}

	public void setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES times)
	{
		mAppConfigDb.setRinseTimes(times);
		mRinseStatus.updateRinseTimes();
	}


	public FRONTEND_STATE getFrontendState()
	{
		return mFrontendState;
	}

	public void setFrontendState(FRONTEND_STATE frontendState)
	{
		this.mFrontendState = frontendState;
	}

	public enum FRONTEND_STATE
	{
		DEFAULT,
		SET_PORTIONS,
		RINSE,
		PRODUCT_CHANGED
	}

	public enum RINSE_TIME
	{
		RINSE_TIME1,
		RINSE_TIME2
	}

	public enum SLOT
	{
		SLOT1,
		SLOT2,
		SLOT3,
		SLOT4
	}

	RinseStatus mRinseStatus;
	RinseRequestCallback mOnRinseCallback = null;
	public void setOnRinseCallback(RinseRequestCallback callback)
	{
		mOnRinseCallback=callback;
		mRinseStatus.setRinseCallback(callback);
	}
	public RinseStatus getRinseStatus()
	{
		return mRinseStatus;
	}

	public void exitRinseMode()
	{
		mRinseStatus.makeNone();
		mFrontendState=FRONTEND_STATE.DEFAULT;
	}

	public void exitPortionSetMode()
	{
		mFrontendState=FRONTEND_STATE.DEFAULT;
	}

	public enum UNITS
	{
		IMPERIAL,
		METRIC
	}
}
