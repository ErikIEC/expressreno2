package com.nestle.express.iec_backend;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;

import java.util.Locale;

/**
 * Created by jason on 12/6/16.
 */
public class NurunApp
{
	private final static String mNurunDirectory = "/mnt/media_rw/extsd/v1/";
	private final static String mLanguageCodeEnglish = "en";
	private final static String mLanguageCodeFrench = "fr";
	private final static String mLanguageCodeSpanish = "es";

	public static void setLocale(Context context, @NonNull Locale locale) {
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		context.getApplicationContext().getResources().updateConfiguration(config, null);
	}

	public static String getLanguageCodeEnglish() {
		return mLanguageCodeEnglish;
	}

	public static String getLanguageCodeFrench() {
		return mLanguageCodeFrench;
	}

	public static String getLanguageCodeSpanish() {
		return mLanguageCodeSpanish;
	}

	public static String getNurunDirectory()
	{
		return mNurunDirectory;
	}
}
