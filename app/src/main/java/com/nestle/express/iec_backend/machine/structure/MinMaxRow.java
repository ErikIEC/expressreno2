/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.iec_backend.machine.structure;

/**
 *
 * @author jason
 */
public class MinMaxRow
{
	public double min, max;
	
	public MinMaxRow(double d)
	{
		min=max=d;
	}
	
	public MinMaxRow(double min_, double max_)
	{
		min=min_;
		max=max_;
	}

	public String toString()
	{
		return max+"/"+min;
	}
}
