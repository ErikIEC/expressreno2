
package com.nestle.express.iec_backend.machine;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import com.nestle.express.BuildConfig;
import com.nestle.express.communications.protocol_layer.ProtocolCallback;
import com.nestle.express.communications.protocol_layer.ProtocolEvent;
import com.nestle.express.communications.protocol_layer.events.EventClearFaults;
import com.nestle.express.communications.protocol_layer.events.EventClearFaultsResponse;
import com.nestle.express.communications.protocol_layer.events.EventDispenseResponse;
import com.nestle.express.communications.protocol_layer.events.EventIORequest;
import com.nestle.express.communications.protocol_layer.events.EventIOResponse;
import com.nestle.express.communications.protocol_layer.events.EventSetParameters;
import com.nestle.express.communications.protocol_layer.events.EventSetParametersResponse;
import com.nestle.express.communications.protocol_layer.events.EventStatusRequest;
import com.nestle.express.communications.protocol_layer.events.EventStatusResponse;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.database.DatabaseDriverV1;
import com.nestle.express.iec_backend.machine.structure.TemperatureHistory;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.communications.protocol_layer.ProtocolLayer;
import com.nestle.express.communications.protocol_layer.events.EventDispense;

import java.util.Arrays;

/**
 * Created by jason on 7/29/16.
 *
 */
public class MachineManager
{
	final String TAG = "MachineManager";
	ProtocolLayer mProtocol;
	EventStatusResponse mStatus=null;
	FaultMatrix mFaults;
	SalesDatabase mSales;
	MachineConfigDatabase mMachineConfigDb;
	ProductManager mProductManager;
	ApplicationManager mAppManager;
	DatabaseDriverV1 mDatabase;
	TemperatureHistory mTemperatureHistory=new TemperatureHistory();
	TimerUtil mTimers[]={new TimerUtil(), new TimerUtil(),
			new TimerUtil(),new TimerUtil()
	};
	int[] mDbVersion;
	public String mAppVersion;

	boolean mDispenseLockout;
	boolean mGotStatusPacket;
	//TODO This shouldn't be done on the mainThread.
	// Might leak, the handler should be destroyed when the machineManager is closed
	Handler mHandler = new Handler(Looper.getMainLooper());
	HandlerThread mBackgroundThread = new HandlerThread("MachinBackground");
	Handler mBackgroundHandler;

	Runnable mPollRunnable = new Runnable()
	{

		@Override
		public void run()
		{

			if (mProtocol!=null)
			{
				mProtocol.poll();
				mProtocol.receive(mHandler);
				mProtocol.sendEventFromQueue();
			}
			mBackgroundHandler.postDelayed(this, 75);
		}
	};

	//TODO This shouldn't be done on the mainThread.
	// Might leak, the handler should be destroyed when the machineManager is closed
	Runnable mRequestStatus = new Runnable()
	{
		@Override
		public void run()
		{
			EventStatusRequest request=new EventStatusRequest();
			mProtocol.transmit(request, new EventStatusResponse());
			mBackgroundHandler.postDelayed(this, 1000);
			mBackgroundHandler.postDelayed(mStatusTimeout, 2500);
		}
	};

	ProtocolCallback mStatusCallback=new ProtocolCallback()
	{
		@Override
		public void run(ProtocolEvent event)
		{
			Log.d("STATUS", "Received");
			mBackgroundHandler.removeCallbacks(mStatusTimeout);
			mGotStatusPacket =true;

			if (System.currentTimeMillis()>statusReceiveEnableTime)
			{
				mStatus=(EventStatusResponse) event;
				mTemperatureHistory.addTemperatures(mStatus.getWater1(), mStatus.getWater2(), mStatus.getConcentrate());
				mFaults.updateState(mStatus);
			}
			else
			{
				long time=statusReceiveEnableTime-System.currentTimeMillis();
				Log.d(TAG, "Status reception disabled for "+time);
			}
		}
	};

	public MachineManager(DatabaseDriverV1 database, ProductManager mProductMan, SalesDatabase sales, ApplicationManager app, Context context, String directory, ProtocolLayer protocol)
	{
		mProductManager=mProductMan;
		mFaults=new FaultMatrix(context);
		//mProductManager=mProductMan;
		mProductManager.setFaultMatrix(mFaults);

		mDatabase=database;
		mSales=sales;
		mMachineConfigDb=new MachineConfigDatabase(directory);
		mDatabase.truncateFaults();
		mAppManager=app;
		mFaults.setOnNewFaultCallback(mOnNewFault);
        mFaults.setOnNewWarningCallback(mOnNewWarning);

		mProtocol=protocol;
		mProtocol.addCallback(new EventStatusResponse(), mStatusCallback);
		mProtocol.setOnErrorCallback(mStatusTimeout);

        mDbVersion = database.getVersion();
        mAppVersion = "App Version: "+ BuildConfig.VERSION_NAME;//BuildConfig.GitHash;

		mBackgroundThread.start();
		mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
		mBackgroundHandler.post(mRequestStatus);
		mBackgroundHandler.post(mPollRunnable);

		getDispenseLockout();

		updateDriverSetpoints();
	}

	public void enterDispenseMode()
	{
		Log.d(TAG, "enterDispenseMode");
	}

	EventDispense.Channel getChannel(ProductSlot slot)
	{
		EventDispense.Channel channel=null;

		if (slot==ProductSlot.SLOT1)
			channel= EventDispense.Channel.CHANNEL_1;
		else if (slot==ProductSlot.SLOT2)
			channel= EventDispense.Channel.CHANNEL_2;
		else if (slot==ProductSlot.SLOT3)
			channel= EventDispense.Channel.CHANNEL_3;
		else if (slot==ProductSlot.SLOT4)
			channel= EventDispense.Channel.CHANNEL_4;

		return channel;
	}

	public NestleProduct generateProduct(ProductSlot slot)
	{
		NestleProduct product=new NestleProduct();
		product.setSlot(slot);
		return product;
	}

	void startDispense(NestleProduct product, EventDispense.Mode type)
	{
		EventDispense dispense=new EventDispense();
		dispense.setChannel(getChannel(product.getSlot()));

		dispense.setMode(type);

		dispense.setSpeedFill(product.getSpeedFill());
		dispense.setSpeedPump(product.getSpeedPump());
		dispense.setStepsStart(product.getStepsStart());
		dispense.setStepsPump(product.getStepsPump());
		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void dispenseStart(NestleProduct product, EventDispense.Mode type)
	{
		Log.d(TAG, "dispenseStart "+product.getSlot().toString()+" "+type.toString());
		startSalesTimer(product.getSlot().ordinal());
		startDispense(product, type);
	}

	final int TIME_DAILY=1000;
	final int TIME_ALLERGEN=2000;
	public void presizedDispenseStart(NestleProduct product, EventDispense.Mode type, ProductSize size, Runnable onFinishCallback)
	{
		Log.d(TAG, "presizedDispenseStart "+product.getSlot().toString()+" "+type.toString()+" "+size.toString());
		startDispense(product, type);

		int time=0;
		if (size==ProductSize.CONCENTRATE)
			time=product.getTimeConcentrate();
		else if (size==ProductSize.LARGE)
			time=product.getTimeLarge();
		else if (size==ProductSize.MEDIUM)
			time=product.getTimeMedium();
		else if (size==ProductSize.SMALL)
			time=product.getTimeSmall();
		else if (size==ProductSize.DAILY)
			time=TIME_DAILY;
		else if (size==ProductSize.ALLERGEN)
			time=TIME_ALLERGEN;

		ProductSlot slot=product.getSlot();
		startSalesTimer(slot.ordinal());

		mPresizedStop[slot.ordinal()]=new PresizedDispenseStop(slot, size, onFinishCallback);
		mHandler.postDelayed(mPresizedStop[slot.ordinal()], time);
	}

	public void rinseStart(NestleProduct product, RinseStatus.RINSE_STATUS status, Runnable onFinishCallback)
	{
		if (status==RinseStatus.RINSE_STATUS.DAILY_RINSE)
			presizedDispenseStart(product, EventDispense.Mode.MODE_WATER,ProductSize.DAILY, onFinishCallback);
		else if (status== RinseStatus.RINSE_STATUS.ALLERGEN_RINSE)
			presizedDispenseStart(product, EventDispense.Mode.MODE_WATER,ProductSize.ALLERGEN, onFinishCallback);
		else
			dispenseStart(product, EventDispense.Mode.MODE_WATER);
	}

	public void rinseEnd(NestleProduct product)
	{
		sendStop(product.getSlot());

		mAppManager.getRinseStatus().setRinseStatus(
				product.getSlot(), RinseStatus.RINSE_STATUS.NONE);
		mAppManager.getRinseStatus().runCallbackIfNeeded();
	}

	public void dispenseWaterNozzleStart()
	{
		Log.d(TAG, "dispenseWaterNozzle Start");
		EventDispense dispense=new EventDispense();

		dispense.setChannel(EventDispense.Channel.CHANNEL_WATER_NOZZLE);
		dispense.setMode(EventDispense.Mode.MODE_WATER);

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void dispenseWaterNozzleEnd()
	{
		Log.d(TAG, "dispenseWaterNozzle End");

		EventDispense dispense=new EventDispense();
		dispense.setChannel(EventDispense.Channel.CHANNEL_WATER_NOZZLE);

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	void sendStop(ProductSlot slot)
	{
		Log.d(TAG, "sendStop " + getChannel(slot));

		EventDispense dispense=new EventDispense();
		dispense.setChannel(getChannel(slot));

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void dispenseEnd(NestleProduct product)
	{
		Log.d(TAG, "dispenseStop");

		int slotNumber=product.getSlot().ordinal();
		stopSalesTimer(slotNumber);

		PresizedDispenseStop stop=mPresizedStop[slotNumber];
		if (stop!=null)
		{
			mHandler.removeCallbacks(stop);
			mPresizedStop[slotNumber]=null;
		}

		sendStop(product.getSlot());
	}

	public void dispenseEndWithCallback(NestleProduct product)
	{
		Log.d(TAG, "dispenseStop");

		int slotNumber=product.getSlot().ordinal();
		stopSalesTimer(slotNumber);

		PresizedDispenseStop stop=mPresizedStop[slotNumber];
		if (stop!=null)
		{
			stop.run();
			mHandler.removeCallbacks(stop);
			mPresizedStop[slotNumber]=null;
		}

		sendStop(product.getSlot());
	}

	public void exitDispenseMode()
	{
		Log.d(TAG, "exitDispenseMode");
		sendStop(ProductSlot.SLOT1);
		sendStop(ProductSlot.SLOT2);
		sendStop(ProductSlot.SLOT3);
		sendStop(ProductSlot.SLOT4);
		dispenseWaterNozzleEnd();

		stopAllSalesTimers();
	}

	public void calibrationWater(ProductSlot slot, NestleProduct product)
	{
		Log.d(TAG, "calibrationDispenseWater "+slot.toString());
		EventDispense dispense=new EventDispense();

		dispense.setChannel(getChannel(slot));
		dispense.setMode(EventDispense.Mode.MODE_CALIBRATE_WATER);

		dispense.setSpeedFill(product.getSpeedFill());
		dispense.setSpeedPump(product.getSpeedPump());
		dispense.setStepsStart(product.getStepsStart());
		dispense.setStepsPump(product.getStepsPump());

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void calibrationConcentrate(ProductSlot slot, NestleProduct product)
	{
		Log.d(TAG, "calibrationConcentrate "+slot.toString());
		EventDispense dispense=new EventDispense();

		dispense.setChannel(getChannel(slot));
		dispense.setMode(EventDispense.Mode.MODE_CALIBRATE_CONCENTRATE);

		dispense.setSpeedFill(product.getSpeedFill());
		dispense.setSpeedPump(product.getSpeedPump());
		dispense.setStepsStart(product.getStepsStart());
		dispense.setStepsPump(product.getStepsPump());

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void calibrationBrix(ProductSlot slot, NestleProduct product)
	{
		Log.d(TAG, "calibrationBrix "+slot.toString());
		EventDispense dispense=new EventDispense();

		dispense.setChannel(getChannel(slot));
		dispense.setMode(EventDispense.Mode.MODE_NORMAL);

		dispense.setSpeedFill(product.getSpeedFill());
		dispense.setSpeedPump(product.getSpeedPump());
		dispense.setStepsStart(product.getStepsStart());
		dispense.setStepsPump(product.getStepsPump());

		mProtocol.transmit(dispense, new EventDispenseResponse());
	}

	public void calibrationStop(ProductSlot slot)
	{
		Log.d(TAG, "calibrationStop");
		sendStop(slot);
	}

	public EventStatusResponse getStatus()
	{
		return mStatus;
	}

	public void startIO(EventIORequest request)
	{
		mProtocol.transmit(request, new EventIOResponse());
		Log.d(TAG, "StartIO");
	}

	public void stopIO()
	{
		EventIORequest request=new EventIORequest();
		request.inTestMode=false;
		mProtocol.transmit(request, new EventIOResponse());
		Log.d(TAG, "StopIO");
	}

	public void setOnFaultCallback(Runnable callback)
	{
		mFaults.setOnFaultCallback(callback);
	}

	public FaultMatrix getFaultMatrix()
	{
		return mFaults;
	}

	public String getVersion(){
        String version = Arrays.toString(mDbVersion).replace(", ", ".");
        return version.substring(1, version.length()-1);
    }

    public boolean getDispenseLockout()
	{
		mDispenseLockout=mMachineConfigDb.getLockout();
		return mMachineConfigDb.getLockout();
	}

	public void setDispenseLockout(boolean value)
	{
		mDispenseLockout=value;
		mMachineConfigDb.setLockout(value);
	}

	public void save()
	{
		mMachineConfigDb.save();
	}

	public void close()
	{
		save();
	}

	public void setWater1Setpoint(double temperature)
	{
		mMachineConfigDb.setWater1(temperature);
	}

	public void setWater2Setpoint(double temperature)
	{
		mMachineConfigDb.setWater2(temperature);
	}

	public void setConcentrateSetpoint(double temperature)
	{
		mMachineConfigDb.setConcentrate(temperature);
	}

	public double getWater1Setpoint()
	{
		return mMachineConfigDb.getWater1();
	}

	public double getWater2Setpoint()
	{
		return mMachineConfigDb.getWater2();
	}

	public double getConcentrateSetpoint()
	{
		return mMachineConfigDb.getConcentrate();
	}

	public void updateDriverSetpoints()
	{
		EventSetParameters setpoints=new EventSetParameters();
		setpoints.water1=getWater1Setpoint();
		setpoints.water2=getWater2Setpoint();
		setpoints.concentrate=getConcentrateSetpoint();

		mProtocol.transmit(setpoints, new EventSetParametersResponse());
	}

	public enum ProductSize
	{
		SMALL,
		MEDIUM,
		LARGE,
		CONCENTRATE,
		DAILY,
		ALLERGEN
	}

	class PresizedDispenseStop implements Runnable
	{
		Runnable mOnFinishCallback;
		ProductSize mSize;
		ProductSlot mSlot;
		public PresizedDispenseStop(ProductSlot slot, ProductSize size, Runnable onFinishCallback)
		{
			mSlot=slot;
			mSize=size;
			mOnFinishCallback=onFinishCallback;
		}

		@Override
		public void run()
		{
			Log.d(TAG, "PresizedDispenseStop "+mSlot.toString()+" "+mSize.toString());
			stopSalesTimer(mSlot.ordinal());
			sendStop(mSlot);

			if (mOnFinishCallback!=null)
				mOnFinishCallback.run();
		}
	}

	PresizedDispenseStop mPresizedStop[]=new PresizedDispenseStop[4];

	public boolean getAndClearReceivedCommunications()
	{
		boolean status= mGotStatusPacket;
		mGotStatusPacket =false;

		return status;
	}

	public TemperatureHistory getTemperatureHistory()
	{
		return mTemperatureHistory;
	}

	FaultMatrix.OnNewFaultCallback mOnNewFault = new FaultMatrix.OnNewFaultCallback()
	{
		@Override
		public void run(boolean[] newFaults)
		{
			mDatabase.truncateFaults();

			for(int i=0; i<newFaults.length; i++)
			{
				if (newFaults[i])
				{
					Log.d(TAG, "New fault "+i);
					DatabaseFault fault=new DatabaseFault();
					fault.fault_code=i;
					fault.timestamp=System.currentTimeMillis();
					mDatabase.addFault(fault);
				}
			}
		}
	};

	FaultMatrix.OnNewWarningCallback mOnNewWarning = new FaultMatrix.OnNewWarningCallback()
    {
        @Override
        public void run(boolean[] newWarnings)
        {
            mDatabase.truncateFaults();

            for(int i=0; i<newWarnings.length; i++)
            {
                if (newWarnings[i])
                {
                    Log.d(TAG, "New warning "+i);
                    DatabaseFault fault=new DatabaseFault();
                    fault.fault_code=i;
                    fault.timestamp=System.currentTimeMillis();
                    mDatabase.addFault(fault);
                }
            }
        }
    };

	Runnable mStatusTimeout = new Runnable()
	{
		@Override
		public void run()
		{
			mStatus=null;

			mProtocol.reset();
		}
	};

	void startSalesTimer(int channel)
	{
		Log.d("SALES", "Start "+channel);
		mTimers[channel].start();
	}

	void stopSalesTimer(int channel)
	{
		if (mTimers[channel].stop())
		{
			mSales.addSale(channel, (int) mTimers[channel].getTimeElapsed());
		}

		Log.d("SALES", "Stop "+channel+" ("+mTimers[channel].getTimeElapsed()+")");
	}

	void stopAllSalesTimers()
	{
		for(int i=0; i<4; i++)
			stopSalesTimer(i);
	}

	final long statusClearFaultUpdateTime=1000;
	long statusReceiveEnableTime=0;
	public void clearFaults()
	{
		mProtocol.transmit(new EventClearFaults(), new EventClearFaultsResponse());

		statusReceiveEnableTime=System.currentTimeMillis()+statusClearFaultUpdateTime;
		mFaults.clearActiveFaults();

		mProductManager.setProductSlotStatus(ProductSlot.SLOT1, NestleProduct.ProductStatus.DISPENSE);
		mProductManager.setProductSlotStatus(ProductSlot.SLOT2, NestleProduct.ProductStatus.DISPENSE);
		mProductManager.setProductSlotStatus(ProductSlot.SLOT3, NestleProduct.ProductStatus.DISPENSE);
		mProductManager.setProductSlotStatus(ProductSlot.SLOT4, NestleProduct.ProductStatus.DISPENSE);
	}
}
