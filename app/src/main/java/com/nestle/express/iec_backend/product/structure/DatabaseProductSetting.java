package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseProductSetting extends DatabaseRowParent
{
	public int id;
	public double speedFill, speedPump, postRinseVolume;
	public int stepsStart, stepsPump, mode, buttonSetup,
			timeDispenseSmall, timeDispenseMedium, timeDispenseLarge,
			timeDispenseConcentrate;

	public static DatabaseProductSetting createFromCursor(Cursor cursor)
	{
		DatabaseProductSetting row=new DatabaseProductSetting();

		row.id=getIntByName(cursor, "product_id");
		row.speedFill=getDoubleByName(cursor, "speed_fill");
		row.speedPump=getDoubleByName(cursor, "speed_pump");
		row.stepsStart=getIntByName(cursor, "steps_start");
		row.stepsPump=getIntByName(cursor, "steps_pump");
		row.timeDispenseSmall=getIntByName(cursor, "dispense_small");
		row.timeDispenseMedium=getIntByName(cursor, "dispense_medium");
		row.timeDispenseLarge=getIntByName(cursor, "dispense_large");
		row.timeDispenseConcentrate=getIntByName(cursor, "dispense_concentrate");
		row.postRinseVolume=getDoubleByName(cursor, "post_rinse_volume");
		row.buttonSetup=getIntByName(cursor, "button_setup");
		row.mode=getIntByName(cursor, "mode");

		return row;
	}

	public static ArrayList<DatabaseProductSetting> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
			return null;

		ArrayList<DatabaseProductSetting> settings=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			settings.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return settings;
	}
}
