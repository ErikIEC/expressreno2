package com.nestle.express.iec_backend.product.structure;

/**
 * Created by jason on 7/12/16.
 *
 */

//Four slots numbered from left to right
public enum ProductRow
{
	ROW1,
	ROW2,
	ROW3,
	ROW4
}
