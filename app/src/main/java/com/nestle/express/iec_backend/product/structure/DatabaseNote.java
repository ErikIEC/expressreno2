package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

import java.util.ArrayList;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseNote extends DatabaseRowParent
{
	public int timestamp;
	public String note;


	public static DatabaseNote createFromCursor(Cursor cursor)
	{
		DatabaseNote note=new DatabaseNote();

		note.timestamp=getIntByName(cursor, "timestamp");
		note.note=getStringByName(cursor, "note");

		return note;
	}

	public static ArrayList<DatabaseNote> createArrayFromCursor(Cursor cursor)
	{
		if (!cursor.moveToFirst())
		{
			cursor.close();
			return null;
		}
		ArrayList<DatabaseNote> sales=new ArrayList<>();

		while (!cursor.isAfterLast())
		{
			sales.add(createFromCursor(cursor));
			cursor.moveToNext();
		}

		cursor.close();

		return sales;
	}
}
