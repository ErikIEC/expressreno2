package com.nestle.express.iec_backend.database;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.R;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;
import com.nestle.express.iec_backend.product.structure.DatabaseProduct;
import com.nestle.express.iec_backend.product.structure.ProductArt;
import com.nestle.express.iec_backend.product.structure.NestleProduct;

import java.io.File;

/**
 * Created by jason on 7/7/16.
 * <p>
 * Modified by jesse on 11/1/16
 * <p>
 * See documentation in databases.odt
 * <p>
 * The purpose of this class is to load art assets for the application.
 * Static and button art is non-translatable material. Only products
 * will have a unique set of images for each product.
 * <p>
 * Base Art Directory: /mnt/media_rw/extsd/v1/art
 * Static Art: /static/0.png (1.png, 2.png ...)
 * Button Art: /button/1.png (1-pressed, 1-disabled, 2.png ...)
 * Products Art: /products
 */

public class ArtDatabase{
	final String TAG="ArtDatabase";
    private String mLanguageDefault;
	String mRootDirectory;
    String mBaseDirectory;

    final String mProductsDirectory = "/products";
    final String mThumbnailsDirectory = "/thumbnails";

    public ArtDatabase(String directory, String languageDefault) {
		mRootDirectory = directory;
		mBaseDirectory = directory + "/art";
        mLanguageDefault = languageDefault;
    }

    public ProductArt getProductArt(NestleProduct product, String languageSelected) {
        if (product == null)
            return null;

        return new ProductArt(product, mBaseDirectory + mProductsDirectory, mLanguageDefault, languageSelected);
    }

	String getThumbnailPath(DatabaseLanguage language, int id)
	{
		return mBaseDirectory+mThumbnailsDirectory+"/"+language.getIsoCode()+"/"+id+".png";
	}

    public Drawable getThumbnail(ExpressApp app, Resources resources, NestleProduct product)
    {
		DatabaseLanguage language=app.getSelectedLanguage();

		if (product==null)
			return resources.getDrawable(R.drawable.no_thumbnail);
		if (product.getId()<0)
			return resources.getDrawable(R.drawable.no_thumbnail);

        Drawable draw=Drawable.createFromPath(getThumbnailPath(language, product.getId()));

        if (draw==null)
            return resources.getDrawable(R.drawable.no_thumbnail);

        return draw;
    }

    public void checkArt(DatabaseLanguage language, int[] ids) throws Exception
    {
		String errorMessage="";

        for(int id : ids)
        {
			//Check product art
			DatabaseProduct dbProduct=new DatabaseProduct();
			dbProduct.product_id=id;
			NestleProduct product=new NestleProduct(dbProduct);
			ProductArt art=new ProductArt(product, mBaseDirectory + mProductsDirectory, mLanguageDefault, mLanguageDefault);

			errorMessage+=checkExists(art.getFruitPath());
			errorMessage+=checkExists(art.getGlassPath());
			errorMessage+=checkExists(art.getTitlePath());
			errorMessage+=checkExists(art.getCircularBGPath());
			errorMessage+=checkExists(art.getSubtitlePath());
			errorMessage+=checkExists(art.getSubtitleAlignedPath());
			errorMessage+=checkExists(art.getNutritionalInfoPath());
			errorMessage+=checkExists(art.getNutritionalInfoButtonPath());
        }

		for(int id : ids)
		{
			//Check thumbnails
			String path=getThumbnailPath(language, id);
			//Log.d(TAG, path);
			errorMessage+=checkExists(path);
		}

		if (errorMessage.length()>0)
		{
			errorMessage+="Root Directory: "+mRootDirectory;
			throw new Exception(errorMessage);
		}
    }

    String checkExists(String path)
    {
        File file=new File(path);

        if (!file.exists())
			return "Missing Art: "+path.replace(mRootDirectory, "")+"\n";
		return "";
    }
}
