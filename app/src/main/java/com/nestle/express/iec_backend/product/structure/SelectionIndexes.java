package com.nestle.express.iec_backend.product.structure;

import android.util.Log;

/**
 * Created by jason on 7/12/16.
 *
 */
public class SelectionIndexes
{
	final static String TAG="SelectionIndexes";

	int[] mIndexes;

	public SelectionIndexes()
	{
		mIndexes=new int[4];

		for(int i=0; i<4; i++)
			mIndexes[i]=-1;
	}

	public SelectionIndexes(int[] elements)
	{
		mIndexes=new int[4];

		for(int i=0; i<4; i++)
			mIndexes[i]=elements[i];
	}

	public boolean isOutOfBounds(int i)
	{
		if (i<0 || i>3)
			return true;
		return false;
	}

	public int getElement(ProductSlot slot)
	{
		int i=slot.ordinal();

		return mIndexes[i];
	}

	public ProductRow getElementSlot(ProductSlot slot)
	{
		int i=slot.ordinal();

		int index=mIndexes[i];

		if (index==3)	return ProductRow.ROW4;
		if (index==2)	return ProductRow.ROW3;
		if (index==1)	return ProductRow.ROW2;
		return ProductRow.ROW1;
	}

	public int getElement(int i)
	{
		if (isOutOfBounds(i))
		{
			Log.e(TAG, "getElement Out of Bounds");
			return -1;
		}

		return mIndexes[i];
	}

	public void setElement(int slot, int value)
	{
		if (slot<0 || slot>3)
		{
			Log.e(TAG, "setElement Out of Bounds");
			return ;
		}

		mIndexes[slot]=value;
	}

	public void setElement(ProductSlot slot, int value)
	{
		int i=slot.ordinal();

		mIndexes[i]=value;
	}

	public void setElement(ProductSlot slot, ProductRow value)
	{
		int i=slot.ordinal();

		mIndexes[i]=value.ordinal();
	}

	public void getElement(int index, int value)
	{
		if (isOutOfBounds(index))
		{
			Log.e(TAG, "setElement Out of Bounds");
			return;
		}

		mIndexes[index]=value;
	}

	public void getElement(int index, ProductRow value)
	{
		if (isOutOfBounds(index))
		{
			Log.e(TAG, "setElement Out of Bounds");
			return;
		}

		mIndexes[index]=value.ordinal();
	}

	public int[] getElements()
	{
		return mIndexes;
	}
}
