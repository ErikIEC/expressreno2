package com.nestle.express.iec_backend.machine;

/**
 * Created by jason on 3/29/17.
 */
public class TimerUtil
{
	boolean running;
	long startTime, endTime;

	public void start()
	{
		if (!running)
			startTime=System.currentTimeMillis();
		running=true;
	}

	public boolean stop()
	{
		boolean temp=running;
		if (running)
			endTime=System.currentTimeMillis();

		running=false;
		return temp;
	}

	public long getTimeElapsed()
	{
		if (startTime>endTime)
			return 0;

		return endTime-startTime;
	}
}
