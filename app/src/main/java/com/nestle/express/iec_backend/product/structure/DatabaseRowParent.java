package com.nestle.express.iec_backend.product.structure;

import android.database.Cursor;

/**
 * Created by jason on 8/3/16.
 */
public class DatabaseRowParent
{
	public static int getIntByName(Cursor cursor, String name)
	{
		return cursor.getInt(cursor.getColumnIndex(name));
	}

	public static long getLongByName(Cursor cursor, String name)
	{
		return cursor.getLong(cursor.getColumnIndex(name));
	}

	public static String getStringByName(Cursor cursor, String name)
	{
		return cursor.getString(cursor.getColumnIndex(name));
	}

	public static double getDoubleByName(Cursor cursor, String name)
	{
		return cursor.getDouble(cursor.getColumnIndex(name));
	}

	public static byte[] getBlobByName(Cursor cursor, String name)
	{
		return cursor.getBlob(cursor.getColumnIndex(name));
	}
}
