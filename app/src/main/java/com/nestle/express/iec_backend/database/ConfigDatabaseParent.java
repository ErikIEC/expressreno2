package com.nestle.express.iec_backend.database;

/**
 * Created by jason on 7/7/16.
 *
 * This class is the parent of:
 * 	- ApplicationConfigDatabase
 * 	- MachineConfigDatabase
 * 	- ProductConfigDatabase
 *
 * It includes common initialization and data validation functions.
 *  - ie. load, save, clear, limitValue
 *
 */
public class ConfigDatabaseParent
{
	String mComment="Deafault Configuration - Not Initialized";
	String mPath="./default.config";
	ConfigDatabaseDriver mDriver=null;

	public void setComment(String value)
	{
		mComment=value;
	}

	public void setPath(String directory, String filename)
	{
		mPath=directory+"/"+filename;
	}

	public void initialize(String directory, String filename, String comment)
	{
		setPath(directory, filename);
		setComment(comment);
		mDriver=new ConfigDatabaseDriver();
		mDriver.initializeDatabase(mPath, mComment);
	}

	public void load()
	{
		mDriver.load();
	}

	public void save()
	{
		mDriver.save();
	}

	public void clear()
	{
		mDriver.clear();
	}

	public double limitValue(double value, double lower, double upper)
	{
		if (value<lower)
			return lower;

		if (value>upper)
			return upper;

		return value;
	}

	public int limitValue(int value, int lower, int upper)
	{
		if (value<lower)
			return lower;

		if (value>upper)
			return upper;

		return value;
	}

	public int[] limitValue(int[] indexes, int lower, int upper)
	{
		int[] newIndexes=indexes.clone();

		for(int i=0; i<indexes.length; i++)
		{
			if (newIndexes[i]<lower)
				newIndexes[i]=lower;

			if (newIndexes[i]>upper)
				newIndexes[i]=upper;
		}

		return newIndexes;
	}

	public String getSetting(String key, String defaultValue)
	{
		return mDriver.readString(key, defaultValue);
	}

	public double getSetting(String key, double defaultValue, double lowerValue, double upperValue)
	{
		double value=mDriver.readDouble(key, defaultValue);

		return limitValue(value, lowerValue, upperValue);
	}

	public double getSetting(String key, double defaultValue)
	{
		return mDriver.readDouble(key, defaultValue);
	}

	public int getSetting(String key, int defaultValue, int lowerValue, int upperValue)
	{
		int value=mDriver.readInt(key, defaultValue);

		return limitValue(value, lowerValue, upperValue);
	}

	public int getSetting(String key, int defaultValue)
	{
		return mDriver.readInt(key, defaultValue);
	}

	public int[] getSetting(String key, int defaultValue, int size, int lowerValue, int upperValue)
	{
		int[] array=mDriver.readIntArray(key, defaultValue, size);

		return limitValue(array, lowerValue, upperValue);
	}

	public int[] getSetting(String key, int defaultValue, int size)
	{
		return mDriver.readIntArray(key, defaultValue, size);
	}

	public void setSetting(String key, String inputValue)
	{
		mDriver.writeString(key, inputValue);
	}

	public void setSetting(String key, double inputValue, double lowerValue, double upperValue)
	{
		double value=limitValue(inputValue, lowerValue, upperValue);

		mDriver.writeDouble(key, value);
	}

	public void setSetting(String key, double inputValue)
	{
		mDriver.writeDouble(key, inputValue);
	}

	public void setSetting(String key, int inputValue, int lowerValue, int upperValue)
	{
		int value=limitValue(inputValue, lowerValue, upperValue);

		mDriver.writeInt(key, value);
	}

	public void setSetting(String key, int inputValue)
	{
		mDriver.writeInt(key, inputValue);
	}

	public void setSetting(String key, int[] inputValue, int lowerValue, int upperValue)
	{
		int[] array=limitValue(inputValue, lowerValue, upperValue);

		mDriver.writeIntArray(key, array);

	}

	public void setSetting(String key, int[] inputValue)
	{
		mDriver.writeIntArray(key, inputValue);
	}
}
