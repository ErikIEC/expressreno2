package com.nestle.express.iec_backend.product.structure;

import android.util.Log;

import java.io.File;

/**
 * Created by jason on 7/28/16.
 * <p>
 * Modified by jesse on 11/1/16
 */
public class ProductArt {

    private final String DEFAULT_PATH = "/default";

    private int mProductId;
    private String mGlassPath;
    private String mFruitPath;
    private String mTitlePath;
    private String mSubtitlePath;
    private String mSubtitleAlignedPath;
    private String mNutritionalInfoPath;
    private String mNutritionalInfoButtonPath;
    private String mCircularBGPath;

    private String mGlassLanguageDefaultPath;
    private String mFruitLanguageDefaultPath;
    private String mTitleLanguageDefaultPath;
    private String mSubtitleLanguageDefaultPath;
    private String mSubtitleAlignedLanguageDefaultPath;
    private String mNutritionalInfoLanguageDefaultPath;
    private String mNutritionalInfoButtonLanguageDefaultPath;
    private String mCircularBGLanguageDefaultPath;

    private String mGlassDefaultPath;
    private String mFruitDefaultPath;
    private String mTitleDefaultPath;
    private String mSubtitleDefaultPath;
    private String mSubtitleAlignedDefaultPath;
    private String mNutritionalInfoDefaultPath;
    private String mNutritionalInfoButtonDefaultPath;
    private String mCircularBGDefaultPath;

    private int mBackground;
    public String pathDispenseProductLogo;
    private final String mGlassFileName = "glass.png";
    private final String mFruitFileName = "fruit.png";
    private final String mTitleFileName = "title.png";
    private final String mSubtitleFileName = "subtitle.png";
    private final String mSubtitleAlignedFileName = "subtitle-aligned.png";
    private final String mNutritionalInfoFileName = "nutritional_info.png";
    private final String mNutritionalInfoButtonFileName = "nutritional_info_button.png";
    private final String mCircularBGFileName = "circle_bg.png";

    public ProductArt(NestleProduct product, String productsPath, String languageDefault, String languageSelected) {

        mProductId = product.getId(); //TODO Or getArtID ?
        mGlassPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mGlassFileName;
        mFruitPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mFruitFileName;
        mTitlePath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mTitleFileName;
        mSubtitlePath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mSubtitleFileName;
        mSubtitleAlignedPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mSubtitleAlignedFileName;
        mNutritionalInfoPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mNutritionalInfoFileName;
        mNutritionalInfoButtonPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mNutritionalInfoButtonFileName;
        mCircularBGPath = productsPath + "/" + mProductId + "/" + languageSelected + "/" + mCircularBGFileName;

        mGlassLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mGlassFileName;
        mFruitLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mFruitFileName;
        mTitleLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mTitleFileName;
        mSubtitleLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mSubtitleFileName;
        mSubtitleAlignedLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mSubtitleAlignedFileName;
        mNutritionalInfoLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mNutritionalInfoFileName;
        mNutritionalInfoButtonLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mNutritionalInfoButtonFileName;
        mCircularBGLanguageDefaultPath = productsPath + "/" + mProductId + "/" + languageDefault + "/" + mCircularBGFileName;

        mGlassDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mGlassFileName;
        mFruitDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mFruitFileName;
        mTitleDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mTitleFileName;
        mSubtitleDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mSubtitleFileName;
        mSubtitleAlignedDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mSubtitleAlignedFileName;
        mNutritionalInfoDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mNutritionalInfoFileName;
        mNutritionalInfoButtonDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mNutritionalInfoButtonFileName;
        mCircularBGDefaultPath = productsPath + "/" + mProductId + DEFAULT_PATH + "/" + mCircularBGFileName;

        mBackground = product.getProduct().color;
    }


    public int getBackground() {
        return mBackground;
    }

    public String getGlassPath() {
        File file = new File(mGlassPath);
        if (file.exists()) {
            return mGlassPath;
        } else {
            File fileLanguageDefault = new File(mGlassLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mGlassLanguageDefaultPath;
            } else {
                return mGlassDefaultPath;
            }
        }
    }

    public String getFruitPath() {
        File file = new File(mFruitPath);
        if (file.exists()) {
            return mFruitPath;
        } else {
            File fileLanguageDefault = new File(mFruitLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mFruitLanguageDefaultPath;
            } else {
                return mFruitDefaultPath;
            }
        }
    }

    public String getTitlePath() {
        File file = new File(mTitlePath);
        if (file.exists()) {
            return mTitlePath;
        } else {
            File fileLanguageDefault = new File(mTitleLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mTitleLanguageDefaultPath;
            } else {
                return mTitleDefaultPath;
            }
        }
    }

    public String getSubtitlePath() {
        File file = new File(mSubtitlePath);
        if (file.exists()) {
            return mSubtitlePath;
        } else {
            File fileLanguageDefault = new File(mSubtitleLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mSubtitleLanguageDefaultPath;
            } else {
                return mSubtitleDefaultPath;
            }
        }
    }

    public String getSubtitleAlignedPath() {
        File file = new File(mSubtitleAlignedPath);
        if (file.exists()) {
            return mSubtitleAlignedPath;
        } else {
            File fileLanguageDefault = new File(mSubtitleAlignedLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mSubtitleAlignedLanguageDefaultPath;
            } else {
                return mSubtitleAlignedDefaultPath;
            }
        }
    }

    public String getNutritionalInfoPath() {
        File file = new File(mNutritionalInfoPath);
        if (file.exists()) {
            return mNutritionalInfoPath;
        } else {
            File fileLanguageDefault = new File(mNutritionalInfoLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mNutritionalInfoLanguageDefaultPath;
            } else {
                return mNutritionalInfoDefaultPath;
            }
        }
    }

    public String getNutritionalInfoButtonPath() {
        File file = new File(mNutritionalInfoButtonPath);
        if (file.exists()) {
            return mNutritionalInfoButtonPath;
        } else {
            File fileLanguageDefault = new File(mNutritionalInfoButtonLanguageDefaultPath);
            if (fileLanguageDefault.exists()) {
                return mNutritionalInfoButtonLanguageDefaultPath;
            } else {
                return mNutritionalInfoButtonDefaultPath;
            }
        }
    }

    public String getCircularBGPath() {
        File file = new File(mCircularBGFileName);
        if(file.exists()){
            return mCircularBGPath;
        }else{
            File fileLanguageDefault = new File(mCircularBGLanguageDefaultPath);
            if(fileLanguageDefault.exists()){
                return mCircularBGLanguageDefaultPath;
            }else{
                return mCircularBGDefaultPath;
            }
        }
    }
}