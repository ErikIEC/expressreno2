package com.nestle.express.iec_backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jason on 12/28/16.
 */
public class SDCardUtilities
{
	public static final String SD_CHECK_COMMAND="df"; // | grep extsd

	static String runCommand(String command)
	{
		String text="";
		try {
			Process process = Runtime.getRuntime().exec(command);
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));

			String line;
			while ((line= bufferedReader.readLine()) != null)
			{
				text+=line+"\n";
			}

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return text;
	}

	static boolean isSDCardPresent()
	{
		if (runCommand(SD_CHECK_COMMAND).contains("extsd"))
			return true;
		return false;
	}

	public static void isUSBPresent(ExpressApp app) throws Exception {
		String usb=app.getUSBDirectory();
		File file=new File(usb);
		if (!file.exists())
			throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");
	}

	public static void upgradeProductDatabase(ExpressApp app) throws Exception
	{
		String usb=app.getUSBDirectory();
		String inputDirectory=usb+"/import";
		String inputFile=inputDirectory+"/database_v1.db";

		File file=new File(usb);
		if (!file.exists())
			throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");

		file=new File(inputDirectory);
		if (!file.exists())
			throw new Exception("Directory ./import not detected");

		file=new File(inputFile);
		if (!file.exists())
			throw new Exception("File ./import/database_v1.db not found");

		app.getDatabase().updateProducts(inputFile);
		app.getProductManager().updateValidIDs();
	}

	public static void importSettingsAndProductDatabase(ExpressApp app) throws Exception
	{
		String usb=app.getUSBDirectory();
		String inputDirectory=usb+"/import";
		String outputDirectory=app.getSDCardDirectory();

		File file=new File(usb);
		if (!file.exists())
			throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");

		file=new File(inputDirectory);
		if (!file.exists())
			throw new Exception("Directory ./import not detected");

		copyTo(inputDirectory, outputDirectory, "application.config");
		copyTo(inputDirectory, outputDirectory, "machine.config");
		copyTo(inputDirectory, outputDirectory, "product.config");

		app.closeDatabase();
		copyTo(inputDirectory, outputDirectory, "database_v1.db");
		app.openDatabase();
		app.getProductManager().updateValidIDs();
	}

	public static void exportSettingsAndProductDatabase(ExpressApp app) throws Exception
	{
		String inputDirectory=app.getSDCardDirectory();
		String outputDirectory=app.getUSBDirectory()+"/"+formatTimestampName("export");
        String outputDirectory2=app.getUSBDirectory()+"/import";

		File folder=new File(outputDirectory);
		if (!folder.mkdir())
			throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");
        File folder2=new File(outputDirectory2);
        if (!folder2.mkdir())
            throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");

		copyTo(inputDirectory, outputDirectory, "application.config");
		copyTo(inputDirectory, outputDirectory, "machine.config");
		copyTo(inputDirectory, outputDirectory, "product.config");
		copyTo(inputDirectory, outputDirectory, "database_v1.db");
        copyTo(inputDirectory, outputDirectory2, "application.config");
        copyTo(inputDirectory, outputDirectory2, "machine.config");
        copyTo(inputDirectory, outputDirectory2, "product.config");
        copyTo(inputDirectory, outputDirectory2, "database_v1.db");
    }

	public static void exportSalesDatabase(ExpressApp app, ArrayList<String> sales) throws Exception
	{
		String outputDirectory=app.getUSBDirectory()+"/"+formatDatestampName("export");
		File folder=new File(outputDirectory);
		if (!folder.exists()) {
            if (!folder.mkdir())
                throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");
        }

		try {
			FileWriter writer = new FileWriter(outputDirectory+"/"+formatTimestampName("sales")+".csv");
            String header = "date, id, finished volume, concentrate volume\n";
            writer.write(header);
			for(String str: sales) {
				writer.write(str);
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			throw new Exception ("Could not create File");
		}
	}

	public static void importTutorialPack(ExpressApp app) throws Exception
	{
		String usb=app.getUSBDirectory();
		String inputDirectory=usb+"/import/html";
		String outputDirectory=app.getSDCardDirectory()+"/html";

		File file=new File(usb);
		if (!file.exists())
			throw new Exception("USB Drive Not Detected. Please Insert USB Drive.");

		file=new File(inputDirectory);
		if (!file.exists())
			throw new Exception("Directory ./import/html not detected");;

		copyTo(inputDirectory, outputDirectory, "");
	}

	public static void exportHistory(ExpressApp app) throws Exception
	{

	}

	public static void copyTo(String source, String destination, String fileName) throws Exception
	{
		File file=new File(source, fileName);
		if (!file.exists())
			throw new Exception("Source file "+file.getPath()+" does not exist.");

		copyFolder(source+"/"+fileName, destination+"/"+fileName);
	}

	public static void copyFolder(String sourcePath, String destinationPath) throws IOException
	{
		File sourceFolder=new File(sourcePath);
		File destinationFolder=new File(destinationPath);

		if (sourceFolder.isDirectory())
		{
			if (!destinationFolder.exists())
				destinationFolder.mkdir();

			String fileList[] = sourceFolder.list();
			for (String file : fileList)
			{
				File sourceFile = new File(sourceFolder, file);
				File destinationFile = new File(destinationFolder, file);
				copyFolder(sourceFile.getPath(), destinationFile.getPath());
			}
		}
		else
			copyFile(sourcePath, destinationPath);
	}

	public static void copyFile(String sourcePath, String destinationPath) throws IOException
	{
		File source=new File(sourcePath);
		File destination=new File(destinationPath);

		InputStream input = null;
		OutputStream output = null;
		try
		{
			input = new FileInputStream(source);
			output = new FileOutputStream(destination);
			byte[] buffer = new byte[1024];
			int numBytesRead;
			while ((numBytesRead = input.read(buffer)) > 0)
			{
				output.write(buffer, 0, numBytesRead);
			}
		}
		finally
		{
			if (input!=null)
				input.close();
			if (output!=null)
				output.close();
		}
	}

	public static String formatTimestampName(String base)
	{
		Calendar mydate = Calendar.getInstance();
		mydate.setTimeInMillis(System.currentTimeMillis());
		//String dateStamp = mydate.get(Calendar.MONTH)+"-"+mydate.get(Calendar.DAY_OF_MONTH)+"-"+mydate.get(Calendar.YEAR);
		int time = mydate.get(Calendar.HOUR)+mydate.get(Calendar.MINUTE)+mydate.get(Calendar.SECOND);
		return base+"_"+Integer.toString(time);
	}

    public static String formatDatestampName(String base)
    {
        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(System.currentTimeMillis());
        String dateStamp = mydate.get(Calendar.MONTH)+"-"+mydate.get(Calendar.DAY_OF_MONTH)+"-"+mydate.get(Calendar.YEAR);
        return base+"_"+dateStamp;
    }
}
