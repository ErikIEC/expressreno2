package com.nestle.express.iec_backend.database.structure;

import android.graphics.drawable.Drawable;

/**
 * Created by jason on 8/12/16.
 */
public class ButtonArt
{
	public Drawable normal, pressed, disabled;
}
