package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.nestle.express.frontend_setup.R;

import java.text.DecimalFormat;

/**
 * Created by peng on 16/11/30.
 */

public class StepperView extends FrameLayout {
    protected double mSettingMax = 0;
    protected double mSettingMin = 0;
    protected double mSettingValue = 0;
    protected double mIncrementValue = 1;
    protected TextView mText;
    protected ImageView mSubBtn;
    protected ImageView mAddBtn;
    boolean mFirstIncrement=true;


    public StepperView(Context context) {
        super(context);
    }

    public StepperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit(context, attrs);
    }

    public StepperView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit(context, attrs);
    }

    public void commonInit(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_stepper, this, true);
        mText = (TextView) view.findViewById(R.id.stepper_text_tv);
        mSubBtn = (ImageView) view.findViewById(R.id.stepper_sub_iv);
        mAddBtn = (ImageView) view.findViewById(R.id.stepper_add_iv);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.StepperView);
        mSettingMin = ta.getFloat(R.styleable.StepperView_minValue, 0);
        mSettingMax = ta.getFloat(R.styleable.StepperView_maxValue, 0);
        mIncrementValue = ta.getFloat(R.styleable.StepperView_increment, 0);
        ta.recycle();
        mSubBtn.setOnTouchListener(mDecrement);
        mAddBtn.setOnTouchListener(mIncrement);
        updateDisplayText();
    }

    public void setValue(double value)
    {
        mSettingValue=value;
        updateDisplayText();
    }

    public double getValue()
    {
        limitSetting();
        return mSettingValue;
    }

    public void setMax(double max)
    {
        mSettingMax = max;
    }

    public void setMin(double min) {
        mSettingMin = min;
    }

    public void setIncrement(double inc) {
        mIncrementValue = inc;
    }

    OnTouchListener mIncrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch(motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    mHandler.post(mRunnableIncrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mHandler.removeCallbacks(mRunnableIncrement);
                    resetIncrement();
            }
            return true;
        }
    };

    OnTouchListener mDecrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {

            switch (motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    mHandler.post(mRunnableDecrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mHandler.removeCallbacks(mRunnableDecrement);
                    resetDecrement();
            }
            return true;
        }
    };

    private Runnable mRunnableIncrement = new Runnable()
    {
        @Override
        public void run()
        {
            incrementSetting();
            if (mFirstIncrement)
            {
                mFirstIncrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetIncrement()
    {
        mFirstIncrement =true;
    }
    boolean mFirstDecrement =true;
    private Runnable mRunnableDecrement = new Runnable()
    {
        @Override
        public void run()
        {
            decrementSetting();
            if (mFirstDecrement)
            {
                mFirstDecrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetDecrement()
    {
        mFirstDecrement =true;
    }

    public void incrementSetting()
    {
        mSettingValue += mIncrementValue;
        updateDisplayText();
    }
    public void decrementSetting()
    {
        mSettingValue -= mIncrementValue;
        updateDisplayText();
    }

    void limitSetting()
    {
        if (mSettingValue > mSettingMax)
            mSettingValue = mSettingMax;

        if (mSettingValue < mSettingMin)
            mSettingValue = mSettingMin;
    }

    void updateDisplayText()
    {
        limitSetting();
        DecimalFormat format = new DecimalFormat("0.0");

		if (mText.isEnabled())
        	mText.setText(format.format(mSettingValue));
    }

    Handler mHandler = new Handler();

	@Override
    public void setEnabled(boolean value)
	{
		if (value==false)
			mText.setText("0.0");

		mText.setEnabled(value);
		mSubBtn.setEnabled(value);
		mAddBtn.setEnabled(value);
	}
}