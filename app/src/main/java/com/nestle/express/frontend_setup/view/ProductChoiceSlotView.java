package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;
import com.nestle.express.iec_backend.product.structure.ProductRow;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.iec_backend.product.structure.SelectionIndexes;
import com.nestle.express.iec_backend.product.structure.SelectionMatrix;

import java.util.List;

/**
 * Created by jason on 12/22/16.
 */
public class ProductChoiceSlotView extends FrameLayout
{
	final String TAG="ProductSelectionView";

	ProductManager mProductManager;
	ProductSlot mSlot;
	SelectionMatrix mSelectionMatrix;

	TextView slotNumberText;
	Spinner spinner1, spinner2, spinner3, spinner4;

	public ProductChoiceSlotView(Context context) {
		super(context);
	}

	public ProductChoiceSlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public ProductChoiceSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	public void commonInit(Context context, AttributeSet attrs) {
		View view = LayoutInflater.from(context).inflate(R.layout.view_product_choice_slot, this, true);
		spinner1 =(Spinner) view.findViewById(R.id.product_choice_slot_spinner1);
		spinner2 =(Spinner) view.findViewById(R.id.product_choice_slot_spinner2);
		spinner3 =(Spinner) view.findViewById(R.id.product_choice_slot_spinner3);
		spinner4 =(Spinner) view.findViewById(R.id.product_choice_slot_spinner4);
		slotNumberText=(TextView) view.findViewById(R.id.product_choice_slot_number);

		ExpressApp app= (ExpressApp) (((Activity) context).getApplication());
		mProductManager=app.getProductManager();
	}

	public void setSlotAndInitialize(ProductSlot slot)
	{
		mSlot=slot;
		mSelectionMatrix=mProductManager.getSelectionMatrix();
		SelectionIndexes indexes=mProductManager.getSelectedIndex();
		int selected=mProductManager.getSelectedIndex().getElement(mSlot);

		final int SELECTED_BACKGROUND_COLOR=0x10000000; //
		if (selected==3)
			spinner4.setBackgroundColor(SELECTED_BACKGROUND_COLOR);
		else if (selected==2)
			spinner3.setBackgroundColor(SELECTED_BACKGROUND_COLOR);
		else if (selected==1)
			spinner2.setBackgroundColor(SELECTED_BACKGROUND_COLOR);
		else //selected==0
			spinner1.setBackgroundColor(SELECTED_BACKGROUND_COLOR);



		slotNumberText.setText((mSlot.ordinal()+1)+"");

		DatabaseDisplayProduct product1, product2, product3, product4;
		product1=mSelectionMatrix.getElement(mSlot, ProductRow.ROW1);
		product2=mSelectionMatrix.getElement(mSlot, ProductRow.ROW2);
		product3=mSelectionMatrix.getElement(mSlot, ProductRow.ROW3);
		product4=mSelectionMatrix.getElement(mSlot, ProductRow.ROW4);

		Spinner spinner1=(Spinner) findViewById(R.id.product_choice_slot_spinner1);
		Spinner spinner2=(Spinner) findViewById(R.id.product_choice_slot_spinner2);
		Spinner spinner3=(Spinner) findViewById(R.id.product_choice_slot_spinner3);
		Spinner spinner4=(Spinner) findViewById(R.id.product_choice_slot_spinner4);

		List<DatabaseDisplayProduct> array=mProductManager.getDisplayProducts();
		List<DatabaseDisplayProduct> onlyProductsArray=mProductManager.getOnlyDisplayProducts();

		ArrayAdapter<DatabaseDisplayProduct> adapter = new ArrayAdapter<DatabaseDisplayProduct>(getContext(),
				R.layout.spinner_item, array);
		ArrayAdapter<DatabaseDisplayProduct> onlyProductsAdapter = new ArrayAdapter<DatabaseDisplayProduct>(getContext(),
				R.layout.spinner_item, onlyProductsArray);

		if(selected==0) {
			spinner1.setAdapter(onlyProductsAdapter);
			spinner1.setSelection(onlyProductsAdapter.getPosition(product1));
		}
		else {
			spinner1.setAdapter(adapter);
			spinner1.setSelection(adapter.getPosition(product1));
		}

		if (selected==1) {
			spinner2.setAdapter(onlyProductsAdapter);
			spinner2.setSelection(onlyProductsAdapter.getPosition(product2));
		}
		else {
			spinner2.setAdapter(adapter);
			spinner2.setSelection(adapter.getPosition(product2));
		}

		if (selected==2) {
			spinner3.setAdapter(onlyProductsAdapter);
			spinner3.setSelection(onlyProductsAdapter.getPosition(product3));
		}
		else {
			spinner3.setAdapter(adapter);
			spinner3.setSelection(adapter.getPosition(product3));
		}

		if (selected==3) {
			spinner4.setAdapter(onlyProductsAdapter);
			spinner4.setSelection(onlyProductsAdapter.getPosition(product4));
		}
		else {
			spinner4.setAdapter(adapter);
			spinner4.setSelection(adapter.getPosition(product4));
		}

		spinner1.setOnItemSelectedListener(mSelectedListener1);
		spinner2.setOnItemSelectedListener(mSelectedListener2);
		spinner3.setOnItemSelectedListener(mSelectedListener3);
		spinner4.setOnItemSelectedListener(mSelectedListener4);
	}


	AdapterView.OnItemSelectedListener mSelectedListener1 = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			DatabaseDisplayProduct product=(DatabaseDisplayProduct) adapterView.getItemAtPosition(i);
			mSelectionMatrix.setElement(mSlot, ProductRow.ROW1, product);
			mProductManager.updateSelectionMatrix(mSlot, ProductRow.ROW1, product);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) { }
	};

	AdapterView.OnItemSelectedListener mSelectedListener2 = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			DatabaseDisplayProduct product=(DatabaseDisplayProduct) adapterView.getItemAtPosition(i);
			mSelectionMatrix.setElement(mSlot, ProductRow.ROW2, product);
			mProductManager.updateSelectionMatrix(mSlot, ProductRow.ROW2, product);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) { }
	};

	AdapterView.OnItemSelectedListener mSelectedListener3 = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			DatabaseDisplayProduct product=(DatabaseDisplayProduct) adapterView.getItemAtPosition(i);
			mSelectionMatrix.setElement(mSlot, ProductRow.ROW3, product);
			mProductManager.updateSelectionMatrix(mSlot, ProductRow.ROW3, product);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) { }
	};

	AdapterView.OnItemSelectedListener mSelectedListener4 = new AdapterView.OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
			DatabaseDisplayProduct product=(DatabaseDisplayProduct) adapterView.getItemAtPosition(i);
			mSelectionMatrix.setElement(mSlot, ProductRow.ROW4, product);
			mProductManager.updateSelectionMatrix(mSlot, ProductRow.ROW4, product);
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) { }
	};
}
