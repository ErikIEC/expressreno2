package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by peng on 16/11/30.
 */

public class CalibrationAccessSlotView extends FrameLayout {

    private TextView mTitleTv, mBrix, mRatio;
    private String mSettingTitle = "title";
	final int TIME_WATER = 8000, TIME_CONCENTRATE = 8000, TIME_BRIX = 3500;

    StepperView mFillSpeed, mPumpSpeed, mStartSteps, mPumpSteps;
	ImageView mButtonWater, mButtonConcentrate, mButtonBrix, mButtonReset;
    ExpressApp mApp;
    ProductManager mProductManager;
	MachineManager mMachineManager;
    NestleProduct mProduct;
    ProductSlot mSlot = ProductSlot.SLOT1;

    public CalibrationAccessSlotView(Context context) {
        super(context);
        commonInit(context, null);
    }

    public CalibrationAccessSlotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit(context, attrs);
    }

    public CalibrationAccessSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit(context, attrs);
    }

    public void commonInit(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_calibration_access_slot, this, true);
        mTitleTv = (TextView) view.findViewById(R.id.calibration_access_slot_title);
		mBrix = (TextView) view.findViewById(R.id.calibration_access_slot_brix);
		mRatio = (TextView) view.findViewById(R.id.calibration_access_slot_ratio);

		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CalibrationAccessSlotView);
		int slotNumber = ta.getInt(R.styleable.CalibrationAccessSlotView_accessSlotNumber, 0);
        mSettingTitle = ta.getString(R.styleable.CalibrationAccessSlotView_accessTitleText);
        ta.recycle();
        mTitleTv.setText(mSettingTitle);

		switch (slotNumber)
		{
			default:
			case 0: mSlot = ProductSlot.SLOT1; break;
			case 1: mSlot = ProductSlot.SLOT2; break;
			case 2: mSlot = ProductSlot.SLOT3; break;
			case 3: mSlot = ProductSlot.SLOT4; break;
		}

        //parameters embedded in layout xml
        mFillSpeed = (StepperView) view.findViewById(R.id.calibration_access_slot_fill_speed);
        mPumpSpeed = (StepperView) view.findViewById(R.id.calibration_access_slot_pump_speed);
        mStartSteps = (StepperView) view.findViewById(R.id.calibration_access_slot_start_steps);
        mPumpSteps = (StepperView) view.findViewById(R.id.calibration_access_slot_pump_steps);

		mButtonWater= (ImageView) view.findViewById(R.id.calibration_access_slot_water);
		mButtonConcentrate= (ImageView) view.findViewById(R.id.calibration_access_slot_concentrate);
		mButtonBrix= (ImageView) view.findViewById(R.id.calibration_access_slot_check_brix);
		mButtonReset= (ImageView) view.findViewById(R.id.calibration_access_slot_reset);

        mApp= (ExpressApp) (((Activity) context).getApplication());
        mProductManager = mApp.getProductManager();
        mProduct = mProductManager.getSelectedProduct(mSlot);
		mMachineManager = mApp.getMachineManager();

		if (mProduct==null)
		{
			mTitleTv.setText(context.getString(R.string.no_product));
			mBrix.setText("");
			mRatio.setText("");

			mFillSpeed.setEnabled(false);
			mPumpSpeed.setEnabled(false);
			mStartSteps.setEnabled(false);
			mPumpSteps.setEnabled(false);

			mButtonWater.setEnabled(false);
			mButtonConcentrate.setEnabled(false);
			mButtonBrix.setEnabled(false);
			mButtonReset.setEnabled(false);

			return;
		}

        mTitleTv.setText(mProduct.getFullName());
		mBrix.setText(mProduct.getBrixText());
		mRatio.setText(mProduct.getRatioText());

		initializeSteppers(mProduct);

		mButtonWater.setOnClickListener(mDispenseWater);
		mButtonConcentrate.setOnClickListener(mDispenseConcentrate);
		mButtonBrix.setOnClickListener(mDispenseBrix);
		mButtonReset.setOnClickListener(mReset);
    }

	void initializeSteppers(NestleProduct product)
	{
		mFillSpeed.setValue(product.getSpeedFill());
		mPumpSpeed.setValue(product.getSpeedPump());
		mStartSteps.setValue(product.getStepsStart());
		mPumpSteps.setValue(product.getStepsPump());
	}

    public void updateSettings()
    {
		if (mProduct==null)
			return;

        mProduct.setSpeedFill(mFillSpeed.getValue());
        mProduct.setSpeedPump(mPumpSpeed.getValue());
        mProduct.setStepsStart((int) mStartSteps.getValue());
        mProduct.setStepsPump((int) mPumpSteps.getValue());
        mProductManager.updateSelectedProductSetting(mProduct);

		mMachineManager.calibrationStop(mSlot);
    }

	void lockButtons(int time)
	{
		mButtonWater.setEnabled(false);
		mButtonConcentrate.setEnabled(false);
		mButtonBrix.setEnabled(false);
		mButtonReset.setEnabled(false);

		mButtonWater.setAlpha(0.3f);
		mButtonConcentrate.setAlpha(0.3f);
		mButtonBrix.setAlpha(0.3f);
		mButtonReset.setAlpha(0.3f);

		mHandler.postDelayed(mStopDispense, time);
	}

	void stopAndUnlockButtons()
	{
		mButtonWater.setEnabled(true);
		mButtonConcentrate.setEnabled(true);
		mButtonBrix.setEnabled(true);
		mButtonReset.setEnabled(true);

		mButtonWater.setAlpha(1f);
		mButtonConcentrate.setAlpha(1f);
		mButtonBrix.setAlpha(1f);
		mButtonReset.setAlpha(1f);

		mMachineManager.calibrationStop(mSlot);
	}

	OnClickListener mDispenseWater = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			updateSettings();
			mMachineManager.calibrationWater(mSlot, mProduct);
			lockButtons(TIME_WATER);
		}
	};

	OnClickListener mDispenseConcentrate = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			updateSettings();
			mMachineManager.calibrationConcentrate(mSlot, mProduct);
			lockButtons(TIME_CONCENTRATE);
		}
	};

	OnClickListener mDispenseBrix = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			updateSettings();
			mMachineManager.calibrationBrix(mSlot, mProduct);
			lockButtons(TIME_BRIX);
		}
	};

	OnClickListener mReset = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			mProduct.resetSetting();
			initializeSteppers(mProduct);
		}
	};

	Handler mHandler = new Handler();
	Runnable mStopDispense = new Runnable()
	{
		@Override
		public void run()
		{
			stopAndUnlockButtons();
		}
	};
}
