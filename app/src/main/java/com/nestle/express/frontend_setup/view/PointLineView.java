package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by root on 16-10-28.
 */
public class PointLineView extends View {

    private Paint paint = new Paint();
    private int point_padding = 10;

    public PointLineView(Context context) {
        super(context);
    }

    public PointLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PointLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public PointLineView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        int height = getMeasuredHeight();
        int width = getMeasuredWidth();

        if (height > width){
            int y = 0;
            while(y < height) {
                canvas.drawCircle(0, y, 1.3f, paint);
                y += point_padding;
            }
        }else{
            int x = 0;
            while(x < width) {
                canvas.drawCircle(x, 0, 1.3f, paint);
                x += point_padding;
            }
        }
    }


}
