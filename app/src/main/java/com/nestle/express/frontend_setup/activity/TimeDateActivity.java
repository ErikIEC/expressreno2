package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;

/**
 * Created by root on 16-11-14.
 */

public class TimeDateActivity extends BaseActivity{

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_time_date);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.timeanddate);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.timeanddate_breakout_01);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

}
