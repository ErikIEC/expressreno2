package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.view.View;

import com.nestle.express.frontend_setup.R;

/**
 * Created by peng on 16/12/2.
 */

public class StaionFaultActivity extends BaseActivity {
    long[] mHits = new long[3];

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_staion_fault);
    }

    @Override
    protected void initData() {


        findViewById(R.id.station_fault_view).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        System.arraycopy(mHits, 1, mHits, 0, mHits.length - 1);
        mHits[mHits.length - 1] = SystemClock.uptimeMillis();
        if (mHits[0] >= (SystemClock.uptimeMillis() - 1500)) {
            startActivity(new Intent(this, ServiceCenterActivity.class));
        }
    }


    @Override
    protected void initView() {

    }
}
