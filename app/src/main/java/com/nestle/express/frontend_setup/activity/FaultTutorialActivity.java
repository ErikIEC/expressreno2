package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;

/**
 * Created by root on 16-11-15.
 */

public class FaultTutorialActivity extends BaseActivity{
    ImageView[] mFButton;
    private int[] res = {R.id.f1_button, R.id.f2_button, R.id.f3_button, R.id.f4_button,
            R.id.f5_button, R.id.f6_button, R.id.f7_button, R.id.f8_button};

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_fault_tutorials);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_title)).setText(R.string.fault_tutorials);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.faultvideos_breakout_10_normal);

        mFButton = new ImageView[8];
        for(int i=0; i<mFButton.length; i++)
        {
            mFButton[i]=(ImageView) findViewById(res[i]);
            mFButton[i].setTag(i+1);
            mFButton[i].setOnClickListener(mOnFButton);
        }
    }

    View.OnClickListener mOnFButton = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            int imageClicked = (int) view.getTag();

            Intent intent;
            switch(imageClicked) {
                case 1:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f1_tutorial");
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f2_tutorial");
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f3_tutorial");
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f4_tutorial");
                    startActivity(intent);
                    break;
                case 5:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f5_tutorial");
                    startActivity(intent);
                    break;
                case 6:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f6_tutorial");
                    startActivity(intent);
                    break;
                case 7:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f7_tutorial");
                    startActivity(intent);
                    break;
                case 8:
                    intent = new Intent(getApplicationContext(), HTMLViewerActivity.class);
                    intent.putExtra("folder", "f8_tutorial");
                    startActivity(intent);
                    break;
            }
        }
    };
}
