package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.database.ArtDatabase;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.NestleProductButtonSetup;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by jason on 12/21/16.
 */
public class ButtonSetupSlotView extends FrameLayout
{
	public ButtonSetupSlotView(Context context) {
		super(context);
	}

	public ButtonSetupSlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public ButtonSetupSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	TextView mEnableText;
	ProductImageView mProductImage;
	ImageView mSmall, mMedium, mLarge, mSwirl;
	ProductSlot mSlot=null;
	public void commonInit(Context context, AttributeSet attrs) {
		View view = LayoutInflater.from(context).inflate(R.layout.view_button_setup_slot, this, true);
		mSmall= (ImageView) view.findViewById(R.id.view_button_setup_small);
		mMedium= (ImageView) view.findViewById(R.id.view_button_setup_medium);
		mLarge= (ImageView) view.findViewById(R.id.view_button_setup_large);
		mEnableText = (TextView) view.findViewById(R.id.view_button_setup_enable);
		mProductImage=(ProductImageView) findViewById(R.id.view_button_setup_image);
		mSwirl=(ImageView) findViewById(R.id.view_button_setup_swirl);

		ExpressApp app= (ExpressApp) (((Activity) context).getApplication());
		mProductManager=app.getProductManager();

		mSmall.setOnClickListener(onSmall);
		mMedium.setOnClickListener(onMedium);
		mLarge.setOnClickListener(onLarge);
		mEnableText.setOnClickListener(onEnable);
		mEnableText.setAllCaps(true);
	}

	NestleProduct mProduct;
	ProductManager mProductManager;
	public void setSlotAndInitialize(ProductSlot slot)
	{
		mSlot=slot;
		mProduct=mProductManager.getSelectedProduct(slot);
		mProductImage.setSlotAndInitialize(slot);
		updateButtons();
	}

	void updateButtons()
	{
		mHandler.postDelayed(mUpdateButtons, 1);
	}

	OnClickListener onSmall = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			NestleProductButtonSetup setup=mProduct.getButtonSetup();
			setup.setSmall(!setup.isSmall());
			mProduct.setButtonSetup(setup);
			updateButtons();
			mProductManager.updateSelectedProductSetting(mProduct);
		}
	};

	OnClickListener onMedium = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			NestleProductButtonSetup setup=mProduct.getButtonSetup();
			setup.setMedium(!setup.isMedium());
			mProduct.setButtonSetup(setup);
			updateButtons();
			mProductManager.updateSelectedProductSetting(mProduct);
		}
	};

	OnClickListener onLarge = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			NestleProductButtonSetup setup=mProduct.getButtonSetup();
			setup.setLarge(!setup.isLarge());
			mProduct.setButtonSetup(setup);
			updateButtons();
			mProductManager.updateSelectedProductSetting(mProduct);
		}
	};

	OnClickListener onEnable = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			NestleProductButtonSetup setup=mProduct.getButtonSetup();
			setup.setConcentrate(!setup.isConcentrate());
			mProduct.setButtonSetup(setup);
			updateButtons();
			mProductManager.updateSelectedProductSetting(mProduct);
		}
	};

	Handler mHandler = new Handler();
	Runnable mUpdateButtons = new Runnable()
	{
		@Override
		public void run()
		{
			if (mProduct==null || mProduct.getId()<0)
			{
				mSmall.setEnabled(false);
				mMedium.setEnabled(false);
				mLarge.setEnabled(false);
				mEnableText.setEnabled(false);
				return;
			}

			mSmall.setEnabled(true);
			mMedium.setEnabled(true);
			mLarge.setEnabled(true);
			mEnableText.setEnabled(true);

			mSmall.setPressed(mProduct.getButtonSetup().isSmall());
			mMedium.setPressed(mProduct.getButtonSetup().isMedium());
			mLarge.setPressed(mProduct.getButtonSetup().isLarge());

			boolean concentrateEnabled=mProduct.getButtonSetup().isConcentrate();

			if (concentrateEnabled)
			{
				mSmall.setVisibility(GONE);
				mSwirl.setVisibility(VISIBLE);
				mEnableText.setPressed(true);
				//mEnableText.setText(R.string.on);

				//mEnableText.setTextSize(45);
			}
			else
			{
				mSmall.setVisibility(VISIBLE);
				mSwirl.setVisibility(GONE);
				mEnableText.setPressed(false);
				//mEnableText.setText(R.string.enable);
				//mEnableText.setTextSize(30);
			}
		}
	};
}
