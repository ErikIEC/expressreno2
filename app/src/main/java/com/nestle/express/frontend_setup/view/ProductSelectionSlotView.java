package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductRow;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.iec_backend.product.structure.SelectionMatrix;

/**
 * Created by jason on 12/22/16.
 */
public class ProductSelectionSlotView extends FrameLayout
{
	final String TAG="ProductSelectionView";

	ProductManager mProductManager;
	NestleProduct mProduct;
	ProductSlot mSlot;
	NestleProduct.ProductStatus mStatus;
	SelectionMatrix mSelectionMatrix;
	ProductRow mOriginallySelected;

	TextView slotNumber;
	RadioButton radio1, radio2, radio3, radio4;
    boolean startup = true;

	public ProductSelectionSlotView(Context context) {
		super(context);
	}

	public ProductSelectionSlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public ProductSelectionSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	public void commonInit(Context context, AttributeSet attrs) {
		View view = LayoutInflater.from(context).inflate(R.layout.view_product_selection_slot, this, true);
		radio1=(RadioButton) view.findViewById(R.id.product_selection_slot_radio1);
		radio2=(RadioButton) view.findViewById(R.id.product_selection_slot_radio2);
		radio3=(RadioButton) view.findViewById(R.id.product_selection_slot_radio3);
		radio4=(RadioButton) view.findViewById(R.id.product_selection_slot_radio4);
		slotNumber =(TextView) view.findViewById(R.id.product_selection_slot_number);

		ExpressApp app= (ExpressApp) (((Activity) context).getApplication());
		mProductManager=app.getProductManager();

        slotNumber.setOnTouchListener(mOnStatusTouch);

        startup = true;
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.product_selection_slot_radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(!startup) {
                    mStatus = NestleProduct.ProductStatus.DISPENSE;
                    updateProductStatusDisplay();
                }
            }
        });
	}

	public void setSlotAndInitialize(ProductSlot slot)
	{
        mSlot=slot;
		mSelectionMatrix=mProductManager.getSelectionMatrix();
		mOriginallySelected =mProductManager.getSelectedIndex().getElementSlot(mSlot);
		mProduct = mProductManager.getSelectedProduct(slot, mProductManager.getLanguage());
		mStatus = mProduct.getStatus();

		setChecked(mOriginallySelected.ordinal());

		slotNumber.setText((mSlot.ordinal()+1)+"");
		slotNumber.setPressed(false);
		updateProductStatusDisplay();

		DatabaseDisplayProduct product1=mSelectionMatrix.getElement(mSlot, ProductRow.ROW1);
		DatabaseDisplayProduct product2=mSelectionMatrix.getElement(mSlot, ProductRow.ROW2);
		DatabaseDisplayProduct product3=mSelectionMatrix.getElement(mSlot, ProductRow.ROW3);
		DatabaseDisplayProduct product4=mSelectionMatrix.getElement(mSlot, ProductRow.ROW4);


		if (product1.id>0)
			radio1.setText(product1.name+"\n"+product1.id+"");
		else
			radio1.setEnabled(false);

		if (product2.id>0)
			radio2.setText(product2.name+"\n"+product2.id+"");
		else
			radio2.setEnabled(false);

		if (product3.id>0)
			radio3.setText(product3.name+"\n"+product3.id+"");
		else
			radio3.setEnabled(false);

		if (product4.id>0)
			radio4.setText(product4.name+"\n"+product4.id+"");
		else
			radio4.setEnabled(false);

        startup = false;
	}

	void setChecked(int i)
	{
		if (i==3)		radio4.setChecked(true);
		else if (i==2)	radio3.setChecked(true);
		else if (i==1)	radio2.setChecked(true);
		else if (i==0)	radio1.setChecked(true);
	}

	public int getSelectedIndex()
	{
		if (radio4.isChecked())	return 3;
		if (radio3.isChecked())	return 2;
		if (radio2.isChecked())	return 1;
		//radio1 must be checked
		return 0;
	}

	public int getOriginallySelectedIndex()
	{
		return mOriginallySelected.ordinal();
	}

	public void updateProductStatusDisplay(){
		if(mStatus == NestleProduct.ProductStatus.SOLD_OUT){
			slotNumber.setPressed(false);
            slotNumber.setText(R.string.soldout);
            slotNumber.setTextSize(28);
		}
		else {
			slotNumber.setPressed(true);
            slotNumber.setText((mSlot.ordinal()+1)+"");
            slotNumber.setTextSize(50);
        }
	}

    public NestleProduct.ProductStatus getSoldoutStatus(){
        return mStatus;
    }

    View.OnTouchListener mOnStatusTouch = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)  {

                if(mStatus != NestleProduct.ProductStatus.SOLD_OUT) {
                    mStatus = NestleProduct.ProductStatus.SOLD_OUT;
                    //mProduct.setProductStatus(NestleProduct.ProductStatus.SOLD_OUT);
					//mProductManager.setProductSlotStatus(mSlot, NestleProduct.ProductStatus.SOLD_OUT);
                }
                else{
                    mStatus = NestleProduct.ProductStatus.DISPENSE;
                    //mProduct.setProductStatus(NestleProduct.ProductStatus.DISPENSE);
					//mProductManager.setProductSlotStatus(mSlot, NestleProduct.ProductStatus.DISPENSE);
				}

                updateProductStatusDisplay();
            }
            else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)) {

            }

            return true;
        }
    };
}
