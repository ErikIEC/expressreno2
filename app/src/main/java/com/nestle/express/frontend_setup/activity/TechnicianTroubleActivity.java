package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;

/**
 * Created by peng on 16/12/1.
 */
public class TechnicianTroubleActivity extends BaseActivity{
    private TextView mTitleTv;
    ExpressApp mApp;
    WebView mWebview;
    TextView mHome, mBack;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_html);

        mTitleTv = (TextView)findViewById(R.id.tv_title);
        findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.tutorial_icon);
        mTitleTv.setText(R.string.tech_manual);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        mApp=(ExpressApp) getApplication();
        String base="file://"+mApp.getSDCardDirectory()+"/html/"+mApp.getSelectedLanguage().isoCode+"/";

        //String folder=parameters.getString("folder");
        String folder="tech_manual";
        if (folder!=null)
            base+=folder+"/index.html";

        mWebview=(WebView) findViewById(R.id.webview);
        mHome=(TextView) findViewById(R.id.tv_home);
        mBack=(TextView) findViewById(R.id.tv_back);

        mBack.setOnClickListener(this);
        mHome.setOnClickListener(this);

        mWebview.getSettings().setBuiltInZoomControls(true);
        mWebview.getSettings().setDisplayZoomControls(true);

        mWebview.loadUrl(base);
    }
}
