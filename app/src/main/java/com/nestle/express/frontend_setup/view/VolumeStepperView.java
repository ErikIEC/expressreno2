package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;

/**
 * Created by peng on 16/11/30.
 */

public class VolumeStepperView extends FrameLayout {
	ExpressApp mApp;
	ApplicationManager mAppManager;
    protected double mSettingMax = 5;
    protected double mSettingMin = 0;
    protected double mSettingValue = 0;
    protected double mIncrementValue = 0.10;
    protected TextView mText;
    protected ImageView mSubBtn;
    protected ImageView mAddBtn;
    boolean mFirstIncrement=true;


    public VolumeStepperView(Context context) {
        super(context);
    }

    public VolumeStepperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit(context, attrs);
    }

    public VolumeStepperView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit(context, attrs);
    }

    public void commonInit(Context context, AttributeSet attrs) {
		mApp=(ExpressApp) ((Activity) getContext()).getApplication();
		mAppManager=mApp.getApplicationManager();

        View view = LayoutInflater.from(context).inflate(R.layout.view_volume_stepper, this, true);
        mText = (TextView) view.findViewById(R.id.stepper_text_tv);
        mSubBtn = (ImageView) view.findViewById(R.id.stepper_sub_iv);
        mAddBtn = (ImageView) view.findViewById(R.id.stepper_add_iv);
        mSubBtn.setOnTouchListener(mDecrement);
        mAddBtn.setOnTouchListener(mIncrement);
        updateDisplayText();
    }

    public void setValue(double value)
    {
        mSettingValue=value;
        updateDisplayText();
    }

    public double getValue()
    {
        limitSetting();
        return mSettingValue;
    }

    public void setMax(double max)
    {
        updateDisplayText();
        mSettingMax = max;
    }

    public void setMin(double min)
	{
		updateDisplayText();
        mSettingMin = min;
    }

    public void setIncrement(double inc)
	{
		updateDisplayText();
        mIncrementValue = inc;
    }

    OnTouchListener mIncrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch(motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
					mAddBtn.setPressed(true);
                    mHandler.post(mRunnableIncrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
					mAddBtn.setPressed(false);
                    mHandler.removeCallbacks(mRunnableIncrement);
                    resetIncrement();
            }
            return true;
        }
    };

    OnTouchListener mDecrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {

            switch (motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
					mSubBtn.setPressed(true);
                    mHandler.post(mRunnableDecrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
					mSubBtn.setPressed(false);
                    mHandler.removeCallbacks(mRunnableDecrement);
                    resetDecrement();
            }
            return true;
        }
    };

    private Runnable mRunnableIncrement = new Runnable()
    {
        @Override
        public void run()
        {
            incrementSetting();
            if (mFirstIncrement)
            {
                mFirstIncrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetIncrement()
    {
        mFirstIncrement =true;
    }
    boolean mFirstDecrement =true;
    private Runnable mRunnableDecrement = new Runnable()
    {
        @Override
        public void run()
        {
            decrementSetting();
            if (mFirstDecrement)
            {
                mFirstDecrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetDecrement()
    {
        mFirstDecrement =true;
    }

    public void incrementSetting()
    {
        mSettingValue += mIncrementValue;
        updateDisplayText();
    }
    public void decrementSetting()
    {
        mSettingValue -= mIncrementValue;
        updateDisplayText();
    }

    void limitSetting()
    {
        if (mSettingValue > mSettingMax)
            mSettingValue = mSettingMax;

        if (mSettingValue < mSettingMin)
            mSettingValue = mSettingMin;
    }

    void updateDisplayText()
    {
        limitSetting();
		mText.setText(ConversionFunctions.formatVolume(mAppManager, mSettingValue));
    }

    Handler mHandler = new Handler();

    @Override
    public void setEnabled(boolean value)
    {
        if (value==false)
            mText.setText("");

        mText.setEnabled(value);
        mSubBtn.setEnabled(value);
        mAddBtn.setEnabled(value);
    }
}