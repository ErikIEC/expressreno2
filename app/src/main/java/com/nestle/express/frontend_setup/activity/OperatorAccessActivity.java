package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.ActivityUtil;
import com.nestle.express.frontend_setup.utils.Common;
import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.frontend_setup.view.MyGridView;
import com.nestle.express.iec_backend.application.ApplicationManager;


/**
 * Created by root on 16-11-14.
 */

public class OperatorAccessActivity extends BaseActivity implements MyGridView.OnItemClickListener {
    private MyGridView mgv;

    private int[] res = {R.drawable.technician_breakout_02, R.drawable.technician_breakout_03,
            R.drawable.technician_breakout_04, R.drawable.technician_breakout_05, R.drawable.technician_breakout_06,
            R.drawable.technician_breakout_07, R.drawable.technician_breakout_08, R.drawable.technician_breakout_09,
            R.drawable.technician_breakout_10, R.drawable.technician_breakout_11, R.drawable.technician_breakout_12,
            R.drawable.technician_breakout_13, R.drawable.technician_breakout_14};

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_operator_access);
    }

    @Override
    protected void initView() {
        mgv = (MyGridView) findViewById(R.id.mgv);
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_title)).setText(R.string.operator_access);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_01);

    }

    @Override
    protected void initData() {
        mgv.setVertical(3);
        mgv.setHorizontal(5);
        String[] array = getResources().getStringArray(R.array.technician_screen_items);
        boolean[] booleen = SharedPreferencesUtil.getInstance()
                .getEnalbleArray(Common.OPERATOR_ACCESS_ARRAY);
        int index = 0 ;
        for (int i = 0; i < array.length; i++) {
            MyGridView.GridItem item = new MyGridView.GridItem();
            item.setPosition(index);
            item.setRes(res[index++]);
            item.setTitle(array[i]);
            mgv.addItem(item, booleen[i]);
        }
        mgv.setListener(this);
    }

    @Override
    public void onItemClick(MyGridView.GridItem item) {
        switch (item.getPosition()) {
            case 0:
                startActivity(new Intent(getApplicationContext(), SetupActivity.class));
                break;
            case 1:
                startActivity(new Intent(getApplicationContext(), TemperatureActivity.class));
                break;
            case 2:
                startActivity(new Intent(getApplicationContext(), ProductChoiceActivity.class));
                break;
            case 3:
                startActivity(new Intent(getApplicationContext(), ButtonSetUpActivity.class));
                break;
            case 4:
                startActivity(new Intent(getApplicationContext(), CalibrationActivity.class));
                break;
            case 5:
                startActivity(new Intent(getApplicationContext(), CheckActiveFaultsActivity.class));
                break;
            case 6:
                startActivity(new Intent(getApplicationContext(), CheckFaultHistoryActivity.class));
                break;
            case 7:
                startActivity(new Intent(getApplicationContext(), SalesInfoActivity.class));
                break;
            case 8:
                startActivity(new Intent(getApplicationContext(), FileTransfersActivity.class));
                break;
            case 9:
                mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.SET_PORTIONS);
                stopTimeout();
                ActivityUtil.closeAllActivity();
                break;
            case 10:
                startActivity(new Intent(getApplicationContext(), FunctionalityCheckActivity.class));
                break;
            case 11:
                startActivity(new Intent(getApplicationContext(), TechnicianTutorialsActivity.class));
                break;
            case 12:
                startActivity(new Intent(getApplicationContext(), TechnicianTroubleActivity.class));
                break;

        }
    }
}
