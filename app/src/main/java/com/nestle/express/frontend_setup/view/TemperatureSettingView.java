package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;

/**
 * Created by jason on 12/23/16.
 */
public class TemperatureSettingView extends FrameLayout
{
	ExpressApp mApp;
	ApplicationManager mAppManager;
	TemperatureStepperView stepper;
	TextView label, current;
	public TemperatureSettingView(Context context) {
		super(context);
	}

	public TemperatureSettingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public TemperatureSettingView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	public void commonInit(Context context, AttributeSet attrs) {
		mApp=(ExpressApp) ((Activity) getContext()).getApplication();
		mAppManager=mApp.getApplicationManager();

		View view = LayoutInflater.from(context).inflate(R.layout.view_temperature_setting, this, true);
		//mValueText = (TextView) view.findViewById(R.id.stepper_text_tv);
		label=(TextView) findViewById(R.id.view_temperature_setting_label);
		current=(TextView) findViewById(R.id.view_temperature_setting_current);
		stepper=(TemperatureStepperView) findViewById(R.id.view_temperature_setting_stepper);
		stepper.setIncrement(1.0);
	}

	public void setLabel(String text)
	{
		label.setText(text);
	}

	//in degrees F
	public void setCurrentTemperature(double degreesF)
	{
		current.setText(ConversionFunctions.formatTemperature(mAppManager, degreesF));
	}

	public void setMin(double value)
	{
		stepper.setMin(value);
	}

	public void setMax(double value)
	{
		stepper.setMax(value);
	}

	public void setValue(double value)
	{
		stepper.setValue(value);
	}

	public double getValue()
	{
		return stepper.getValue();
	}
}
