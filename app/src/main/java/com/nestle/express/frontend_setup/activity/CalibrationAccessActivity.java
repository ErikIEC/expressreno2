package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.CalibrationAccessSlotView;

/**
 * Created by peng on 16/11/17.
 */
public class CalibrationAccessActivity extends BaseActivity{

    CalibrationAccessSlotView mSlot1, mSlot2, mSlot3, mSlot4;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_calibration_access);
        mSlot1 = (CalibrationAccessSlotView) findViewById(R.id.calibration_slot_1);
        mSlot2 = (CalibrationAccessSlotView) findViewById(R.id.calibration_slot_2);
        mSlot3 = (CalibrationAccessSlotView) findViewById(R.id.calibration_slot_3);
        mSlot4 = (CalibrationAccessSlotView) findViewById(R.id.calibration_slot_4);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.calibration);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_06);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        mSlot1.updateSettings();
        mSlot2.updateSettings();
        mSlot3.updateSettings();
        mSlot4.updateSettings();
    }
}
