package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;

import org.w3c.dom.Text;

/**
 * Created by jason on 1/12/17.
 */
public class PillButtonView extends TextView
{
	public PillButtonView(Context context) {
		super(context);
		commonInit();
	}

	public PillButtonView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit();
	}

	public PillButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit();
	}

	public void commonInit()
	{
		this.setGravity(Gravity.CENTER);
		//this.setTextColor(0xFFFFFFFF); //White
		this.setTextSize(25);
		this.setPaddingRelative(getPaddingLeft(), 8, getPaddingRight(), 8);

		this.setOnTouchListener(mOnTouch);
		this.setClickable(true);

		normal();
	}

	OnTouchListener mOnTouch = new OnTouchListener()
	{
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent)
		{
			if (!isEnabled())
			{
				normal();
				return true;
			}

			switch (motionEvent.getAction())
			{
				case MotionEvent.ACTION_DOWN:
					pressed();
					break;
				case MotionEvent.ACTION_UP:
					normal();
					callOnClick();
					break;
			}
			return true;
		}
	};

	void normal()
	{
		this.setBackground(getResources().getDrawable(R.drawable.shape_language_background_normal));
	}

	void pressed()
	{
		this.setBackground(getResources().getDrawable(R.drawable.shape_language_background_checked));
	}
}
