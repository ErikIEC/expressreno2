package com.nestle.express.frontend_setup.utils;

/**
 * Created by jason on 3/6/17.
 */
public class AverageUtil
{
	double mPreviousAverage;
	double mSampleArray[];
	int mSampleCounter;
	boolean mFirstRun=true;
	public AverageUtil(int samples)
	{
		mSampleArray=new double[samples];
	}

	public AverageUtil(int samples, double def)
	{
		mSampleArray=new double[samples];
		updateAverage(def);
	}

	public double updateAverage(double value)
	{
		if (mFirstRun)
		{
			mFirstRun=false;
			mPreviousAverage=value;

			for(int i=0; i<mSampleArray.length; i++)
				mSampleArray[i]=value;
		}

		mSampleArray[mSampleCounter]=value;

		mSampleCounter++;
		if (mSampleCounter>=mSampleArray.length)
			mSampleCounter=0;

		double accumulator=0;
		for(double d : mSampleArray)
			accumulator+=d;
		accumulator/=mSampleArray.length;

		mPreviousAverage=accumulator;
		return mPreviousAverage;
	}

	public double getAverage()
	{
		return mPreviousAverage;
	}

	public boolean isUninitialized()
	{
		return mFirstRun;
	}
}
