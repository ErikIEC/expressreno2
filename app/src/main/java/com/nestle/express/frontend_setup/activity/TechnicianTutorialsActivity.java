package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.MyGridView;

/**
 * Created by root on 16-11-14.
 */

public class TechnicianTutorialsActivity extends BaseActivity  implements MyGridView.OnItemClickListener{
    private MyGridView mgv;

    private int[] res = {R.drawable.techniciantutorials_breakout_01,R.drawable.techniciantutorials_breakout_02,
            R.drawable.techniciantutorials_breakout_03,R.drawable.techniciantutorials_breakout_04,
            R.drawable.techniciantutorials_breakout_05,R.drawable.techniciantutorials_breakout_06,
            R.drawable.techniciantutorials_breakout_07,R.drawable.techniciantutorials_breakout_08,
            R.drawable.techniciantutorials_breakout_09,R.drawable.techniciantutorials_breakout_10,
            R.drawable.techniciantutorials_breakout_11,R.drawable.techniciantutorials_breakout_12,
            R.drawable.techniciantutorials_breakout_13};

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_technician_tutorials);
    }

    @Override
    protected void initView() {
        mgv = (MyGridView)findViewById(R.id.mgv);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.technician_tutorials);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_13);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);

		mgv.setListener(this);
		findViewById(R.id.tv_back).setOnClickListener(this);
		findViewById(R.id.tv_home).setOnClickListener(this);
	}

    @Override
    protected void initData() {
        String[] array = getResources().getStringArray(R.array.technician_tutorials);
        for (int i = 0 ; i < res.length;i++){
            MyGridView.GridItem item = new MyGridView.GridItem();
            item.setPosition(i+1);
            item.setRes(res[i]);
            item.setTitleDesc(array[i]);
            mgv.addItem(item,true);
        }
    }

	@Override
	public void onItemClick(MyGridView.GridItem item)
	{
		Intent intent;
		switch (item.getPosition()){
			case 1:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "button_setup");
				startActivity(intent);
				break;
			case 2:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "set_portions");
				startActivity(intent);
				break;
			case 3:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "pump_motor_install");
				startActivity(intent);
				break;
			case 4:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "condenser_fan_install");
				startActivity(intent);
				break;
			case 5:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "thermister_drain_install");
				startActivity(intent);
				break;
			case 6:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "home_sensor_install");
				startActivity(intent);
				break;
			case 7:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "ada_board_install");
				startActivity(intent);
				break;
			case 8:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "door_led_install");
				startActivity(intent);
				break;
			case 9:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "touch_screen_install");
				startActivity(intent);
				break;
			case 10:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "compartment_install");
				startActivity(intent);
				break;
			case 11:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "evaporator_duct_install");
				startActivity(intent);
				break;
			case 12:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "water_system_setup");
				startActivity(intent);
				break;
			case 13:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "water_inlet_fitting");
				startActivity(intent);
				break;
		}
	}

}
