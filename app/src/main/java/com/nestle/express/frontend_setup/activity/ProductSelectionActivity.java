package com.nestle.express.frontend_setup.activity;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.ActivityUtil;
import com.nestle.express.frontend_setup.view.ProductSelectionSlotView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.iec_backend.product.structure.SelectionIndexes;

/**
 * Created by root on 16-10-29.
 */
public class ProductSelectionActivity extends BaseActivity
{
	ProductManager mProductManager;
	ProductSelectionSlotView slot1, slot2, slot3, slot4;

	@Override
	protected void initContentView()
	{
		setContentView(R.layout.activity_product_selection);

		mProductManager=((ExpressApp) getApplication()).getProductManager();

		slot1=(ProductSelectionSlotView) findViewById(R.id.product_selection_slot1);
		slot2=(ProductSelectionSlotView) findViewById(R.id.product_selection_slot2);
		slot3=(ProductSelectionSlotView) findViewById(R.id.product_selection_slot3);
		slot4=(ProductSelectionSlotView) findViewById(R.id.product_selection_slot4);

		slot1.setSlotAndInitialize(ProductSlot.SLOT1);
		slot2.setSlotAndInitialize(ProductSlot.SLOT2);
		slot3.setSlotAndInitialize(ProductSlot.SLOT3);
		slot4.setSlotAndInitialize(ProductSlot.SLOT4);

		TextView mTitleTv = (TextView)findViewById(R.id.tv_title);
		findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_04);
		mTitleTv.setText(R.string.product_select);
	}

	@Override
	protected void initView()
	{

	}

	@Override
	protected void initData()
	{
		Log.d("Product Selection", " initData Save Sales Data");
		mApp.getSalesDatabase().save();		// save data in case a new flavor selected
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		Log.d("Product Selection ", "onPause: ");

		SelectionIndexes index=mProductManager.getSelectedIndex();
		index.setElement(ProductSlot.SLOT1, slot1.getSelectedIndex());
		index.setElement(ProductSlot.SLOT2, slot2.getSelectedIndex());
		index.setElement(ProductSlot.SLOT3, slot3.getSelectedIndex());
		index.setElement(ProductSlot.SLOT4, slot4.getSelectedIndex());

		RinseStatus status=mApp.getApplicationManager().getRinseStatus();
		status.clearChangeStatus();

		if(slot1.getSelectedIndex() != slot1.getOriginallySelectedIndex()) {
			Log.d("Product Selection ", "onPause: Slot1 updated ");
			status.setRinseStatus(ProductSlot.SLOT1, RinseStatus.RINSE_STATUS.ALLERGEN_RINSE);
		}
		if(slot2.getSelectedIndex() != slot2.getOriginallySelectedIndex()) {
			Log.d("Product Selection ", "onPause: Slot2 updated ");
			status.setRinseStatus(ProductSlot.SLOT2, RinseStatus.RINSE_STATUS.ALLERGEN_RINSE);
		}
		if(slot3.getSelectedIndex() != slot3.getOriginallySelectedIndex()) {
			Log.d("Product Selection ", "onPause: Slot3 updated ");
			status.setRinseStatus(ProductSlot.SLOT3, RinseStatus.RINSE_STATUS.ALLERGEN_RINSE);
		}
		if(slot4.getSelectedIndex() != slot4.getOriginallySelectedIndex()) {
			Log.d("Product Selection ", "onPause: Slot4 updated ");
			status.setRinseStatus(ProductSlot.SLOT4, RinseStatus.RINSE_STATUS.ALLERGEN_RINSE);
		}

		mProductManager.setSelectionIndex(index);

        if(slot1.getSoldoutStatus() == NestleProduct.ProductStatus.SOLD_OUT)
            mProductManager.setProductSlotStatus(ProductSlot.SLOT1, NestleProduct.ProductStatus.SOLD_OUT);
        if(slot2.getSoldoutStatus() == NestleProduct.ProductStatus.SOLD_OUT)
            mProductManager.setProductSlotStatus(ProductSlot.SLOT2, NestleProduct.ProductStatus.SOLD_OUT);
        if(slot3.getSoldoutStatus() == NestleProduct.ProductStatus.SOLD_OUT)
            mProductManager.setProductSlotStatus(ProductSlot.SLOT3, NestleProduct.ProductStatus.SOLD_OUT);
        if(slot4.getSoldoutStatus() == NestleProduct.ProductStatus.SOLD_OUT)
            mProductManager.setProductSlotStatus(ProductSlot.SLOT4, NestleProduct.ProductStatus.SOLD_OUT);

        status.runCallbackIfNeeded();

		if((mApp.getApplicationManager().getRinseStatus().getRinseStatus(ProductSlot.SLOT1) == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE) ||
				(mApp.getApplicationManager().getRinseStatus().getRinseStatus(ProductSlot.SLOT2) == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE) ||
				(mApp.getApplicationManager().getRinseStatus().getRinseStatus(ProductSlot.SLOT3) == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE) ||
				(mApp.getApplicationManager().getRinseStatus().getRinseStatus(ProductSlot.SLOT4) == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE))
		{
			mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.RINSE);
			stopTimeout();
			ActivityUtil.closeAllActivity();
		}

		mApp.getSalesDatabase().save();
	}
}
