package com.nestle.express.frontend_setup.utils;

/**
 * Created by peng on 16/12/5.
 */

public class Common {
    public static final String OPERATOR_ACCESS_ARRAY = "operator_access_array";
    public static final String PRODUCT_CHOICE_ARRAY_v1 = "product_choice_array_v1";
    public static final String PRODUCT_CHOICE_ARRAY_v2 = "product_choice_array_v2";
    public static final String PRODUCT_CHOICE_ARRAY_v3 = "product_choice_array_v3";
    public static final String PRODUCT_CHOICE_ARRAY_v4 = "product_choice_array_v4";
    public static final String PRODUCT_SELECTION = "product_selection";


}
