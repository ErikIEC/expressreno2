package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peng on 16-12-30.
 */

public class SalesInfoView extends FrameLayout {
    private int max;
    private List<UnitView> mUnitViews = new ArrayList<>();
    private List<ItemView> mItemViews = new ArrayList<>();
    private List<int[]> mData;

    public SalesInfoView(Context context) {
        super(context);
        initView(context, null);
    }

    public SalesInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }


    public SalesInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.view_sales_info, this, true);
        mUnitViews.add((UnitView) findViewById(R.id.unitview0));
        mUnitViews.add((UnitView) findViewById(R.id.unitview1));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview0),
                (TextView) findViewById(R.id.sales_info_tv0)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview1),
                (TextView) findViewById(R.id.sales_info_tv1)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview2),
                (TextView) findViewById(R.id.sales_info_tv2)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview3),
                (TextView) findViewById(R.id.sales_info_tv3)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview4),
                (TextView) findViewById(R.id.sales_info_tv4)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview5),
                (TextView) findViewById(R.id.sales_info_tv5)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview6),
                (TextView) findViewById(R.id.sales_info_tv6)));
        mItemViews.add(new ItemView(
                (BarGraphView)findViewById(R.id.bargraphview7),
                (TextView) findViewById(R.id.sales_info_tv7)));
    }

    public void setData(int max, BarGraphView.TimeType timeType, List<int[]> data,
                        List<DatabaseDisplayProduct> strings, List<Boolean> booleen) {
        this.max = max;
        this.mData = data;
        for (UnitView unitView : mUnitViews) {
            unitView.setMax(max);
        }
        for (int i = 0; i < data.size(); i++) {
            ItemView itemView = mItemViews.get(i);
            itemView.mBarGraphView.setTimeType(timeType);
            itemView.mBarGraphView.setData(max, data.get(i), booleen.get(i));
            //if(TextUtils.isEmpty(itemView.mTextView.getText().toString())){
            itemView.mTextView.setText(strings.get(i).name + "\n" + strings.get(i).id);
            //}
        }
    }

    public void setData(int max, BarGraphView.TimeType timeType, List<int[]> data, String[] strings) {
        this.max = max;
        this.mData = data;
        for (UnitView unitView : mUnitViews) {
            unitView.setMax(max);
        }
        for (int i = 0; i < data.size(); i++) {
            ItemView itemView = mItemViews.get(i);
            itemView.mBarGraphView.setTimeType(timeType);
            itemView.mBarGraphView.setData(max, data.get(i));
            if(TextUtils.isEmpty(itemView.mTextView.getText().toString())){
                itemView.mTextView.setText(strings[i]);
            }
        }
    }

    class ItemView{
        BarGraphView mBarGraphView;
        TextView mTextView;
        ItemView(BarGraphView mBarGraphView, TextView mTextView){
            this.mBarGraphView = mBarGraphView;
            this.mTextView = mTextView;
        }
    }
}
