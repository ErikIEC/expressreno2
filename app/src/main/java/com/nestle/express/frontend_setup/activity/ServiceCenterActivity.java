package com.nestle.express.frontend_setup.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Handler;


import com.nestle.express.communications.protocol_layer.events.EventStatusResponse;
import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.ActivityUtil;
import com.nestle.express.frontend_setup.utils.AverageUtil;
import com.nestle.express.frontend_setup.utils.Common;
import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.Locale;

/**
 * Created by root on 16-10-29.
 */
public class ServiceCenterActivity extends BaseActivity {

    private static final String TAG = "ServiceCenterActivity";
    private TextView mPinContentTv;
    private String pinStr = "PIN #     ";


    private NumClick numClick = new NumClick();
    private TextView mTitleTv;
    private boolean isFirst = true;
    private Runnable clearRunable = new Runnable() {
        @Override
        public void run() {
            mPinContentTv.setText(pinStr);
        }
    };
    private TextView mLockTv;
    private boolean isLock = false;
    private TextView mSetPortionsTv;
    private TextView mProductSelectionTv;
    private TextView mRinseTv;
    TextView mSystemDataText1, mSystemDataText2, mSystemDataText3, mSystemDataText4,
            mWaterTemperature, mConcentrateTemperature;
    private String[] mRefrigState = {"Pulldown","Pulldown Defrost","Recool","Recool Defrost","Cooling","Defrost","Recovery Defrost","RecovCool","RecovCool Defrost","Off","Error"};
	ExpressApp mApp;
	ApplicationManager mAppManager;
    MachineManager mMachine;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //((ExpressApp) getApplication()).setVersion(getIntent().getStringExtra("gitHash"));
        //Log.d(TAG, ((ExpressApp) getApplication()).getVersion());
    }

    boolean isSystemDataShown=false;


    public void showSystemData(View v)
    {
        isSystemDataShown=!isSystemDataShown;
        //service_center_right_main_pane
        Log.d(TAG, "System Data!");
        if (isSystemDataShown)
        {
            findViewById(R.id.service_center_right_main_pane).setVisibility(View.GONE);
            findViewById(R.id.service_center_right_secondary_pane).setVisibility(View.VISIBLE);
        }
        else
        {
            findViewById(R.id.service_center_right_secondary_pane).setVisibility(View.GONE);
            findViewById(R.id.service_center_right_main_pane).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initContentView() {
        Log.d(TAG, "initContentView: Locale - " + Locale.getDefault().getLanguage());
        Log.d(TAG, "initContentView: Temp Language - " + getLanguage());
        //mAppManager.setTempUnits(mAppManager.getUnits());       // add to set temp units to default units
        if(isFirst && !Locale.getDefault().getLanguage().equals(getLanguage())){
            isFirst = false;
            ExpressApp app = (ExpressApp) getApplication();
            setLanguage(app.getSelectedLanguage().isoCode);
            Log.d(TAG, "initContentView: isFirst set to " + app.getSelectedLanguage().isoCode);
        }
        setContentView(R.layout.activity_service_center);

        mSystemDataText1=(TextView) findViewById(R.id.service_center_system_data_text1);
        mSystemDataText2=(TextView) findViewById(R.id.service_center_system_data_text2);
        mSystemDataText3=(TextView) findViewById(R.id.service_center_system_data_text3);
        mSystemDataText4=(TextView) findViewById(R.id.service_center_system_data_text4);
        //mWaterTemperature=(TextView) findViewById(R.id.service_center_water_temperature);
        //mConcentrateTemperature=(TextView) findViewById(R.id.service_center_concentrate_temperature);

        mApp=(ExpressApp)getApplication();
		mMachine=mApp.getMachineManager();
		mAppManager=mApp.getApplicationManager();
    }

    @Override
    protected void initData() {
        boolean[] operators = SharedPreferencesUtil.getInstance().
                getEnalbleArray(Common.OPERATOR_ACCESS_ARRAY);
        if(operators.length >= 10){
            mSetPortionsTv.setEnabled(operators[9]);
        }
        updateDispenserLock();
    }


    void updateDispenserLock()
    {
        isLock = mMachine.getDispenseLockout();
        if(!isLock) {
            Drawable drawable = getResources().getDrawable(R.drawable.good_unlock);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            mLockTv.setCompoundDrawables(null, drawable, null ,null);
            mLockTv.setText(R.string.unlock_dispenser);
            mProductSelectionTv.setEnabled(false);
            mRinseTv.setEnabled(false);
        }
        else {
            Drawable drawable = getResources().getDrawable(R.drawable.good_lock);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            mLockTv.setCompoundDrawables(null, drawable, null ,null);
            mLockTv.setText(R.string.lock_dispenser);
            mProductSelectionTv.setEnabled(true);
            mRinseTv.setEnabled(true);
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        initData();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mApp.getSalesDatabase().save();
        clearTimeout();
        mHandler.post(mUpdateSystemDataText);
    }


    @Override
    protected void initView() {
        findViewById(R.id.tv_center_button_0).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_1).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_2).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_3).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_4).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_5).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_6).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_7).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_8).setOnClickListener(numClick);
        findViewById(R.id.tv_center_button_9).setOnClickListener(numClick);
        findViewById(R.id.ll_bottom_1).setOnClickListener(this);
        findViewById(R.id.ll_tutorials).setOnClickListener(this);
        findViewById(R.id.tv_clear).setOnClickListener(this);
		findViewById(R.id.service_center_rinse).setOnClickListener(this);
		findViewById(R.id.service_center_set_portions_tv).setOnClickListener(this);
        mLockTv = (TextView)findViewById(R.id.service_center_lock_tv);
        mLockTv.setOnClickListener(this);
        mSetPortionsTv = (TextView)findViewById(R.id.service_center_set_portions_tv);
        mProductSelectionTv = (TextView)findViewById(R.id.ll_bottom_1);
        mRinseTv = (TextView)findViewById(R.id.service_center_rinse);
        mPinContentTv = (TextView)findViewById(R.id.tv_pin_content);
        mPinContentTv.setBackgroundResource(R.drawable.shape_pin);
        mTitleTv = (TextView)findViewById(R.id.tv_title);
        //mTitleTv.setText(getString(R.string.setup_service_center));
        mTitleTv.setText(getString(R.string.service_center_no_info));
    }


    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: Check locale " + Locale.getDefault().getLanguage());
        Log.d(TAG, "onDestroy: Check Temp Language " + getLanguage());

        /* fix language reset to default on exit if different language selected in technician screens */
        if(!Locale.getDefault().getLanguage().equals(getLanguage())){
            Log.d(TAG, "onDestroy: Language needs reset");
            if(getLanguage().equals("en")) {
                Log.d("LANGUAGE","setLanguage(mApp.getLanguageByIsoCode(\"fr\").isoCode)");
                setLanguage(mApp.getLanguageByIsoCode("fr").isoCode);
                //mAppManager.setUnits(ApplicationManager.UNITS.METRIC);
            } else {
                Log.d("LANGUAGE","setLanguage(mApp.getLanguageByIsoCode(\"en\").isoCode)");
                setLanguage(mApp.getLanguageByIsoCode("en").isoCode);
                //mAppManager.setUnits(ApplicationManager.UNITS.IMPERIAL);
            }
            Log.d(TAG, "onDestroy: Language updated to " + getLanguage());
        }
        mAppManager.setTempUnits(mAppManager.getUnits());       // add to set temp units to default units
        Log.d(TAG, "onDestroy: Units set to " + mAppManager.getTempUnits());

        mPinContentTv.removeCallbacks(clearRunable);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_back || id == R.id.tv_home) {
            if(!mApp.getApplicationManager().getFrontendState().equals(ApplicationManager.FRONTEND_STATE.PRODUCT_CHANGED)){
                mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.DEFAULT);
            }
            stopTimeout();
            //ActivityUtil.closeAllActivity();
            finish();

            final Activity self=this;

            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    startActivity(new Intent(self, BlankActivity.class));
                }
            }, 50);
        }else if (id == R.id.ll_bottom_1){
            mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.PRODUCT_CHANGED);
            startActivity(new Intent(getApplication(),ProductSelectionActivity.class));
        }else if (id == R.id.ll_tutorials){
            startActivity(new Intent(getApplication(),TutorialsActivity.class));
        }else if (id == R.id.tv_clear){
            mPinContentTv.setText(pinStr);
        }else if(id == R.id.service_center_lock_tv){
			boolean state=mMachine.getDispenseLockout();
			state=!state;
			mMachine.setDispenseLockout(state);
			mMachine.save();
            stopTimeout();
            finish();
        }
		else if(id == R.id.service_center_rinse)
		{
			mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.RINSE);
            mApp.getApplicationManager().getRinseStatus().makeDaily();
            mApp.getApplicationManager().getRinseStatus().runCallbackIfNeeded();
            stopTimeout();
            ActivityUtil.closeAllActivity();
		}
		else if(id == R.id.service_center_set_portions_tv)
		{
			mApp.getApplicationManager().setFrontendState(ApplicationManager.FRONTEND_STATE.SET_PORTIONS);
            stopTimeout();
            ActivityUtil.closeAllActivity();
		}
    }

    class NumClick implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            String num = "";
            int i = view.getId();
            if (i == R.id.tv_center_button_1) {
                num = "1";
            } else if (i == R.id.tv_center_button_2) {
                num = "2";
            }else if(i == R.id.tv_center_button_3){
                num = "3";
            }else if(i == R.id.tv_center_button_4){
                num = "4";
            }else if(i == R.id.tv_center_button_5){
                num = "5";
            }else if(i == R.id.tv_center_button_6){
                num = "6";
            }else if(i == R.id.tv_center_button_7){
                num = "7";
            }else if(i == R.id.tv_center_button_8){
                num = "8";
            }else if(i == R.id.tv_center_button_9){
                num = "9";
            }else if(i == R.id.tv_center_button_0){
                num = "0";
            }
            String text = mPinContentTv.getText().toString() + num;
            mPinContentTv.setText(text);
            if (text.length() == pinStr.length() + 4){
                text = text.substring(pinStr.length());
                if (text.equals("1234")){
                    startActivity(new Intent(getApplication(),TechnicianScreenActivity.class));
                    mPinContentTv.postDelayed(clearRunable, 2000);
                }else if (text.equals("2580")){
                    startActivity(new Intent(getApplication(),CalibrationAccessActivity.class));
                    mPinContentTv.postDelayed(clearRunable, 2000);
                }else if (text.equals("7777")){
                    startActivity(new Intent(getApplication(),OperatorAccessActivity.class));
                    mPinContentTv.postDelayed(clearRunable, 2000);
                }else {
                    mPinContentTv.setText(pinStr);
                }
            }
        }
    };

    void appendText(TextView view, String text)
    {
        view.append(text+"\n");
    }

    void appendText(TextView view, String name, double value)
    {
        view.append(name+":\t"+Math.round(value*100.f)/100.f+"\n");
    }

    void appendText(TextView view, String name, String text)
    {
        view.append(name+":\t"+text+"\n");
    }

    void appendText(TextView view, String name, boolean value)
    {
        String text="Off";
        if (value==true)
            text="On";

        view.append(name+":\t"+text+"\n");
    }

    void appendMotor(TextView view, String name, EventStatusResponse.MotorFaults faults)
    {
        appendText(view, "            "+name+":");
        appendText(view, "UnderVoltage", faults.stepperUVLO);
        appendText(view, "ThermalWarn", faults.stepperTHWRN);
        appendText(view, "ThermalCutOff", faults.stepperTHSD);
        appendText(view, "OverCurrent", faults.stepperOCD);
        appendText(view, "HomeSenseError", faults.homeSenseError);
        appendText(view, "WaterSolError", faults.waterError);
        appendText(view, "");
    }

	AverageUtil mAverageWater=new AverageUtil(60*5, 41.0), mAverageConcentrate=new AverageUtil(60*5, 41.0);

    Runnable mUpdateSystemDataText = new Runnable()
    {
        @Override
        public void run()
        {
            boolean isRefrigError = false;

            mHandler.postDelayed(mUpdateSystemDataText, 1000);
            Log.d(TAG, "update system data text");

            mSystemDataText1.setText("");
            mSystemDataText2.setText("");
            mSystemDataText3.setText("");
            mSystemDataText4.setText("");
			//mWaterTemperature.setText(R.string.updating);
			//mConcentrateTemperature.setText(R.string.updating);
            //boolean[] booleen = SharedPreferencesUtil.getInstance()
             //       .getEnalbleArray(Common.OPERATOR_ACCESS_ARRAY);
            //if(booleen.length >= 9){
            //    mSetPortionsTv.setEnabled(booleen[9]);
            //}
            EventStatusResponse status=mMachine.getStatus();

            if (status==null)
            {
                Log.d(TAG, "Status packet is null");
				mSystemDataText1.setText(R.string.comm_error);
                return;
            }

            if((mMachine.getFaultMatrix().getSlotFaultStatus(ProductSlot.SLOT1)==true)||(mMachine.getFaultMatrix().getSlotFaultStatus(ProductSlot.SLOT2)==true)||
                    (mMachine.getFaultMatrix().getSlotFaultStatus(ProductSlot.SLOT3)==true)||(mMachine.getFaultMatrix().getSlotFaultStatus(ProductSlot.SLOT4)==true))
            {
                ((ImageView) findViewById(R.id.service_center_ready_icon)).setImageResource(R.drawable.service_center_notready);
                ((TextView)findViewById(R.id.service_center_ready_status)).setText(R.string.not_ready);
                ((TextView)findViewById(R.id.service_center_ready_status)).setTextColor(getResources().getColor(android.R.color.holo_red_dark));

            }
            else if((mMachine.getFaultMatrix().getSlotWarnStatus(ProductSlot.SLOT1)==true)||(mMachine.getFaultMatrix().getSlotWarnStatus(ProductSlot.SLOT2)==true)||
                    (mMachine.getFaultMatrix().getSlotWarnStatus(ProductSlot.SLOT3)==true)||(mMachine.getFaultMatrix().getSlotWarnStatus(ProductSlot.SLOT4)==true))
            {
                ((ImageView) findViewById(R.id.service_center_ready_icon)).setImageResource(R.drawable.service_center_warning);
                ((TextView)findViewById(R.id.service_center_ready_status)).setText(R.string.warning2);
                ((TextView)findViewById(R.id.service_center_ready_status)).setTextColor(getResources().getColor(com.nestle.express.R.color.color_pink_yellow));
            }
            else{
                ((ImageView) findViewById(R.id.service_center_ready_icon)).setImageResource(R.drawable.service_center_01);
                ((TextView)findViewById(R.id.service_center_ready_status)).setText(R.string.ready);
                ((TextView)findViewById(R.id.service_center_ready_status)).setTextColor(getResources().getColor(android.R.color.holo_green_dark));
            }

            if(status.isFaultWater1()||status.isFaultWater2()||status.isFaultConcentrate()||status.isFlagWaterSolErr()||
               status.isFlagConcSolErr()||status.isFlagCompErr()||status.isFlagCabFanError()||status.isFlagOvertempErr()) {
                isRefrigError = true;
            }

            double water=mAverageWater.updateAverage(status.getWater1());

            double concentrate=mAverageConcentrate.getAverage();
            if (!status.isFlagDefrost())
                concentrate=mAverageConcentrate.updateAverage(status.getConcentrate());

			//mWaterTemperature.setText(ConversionFunctions.formatTemperature(mAppManager, water));

			//if (!mAverageConcentrate.isUninitialized())
				//mConcentrateTemperature.setText(ConversionFunctions.formatTemperature(mAppManager, concentrate));

            //appendText(mSystemDataText1, "Water1", status.getWater1());
            appendText(mSystemDataText1, "Water1", ConversionFunctions.formatTemperature(mAppManager, status.getWater1()));
            appendText(mSystemDataText1, "Water1SetP", ConversionFunctions.formatTemperature(mAppManager, mMachine.getWater1Setpoint()));
            //appendText(mSystemDataText1, "Water2", status.getWater2());
            //appendText(mSystemDataText1, "Concentrate", status.getConcentrate());
            appendText(mSystemDataText1, "");
            appendText(mSystemDataText1, "DefrostCycle", status.isFlagDefrost());
            appendText(mSystemDataText1, "AntiShortCycle", status.isFaultShort());
            appendText(mSystemDataText1, "Water1Call", status.isCallWater1());
            appendText(mSystemDataText1, "Water2Call", status.isCallWater2());
            appendText(mSystemDataText1, "ConcCall", status.isCallConcentrate());
            appendText(mSystemDataText1, "");
            appendText(mSystemDataText1, "Water1Fault", status.isFaultWater1());
            appendText(mSystemDataText1, "Water2Fault", status.isFaultWater2());
            appendText(mSystemDataText1, "ConcThermFault", status.isFaultConcentrate());
            appendText(mSystemDataText1, "WaterRefSolErr", status.isFlagWaterSolErr());
            appendText(mSystemDataText1, "ConcRefSolErr", status.isFlagConcSolErr());
			appendText(mSystemDataText1, "CompErr", status.isFlagCompErr());
			//appendText(mSystemDataText1, "CompFanErr", status.isFlagCompFanErr());
            appendText(mSystemDataText1, "CabFanErr", status.isFlagCabFanError());
			appendText(mSystemDataText1, "SlowCoolWarning", status.isFlagCoolErr());
			appendText(mSystemDataText1, "ConcOvertempErr", status.isFlagOvertempErr());

            appendText(mSystemDataText2, "Water2", ConversionFunctions.formatTemperature(mAppManager, status.getWater2()));
            appendText(mSystemDataText2, "Water2SetP", ConversionFunctions.formatTemperature(mAppManager, mMachine.getWater2Setpoint()));
            appendText(mSystemDataText2, "");
            appendText(mSystemDataText2, "W1SolCurrent", status.getCurrentW1());
            appendText(mSystemDataText2, "W2SolCurrent", status.getCurrentW2());
            appendText(mSystemDataText2, "W3SolCurrent", status.getCurrentW3());
            appendText(mSystemDataText2, "W4SolCurrent", status.getCurrentW4());
            appendText(mSystemDataText2, "W5SolCurrent", status.getCurrentW5());
            appendText(mSystemDataText2, "CabFanCurrent", status.getCurrentCabFan());
            appendText(mSystemDataText2, "WRefSolCurrent", status.getCurrentWSol());
            appendText(mSystemDataText2, "CRefSolCurrent", status.getCurrentCSol());
            appendText(mSystemDataText2, "");
            appendText(mSystemDataText2, "DoorSwitch", status.isIoDoor());
            appendText(mSystemDataText2, "Compressor", status.isIoCompressor());
            appendText(mSystemDataText2, "CompFan", status.isIoCompressorFan());
            appendText(mSystemDataText2, "CabinetFan", status.isIoCabinet());
            appendText(mSystemDataText2, "ConcRefSol", status.isIoConcentrate());
            appendText(mSystemDataText2, "WatRefSol", status.isIoWater());
            //appendText(mSystemDataText2, "ioFreeze", status.isIoDefrost());

            appendText(mSystemDataText3, "Concentrate", ConversionFunctions.formatTemperature(mAppManager, status.getConcentrate()));
            appendText(mSystemDataText3, "ConcSetP", ConversionFunctions.formatTemperature(mAppManager, mMachine.getConcentrateSetpoint()));
            appendText(mSystemDataText3, "");
            appendMotor(mSystemDataText3, "Slot1", status.getMotor1());
            appendMotor(mSystemDataText3, "Slot2", status.getMotor2());

            appendText(mSystemDataText4, "Refrigeration State:");
            if(isRefrigError)
                appendText(mSystemDataText4, mRefrigState[10]);
            else
                appendText(mSystemDataText4, mRefrigState[status.getRefrigState()]+" "+status.getCompressorRunTimer());
            appendText(mSystemDataText4, "");
            appendMotor(mSystemDataText4, "Slot3", status.getMotor3());
            appendMotor(mSystemDataText4, "Slot4", status.getMotor4());

            appendText(mSystemDataText1, "");
            appendText(mSystemDataText1, mMachine.mAppVersion);
            appendText(mSystemDataText2, "");
            appendText(mSystemDataText2, "IO PCB Firmware: "+status.getVersionMajor()+"."+status.getVersionMinor());
            appendText(mSystemDataText3, "SD Card DB Version: " + mMachine.getVersion());
			//appendText(mSystemDataText4, "*AV: "+Build.DISPLAY);
            appendText(mSystemDataText4, "Android OS: "+Build.VERSION.RELEASE);
        }
    };

    android.os.Handler mHandler=new Handler();

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacksAndMessages(null);
    }
}
