package com.nestle.express.frontend_setup.activity;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.ActiveFaultRowView;
import com.nestle.express.frontend_setup.view.PillButtonView;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.machine.structure.MinMaxRow;
import com.nestle.express.iec_backend.machine.structure.TemperatureHistory;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;

import java.util.ArrayList;

/**
 * Created by peng on 16/11/17.
 */

public class CheckFaultHistoryActivity extends BaseActivity{
    ExpressApp mApp;
    ApplicationManager mApplication;
    MachineManager mMachine;
    TemperatureHistory mHistory;
	PillButtonView mClear;
    TextView mWaterHigh, mWaterLow, mConcentrateHigh, mConcentrateLow;
	LinearLayout mActiveFaultList;
    private ImageView mFaultLoadingImageView;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_check_fault_history);
        mWaterHigh=(TextView) findViewById(R.id.active_fault_water_high);
        mWaterLow=(TextView) findViewById(R.id.active_fault_water_low);
        mConcentrateHigh=(TextView) findViewById(R.id.active_fault_concentate_high);
        mConcentrateLow=(TextView) findViewById(R.id.active_fault_concentrate_low);
		mActiveFaultList=(LinearLayout) findViewById(R.id.active_fault_list);
		mClear=(PillButtonView) findViewById(R.id.tv_clear_fault_history);
		mClear.setVisibility(View.VISIBLE);
		mClear.setOnClickListener(mOnClear);

        if(getLanguage().equals("en")) {
            mFaultLoadingImageView = (ImageView) findViewById(R.id.fault_loading);
        }
        else {
            mFaultLoadingImageView = (ImageView) findViewById(R.id.fault_loading_fr);
        }

        mApp=(ExpressApp) getApplication();
		mApplication=mApp.getApplicationManager();
        mMachine=mApp.getMachineManager();
        mHistory=mMachine.getTemperatureHistory();

        FaultLoadingBannerRequired();
        mHandler.post(mRunnable);
		mHandler.postDelayed(mInitialize, 1000);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_title)).setText(R.string.fault_history);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.fault_icon_sm);
    }

	String getTemperature(double d)
	{
		return ConversionFunctions.formatTemperature(mApplication, d);
	}

    Handler mHandler=new Handler();
    Runnable mRunnable=new Runnable()
    {
        @Override
        public void run()
        {
            MinMaxRow water1=mHistory.getRangeWater1();
            MinMaxRow water2=mHistory.getRangeWater1();
            MinMaxRow concentrate=mHistory.getRangeWater1();

			double max=water1.max;
			if (water2.max>water1.max)
				max=water2.max;

			double min=water1.min;
			if (water2.min>water1.min)
				max=water2.min;

			mWaterHigh.setText(getTemperature(max));
            mWaterLow.setText(getTemperature(min));
            mConcentrateHigh.setText(getTemperature(concentrate.max));
            mConcentrateLow.setText(getTemperature(concentrate.min));

			mHandler.postDelayed(this, 1000);
        }
    };

	Runnable mInitialize=new Runnable()
	{
		@Override
		public void run()
		{
			ArrayList<DatabaseFault> faults=mApp.getDatabase().getFaults();

			if (faults!=null)
			{
				for(DatabaseFault fault:faults)
				{
					ActiveFaultRowView faultRow1=new ActiveFaultRowView(getBaseContext(), fault);
					mActiveFaultList.addView(faultRow1);
				}
			}
			FaultLoadingBannerDone();
		}
	};

    private void FaultLoadingBannerRequired() {
        mFaultLoadingImageView.setVisibility(View.VISIBLE);
        Log.d("Fault Loading Banner", " VISIBLE");
    }

    private void FaultLoadingBannerDone() {
        mFaultLoadingImageView.setVisibility(View.GONE);
        Log.d("Fault Loading Banner", " GONE");
    }

	View.OnClickListener mOnClear = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			mApp.getDatabase().clearFault();
			mActiveFaultList.removeAllViews();
		}
	};
}
