package com.nestle.express.frontend_setup.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.ActivityUtil;

/**
 * Created by jason on 5/1/17.
 */
public class BlankActivity extends Activity
{
	Runnable mKill = new Runnable()
	{
		@Override
		public void run()
		{
			ActivityUtil.closeAllActivity();
			mHandler.postDelayed(mFinish, 10);
		}
	};

	Runnable mFinish = new Runnable()
	{
		@Override
		public void run()
		{
			finish();
		}
	};

	Handler mHandler=new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_blank);

		Log.d("BlankActivity", "BLANK");

		mHandler.postDelayed(mKill, 10);
	}
}
