package com.nestle.express.frontend_setup.activity;

import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.communications.protocol_layer.events.EventIORequest;
import com.nestle.express.communications.protocol_layer.events.EventStatusResponse;
import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.machine.MachineManager;

/**
 * Created by peng on 16/11/17.
 */
public class FunctionalityCheckActivity extends BaseActivity{
    final String TAG="FunctionalityCheckAct";
    final int TEST_DOOR_LED_BOTTOM=0, TEST_PUMP1=1, TEST_PUMP2=2,
            TEST_PUMP3=3, TEST_PUMP4=4, TEST_VALVE1=5, TEST_VALVE2=6, TEST_VALVE3=7,
            TEST_VALVE4=8, TEST_VALVE5=9, TEST_EVAP_FAN=10, TEST_COND_FAN=11,
            TEST_WATER_REF_SOL=12, TEST_CONC_REF_SOL=13, TEST_COMPRESSOR=14;

    int ids[]=new int[] {R.id.testDoorLEDBottom, R.id.testPump1,
            R.id.testPump2, R.id.testPump3, R.id.testPump4, R.id.testValve1, R.id.testValve2,
            R.id.testValve3, R.id.testValve4, R.id.testValve5, R.id.testEvapFan,
            R.id.testCondFan, R.id.testWaterRefSol, R.id.testConcRefSol,
            R.id.testCompressor};
            //R.id.testCompressor, R.id.testWaterCall, R.id.testConcentrateCall};

    View[] buttons=new View[ids.length];
    View buttonDoorSwitch;
    View systemData;
    MachineManager mMachineManager;

    TextView mSystemDataText1, mSystemDataText2, mSystemDataText3, mSystemDataText4;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_functionality_check);

        mSystemDataText1=(TextView) findViewById(R.id.FunctionalityTextViewCol1);
        mSystemDataText2=(TextView) findViewById(R.id.FunctionalityTextViewCol2);
        mSystemDataText3=(TextView) findViewById(R.id.FunctionalityTextViewCol3);
        mSystemDataText4=(TextView) findViewById(R.id.FunctionalityTextViewCol4);

        for(int i=0; i<ids.length; i++)
            buttons[i]=findViewById(ids[i]);

        buttonDoorSwitch=findViewById(R.id.testDoorSwitch);
        systemData = findViewById(R.id.system_data);
        systemData.setOnClickListener(mSystemData);

        //buttons[TEST_DOOR_LED_TOP].setOnClickListener(mTestDoorLEDTop);
        //buttons[TEST_DOOR_LED_BOTTOM].setOnClickListener(mTestDoorLEDBottom);
        //buttons[TEST_PUMP1].setOnClickListener(mTestPump1);
        //buttons[TEST_PUMP2].setOnClickListener(mTestPump2);
        //buttons[TEST_PUMP3].setOnClickListener(mTestPump3);
        //buttons[TEST_PUMP4].setOnClickListener(mTestPump4);
        //buttons[TEST_VALVE1].setOnClickListener(mTestValve1);
        //buttons[TEST_VALVE2].setOnClickListener(mTestValve2);
        //buttons[TEST_VALVE3].setOnClickListener(mTestValve3);
        //buttons[TEST_VALVE4].setOnClickListener(mTestValve4);
        //buttons[TEST_VALVE5].setOnClickListener(mTestValve5);
        //buttons[TEST_EVAP_FAN].setOnClickListener(mTestEvapFan);
        //buttons[TEST_HEATER].setOnClickListener(mTestHeater);
        //buttons[TEST_COND_FAN].setOnClickListener(mTestCondFan);
        //buttons[TEST_WATER_REF_SOL].setOnClickListener(mTestWaterRefSol);
        //buttons[TEST_CONC_REF_SOL].setOnClickListener(mTestConcRefSol);
        //buttons[TEST_WATER_CALL].setOnClickListener(mTestWater);
        //buttons[TEST_CONCENTRATE_CALL].setOnClickListener(mTestConcentrate);
        //buttons[TEST_COMPRESSOR].setOnClickListener(mTestCompressor);

        // change to onTouchListener for press and hold activation
        buttons[TEST_DOOR_LED_BOTTOM].setOnTouchListener(mTestDoorLEDBottom);
        buttons[TEST_PUMP1].setOnTouchListener(mTestPump1);
        buttons[TEST_PUMP2].setOnTouchListener(mTestPump2);
        buttons[TEST_PUMP3].setOnTouchListener(mTestPump3);
        buttons[TEST_PUMP4].setOnTouchListener(mTestPump4);
        buttons[TEST_VALVE1].setOnTouchListener(mTestValve1);
        buttons[TEST_VALVE2].setOnTouchListener(mTestValve2);
        buttons[TEST_VALVE3].setOnTouchListener(mTestValve3);
        buttons[TEST_VALVE4].setOnTouchListener(mTestValve4);
        buttons[TEST_VALVE5].setOnTouchListener(mTestValve5);
        buttons[TEST_EVAP_FAN].setOnTouchListener(mTestEvapFan);
        //buttons[TEST_HEATER].setOnTouchListener(mTestHeater);
        buttons[TEST_COND_FAN].setOnTouchListener(mTestCondFan);
        buttons[TEST_WATER_REF_SOL].setOnTouchListener(mTestWaterRefSol);
        buttons[TEST_CONC_REF_SOL].setOnTouchListener(mTestConcRefSol);
        buttons[TEST_COMPRESSOR].setOnTouchListener(mTestCompressor);

        buttonDoorSwitch.setClickable(false);

		mHandler.post(mUpdateDoorSwitch);
    }

    @Override
    protected void initData()
    {

    }

    @Override
    protected void initView() {
        mMachineManager=((ExpressApp) getApplication()).getMachineManager();
        //mgv = (MyGridView)findViewById(R.id.mgv);
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.functionality_check);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_12);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);

        mMachineManager.startIO(new EventIORequest());
    }

    void unlockButton(int index)
    {
        buttons[index].setAlpha(1.0f);
        buttons[index].setClickable(true);
        buttons[index].setPressed(false);
        buttons[index].setSelected(false);
        buttons[index].setActivated(false);
    }

    void unlockAllButtons()
    {
        for(int i=0; i<ids.length; i++)
            unlockButton(i);
    }

    void lockoutForTime(int milliseconds, int enabledIndex)
    {
        mActiveButtonIndex=enabledIndex;
        mHandler.postDelayed(mSetButtonActive, 1);

        for(int i=0; i<ids.length; i++)
            lockButton(i);

        mHandler.postDelayed(mUnlockRunnable, milliseconds);
    }

    void lockoutButtons(int enabledIndex)
    {
        mActiveButtonIndex=enabledIndex;
        mHandler.postDelayed(mSetButtonActive, 1);

        for(int i=0; i<ids.length; i++) {
            if( i != enabledIndex)
                lockButton(i);
        }
    }

    void lockButton(int index)
    {
        buttons[index].setAlpha(0.3f);
        buttons[index].setClickable(false);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mMachineManager.stopIO();
		mHandler.removeCallbacks(mUnlockRunnable);
    }

    Handler mHandler=new Handler();
    EventIORequest mStopRequest=new EventIORequest();
    Runnable mUnlockRunnable=new Runnable()
    {
        @Override
        public void run()
        {
            unlockAllButtons();
            mStopRequest.motorEnabled=false;
            mStopRequest.valveEnabled=false;
            EventIORequest request=new EventIORequest();
            request.motorTest=mStopRequest.motorTest;
            request.valveTest=mStopRequest.valveTest;
            mMachineManager.startIO(request);
        }
    };

    int mActiveButtonIndex;
    Runnable mSetButtonActive=new Runnable()
    {
        @Override
        public void run()
        {
            buttons[mActiveButtonIndex].setAlpha(1.0f);
            buttons[mActiveButtonIndex].setPressed(true);
            buttons[mActiveButtonIndex].setSelected(true);
            buttons[mActiveButtonIndex].setActivated(true);
        }
    };

	void sendIORequest(EventIORequest request)
	{
        mMachineManager.startIO(request);
		mStopRequest=request;
	}

    void sendStopIORequest()
    {
        unlockAllButtons();
        mStopRequest.motorEnabled=false;
        mStopRequest.valveEnabled=false;
        EventIORequest request=new EventIORequest();
        request.motorTest=mStopRequest.motorTest;
        request.valveTest=mStopRequest.valveTest;
        mMachineManager.startIO(request);
    }

    final int DEFAULT_TEST_TIME=0, MOTOR_TEST_TIME=0, COMPRESSOR_TEST_TIME=30000;
    /*View.OnClickListener mTestDoorLEDTop=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_DOOR_LED_TOP);
            EventIORequest request=new EventIORequest();
            request.testDoorLED=true;
			sendIORequest(request);
        }
    };*/

    /*View.OnClickListener mTestDoorLEDBottom=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_DOOR_LED_BOTTOM);
            EventIORequest request=new EventIORequest();
            request.testDispenseLED =true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestDoorLEDBottom=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_DOOR_LED_BOTTOM].isClickable())  {
                // start action
                lockoutButtons(TEST_DOOR_LED_BOTTOM);
                EventIORequest request=new EventIORequest();
                request.testDispenseLED =true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_DOOR_LED_BOTTOM].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestPump1=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_PUMP1);
            EventIORequest request=new EventIORequest();
            request.motorTest=1;
            request.motorEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestPump1=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_PUMP1].isClickable())  {
                // start action
                lockoutButtons(TEST_PUMP1);
                EventIORequest request=new EventIORequest();
                request.motorTest=1;
                request.motorEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_PUMP1].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestPump2=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_PUMP2);
            EventIORequest request=new EventIORequest();
            request.motorTest=2;
            request.motorEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestPump2=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_PUMP2].isClickable())  {
                // start action
                lockoutButtons(TEST_PUMP2);
                EventIORequest request=new EventIORequest();
                request.motorTest=2;
                request.motorEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_PUMP2].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestPump3=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_PUMP3);
            EventIORequest request=new EventIORequest();
            request.motorTest=3;
            request.motorEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestPump3=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_PUMP3].isClickable())  {
                // start action
                lockoutButtons(TEST_PUMP3);
                EventIORequest request=new EventIORequest();
                request.motorTest=3;
                request.motorEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_PUMP3].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestPump4=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_PUMP4);
            EventIORequest request=new EventIORequest();
            request.motorTest=4;
            request.motorEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestPump4=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_PUMP4].isClickable())  {
                // start action
                lockoutButtons(TEST_PUMP4);
                EventIORequest request=new EventIORequest();
                request.motorTest=4;
                request.motorEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_PUMP4].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestValve1=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_VALVE1);
            EventIORequest request=new EventIORequest();
            request.valveTest=1;
            request.valveEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestValve1=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_VALVE1].isClickable())  {
                // start action
                lockoutButtons(TEST_VALVE1);
                EventIORequest request=new EventIORequest();
                request.valveTest=1;
                request.valveEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_VALVE1].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestValve2=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_VALVE2);
            EventIORequest request=new EventIORequest();
            request.valveTest=2;
            request.valveEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestValve2=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_VALVE2].isClickable())  {
                // start action
                lockoutButtons(TEST_VALVE2);
                EventIORequest request=new EventIORequest();
                request.valveTest=2;
                request.valveEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_VALVE2].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestValve3=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_VALVE3);
            EventIORequest request=new EventIORequest();
            request.valveTest=3;
            request.valveEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestValve3=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_VALVE3].isClickable())  {
                // start action
                lockoutButtons(TEST_VALVE3);
                EventIORequest request=new EventIORequest();
                request.valveTest=3;
                request.valveEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_VALVE3].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestValve4=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_VALVE4);
            EventIORequest request=new EventIORequest();
            request.valveTest=4;
            request.valveEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestValve4=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_VALVE4].isClickable())  {
                // start action
                lockoutButtons(TEST_VALVE4);
                EventIORequest request=new EventIORequest();
                request.valveTest=4;
                request.valveEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_VALVE4].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestValve5=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(MOTOR_TEST_TIME, TEST_VALVE5);
            EventIORequest request=new EventIORequest();
            request.valveTest=5;
            request.valveEnabled=true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestValve5=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_VALVE5].isClickable())  {
                // start action
                lockoutButtons(TEST_VALVE5);
                EventIORequest request=new EventIORequest();
                request.valveTest=5;
                request.valveEnabled=true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_VALVE5].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestEvapFan=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_EVAP_FAN);
            EventIORequest request=new EventIORequest();
            request.testEvapFan =true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestEvapFan=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_EVAP_FAN].isClickable())  {
                // start action
                lockoutButtons(TEST_EVAP_FAN);
                EventIORequest request=new EventIORequest();
                request.testEvapFan =true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_EVAP_FAN].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestHeater=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_HEATER);
            EventIORequest request=new EventIORequest();
            request.testHeater =true;
			sendIORequest(request);
        }
    };

    View.OnClickListener mTestCondFan=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_COND_FAN);
            EventIORequest request=new EventIORequest();
            request.testCondFan =true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestCondFan=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_COND_FAN].isClickable())  {
                // start action
                lockoutButtons(TEST_COND_FAN);
                EventIORequest request=new EventIORequest();
                request.testCondFan =true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_COND_FAN].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestWaterRefSol=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_WATER_REF_SOL);
            EventIORequest request=new EventIORequest();
            request.testWaterRefSol =true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestWaterRefSol=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_WATER_REF_SOL].isClickable())  {
                // start action
                lockoutButtons(TEST_WATER_REF_SOL);
                EventIORequest request=new EventIORequest();
                request.testWaterRefSol =true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_WATER_REF_SOL].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestConcRefSol=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(DEFAULT_TEST_TIME, TEST_CONC_REF_SOL);
            EventIORequest request=new EventIORequest();
            request.testConcRefSol =true;
			sendIORequest(request);
        }
    };*/

    View.OnTouchListener mTestConcRefSol=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_CONC_REF_SOL].isClickable())  {
                // start action
                lockoutButtons(TEST_CONC_REF_SOL);
                EventIORequest request=new EventIORequest();
                request.testConcRefSol =true;
                sendIORequest(request);
            } else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)
                    && buttons[TEST_CONC_REF_SOL].isClickable()) {
                // stop action
                sendStopIORequest();
            }

            return true;
        }
    };

    /*View.OnClickListener mTestCompressor=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if(buttons[TEST_COMPRESSOR].isClickable()) {
                lockoutForTime(COMPRESSOR_TEST_TIME, TEST_COMPRESSOR);
                EventIORequest request = new EventIORequest();
                request.compressorTest = request.CTEST_COMPRESSOR;
                sendIORequest(request);
            }
        }
    };*/

    View.OnTouchListener mTestCompressor=new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if ((motionEvent.getAction() == MotionEvent.ACTION_DOWN) && buttons[TEST_COMPRESSOR].isClickable()) {
                // start action
                lockoutForTime(COMPRESSOR_TEST_TIME, TEST_COMPRESSOR);
                EventIORequest request = new EventIORequest();
                request.compressorTest = request.CTEST_COMPRESSOR;
                sendIORequest(request);
            }

            return true;
        }
    };

    /*View.OnClickListener mTestWater=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(COMPRESSOR_TEST_TIME, TEST_WATER_CALL);
            EventIORequest request=new EventIORequest();
            request.compressorTest = request.CTEST_WATER;
			sendIORequest(request);
        }
    };

    View.OnClickListener mTestConcentrate=new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            lockoutForTime(COMPRESSOR_TEST_TIME, TEST_CONCENTRATE_CALL);
            EventIORequest request=new EventIORequest();
            request.compressorTest = request.CTEST_CONCENTRATE;
			sendIORequest(request);
        }
    };*/

    boolean isSystemDataShown=false;
    View.OnClickListener mSystemData = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            isSystemDataShown = !isSystemDataShown;
            //service_center_right_main_pane
            if (isSystemDataShown) {
                Log.d(TAG, "System Data On!");
                findViewById(R.id.FunctionalityTextViewCol1).setVisibility(View.VISIBLE);
                findViewById(R.id.FunctionalityTextViewCol2).setVisibility(View.VISIBLE);
                findViewById(R.id.FunctionalityTextViewCol3).setVisibility(View.VISIBLE);
                findViewById(R.id.FunctionalityTextViewCol4).setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "System Data Off!");
                findViewById(R.id.FunctionalityTextViewCol1).setVisibility(View.GONE);
                findViewById(R.id.FunctionalityTextViewCol2).setVisibility(View.GONE);
                findViewById(R.id.FunctionalityTextViewCol3).setVisibility(View.GONE);
                findViewById(R.id.FunctionalityTextViewCol4).setVisibility(View.GONE);
            }
        }
    };

    void appendText(TextView view, String text)
    {
        view.append(text+"\n");
    }

    void appendText(TextView view, String name, double value)
    {
        view.append(name+":\t"+Math.round(value*100.f)/100.f+"\n");
    }

    void appendText(TextView view, String name, boolean value)
    {
        String text="Off";
        if (value==true)
            text="On";

        view.append(name+":\t"+text+"\n");
    }

    void appendMotor(TextView view, String name, EventStatusResponse.MotorFaults faults)
    {
        appendText(view, name+":");
        if(faults.stepperUVLO||faults.stepperTHWRN||faults.stepperTHSD||faults.stepperOCD)
            appendText(view, "Stepper Driver Err");
        else
            appendText(view, "Stepper Driver Ok");
        appendText(view, "HomeSenseError", faults.homeSenseError);
        appendText(view, "");
    }

    Runnable mUpdateDoorSwitch = new Runnable()
	{
		@Override
		public void run()
		{
            mSystemDataText1.setText("");
            mSystemDataText2.setText("");
            mSystemDataText3.setText("");
            mSystemDataText4.setText("");

            EventStatusResponse status=mMachineManager.getStatus();
            if (status==null) {
                mSystemDataText1.setText("Communications Error");
                return;
            }

            /* add system status info */
            appendText(mSystemDataText1, "Water1", status.getWater1());
            appendText(mSystemDataText1, "Water2", status.getWater2());
            appendText(mSystemDataText1, "Concentrate", status.getConcentrate());
            appendText(mSystemDataText1, "");

            appendText(mSystemDataText2, "W1SolCurrent", status.getCurrentW1());
            appendText(mSystemDataText2, "W2SolCurrent", status.getCurrentW2());
            appendText(mSystemDataText2, "W3SolCurrent", status.getCurrentW3());
            appendText(mSystemDataText2, "W4SolCurrent", status.getCurrentW4());
            appendText(mSystemDataText2, "W5SolCurrent", status.getCurrentW5());
            appendText(mSystemDataText2, "CabFanCurrent", status.getCurrentCabFan());
            appendText(mSystemDataText2, "WRefSolCurrent", status.getCurrentWSol());
            appendText(mSystemDataText2, "CRefSolCurrent", status.getCurrentCSol());

            appendText(mSystemDataText3, "DoorSwitch", status.isIoDoor());
            appendText(mSystemDataText3, "Compressor", status.isIoCompressor());
            appendText(mSystemDataText3, "CompFan", status.isIoCompressorFan());
            appendText(mSystemDataText3, "CabinetFan", status.isIoCabinet());
            appendText(mSystemDataText3, "ConcRefSol", status.isIoConcentrate());
            appendText(mSystemDataText3, "WatRefSol", status.isIoWater());

            appendMotor(mSystemDataText4, "motor1", status.getMotor1());
            appendMotor(mSystemDataText4, "motor2", status.getMotor2());
            appendMotor(mSystemDataText4, "motor3", status.getMotor3());
            appendMotor(mSystemDataText4, "motor4", status.getMotor4());

            if (status.isIoDoor())
			{
                buttonDoorSwitch.setPressed(true);
                buttonDoorSwitch.setSelected(true);
                buttonDoorSwitch.setActivated(true);
			}
			else
			{
                buttonDoorSwitch.setPressed(false);
                buttonDoorSwitch.setSelected(false);
                buttonDoorSwitch.setActivated(false);
			}

			mHandler.postDelayed(this, 1000);
		}
	};
}
