package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 16-10-27.
 */
public class MyGridView extends ViewGroup implements View.OnClickListener {

    private List<GridItem> items = new ArrayList<>();
    private List<View> views = new ArrayList<>();
    private int height;
    private int width;
    private int point_padding = 10;
    private Paint paint = new Paint();
    private int item_height;
    private int item_width;
    private int item_padding = 20;

    private int horizontal = 5;
    private int vertical = 3;

    public int getVertical() {
        return vertical;
    }

    public void setVertical(int vertical) {
        this.vertical = vertical;
    }

    public int getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(int horizontal) {
        this.horizontal = horizontal;
    }

    private OnItemClickListener listener;

    public MyGridView(Context context) {
        this(context, null);
    }

    public MyGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public MyGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()));
        paint.setColor(Color.BLACK);
    }

    public OnItemClickListener getListener() {
        return listener;
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {
        int index = 0;
        for (int y = 0; y < vertical; y++) {
            for (int x = 0; x < horizontal; x++) {
                if (index == getChildCount())
                    return;
                int l = (x * item_width);
                int t = (y * item_height);
                getChildAt(index).layout(l + item_padding, t + item_padding, l + item_width - item_padding, t + item_height - item_padding);
                index++;
            }
        }
    }

    public void addItem(GridItem item, boolean isEnabale) {
        items.add(item);
        View view = View.inflate(getContext(), R.layout.item_my_grid, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_title);
        if (item.title != null) {
            tv.setText(item.title);
        }
        if (item.res != 0) {
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_icon);
            imageView.setImageResource(item.res);
            imageView.setEnabled(isEnabale);
        }
        tv.setEnabled(isEnabale);
        view.setEnabled(isEnabale);
        if(!isEnabale)
            view.setVisibility(View.GONE);
        view.setOnClickListener(this);
        view.setTag(item);
        views.add(view);
        addView(view);
    }

	public void addItemAtIndex(GridItem item, int index, boolean enabled) {
		items.remove(index);
		views.remove(index);
		items.add(index, item);

		View view = View.inflate(getContext(), R.layout.item_my_grid, null);
		TextView tv = (TextView) view.findViewById(R.id.tv_title);
		if (item.title != null) {
			tv.setText(item.title);
		}
		if (item.res != 0) {
			ImageView imageView = (ImageView) view.findViewById(R.id.iv_icon);
			imageView.setImageResource(item.res);
			imageView.setEnabled(enabled);
		}
		tv.setEnabled(enabled);
		view.setEnabled(enabled);
		view.setOnClickListener(this);
		view.setTag(item);
		views.add(index, view);
		addView(view, index);
	}

    public void addCheckItem(GridItem item, boolean isCheck) {
        items.add(item);
        View view;
        view = View.inflate(getContext(), R.layout.item_my_check_grid, null);
        CheckBox tv = (CheckBox) view.findViewById(R.id.tv_title);
        if (item.title != null) {
            tv.setText(item.title);
        }
        if (item.res != 0) {
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_icon);
            imageView.setImageResource(item.res);
        }
        tv.setChecked(isCheck);
        view.setSelected(isCheck);
        view.setOnClickListener(this);
        view.setTag(item);
        views.add(view);
        addView(view);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        item_height = (int) (height / 1f / vertical);
        item_width = (int) (width / 1f / horizontal);
        setMeasuredDimension(width, height);
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            getChildAt(i).measure(MeasureSpec.makeMeasureSpec(item_width - (item_padding * 2), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(item_height - (item_padding * 2), MeasureSpec.EXACTLY));
        }
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        for (int i = 1; i < vertical; i++) {
            drawPointLine(canvas, item_height * i);
        }

        for (int i = 1; i < horizontal; i++) {
            drawPointLineV(canvas, item_width * i);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        canvas.drawColor(Color.WHITE);
//
//        for (int i = 1; i < vertical; i++){
//            drawPointLine(canvas, item_height * i);
//        }
//
//        for (int i = 1; i < horizontal ; i++) {
//            drawPointLineV(canvas, item_width * i);
//        }


//        int x = 0;
//        int y = 0;
//        int index = 0;
//        paint.setTextSize(30);
//        for (GridItem item : items) {
//            if (index == horizontal){
//                y += item_height;
//                index = 0;
//            }
//            x = index * item_width;
//
//            drawItem(canvas,item,x,y);
//            index++;
//        }

    }

    private Rect rect = new Rect();
    private Rect textRect = new Rect();

    private void drawItem(Canvas canvas, GridItem item, int x, int y) {
        if (item.desc == null)
            item.desc = "";
        paint.getTextBounds(item.desc, 0, item.desc.length(), textRect);
        int text_y = y + item_height - item_padding;
        if (item.res == 0) {
            text_y = y + ((item_height + textRect.height()) / 2);
        }
        canvas.drawText(item.desc, x + ((item_width - textRect.width()) / 2), text_y, paint);
        paint.getTextBounds(item.title, 0, item.title.length(), textRect);
        canvas.drawText(item.title, x + ((item_width - textRect.width()) / 2), text_y - 10 - textRect.height(), paint);

        if (item.res != 0) {
            Bitmap bitmap = readBitmap(getContext(), item.res);
            int height = item_height - ((y + item_height) - (text_y - 10 - (textRect.height() * 2))) - 20;
            int icon_width = (item_width > height ? height : item_width);
            int left = x + ((item_width - icon_width) / 2);
            int top = y + ((item_height - icon_width) / 2);
            float offset = icon_width / (bitmap.getWidth() / 1.0f / bitmap.getHeight());
            rect.set(left, top, left + icon_width, (int) (top + offset));
            canvas.drawBitmap(bitmap, null, rect, paint);
        }
//        canvas.drawText(item.title,x + ((item_width - textRect.width()) / 2),y + item_height - 25,paint);
    }

    public static Bitmap readBitmap(Context context, int resId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inSampleSize = 1;
        return BitmapFactory.decodeResource(context.getResources(), resId, options);
    }

    private void drawPointLine(Canvas canvas, int y) {
        int pointOffset = 0;
        while (pointOffset < width) {
            canvas.drawCircle(pointOffset, y, 1.3f, paint);
            pointOffset += point_padding;
        }
    }

    private void drawPointLineV(Canvas canvas, int x) {
        int pointOffset = 0;
        while (pointOffset < height) {
            canvas.drawCircle(x, pointOffset, 1.3f, paint);
            pointOffset += point_padding;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                break;
            case MotionEvent.ACTION_UP:

                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void onClick(View view) {
        View view1 = view.findViewById(R.id.tv_title);
        if (view1 instanceof CheckBox) {
            ((CheckBox) view1).setChecked(!((CheckBox) view1).isChecked());
            view.setSelected(!view.isSelected());
        }
        if (listener != null) {
            GridItem item = (GridItem) view.getTag();
            listener.onItemClick(item);
        }
    }

    public boolean[] getCheckArray() {
        int i = 0;
        boolean[] integers = new boolean[15];
        for (View view : views) {
            View mCheckBox = view.findViewById(R.id.tv_title);
            if(mCheckBox != null && mCheckBox instanceof CheckBox){
                integers[i] = ((CheckBox)mCheckBox).isChecked();
            }
            i++;
        }
        return integers;
    }

    public static class GridItem {
        int res, text_color;
        String title, desc;
        int position;

        public void setTitleDesc(String string) {
            if (string != null) {
                title = string;
//                String[] split = string.split("\n");
//                if (split != null && split.length > 0) {
//                    title = split[0];
//                    if (split.length > 1)
//                        desc = split[1];
//                } else {
//                    title = "";
//                    desc = "";
//                }
            }
        }

        public int getText_color() {
            return text_color;
        }

        public void setText_color(int text_color) {
            this.text_color = text_color;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public int getRes() {
            return res;
        }

        public void setRes(int res) {
            this.res = res;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public interface OnItemClickListener {
        void onItemClick(GridItem item);
    }
}
