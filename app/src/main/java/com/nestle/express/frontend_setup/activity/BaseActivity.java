package com.nestle.express.frontend_setup.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.ActivityUtil;
import com.nestle.express.frontend_setup.utils.CountDownUtil;
import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.product.structure.DatabaseLanguage;

import java.util.Locale;


/**
 * Created by root on 16-10-26.
 */
public abstract class BaseActivity extends FragmentActivity implements View.OnClickListener, View.OnTouchListener
{
	ExpressApp mApp;

    ApplicationManager mAppManager;
    private static final String TAG = "BaseActivity";
    private Handler handler = new Handler();
    public static final String CHANGE_LANGUAGE = "change_language";
    static public final String FRENCH_ISO_CODE = "fr";
    static public final String ENGLISH_ISO_CODE = "en";
    static public final String SPANISH_ISO_CODE = "es";
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            changeLanguage();
        }
    };

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent)
	{
		Log.d(TAG, "Touch");
		clearTimeout();

		return false;
	}

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityUtil.addActivity(this);
        LocalBroadcastManager.getInstance(
                getApplicationContext()).registerReceiver(broadcastReceiver,
                new IntentFilter(CHANGE_LANGUAGE));
        setFullscreen();
		mApp=(ExpressApp) getApplication();
		mAppManager=mApp.getApplicationManager();

		initContentView();
		initPubView();
		initView();
		initData();
    }

	void clearTimeout()
	{
		CountDownUtil.getInstance().stopCountDown();
		CountDownUtil.getInstance().startCountDown(getApplicationContext(), mAppManager.getTimeoutSettingsMenu());
	}

	public void stopTimeout()
	{
		CountDownUtil.getInstance().stopCountDown();
	}

    private void initPubView() {
		clearTimeout();

        if (this instanceof ServiceCenterActivity) {
            findViewById(R.id.rg_language).setVisibility(View.VISIBLE);
        }

        if(this instanceof StaionFaultActivity){
            CountDownUtil.getInstance().stopCountDown();
        }else {
			clearTimeout();
		}

        try {
            findViewById(R.id.tv_back).setOnClickListener(this);
            findViewById(R.id.tv_home).setOnClickListener(this);
            Log.d("BaseActivity", "initPubView: " + getLanguage());
            ((RadioGroup) findViewById(R.id.rg_language))
                    .check(getLanguage().equals("en") ? R.id.rb_language_en : R.id.rb_language_fr);
            ((RadioGroup) findViewById(R.id.rg_language))
                    .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            if (checkedId == R.id.rb_language_en) {
                                setLanguage(mApp.getLanguageByIsoCode("en").isoCode);
								//mAppManager.setUnits(ApplicationManager.UNITS.IMPERIAL);
                                mAppManager.setTempUnits(ApplicationManager.UNITS.IMPERIAL);
                                Log.d("BaseActivity", "initPubView: Units set to " + mAppManager.getTempUnits());
                            } else if (checkedId == R.id.rb_language_fr) {
                                setLanguage(mApp.getLanguageByIsoCode("fr").isoCode);
								//mAppManager.setUnits(ApplicationManager.UNITS.METRIC);
                                mAppManager.setTempUnits(ApplicationManager.UNITS.METRIC);
                                Log.d("BaseActivity", "initPubView: Units set to " + mAppManager.getTempUnits());
                            }
                        }
                    });
        } catch (Exception e) {

        }

    }

    void setFullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//mHandler.postDelayed(mFullscreenRunnable, 1000);
    }

    protected void initContentView()
    {

    };

    protected void initData()
    {

    };

    protected void initView()
    {

    };



    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tv_back) {
            stopTimeout();
            finish();
        } else if (id == R.id.tv_home) {
			stopTimeout();
			//ActivityUtil.closeAllActivity();
			finish();

			final Activity self=this;

			mHandler.postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					startActivity(new Intent(self, BlankActivity.class));
				}
			}, 50);
		}
    }

    private void changeLanguage() {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        // 应用用户选择语言
        config.locale = new Locale(getLanguage());

        resources.updateConfiguration(config, dm);
        initContentView();
        initPubView();
        initView();
        initData();
    }

    public void onEnglish() {
        Locale locale = new Locale(ENGLISH_ISO_CODE);
        onLocaleSelected(locale);
    }

    public void onFrench() {
        Locale locale = new Locale(FRENCH_ISO_CODE);
        onLocaleSelected(locale);
    }

    public void onSpanish() {
        Locale locale = new Locale(SPANISH_ISO_CODE);
        onLocaleSelected(locale);
    }

    public void setLanguage(String language) {
		DatabaseLanguage lang=mApp.getLanguageByIsoCode(language);
		mApp.setSelectedLanguage(lang);
        SharedPreferencesUtil.getInstance().setLanguage(lang.getIsoCode());
    }

    protected void onLocaleSelected(Locale locale) {
        if (locale != null
                && !locale.getLanguage().equalsIgnoreCase(SharedPreferencesUtil.getInstance().getCurrentLocale(this))) {
            setLocaleValues(locale);

            ExpressApp.setLocale(this, locale);
            Intent intent = new Intent(getApplicationContext(), this.getClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    private void setLocaleValues(Locale locale) {
        SharedPreferencesUtil.getInstance().saveCurrentLocale(this, locale.getLanguage());
        //SharedPreferencesUtil.getInstance().saveCurrentTimeLocaleChanged(this, System.currentTimeMillis());
        //SharedPreferencesUtil.getInstance().saveDelayToResetDefaultLocale(this, 1000 * 60 * 1);
    }

    public String getLanguage() {
        return SharedPreferencesUtil.getInstance().getLanguage();
    }

    protected void onResume() {
        super.onResume();
		setFullscreen();
		Log.d(TAG, "Resume");
		View v=getWindow().getDecorView().findViewById(android.R.id.content);
		if (v!=null)
			v.setOnTouchListener(this);
    }

    @Override
    protected void onDestroy() {
        stopTimeout();
        super.onDestroy();
        ActivityUtil.delActivity(this);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
	protected void onPause()
	{
		super.onPause();
		mHandler.removeCallbacks(mFullscreenRunnable);
	}

	Handler mHandler = new Handler();
	Runnable mFullscreenRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			setFullscreen();
		}
	};
}
