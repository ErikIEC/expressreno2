package com.nestle.express.frontend_setup.utils;

import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.structure.RowSale;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by peng on 16-12-29.
 */

public class SaleTimeUtil {
    public static int[] getSaleDayDate(RowSale[] rowSales, int productId, int displayType, ApplicationManager.UNITS units) {
        long mNowDay = System.currentTimeMillis() / (24 * 60 * 60 * 1000);
        int[] mWeekData = new int[7];
        double volume;

        for (RowSale rowSale : rowSales) {
            if (rowSale.productId != productId) continue;

            if(displayType == 1)
                volume = finishedVolume(rowSale);
            else
                volume = productDayVolume(rowSale);

            if(units == ApplicationManager.UNITS.METRIC)
                volume=ConversionFunctions.convertOztoMl(volume);

            switch ((int) mNowDay - rowSale.unixDayNumber) {
                case 0:
                    mWeekData[6] += volume;
                    break;
                case 1:
                    mWeekData[5] += volume;
                    break;
                case 2:
                    mWeekData[4] += volume;
                    break;
                case 3:
                    mWeekData[3] += volume;
                    break;
                case 4:
                    mWeekData[2] += volume;
                    break;
                case 5:
                    mWeekData[1] += volume;
                    break;
                case 6:
                    mWeekData[0] += volume;
                    break;

            }
        }
        return mWeekData;
    }

    public static int[] getSaleWeekDate(RowSale[] rowSales, int productId, int displayType, ApplicationManager.UNITS units) {
        long mNowDay = System.currentTimeMillis() / (24 * 60 * 60 * 1000);
        int[] mWeekVol= new int[7];
        double[] mWeekSales = new double[7];
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(System.currentTimeMillis()));
        int w = cal.get(Calendar.DAY_OF_WEEK);
        double volume;

        for (RowSale rowSale : rowSales) {
            if (rowSale.productId != productId) continue;

            if(displayType == 1) {
                volume = finishedVolume(rowSale);
                if (units == ApplicationManager.UNITS.METRIC)
                    volume = ConversionFunctions.convertOztoMl(volume);
            }
            else
                volume = productBottleVolume(rowSale);

            if ((int) mNowDay - rowSale.unixDayNumber < w) {
                mWeekSales[6] += volume;
            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 1 * 7) {
                mWeekSales[5] += volume;

            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 2 * 7) {
                mWeekSales[4] += volume;

            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 3 * 7) {
                mWeekSales[3] += volume;

            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 4 * 7) {
                mWeekSales[2] += volume;

            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 5 * 7) {
                mWeekSales[1] += volume;

            } else if ((int) mNowDay - rowSale.unixDayNumber < w + 6 * 7) {
                mWeekSales[0] += volume;
            }
        }

        for(int i=0;i<7;i++){
            mWeekVol[i] = (int)mWeekSales[i];
        }
        return mWeekVol;
    }


    public static int[] getSaleTotalDate(RowSale[] rowSales, int productId, int displayType, ApplicationManager.UNITS units) {
        int[] mWeekDate = new int[1];
        double volume;
        double totalVolume = 0;
        for (RowSale rowSale : rowSales) {
            if (rowSale.productId != productId) continue;

            if(displayType == 1) {
                volume = finishedVolume(rowSale);
                if (units == ApplicationManager.UNITS.METRIC)
                    volume = ConversionFunctions.convertOztoMl(volume);
            }
            else
                volume = productCaseVolume(rowSale);

            totalVolume += volume;
        }

        mWeekDate[0] = (int)totalVolume;
        return mWeekDate;
    }

    public static int[] getSaleMonthDate(RowSale[] rowSales, int productId, int displayType, ApplicationManager.UNITS units) {
        int[] mMonthDays = new int[7];
        Calendar cal = Calendar.getInstance();
        int nowMonth = cal.get(Calendar.MONTH);
        int nowYear = cal.get(Calendar.YEAR);
        for (int i = 0; i < mMonthDays.length; i++) {
            if (nowMonth - i >= 0) {
                cal.set(Calendar.YEAR, nowYear);
                cal.set(Calendar.MONTH, nowMonth - i);
            } else {
                cal.set(Calendar.YEAR, nowYear - 1);
                cal.set(Calendar.MONTH, 11 + (nowYear - i));
            }
            if (i == 0) {
                mMonthDays[i] = cal.getActualMaximum(Calendar.DATE);
            } else {
                mMonthDays[i] = mMonthDays[i - 1] + cal.getActualMaximum(Calendar.DATE);
            }
        }

        long mNowDay = System.currentTimeMillis() / (24 * 60 * 60 * 1000);
        int[] mMonthVol = new int[7];
        double[] mMonthSales = new double[7];
        double volume;
        for (RowSale rowSale : rowSales) {
            if (rowSale.productId != productId) continue;
            if(displayType == 1) {
                volume = finishedVolume(rowSale);
                if (units == ApplicationManager.UNITS.METRIC)
                    volume = ConversionFunctions.convertOztoMl(volume);
            }
            else
                volume = productCaseVolume(rowSale);

            int daynum = (int) mNowDay - rowSale.unixDayNumber;
            if (daynum <= mMonthDays[0]) {
                mMonthSales[6] += volume;
            } else if (daynum <= mMonthDays[1]) {
                mMonthSales[5] += volume;
            } else if (daynum <= mMonthDays[2]) {
                mMonthSales[4] += volume;

            } else if (daynum <= mMonthDays[3]) {
                mMonthSales[3] += volume;

            } else if (daynum <= mMonthDays[4]) {
                mMonthSales[2] += volume;

            } else if (daynum <= mMonthDays[5]) {
                mMonthSales[1] += volume;

            } else if (daynum <= mMonthDays[6]) {
                mMonthSales[0] += volume;
            }
        }

        for(int i=0;i<7;i++){
            mMonthVol[i] = (int)mMonthSales[i];
        }
        return mMonthVol;
    }

    public static int getMax(List<int[]> infoList) {
        int[] ints = new int[infoList.size()];
        for (int i = 0; i < infoList.size(); i++) {
            int[] arr = infoList.get(i);
            ints[i] = arr[0];
            for (int j = 1; j < arr.length; j++) {
                if (arr[j] > ints[i]) {
                    ints[i] = arr[j];
                }
            }
        }
        int max = ints[0];
        for (int i = 1; i < ints.length; i++) {
            if (ints[i] > max) {
                max = ints[i];
            }
        }
        int pow = 1;
        while (true) {
            if (max / Math.pow(10, pow) < 10) {
                int num = (max + (int) Math.pow(10, pow)) / (int) Math.pow(10, pow) * (int) Math.pow(10, pow);
                max = num;
                break;
            }
            pow++;
        }

        return (max+(max/10));
    }

    public static int getTotal(List<int[]> infoList) {
        int total=0;
        for (int i = 0; i < infoList.size(); i++) {
            int[] entry = infoList.get(i);
            for (int j = 0; j < entry.length; j++) {
                total += entry[j];
            }
        }

        return total;
    }

    public static int getLastColumnTotal(List<int[]> infoList) {
        int total=0;
        for (int i = 0; i < infoList.size(); i++) {
            int[] entry = infoList.get(i);
            total += entry[entry.length-1];
        }

        return total;
    }

    public static List<int[]> calcVolLiters(List<int[]> infoList) {
        for (int i = 0; i < infoList.size(); i++) {
            int[] entry = infoList.get(i);
            for (int j = 0; j < entry.length; j++) {
                entry[j] = entry[j] / 1000;
            }
            infoList.set(i, entry);
        }

        return infoList;
    }

    public static String[] getDayStr(long time) {
        Date dt = new Date(time);
        String[] weekDays = {"S", "M", "T", "W", "R", "F", "S"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        String[] weekStr = new String[weekDays.length];
        for (int i = 0; i < weekDays.length; i++) {
            if (w - i >= 0) {
                weekStr[weekDays.length - 1 - i] = weekDays[w - i];
            } else {
                weekStr[weekDays.length - 1 - i] = weekDays[weekDays.length + (w - i)];

            }
        }
        return weekStr;
    }

    public static String[] getMonthStr(long time) {
        Date dt = new Date(time);
        String[] monthStr = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int month = cal.get(Calendar.MONTH);
        String[] weekStr = new String[7];
        for (int i = 0; i < weekStr.length; i++) {
            if (month - i >= 0) {
                weekStr[weekStr.length - 1 - i] = monthStr[month - i];
            } else {
                weekStr[weekStr.length - 1 - i] = monthStr[monthStr.length + (month - i)];

            }
        }
        return weekStr;
    }

    public static String[] getWeekStr(long time) {
        Date dt = new Date(time);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int week = cal.get(Calendar.WEEK_OF_YEAR);
        String[] strings = new String[7];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = Integer.toString(week - strings.length + i);
        }
        return strings;
    }

    public static double finishedVolume(RowSale salesData){
        double dispensedSeconds = salesData.volume/1000d;
        double volume;

        volume = ((1.2*dispensedSeconds)/salesData.ratio)*(salesData.ratio+1);

        return volume;
    }

    public static double productDayVolume(RowSale salesData){
        double dispensedSeconds = salesData.volume/1000d;
        double volume;

        volume = ((1.2*dispensedSeconds)/salesData.ratio);

        return volume;
    }

    public static double productBottleVolume(RowSale salesData){
        double dispensedSeconds = salesData.volume/1000d;
        double volume;

        volume = ((1.2*dispensedSeconds)/(salesData.ratio*33.814))/3;

        return volume;
    }

    public static double productCaseVolume(RowSale salesData){
        double dispensedSeconds = salesData.volume/1000d;
        double volume;

        volume = ((1.2*dispensedSeconds)/(salesData.ratio*33.814))/6;

        return volume;
    }

}
