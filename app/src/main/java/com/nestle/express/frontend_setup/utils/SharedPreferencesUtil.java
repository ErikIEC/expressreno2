package com.nestle.express.frontend_setup.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.nestle.express.frontend_setup.activity.BaseActivity;
import com.nestle.express.iec_backend.ExpressApp;

import org.json.JSONArray;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by peng on 16-12-22.
 */

public class SharedPreferencesUtil {
    private static SharedPreferencesUtil mSharedPreferencesUtil;
    private SharedPreferences preferences;
    static public final String FRENCH_ISO_CODE = "fr";
    static public final String ENGLISH_ISO_CODE = "en";
    static public final String SPANISH_ISO_CODE = "es";
    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";
    private static final String CURRENT_TIME_LOCALE_CHANGED = "CURRENT_TIME_LOCALE_CHANGED";
    private static final String DELAY_TO_RESET_DEFAULT_LOCALE = "DELAY_TO_RESET_DEFAULT_LOCALE";


    private boolean[] operators = new boolean[]{
            false,false,false,false,true,true,false,true,false,false,false,false,false
    };

    public static SharedPreferencesUtil getInstance() {
        if(mSharedPreferencesUtil == null){
            mSharedPreferencesUtil = new SharedPreferencesUtil();
            mSharedPreferencesUtil.preferences = ExpressApp.getApp()
                    .getSharedPreferences("config", MODE_PRIVATE);
        }
        return mSharedPreferencesUtil;
    }


    public void saveEnalbleArray(String key, boolean[] booleanArray) {
        JSONArray jsonArray = new JSONArray();
        for (Boolean b : booleanArray) {
            jsonArray.put(b);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, jsonArray.toString());
        editor.apply();
    }
    public boolean[] getEnalbleArray(String key) {
        boolean[] resArray = new boolean[15];
        try {
            JSONArray jsonArray = new JSONArray(preferences.getString(key, "[]"));
            if(jsonArray.length() <= 1){
                return operators;
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                resArray[i] = (jsonArray.getBoolean(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return operators;
        }

        return resArray;
    }
    public void saveStrArray(String key, String[] strArray) {
        JSONArray jsonArray = new JSONArray();
        for (String b : strArray) {
            jsonArray.put(b);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, jsonArray.toString());
        editor.apply();
    }
    public String[] getStrArray(String key) {

        try {
            JSONArray jsonArray = new JSONArray(preferences.getString(key, "[]"));
            if(jsonArray.length() <= 0){
                return null;
            }
            String[] resArray = new String[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                resArray[i] = (jsonArray.getString(i));
            }
            return resArray;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    public void saveIntArray(String key, int[] strArray) {
        JSONArray jsonArray = new JSONArray();
        for (int b : strArray) {
            jsonArray.put(b);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, jsonArray.toString());
        editor.apply();
    }
    public int[] getIntArray(String key, int[] ints) {

        try {
            JSONArray jsonArray = new JSONArray(preferences.getString(key, "[]"));
            if(jsonArray.length() <= 0){
                return ints;
            }
            int[] resArray = new int[jsonArray.length()];
            for (int i = 0; i < jsonArray.length(); i++) {
                resArray[i] = (jsonArray.getInt(i));
            }
            return resArray;
        } catch (Exception e) {
            e.printStackTrace();
            return ints;
        }

    }

    /**
     * Save the current locale
     *
     * @param context  : context of preferences
     * @param locale : value of default locale
     */
    public void saveCurrentLocale(Context context, String locale) {
        SharedPreferences.Editor prefsEdit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefsEdit.putString(CURRENT_LOCALE, locale);
        prefsEdit.commit();
    }
    /**
     * Save the current time after a locale change.
     *
     * @param context  : context of preferences
     * @param currentTimeInMillis : current time when the locale has been changed.
     */
    public void saveCurrentTimeLocaleChanged(Context context, Long currentTimeInMillis) {
        SharedPreferences.Editor prefsEdit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefsEdit.putLong(CURRENT_TIME_LOCALE_CHANGED, currentTimeInMillis);
        prefsEdit.commit();
    }

    /**
     * Save the delay before going back_btn to default locale
     *  @param context  : context of preferences
     * @param delay : delay before going back_btn to default locale.
     */
    public void saveDelayToResetDefaultLocale(Context context, int delay) {
        SharedPreferences.Editor prefsEdit = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefsEdit.putInt(DELAY_TO_RESET_DEFAULT_LOCALE, delay);
        prefsEdit.commit();
    }
    public String getCurrentLocale(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(CURRENT_LOCALE, ENGLISH_ISO_CODE);
    }

    public void setLanguage(String language) {
        preferences.edit().putString("language", language).apply();
        LocalBroadcastManager.getInstance(ExpressApp.getApp()).sendBroadcast(new Intent(BaseActivity.CHANGE_LANGUAGE));


    }



    public String getLanguage() {
        return preferences.getString("language", "en");
    }
}
