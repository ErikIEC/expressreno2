package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.database.ArtDatabase;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by jason on 1/5/17.
 */
public class ProductImageView extends FrameLayout
{
	public ProductImageView(Context context) {
		super(context);
	}

	public ProductImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public ProductImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	ImageView  mImage;
	ExpressApp mApp;
	ProductManager mProductManager;
	ArtDatabase mArt;
	public void commonInit(Context context, AttributeSet attrs) {
		View view = LayoutInflater.from(context).inflate(R.layout.view_product_image, this, true);
		mImage=(ImageView) findViewById(R.id.view_product_image);

		mApp= (ExpressApp) (((Activity) context).getApplication());
		mProductManager=mApp.getProductManager();
		mArt=mApp.getArtDatabase();
	}

	public void setSlotAndInitialize(ProductSlot slot)
	{
		NestleProduct product=mProductManager.getSelectedProduct(slot);
		Drawable image=mArt.getThumbnail(mApp, getResources(), product);
		mImage.setImageDrawable(image);
	}
}
