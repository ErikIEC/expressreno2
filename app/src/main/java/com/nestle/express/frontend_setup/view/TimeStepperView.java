package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;

import java.text.DecimalFormat;

/**
 * Created by peng on 16/11/30.
 */

public class TimeStepperView extends FrameLayout {
    //Time is stored in minutes
    protected int mSettingMax = 60*24-15;;
    protected int mSettingMin = 0;
    protected int mSettingValue = 0;
    protected int mIncrementValue = 15;
    protected TextView mValueText, mLabel;
    protected ImageView mSubBtn, mAddBtn, mIcon;
    boolean mFirstIncrement=true;
    private ValueChangeListener mValueChangeListener;

    public TimeStepperView(Context context) {
        super(context);
    }

    public TimeStepperView(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit(context, attrs);
    }

    public TimeStepperView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit(context, attrs);
    }

    public void commonInit(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_time_stepper, this, true);
        mValueText = (TextView) view.findViewById(R.id.view_time_stepper_text);
        mSubBtn = (ImageView) view.findViewById(R.id.view_time_stepper_sub);
        mAddBtn = (ImageView) view.findViewById(R.id.view_time_stepper_add);
		mLabel = (TextView) view.findViewById(R.id.view_time_stepper_label);;
		mIcon = (ImageView) view.findViewById(R.id.view_time_stepper_icon);;
        mSubBtn.setOnTouchListener(mDecrement);
        mAddBtn.setOnTouchListener(mIncrement);
        updateDisplayText();
    }

    public void setValue(int value)
    {
        mSettingValue=value;
        updateDisplayText();
    }

    public int getValue()
    {
        limitSetting();
        return mSettingValue;
    }

    public void setMax(int max)
    {
        updateDisplayText();
        mSettingMax = max;
    }

    public void setMin(int min)
	{
		updateDisplayText();
        mSettingMin = min;
    }

    public void setIncrement(int inc)
	{
		updateDisplayText();
        mIncrementValue = inc;
    }

    OnTouchListener mIncrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            switch(motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    mAddBtn.setPressed(true);
                    mHandler.post(mRunnableIncrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mAddBtn.setPressed(false);
                    mHandler.removeCallbacks(mRunnableIncrement);
                    resetIncrement();
            }
            return true;
        }
    };

    OnTouchListener mDecrement = new OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {

            switch (motionEvent.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    mSubBtn.setPressed(true);
                    mHandler.post(mRunnableDecrement);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mSubBtn.setPressed(false);
                    mHandler.removeCallbacks(mRunnableDecrement);
                    resetDecrement();
            }
            return true;
        }
    };

    private Runnable mRunnableIncrement = new Runnable()
    {
        @Override
        public void run()
        {
            incrementSetting();
            if (mFirstIncrement)
            {
                mFirstIncrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetIncrement()
    {
        mFirstIncrement =true;
    }
    boolean mFirstDecrement =true;
    private Runnable mRunnableDecrement = new Runnable()
    {
        @Override
        public void run()
        {
            decrementSetting();
            if (mFirstDecrement)
            {
                mFirstDecrement =false;
                mHandler.postDelayed(this, 500);
            }
            else
            {
                mHandler.postDelayed(this, 50);
            }
        }
    };

    void resetDecrement()
    {
        mFirstDecrement =true;
    }

    public void incrementSetting()
    {
        mSettingValue += mIncrementValue;
        updateDisplayText();
    }
    public void decrementSetting()
    {
        mSettingValue -= mIncrementValue;
        updateDisplayText();
    }

    void limitSetting()
    {
		//Wrap value instead of limit
        if (mSettingValue > mSettingMax)
            mSettingValue = mSettingMin;

        if (mSettingValue < mSettingMin)
            mSettingValue = mSettingMax;
    }


    void updateDisplayText()
    {
        limitSetting();
        DecimalFormat format = new DecimalFormat("##");

		boolean isPM=false;
		int hours=mSettingValue/60;
		int minutes=mSettingValue%60;

		if (hours>11)
		{
			hours-=12;
			isPM=true;
		}

		if (hours==0)
			hours=12;

		String amPM="AM";
		if (isPM)
			amPM="PM";

		String minStr=String.format("%02d", minutes);
        String timeStr = hours+":"+minStr+" "+amPM;
        if(mValueChangeListener != null) mValueChangeListener.onChange(timeStr);
        mValueText.setText(timeStr);


    }

    Handler mHandler = new Handler();

	public void setIcon(int id)
	{
		mIcon.setImageDrawable(getContext().getDrawable(id));
	}

	public void setLabel(int id)
	{
		mLabel.setText(getContext().getString(id));
	}

	public void setValueChangeListener(ValueChangeListener valueChangeListener){
        this.mValueChangeListener = valueChangeListener;
    }

    @Override
    public void setEnabled(boolean value)
    {
        if (value==false)
			mValueText.setText("");
		else
			updateDisplayText();

		mValueText.setEnabled(value);
        mSubBtn.setEnabled(value);
        mAddBtn.setEnabled(value);
    }

    public interface ValueChangeListener{
        void onChange(String time);
    }
}