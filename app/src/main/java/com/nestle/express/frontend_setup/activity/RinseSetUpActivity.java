package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.ConcentrateRinseSlotView;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by peng on 16/11/29.
 */

public class RinseSetUpActivity extends BaseActivity{

    ConcentrateRinseSlotView slot1, slot2, slot3, slot4;
    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_rinse_setup);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.concentrate_rinse_setup);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.concentraterinse_breakout_01);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);

        slot1=(ConcentrateRinseSlotView) findViewById(R.id.concentrate_rinse_slot1);
        slot2=(ConcentrateRinseSlotView) findViewById(R.id.concentrate_rinse_slot2);
        slot3=(ConcentrateRinseSlotView) findViewById(R.id.concentrate_rinse_slot3);
        slot4=(ConcentrateRinseSlotView) findViewById(R.id.concentrate_rinse_slot4);

        slot1.setSlotAndInitialize(ProductSlot.SLOT1);
        slot2.setSlotAndInitialize(ProductSlot.SLOT2);
        slot3.setSlotAndInitialize(ProductSlot.SLOT3);
        slot4.setSlotAndInitialize(ProductSlot.SLOT4);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        slot1.saveProduct();
        slot2.saveProduct();
        slot3.saveProduct();
        slot4.saveProduct();
    }
}
