package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.TimeoutStepperView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;

/**
 * Created by root on 16-11-14.
 */

public class TimeoutSettingActivity extends BaseActivity{
	ExpressApp mApp;
	ApplicationManager mAppManager;
    TimeoutStepperView mIdle, mTechnician, mPortionSet, mRinse;
    @Override
    protected void initContentView()
	{
		mApp = (ExpressApp) getApplication();
		mAppManager = mApp.getApplicationManager();

        setContentView(R.layout.activity_timeout_setting);
		mIdle = (TimeoutStepperView) findViewById(R.id.timeout_idle);
		mTechnician = (TimeoutStepperView) findViewById(R.id.timeout_technician);
		mPortionSet = (TimeoutStepperView) findViewById(R.id.timeout_portion_setting);
		//mRinse = (TimeoutStepperView) findViewById(R.id.timeout_rinse);

		mIdle.setMin(30);
		mTechnician.setMin(30);
		mPortionSet.setMin(2);
		//mRinse.setMin(30);

		mIdle.setMax(300);
		mTechnician.setMax(600);
		mPortionSet.setMax(240);
		//mRinse.setMax(300);

		mIdle.setIncrement(5);
		mTechnician.setIncrement(5);
		mPortionSet.setIncrement(1);
		//mRinse.setIncrement(5);

		mIdle.setValue(mAppManager.getTimeoutIdle());
		mTechnician.setValue(mAppManager.getTimeoutSettingsMenu());
		mPortionSet.setValue(mAppManager.getTimeoutPortionSet());
		//mRinse.setValue(mAppManager.getTimeoutRinse());

		mIdle.setIcon(R.drawable.timeoutsettings_breakout_01);
		mTechnician.setIcon(R.drawable.timeoutsettings_breakout_02);
		mPortionSet.setIcon(R.drawable.timeoutsettings_breakout_03);
		//mRinse.setIcon(R.drawable.timeoutsettings_breakout_04);

		mIdle.setLabel(R.string.idle_screen);
		mTechnician.setLabel(R.string.technician_screen);
		mPortionSet.setLabel(R.string.portion_dsetting);
		//mRinse.setLabel(R.string.rinse);
    }

	@Override
	protected void initData()
	{

	}

	@Override
    protected void initView()
	{
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.timeout_setting);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.timeoutsettings_breakout_05);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

	@Override
	protected void onPause()
	{
		super.onPause();

		mAppManager.setTimeoutIdle((int) mIdle.getValue());
		mAppManager.setTimeoutTechnician((int) mTechnician.getValue());
		mAppManager.setTimeoutPortionSet((int) mPortionSet.getValue());
		//mAppManager.setTimeoutRinse((int) mRinse.getValue());
		mAppManager.setTimeoutRinse(300);
		mAppManager.save();
	}
}
