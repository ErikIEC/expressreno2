package com.nestle.express.frontend_setup.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.TimeStepperView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationConfigDatabase;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.TimeOfDay;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.ProductManager;

import static com.nestle.express.frontend_setup.R.color.primary_text_default_material_light;

/**
 * Created by root on 16-11-14.
 */

public class RinseManagementActivity extends BaseActivity{
    ExpressApp mApp;
    ApplicationManager mAppManager;
    ProductManager mProductManager;
    MachineManager mMachineManager;

    TextView enableRinse, additonalRinse;
    RadioButton rinseAuto, rinseManual;
	TimeStepperView time1, time2;
	private boolean isFirstChange = true;
    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_rinse_management);

        mApp=(ExpressApp) getApplication();
        mAppManager=mApp.getApplicationManager();

        enableRinse=(TextView) findViewById(R.id.rinse_management_enable);
		additonalRinse=(TextView) findViewById(R.id.rinse_additional_enable);
        //rinseAuto=(RadioButton) findViewById(R.id.rinse_management_auto);
        //rinseManual=(RadioButton) findViewById(R.id.rinse_management_manual);

		//rinseAuto.setOnClickListener(mOnRadioClicked);
		//rinseManual.setOnClickListener(mOnRadioClicked);

		time1=(TimeStepperView) findViewById(R.id.rinse_time1);
		time2=(TimeStepperView) findViewById(R.id.rinse_time2);

		time1.setEnabled(false);
		time1.setValue(mAppManager.getRinseTime(ApplicationManager.RINSE_TIME.RINSE_TIME1).getMinutes());
        time2.setEnabled(false);
        time2.setValue(mAppManager.getRinseTime(ApplicationManager.RINSE_TIME.RINSE_TIME2).getMinutes());
		//time2.setLabel(R.string.additional_nrinse);
		//time2.setIcon(R.drawable.rinsemanagement_icons_04);

        //enableRinse.setOnClickListener(mOnEnable);
        //additonalRinse.setOnClickListener(mOnAdditional);
        enableRinse.setOnTouchListener(mOnEnableTouch);
        additonalRinse.setOnTouchListener(mOnAdditionalTouch);

        updateRinseConfiguration();

        //mHandler.post(mSetPressed);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        findViewById(R.id.tv_home).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.rinse_management);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.rinsemanagement_icons_02);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        (findViewById(R.id.tv_right)).setVisibility(View.VISIBLE);
        (findViewById(R.id.tv_right)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //super.onClick(view);
        int id = view.getId();
        if (id == R.id.tv_back) {
            saveSettings();
            finish();
        } else if (id == R.id.tv_home) {
            stopTimeout();
            saveSettings();
            finish();

            final Activity self=this;

            mHandler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    startActivity(new Intent(self, BlankActivity.class));
                }
            }, 50);
        }
        if(id == R.id.tv_right){
            startActivity(new Intent(this, RinseSetUpActivity.class));
        }
    }

    private void saveSettings(){
        int minutes1=time1.getValue();
        int minutes2=time2.getValue();

        TimeOfDay timeSetting1=new TimeOfDay(minutes1);
        TimeOfDay timeSetting2=new TimeOfDay(minutes2);

        mAppManager.setRinseTime(ApplicationManager.RINSE_TIME.RINSE_TIME1, timeSetting1);
        mAppManager.setRinseTime(ApplicationManager.RINSE_TIME.RINSE_TIME2, timeSetting2);
        mAppManager.save();
    }

	/*
    @Override
	protected void onPause()
	{
		super.onPause();


	}*/

	/*void updateRadioButton()
	{
		if (mAppManager.getRinseMode()== ApplicationConfigDatabase.RINSE_MODE.AUTOMATIC)
		{
			rinseAuto.setChecked(true);
			rinseAuto.setPressed(true);
			rinseManual.setChecked(false);
			rinseManual.setPressed(false);
		}
		else
		{
			rinseAuto.setChecked(false);
			rinseAuto.setPressed(false);
			rinseManual.setChecked(true);
			rinseManual.setPressed(true);
		}
	}*/

	/*View.OnClickListener mOnRadioClicked = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			boolean checked = ((RadioButton) view).isChecked();
			Log.d("RADIO", checked+" "+view.getId());

			if (!checked)
				return;

			// Check which radio button was clicked
			int id=view.getId();

			if (id==R.id.rinse_management_auto)
				mAppManager.setRinseMode(ApplicationConfigDatabase.RINSE_MODE.AUTOMATIC);
			else
				mAppManager.setRinseMode(ApplicationConfigDatabase.RINSE_MODE.MANUAL);

			mHandler.post(mSetPressed);
		}
	};*/


	View.OnClickListener mOnEnable = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (mAppManager.getRinseTimes()== ApplicationConfigDatabase.RINSE_TIMES.OFF) {
                if((time2.getValue()%15)==0)
                    mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY);
                else
                    mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY);
            }
            else
                mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.OFF);

			//mHandler.post(mSetPressed);
            updateRinseConfiguration();
        }
    };

    /*
    View.OnClickListener mOnAdditional = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if (mAppManager.getRinseTimes()== ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY) {
                mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY);
                if((time2.getValue()%15)!=0)
                    time2.setValue(0);
            }
            else {
                mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY);
                time2.setEnabled(false);
                time2.setValue(1);
                additonalRinse.setPressed(false);
                additonalRinse.setTextColor(getResources().getColor(R.color.primary_text_disabled_material_light));
            }

            //mHandler.post(mSetPressed);
            updateRinseConfiguration();
        }
    };
    */

    View.OnTouchListener mOnEnableTouch = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)  {

                if (mAppManager.getRinseTimes()== ApplicationConfigDatabase.RINSE_TIMES.OFF) {
                    if((time2.getValue()%15)==0)
                        mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY);
                    else
                        mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY);
                }
                else
                    mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.OFF);

                //mHandler.post(mSetPressed);
                updateRinseConfiguration();
            }
            else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)) {

            }

            return true;
        }
    };

    View.OnTouchListener mOnAdditionalTouch = new View.OnTouchListener()
    {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent)
        {
            clearTimeout();

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)  {
                if (mAppManager.getRinseTimes() == ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY) {
                    mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY);
                    if ((time2.getValue() % 15) != 0)
                        time2.setValue(0);
                } else {
                    mAppManager.setRinseTimes(ApplicationConfigDatabase.RINSE_TIMES.ONCE_DAILY);
                    time2.setEnabled(false);
                    time2.setValue(1);
                    additonalRinse.setPressed(false);
                    additonalRinse.setTextColor(getResources().getColor(R.color.primary_text_disabled_material_light));
                }

                //mHandler.post(mSetPressed);
                updateRinseConfiguration();
            }
            else if ((motionEvent.getAction() == MotionEvent.ACTION_UP ||
                    motionEvent.getAction() == MotionEvent.ACTION_CANCEL)) {

            }

            return true;
        }
    };

    private void updateRinseConfiguration(){
        enableRinse.setEnabled(true);
        //updateRadioButton();

        if (mAppManager.getRinseTimes()== ApplicationConfigDatabase.RINSE_TIMES.OFF)
        {
            time1.setEnabled(false);
            time2.setEnabled(false);
            //rinseAuto.setEnabled(false);
            //rinseManual.setEnabled(false);
            additonalRinse.setEnabled(false);
            additonalRinse.setTextColor(getResources().getColor(R.color.primary_text_disabled_material_light));
            additonalRinse.setPressed(false);
            enableRinse.setPressed(false);
        }
        else
        {
            time1.setEnabled(true);
				/*time1.setValueChangeListener(new TimeStepperView.ValueChangeListener() {
					@Override
					public void onChange(String time) {
						if(isFirstChange){
							time2.setEnabled(true);
							isFirstChange = false;
						}
					}
				});*/
            //time2.setEnabled(true);
            //rinseAuto.setEnabled(true);
            //rinseManual.setEnabled(true);
            enableRinse.setPressed(true);
            additonalRinse.setEnabled(true);
            additonalRinse.setTextColor(getResources().getColor(R.color.primary_text_default_material_light));

            if (mAppManager.getRinseTimes()== ApplicationConfigDatabase.RINSE_TIMES.TWICE_DAILY) {
                additonalRinse.setPressed(true);
                time2.setEnabled(true);
            }
            else{
                additonalRinse.setPressed(false);
                time2.setEnabled(false);
            }
        }
    }

    /*
    Handler mHandler=new Handler();
	Runnable mSetPressed = new Runnable()
	{
		@Override
		public void run()
		{
            updateRinseConfiguration();
		}
	};
    */

}
