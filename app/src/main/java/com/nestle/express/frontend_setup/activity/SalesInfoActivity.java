package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.SaleTimeUtil;
import com.nestle.express.frontend_setup.view.BarGraphView;
import com.nestle.express.frontend_setup.view.SalesInfoView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.SalesDatabase;
import com.nestle.express.iec_backend.machine.structure.RowSale;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.DatabaseDisplayProduct;
import com.nestle.express.iec_backend.product.structure.ProductRow;
import com.nestle.express.iec_backend.product.structure.ProductSlot;
import com.nestle.express.iec_backend.product.structure.SelectionMatrix;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.text.DateFormat;

/**
 * Created by peng on 16/12/1.
 */
public class SalesInfoActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener {
    private TextView mTitleTv;
    private SalesInfoView mUnitView;
    private RowSale[] sales;
    private ProductManager mProductManager;
    private ApplicationManager mAppManager;
    private SelectionMatrix mSelectionMatrix;
    private List<DatabaseDisplayProduct> productList = new ArrayList<>();
    private List<DatabaseDisplayProduct> fullProductList = new ArrayList<>();
    private List<Boolean> checkList = new ArrayList<>();
    private TextView mSetPageTv;
    private TextView mSetRowTopTv;
    private TextView mSetRowBotTv;
    private int mPageNum;
    private int mDisplayType;
    private ApplicationManager.UNITS mUnits;

    private Runnable selectDay = new Runnable() {
        @Override
        public void run() {
            selectDay(sales);
            mLoadingIv.setVisibility(View.GONE);
        }
    };
    private Runnable selectWeek = new Runnable() {
        @Override
        public void run() {
            selectWeek(sales);
            mLoadingIv.setVisibility(View.GONE);
        }
    };
    private Runnable selectMonth = new Runnable() {
        @Override
        public void run() {
            selectMonth(sales);
            mLoadingIv.setVisibility(View.GONE);
        }
    };
    private Runnable selectTotal = new Runnable() {
        @Override
        public void run() {
            selectTotal(sales);
            mLoadingIv.setVisibility(View.GONE);
        }
    };
    private Runnable selectNow = selectDay;
    private ImageView mLoadingIv;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_sales_info);
    }

    @Override
    protected void initData() {
        setPage(1, selectDay);
    }

    private void setPage(final int page, final Runnable select) {
        mPageNum = page;
        mLoadingIv.setVisibility(View.VISIBLE);
        if(mPageNum == 1){
            mSetPageTv.setText(R.string.next);
            mSetRowTopTv.setText("A");
            mSetRowBotTv.setText("B");
        }else {
            mSetPageTv.setText(R.string.previous);
            mSetRowTopTv.setText("C");
            mSetRowBotTv.setText("D");
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(mProductManager == null){
                    ExpressApp myApp = (ExpressApp) getApplication();
                    SalesDatabase mySalesDb = myApp.getSalesDatabase();
                    mProductManager=((ExpressApp) getApplication()).getProductManager();
                    mAppManager=((ExpressApp) getApplication()).getApplicationManager();
                    mUnits = mAppManager.getTempUnits();
                    mSelectionMatrix=mProductManager.getSelectionMatrix();
                    //if(sales == null) sales = mySalesDb.getSales();
                    if(sales == null) sales = mySalesDb.getRealSales();
                    getProductData();           // get full loaded product list for total sales calculations
                }
                checkList.clear();
                productList.clear();
                if(page == 1){
                    checkList.add(addProduct(ProductSlot.SLOT1, ProductRow.ROW1));
                    checkList.add(addProduct(ProductSlot.SLOT2, ProductRow.ROW1));
                    checkList.add(addProduct(ProductSlot.SLOT3, ProductRow.ROW1));
                    checkList.add(addProduct(ProductSlot.SLOT4, ProductRow.ROW1));
                    checkList.add(addProduct(ProductSlot.SLOT1, ProductRow.ROW2));
                    checkList.add(addProduct(ProductSlot.SLOT2, ProductRow.ROW2));
                    checkList.add(addProduct(ProductSlot.SLOT3, ProductRow.ROW2));
                    checkList.add(addProduct(ProductSlot.SLOT4, ProductRow.ROW2));
                }else if(page == 2){
                    checkList.add(addProduct(ProductSlot.SLOT1, ProductRow.ROW3));
                    checkList.add(addProduct(ProductSlot.SLOT2, ProductRow.ROW3));
                    checkList.add(addProduct(ProductSlot.SLOT3, ProductRow.ROW3));
                    checkList.add(addProduct(ProductSlot.SLOT4, ProductRow.ROW3));
                    checkList.add(addProduct(ProductSlot.SLOT1, ProductRow.ROW4));
                    checkList.add(addProduct(ProductSlot.SLOT2, ProductRow.ROW4));
                    checkList.add(addProduct(ProductSlot.SLOT3, ProductRow.ROW4));
                    checkList.add(addProduct(ProductSlot.SLOT4, ProductRow.ROW4));
                }
                runOnUiThread(select);

            }
        }).start();

    }


    @Override
    protected void initView() {

        mTitleTv = (TextView) findViewById(R.id.tv_title);
        mUnitView = (SalesInfoView) findViewById(R.id.sales_info_view);
        if(getLanguage().equals("en")) {
            mLoadingIv = (ImageView) findViewById(R.id.sales_loading);
        }
        else {
            mLoadingIv = (ImageView) findViewById(R.id.sales_loading_fr);
        }
        mSetPageTv = (TextView) findViewById(R.id.set_page_tv);
        mSetRowTopTv = (TextView) findViewById(R.id.row_num_top_tv);
        mSetRowBotTv = (TextView) findViewById(R.id.row_num_bot_tv);
        findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_09);
        ((RadioGroup)findViewById(R.id.sales_info_rg)).setOnCheckedChangeListener(this);

        String currentDate = DateFormat.getDateInstance().format(Calendar.getInstance().getTime());
        mTitleTv.setText(getString(R.string.sales_info) + " - " + currentDate);

        ((RadioGroup) findViewById(R.id.rg_sales)).check(R.id.rb_sales_fin);
        mDisplayType = 1;
        ((RadioGroup) findViewById(R.id.rg_sales)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_sales_fin) {
                    ((RadioGroup) findViewById(R.id.rg_sales)).check(R.id.rb_sales_fin);
                    mDisplayType = 1;
                }
                else if (checkedId == R.id.rb_sales_conc) {
                    ((RadioGroup) findViewById(R.id.rg_sales)).check(R.id.rb_sales_conc);
                    mDisplayType = 0;
                }

                updateBarGraph();
            }
        });

        mSetPageTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mPageNum == 1){
                    setPage(2, selectNow);
                }else {
                    setPage(1, selectNow);
                }
            }
        });

    }

    public boolean addProduct(ProductSlot slot, ProductRow row)
    {
        //mSelectionMatrix=mProductManager.getSelectionMatrix();
        ProductRow selected=mProductManager.getSelectedIndex().getElementSlot(slot);
        productList.add(mSelectionMatrix.getElement(slot, row));
        return selected.ordinal() == row.ordinal();
    }

    public void getProductData()
    {
        fullProductList.clear();
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT1, ProductRow.ROW1));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT2, ProductRow.ROW1));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT3, ProductRow.ROW1));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT4, ProductRow.ROW1));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT1, ProductRow.ROW2));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT2, ProductRow.ROW2));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT3, ProductRow.ROW2));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT4, ProductRow.ROW2));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT1, ProductRow.ROW3));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT2, ProductRow.ROW3));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT3, ProductRow.ROW3));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT4, ProductRow.ROW3));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT1, ProductRow.ROW4));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT2, ProductRow.ROW4));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT3, ProductRow.ROW4));
        fullProductList.add(mSelectionMatrix.getElement(ProductSlot.SLOT4, ProductRow.ROW4));
    }

    private void selectWeek(RowSale[] sales){
        selectNow = selectWeek;
        TextView labelTotalTv = (TextView)findViewById(R.id.product_totals);
        TextView yaxisTopTv = (TextView)findViewById(R.id.yaxis_top_tv);
        TextView yaxisBotTv = (TextView)findViewById(R.id.yaxis_bot_tv);
        labelTotalTv.setVisibility(View.VISIBLE);
        yaxisTopTv.setVisibility(View.VISIBLE);
        yaxisBotTv.setVisibility(View.VISIBLE);

        List<int[]> infoList = new ArrayList<>();
        for (DatabaseDisplayProduct product : productList) {
            infoList.add(SaleTimeUtil.getSaleWeekDate(sales, product.id, mDisplayType, mUnits));
        }
        int max = SaleTimeUtil.getMax(infoList);

        List<int[]> infoFullList = new ArrayList<>();
        for (DatabaseDisplayProduct product : fullProductList) {
            infoFullList.add(SaleTimeUtil.getSaleWeekDate(sales, product.id, mDisplayType, mUnits));
        }
        int total = SaleTimeUtil.getLastColumnTotal(infoFullList);

        String totalString;
        if(mDisplayType==1) {
            if (mUnits == ApplicationManager.UNITS.IMPERIAL) {
                mUnitView.setData(max, BarGraphView.TimeType.WEEK, infoList, productList, checkList);
                totalString = " " + total + " oz";
                yaxisTopTv.setText(getString(R.string.sales_yaxis_oz));
                yaxisBotTv.setText(getString(R.string.sales_yaxis_oz));
            } else {
                if (total < 3000) {
                    mUnitView.setData(max, BarGraphView.TimeType.WEEK, infoList, productList, checkList);
                    totalString = " " + total + " ml";
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_ml));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_ml));
                } else {
                    infoList = SaleTimeUtil.calcVolLiters(infoList);
                    mUnitView.setData(max / 1000, BarGraphView.TimeType.WEEK, infoList, productList, checkList);
                    double volume = total / 1000d;
                    String s = new DecimalFormat("#.##").format(volume);
                    totalString = " " + s + " l";
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_l));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_l));
                }
            }
        }
        else{
            mUnitView.setData(max, BarGraphView.TimeType.WEEK, infoList, productList, checkList);
            totalString = " " + total + " " + getString(R.string.sales_bottles);
            yaxisTopTv.setText(getString(R.string.sales_bottles));
            yaxisBotTv.setText(getString(R.string.sales_bottles));
        }
        labelTotalTv.setText(getString(R.string.sales_week_total) + totalString);
    }

    private void selectMonth(RowSale[] sales){
        selectNow = selectMonth;
        TextView labelTotalTv = (TextView)findViewById(R.id.product_totals);
        TextView yaxisTopTv = (TextView)findViewById(R.id.yaxis_top_tv);
        TextView yaxisBotTv = (TextView)findViewById(R.id.yaxis_bot_tv);
        labelTotalTv.setVisibility(View.VISIBLE);
        yaxisTopTv.setVisibility(View.VISIBLE);
        yaxisBotTv.setVisibility(View.VISIBLE);

        List<int[]> infoList = new ArrayList<>();
        for (DatabaseDisplayProduct product : productList) {
            infoList.add(SaleTimeUtil.getSaleMonthDate(sales, product.id, mDisplayType, mUnits));
        }
        int max = SaleTimeUtil.getMax(infoList);

        List<int[]> infoFullList = new ArrayList<>();
        for (DatabaseDisplayProduct product : fullProductList) {
            infoFullList.add(SaleTimeUtil.getSaleMonthDate(sales, product.id, mDisplayType, mUnits));
        }
        int total = SaleTimeUtil.getLastColumnTotal(infoFullList);

        String totalString;
        if(mDisplayType==1) {
            if (mUnits == ApplicationManager.UNITS.IMPERIAL) {
                mUnitView.setData(max, BarGraphView.TimeType.MONTH, infoList, productList, checkList);
                totalString = " " + total + " oz";
                yaxisTopTv.setText(getString(R.string.sales_yaxis_oz));
                yaxisBotTv.setText(getString(R.string.sales_yaxis_oz));
            } else {
                if (total < 3000) {
                    mUnitView.setData(max, BarGraphView.TimeType.MONTH, infoList, productList, checkList);
                    totalString = " " + total + " ml";
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_ml));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_ml));
                } else {
                    infoList = SaleTimeUtil.calcVolLiters(infoList);
                    mUnitView.setData(max / 1000, BarGraphView.TimeType.MONTH, infoList, productList, checkList);
                    double volume = total / 1000d;
                    String s = new DecimalFormat("#.##").format(volume);
                    totalString = " " + s + " l";
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_l));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_l));
                }
            }
        }
        else{
            mUnitView.setData(max, BarGraphView.TimeType.MONTH, infoList, productList, checkList);
            totalString = " " + total + " " + getString(R.string.sales_cases);
            yaxisTopTv.setText(getString(R.string.sales_cases));
            yaxisBotTv.setText(getString(R.string.sales_cases));
        }
        labelTotalTv.setText(getString(R.string.sales_month_total) + totalString);
    }

    private void selectTotal(RowSale[] sales){
        selectNow = selectTotal;
        TextView labelTotalTv = (TextView)findViewById(R.id.product_totals);
        TextView yaxisTopTv = (TextView)findViewById(R.id.yaxis_top_tv);
        TextView yaxisBotTv = (TextView)findViewById(R.id.yaxis_bot_tv);
        labelTotalTv.setVisibility(View.GONE);
        yaxisTopTv.setVisibility(View.VISIBLE);
        yaxisBotTv.setVisibility(View.VISIBLE);


        List<int[]> infoList = new ArrayList<>();
        for (DatabaseDisplayProduct product : productList) {
            infoList.add(SaleTimeUtil.getSaleTotalDate(sales, product.id, mDisplayType, mUnits));
        }
        int max = SaleTimeUtil.getMax(infoList);

        if(mDisplayType==1) {
            if (mUnits == ApplicationManager.UNITS.IMPERIAL) {
                mUnitView.setData(max, BarGraphView.TimeType.TOTAL, infoList, productList, checkList);
                yaxisTopTv.setText(getString(R.string.sales_yaxis_oz));
                yaxisBotTv.setText(getString(R.string.sales_yaxis_oz));
            } else {
                if (max < 3000) {
                    mUnitView.setData(max, BarGraphView.TimeType.TOTAL, infoList, productList, checkList);
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_ml));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_ml));
                } else {
                    infoList = SaleTimeUtil.calcVolLiters(infoList);
                    mUnitView.setData(max / 1000, BarGraphView.TimeType.TOTAL, infoList, productList, checkList);
                    yaxisTopTv.setText(getString(R.string.sales_yaxis_l));
                    yaxisBotTv.setText(getString(R.string.sales_yaxis_l));
                }
            }
        }
        else{
            mUnitView.setData(max, BarGraphView.TimeType.TOTAL, infoList, productList, checkList);
            yaxisTopTv.setText(getString(R.string.sales_cases));
            yaxisBotTv.setText(getString(R.string.sales_cases));
        }
    }

    private void selectDay(RowSale[] sales){
        selectNow = selectDay;
        TextView labelTotalTv = (TextView)findViewById(R.id.product_totals);
        TextView yaxisTopTv = (TextView)findViewById(R.id.yaxis_top_tv);
        TextView yaxisBotTv = (TextView)findViewById(R.id.yaxis_bot_tv);
        labelTotalTv.setVisibility(View.VISIBLE);
        yaxisTopTv.setVisibility(View.VISIBLE);
        yaxisBotTv.setVisibility(View.VISIBLE);

        List<int[]> infoList = new ArrayList<>();
        for (DatabaseDisplayProduct product : productList) {
            infoList.add(SaleTimeUtil.getSaleDayDate(sales, product.id, mDisplayType, mUnits));
        }
        int max = SaleTimeUtil.getMax(infoList);

        List<int[]> infoFullList = new ArrayList<>();
        for (DatabaseDisplayProduct product : fullProductList) {
            infoFullList.add(SaleTimeUtil.getSaleDayDate(sales, product.id, mDisplayType, mUnits));
        }
        int total = SaleTimeUtil.getLastColumnTotal(infoFullList);

        String totalString;
        if(mUnits == ApplicationManager.UNITS.IMPERIAL) {
            mUnitView.setData(max, BarGraphView.TimeType.DAY, infoList, productList, checkList);
            totalString = " " + total + " oz";
            yaxisTopTv.setText(getString(R.string.sales_yaxis_oz));
            yaxisBotTv.setText(getString(R.string.sales_yaxis_oz));
        }
        else {
            if(total < 3000){
                mUnitView.setData(max, BarGraphView.TimeType.DAY, infoList, productList, checkList);
                totalString = " " + total + " ml";
                yaxisTopTv.setText(getString(R.string.sales_yaxis_ml));
                yaxisBotTv.setText(getString(R.string.sales_yaxis_ml));
            }
            else {
                infoList = SaleTimeUtil.calcVolLiters(infoList);
                mUnitView.setData(max/1000, BarGraphView.TimeType.DAY, infoList, productList, checkList);
                double volume = total / 1000d;
                String s = new DecimalFormat("#.##").format(volume);
                totalString = " " + s + " l";
                yaxisTopTv.setText(getString(R.string.sales_yaxis_l));
                yaxisBotTv.setText(getString(R.string.sales_yaxis_l));
            }
        }
        labelTotalTv.setText(getString(R.string.sales_daily_total) + totalString);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId == R.id.sales_info_days_rb){
                selectDay(sales);
        }else if(checkedId == R.id.sales_info_month_rb){
            selectMonth(sales);
        }else if(checkedId == R.id.sales_info_week_rb){
            selectWeek(sales);
        }else if(checkedId == R.id.sales_info_total_rb){
            selectTotal(sales);
        }
    }

    public void updateBarGraph() {
        int checkedId = ((RadioGroup)findViewById(R.id.sales_info_rg)).getCheckedRadioButtonId();

        if(checkedId == R.id.sales_info_days_rb){
            selectDay(sales);
        }else if(checkedId == R.id.sales_info_month_rb){
            selectMonth(sales);
        }else if(checkedId == R.id.sales_info_week_rb){
            selectWeek(sales);
        }else if(checkedId == R.id.sales_info_total_rb){
            selectTotal(sales);
        }
    }
}
