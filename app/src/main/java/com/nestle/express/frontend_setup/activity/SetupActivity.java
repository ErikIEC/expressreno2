package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.MyGridView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;

/**
 * Created by root on 16-10-28.
 */
public class SetupActivity extends BaseActivity implements MyGridView.OnItemClickListener {

    private TextView tv_title;
    private ImageView iv_icon;
    private int[] item_icons = {R.drawable.setup_breakout_01, R.drawable.setup_breakout_02,
            R.drawable.setup_breakout_03, R.drawable.setup_breakout_04,
            R.drawable.setup_breakout_05, R.drawable.setup_breakout_06,
            R.drawable.setup_breakout_07};
    private MyGridView mgv;

    ExpressApp mApp;
    ApplicationManager mAppManager;

    @Override
    protected void onResume()
    {
        super.onResume();
        //When system time is updated , update rinse timers.
        ((ExpressApp) getApplication()).getApplicationManager().getRinseStatus().updateRinseTimes();
    }

    @Override
    protected void initContentView() {
        mApp=(ExpressApp) getApplication();
        mAppManager = mApp.getApplicationManager();

        setContentView(R.layout.activity_setup);
    }

    @Override
    protected void initView() {
        tv_title = (TextView)findViewById(R.id.tv_title);
        iv_icon = (ImageView)findViewById(R.id.iv_icon);
        mgv = (MyGridView)findViewById(R.id.mgv);
        mgv.setListener(this);
        findViewById(R.id.tv_back).setOnClickListener(this);

    }

    @Override
    protected void initData() {
        tv_title.setText(R.string.setup_title);
        iv_icon.setVisibility(View.VISIBLE);
        iv_icon.setImageResource(R.drawable.setup_breakout_08);


        String[] item_titles = getResources().getStringArray(R.array.setup_item_titles);
        int index = 0;
        for (String title:item_titles) {
            MyGridView.GridItem item = new MyGridView.GridItem();
            if (index==1)
                title=getUnitsText();

            item.setTitle(title);
            item.setRes(item_icons[index++]);
            item.setDesc(title);
            item.setPosition(index);
            if(index == 5){
                mgv.addItem(item,false);
            }else {
                mgv.addItem(item,true);
            }
        }
    }

    String getUnitsText()
    {
        if (mAppManager.getUnits()==ApplicationManager.UNITS.IMPERIAL)
            return getString(R.string.units_imperial);
        return getString(R.string.units_metric);
    }

    void updateUnits(MyGridView gv, MyGridView.GridItem item)
    {
        if (mAppManager.getUnits()==ApplicationManager.UNITS.IMPERIAL)
            mAppManager.setUnits(ApplicationManager.UNITS.METRIC);
        else
            mAppManager.setUnits(ApplicationManager.UNITS.IMPERIAL);
        mAppManager.setTempUnits(mAppManager.getUnits());
        Log.d("SetupActivity", "updateUnits: Units set to" + mAppManager.getTempUnits());

        gv.removeViewAt(1);
        item.setTitle(getUnitsText());
        gv.addItemAtIndex(item, 1, true);
    }

    @Override
    public void onItemClick(MyGridView.GridItem item) {

        switch (item.getPosition()){
            case 1:
                if(getLanguage().equals("en")) {
                    setLanguage(mApp.getLanguageByIsoCode("fr").isoCode);
                    mAppManager.setUnits(ApplicationManager.UNITS.METRIC);
                    mAppManager.setTempUnits(mAppManager.getUnits());
                    onFrench();
                }
                else {
                    setLanguage(mApp.getLanguageByIsoCode("en").isoCode);
                    mAppManager.setUnits(ApplicationManager.UNITS.IMPERIAL);
                    mAppManager.setTempUnits(mAppManager.getUnits());
                    onEnglish();
                }
                break;
            case 2:
                updateUnits(mgv, item);
                break;
            case 3:
                //startActivity(new Intent(getApplicationContext(),TimeDateActivity.class));
                startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
                break;
            case 4:
                startActivity(new Intent(getApplicationContext(),TimeoutSettingActivity.class));
                break;
            case 6:
                startActivity(new Intent(getApplicationContext(),RinseManagementActivity.class));
                break;
            case 7:
                startActivity(new Intent(getApplicationContext(),OperatorSetUpActivity.class));
                break;
        }
    }
}
