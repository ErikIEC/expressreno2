package com.nestle.express.frontend_setup.view;

/**
 * Created by peng on 16-12-27.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.nestle.express.frontend_setup.R;


public class UnitView extends View {

    private int places = 2;
    private String[] mTitle = {"0", "50", "100", "150", "200"};

    private float height, itemHeight;
    private float width, itemWidth;

    private float textWidth, textHeight;

    private int max = 200;
    private int top = 10;

    private int paddingLift = 20, paddingRight = 20;
    private Paint paint;
    Rect rect = new Rect();

    public UnitView(Context context) {
        this(context, null);

    }

    public void setMax(int max) {
        this.max = max;
        for (int i = 0; i < mTitle.length; i++) {
            if (i == 0) mTitle[i] = Integer.toString(0);
            else mTitle[i] = Integer.toString(max / (mTitle.length - 1) * i);
        }
        requestLayout();
        invalidate();
    }

    public UnitView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setAntiAlias(true);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        textWidth = 0;
        for (String s : mTitle) {
            paint.getTextBounds(s, 0, s.length(), rect);
            textWidth = rect.width() > textWidth ? rect.width() : textWidth;
        }
        setMeasuredDimension((int) textWidth, getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
        width = getMeasuredWidth();
        textHeight = rect.height();
        height = getMeasuredHeight() - textHeight - top;
        itemHeight = (height - top) / (mTitle.length - 1);
    }

    private RectF rectF = new RectF();

    @Override
    protected void onDraw(Canvas canvas) {


        int index = 0;
        //paint.setColor(Color.rgb(196, 196, 196));
        paint.setColor(Color.BLACK);

        paint.setStrokeWidth(3);

        for (String s : mTitle) {
            float y = height - index * itemHeight;
            canvas.drawText(s, 0, y + textHeight * 2 / 3, paint);
            //canvas.drawText(s, width, y + textHeight * 2 / 3, paint);
            //canvas.drawLine(textWidth,y,width,y,paint);
            index++;
        }
    }
}