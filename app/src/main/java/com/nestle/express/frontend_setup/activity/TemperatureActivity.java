package com.nestle.express.frontend_setup.activity;

import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.communications.protocol_layer.events.EventStatusResponse;
import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.TemperatureSettingView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.machine.MachineManager;

/**
 * Created by root on 16-11-14.
 */

public class TemperatureActivity extends BaseActivity{
    ExpressApp mApp;
	MachineManager mMachine;
    TemperatureSettingView water1, water2, concentrate;
    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_temperature_setting);

        mApp = (ExpressApp) getApplication();
		mMachine=mApp.getMachineManager();

        water1=(TemperatureSettingView) findViewById(R.id.temperature_setting_water1);
        water2=(TemperatureSettingView) findViewById(R.id.temperature_setting_water2);
        concentrate=(TemperatureSettingView) findViewById(R.id.temperature_setting_concentrate);

        water1.setLabel(getString(R.string.water_nthermister_1));
        water2.setLabel(getString(R.string.water_nthermister_2));
        concentrate.setLabel(getString(R.string.concentrate_nthermister));

        water1.setMin(25);
        water2.setMin(25);
        concentrate.setMin(35);

        water1.setMax(50);
        water2.setMax(50);
        concentrate.setMax(45);

		water1.setValue(mMachine.getWater1Setpoint());
		water2.setValue(mMachine.getWater2Setpoint());
		concentrate.setValue(mMachine.getConcentrateSetpoint());

		mHandler.post(mUpdateTemperatures);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.temperature);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.timeoutsettings_breakout_05);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

		mHandler.removeCallbacks(mUpdateTemperatures);

        double setWater1=water1.getValue();
        double setWater2=water2.getValue();
        double setConcentrate=concentrate.getValue();

		mMachine.setWater1Setpoint(setWater1);
		mMachine.setWater2Setpoint(setWater2);
		mMachine.setConcentrateSetpoint(setConcentrate);
		mMachine.save();

		mMachine.updateDriverSetpoints();
    }

	Runnable mUpdateTemperatures = new Runnable()
	{
		@Override
		public void run()
		{
			EventStatusResponse status=mMachine.getStatus();
			if (status==null)
				return;

			water1.setCurrentTemperature(status.getWater1());
			water2.setCurrentTemperature(status.getWater2());
			concentrate.setCurrentTemperature(status.getConcentrate());

			mHandler.postDelayed(this, 1000);
		}
	};
	Handler mHandler = new Handler();
}
