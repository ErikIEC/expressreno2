package com.nestle.express.frontend_setup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;

/**
 * Created by jason on 1/4/17.
 */
public class HTMLViewerActivity extends BaseActivity
{
	ExpressApp mApp;
	WebView mWebview;
	TextView mTitle, mHome, mBack;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		mApp=(ExpressApp) getApplication();
		String base="file://"+mApp.getSDCardDirectory()+"/html/"+mApp.getSelectedLanguage().isoCode+"/";

		setContentView(R.layout.activity_html);
		((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.tutorial_viewer);
		(findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);

		Bundle parameters = getIntent().getExtras();
		if (parameters!=null)
		{
			String folder=parameters.getString("folder");
			if (folder!=null)
				base+=folder+"/index.html";
		}

		mWebview=(WebView) findViewById(R.id.webview);
		mTitle=(TextView) findViewById(R.id.tv_title);
		mHome=(TextView) findViewById(R.id.tv_home);
		mBack=(TextView) findViewById(R.id.tv_back);

		mTitle.setText(getString(R.string.tutorial_viewer));
		mBack.setOnClickListener(this);
		mHome.setOnClickListener(this);

		mWebview.getSettings().setBuiltInZoomControls(true);
		mWebview.getSettings().setDisplayZoomControls(true);

		mWebview.loadUrl(base);
	}
}
