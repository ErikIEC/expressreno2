package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.ButtonSetupSlotView;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by peng on 16/11/17.
 */
public class ButtonSetUpActivity extends BaseActivity{
	ButtonSetupSlotView slot1, slot2, slot3, slot4;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_button_set_up);
		slot1=(ButtonSetupSlotView) findViewById(R.id.button_setup_slot1);
		slot2=(ButtonSetupSlotView) findViewById(R.id.button_setup_slot2);
		slot3=(ButtonSetupSlotView) findViewById(R.id.button_setup_slot3);
		slot4=(ButtonSetupSlotView) findViewById(R.id.button_setup_slot4);

		slot1.setSlotAndInitialize(ProductSlot.SLOT1);
		slot2.setSlotAndInitialize(ProductSlot.SLOT2);
		slot3.setSlotAndInitialize(ProductSlot.SLOT3);
		slot4.setSlotAndInitialize(ProductSlot.SLOT4);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        findViewById(R.id.tv_home).setOnClickListener(this);
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.button_setup);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.buttonsetup_breakout_01);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

}
