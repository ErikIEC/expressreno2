package com.nestle.express.frontend_setup.view;

/**
 * Created by peng on 16-12-27.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.view.View;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.SaleTimeUtil;


public class BarGraphView extends View {

    private int places = 2;
    private String[] mTitle = {"0", "50", "100", "150", "200"};
    private String[] mBottomTitle = SaleTimeUtil.getMonthStr(System.currentTimeMillis());
    private int mColor = Color.RED;

    private float height, itemHeight;
    private float width, itemWidth;

    private float textWidth, textHeight;

    private int max = 200;
    private int[] mData = new int[0];
    private int top = 10;

    private int paddingLift = 20, paddingRight = 20;
    private Paint paint;
    Rect rect = new Rect();
    private boolean mEnable;
    private int mEnableColor;

    public BarGraphView(Context context) {
        this(context, null);

    }

    public BarGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setAntiAlias(true);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.BarGraphView);
        places = ta.getInt(R.styleable.BarGraphView_places, places);
        mColor = ta.getColor(R.styleable.BarGraphView_contentColor, mColor);
        mEnableColor = mColor;
        ta.recycle();
    }

    public void setData(int max, int[] data, boolean isEnable) {
        this.max = max;
        this.mData = data;
        this.mEnable = isEnable;
        if(mEnable == false){
            mColor = Color.LTGRAY;//getResources().getColor(R.color.color_808080);
        }else {
            mColor = mEnableColor;
        }
        invalidate();
    }

    public void setData(int max, int[] data) {
        this.max = max;
        this.mData = data;
        invalidate();
    }

    public void setContentColor(@ColorRes int color){
        mColor = getResources().getColor(color);
        invalidate();
    }

    public enum TimeType {
        MONTH,
        WEEK,
        DAY,
        TOTAL
    }

    public void setTimeType(TimeType timeType) {
        if (timeType == TimeType.MONTH) {
            mBottomTitle = SaleTimeUtil.getMonthStr(System.currentTimeMillis());
        } else if (timeType == TimeType.WEEK) {
            mBottomTitle = SaleTimeUtil.getWeekStr(System.currentTimeMillis());
        } else if (timeType == TimeType.DAY) {
            mBottomTitle = SaleTimeUtil.getDayStr(System.currentTimeMillis());
        } else if (timeType == TimeType.TOTAL) {
            mBottomTitle = new String[]{"", "", ""};
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        for (String s : mTitle) {
            paint.getTextBounds(s, 0, s.length(), rect);
            textWidth = rect.width() > textWidth ? rect.width() : textWidth;
        }
        textWidth = 0;
        width = getMeasuredWidth();
        textHeight = rect.height();
        height = getMeasuredHeight() - textHeight * 2 - top;
        itemHeight = (height - top) / (mTitle.length - 1);

    }

    private RectF rectF = new RectF();

    @Override
    protected void onDraw(Canvas canvas) {

        if (mData != null && mData.length > 0) {
            float lift = textWidth + paddingLift;
            float w = width - lift - paddingRight;
            itemWidth = w / mData.length;
            for (int i = 0; i < mData.length; i++) {
                int data = mData[i];
                String title = mBottomTitle[i];

                float offset = data / 1.0f / max * (height - top);
                float x = lift + (i * itemWidth);
                rectF.set(x, height - offset, x + itemWidth, height);
                paint.setColor(mColor);
                String topTitle = Integer.toString(data);
                paint.getTextBounds(topTitle, 0, topTitle.length(), rect);
                int textWidth = rect.width();
                float textX = itemWidth / 2 - textWidth / 2 + x;
                //canvas.drawText(topTitle, textX, height - offset - (textHeight / 3 * 2), paint);
                canvas.drawRect(rectF, paint);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                canvas.drawRect(rectF, paint);
                paint.setStyle(Paint.Style.FILL);
                paint.getTextBounds(title, 0, title.length(), rect);
                textWidth = rect.width();
                textX = itemWidth / 2 - textWidth / 2 + x;
                canvas.drawText(title, textX, height + textHeight, paint);

            }
        }
        int index = 0;
        paint.setColor(Color.rgb(196, 196, 196));

        paint.setStrokeWidth(3);
        if (places == 1) {
            canvas.drawLine(textWidth, top, textWidth, height, paint);
        } else if (places == 3) {
            canvas.drawLine(width, top, width, height, paint);
        }

        for (String s : mTitle) {
            float y = height - index * itemHeight;
            //canvas.drawText(s,0,y,paint);
            canvas.drawLine(textWidth, y, width, y, paint);
            index++;
        }

        if (mData != null && mData.length > 0) {
            float lift = textWidth + paddingLift;
            float w = width - lift - paddingRight;
            itemWidth = w / mData.length;
            for (int i = 0; i < mData.length; i++) {
                int data = mData[i];
                float offset = data / 1.0f / max * (height - top);
                float x = lift + (i * itemWidth);
                //paint.setColor(mColor);
                paint.setColor(Color.BLACK);
                String topTitle = Integer.toString(data);
                paint.getTextBounds(topTitle, 0, topTitle.length(), rect);
                int textWidth = rect.width();
                float textX = itemWidth / 2 - textWidth / 2 + x;
                canvas.drawText(topTitle, textX, height - offset - (textHeight / 3 * 2), paint);
            }
        }
    }
}