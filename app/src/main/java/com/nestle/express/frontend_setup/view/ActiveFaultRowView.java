package com.nestle.express.frontend_setup.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.machine.FaultMatrix;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jason on 1/13/17.
 */
public class ActiveFaultRowView extends FrameLayout
{
	DatabaseFault mFault;
	ImageView mButton;
	TextView mDate, mText, mFaultCode;

	public ActiveFaultRowView(Context context, DatabaseFault fault)
	{
		super(context);
		commonInit(context, fault);
	}

	public ActiveFaultRowView(Context context)
	{
		super(context);
	}

	public void commonInit(Context context, DatabaseFault fault)
	{
		mFault=fault;

		View view = LayoutInflater.from(context).inflate(R.layout.view_active_fault, this, true);
		mText = (TextView) view.findViewById(R.id.view_active_fault_text);
		mDate = (TextView) view.findViewById(R.id.view_active_fault_date);
		mButton = (ImageView) view.findViewById(R.id.view_active_fault_button);
		mFaultCode = (TextView) view.findViewById(R.id.view_active_fault_code);

		Date date=new Date(mFault.timestamp);

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy h:mm a");

		mDate.setText(dateFormat.format(date));
		mText.setText(FaultMatrix.decode(getContext(), mFault.fault_code));
		mFaultCode.setText(FaultMatrix.decodeFNumber(mFault.fault_code));
	}


}
