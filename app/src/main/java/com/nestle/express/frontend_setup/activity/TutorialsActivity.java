package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.MyGridView;


/**
 * Created by root on 16-11-14.
 */

public class TutorialsActivity extends BaseActivity implements MyGridView.OnItemClickListener {
    private MyGridView mgv;

    private int[] res = {R.drawable.tutorials_breakout_01,R.drawable.tutorials_breakout_02,
            R.drawable.tutorials_breakout_03,R.drawable.tutorials_breakout_04,
            R.drawable.tutorials_breakout_05,R.drawable.tutorials_breakout_06};

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_tutorials);
		Log.d("TUTORIALS", "init");
    }

    @Override
    protected void initView() {
        mgv = (MyGridView)findViewById(R.id.mgv);

        ((TextView)findViewById(R.id.tv_title)).setText(R.string.tutorials);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.home_icons_breakout_05);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);

		mgv.setListener(this);
        findViewById(R.id.tv_back).setOnClickListener(this);
        findViewById(R.id.tv_home).setOnClickListener(this);
    }

    @Override
    protected void initData() {
        String[] array = getResources().getStringArray(R.array.tutorial_array);
        for (int i =0 ; i < res.length;i++){
            MyGridView.GridItem item = new MyGridView.GridItem();
            item.setRes(res[i]);
            item.setTitleDesc(array[i]);
			item.setPosition(i+1);
            mgv.addItem(item,true);
        }

		mgv.setOnClickListener(this);
    }

    @Override
    public void onItemClick(MyGridView.GridItem item)
    {
		/*
			clean_machine_interior
			drip_tray_cleaning
			loading_product
			product_selection
			rinse_management
			set_portions
		 */

		Intent intent;
		switch (item.getPosition()){
			case 1:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "product_selection");
				startActivity(intent);
				break;
			case 2:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "set_portions");
				startActivity(intent);
				break;
			case 3:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "rinse_management");
				startActivity(intent);
				break;
			case 4:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "drip_tray_cleaning");
				startActivity(intent);
				break;
			case 5:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "clean_machine_interior");
				startActivity(intent);
				break;
			case 6:
				intent=new Intent(getApplicationContext(),HTMLViewerActivity.class);
				intent.putExtra("folder", "loading_product");
				startActivity(intent);
				break;
			default:
				break;
		}
    }
}
