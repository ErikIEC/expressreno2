package com.nestle.express.frontend_setup.activity;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.SDCardUtilities;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.SalesDatabase;
import com.nestle.express.iec_backend.machine.structure.RowSale;
import com.nestle.express.iec_backend.product.structure.DatabaseFault;

import java.util.ArrayList;

import static com.nestle.express.frontend_setup.utils.SaleTimeUtil.finishedVolume;
import static com.nestle.express.frontend_setup.utils.SaleTimeUtil.productDayVolume;

/**
 * Created by root on 16-11-14.
 */

public class FileTransfersActivity extends BaseActivity{
    ExpressApp mApp;
	TextView mClearSale;
	TextView mExportHistory;
    private ImageView mExportHistoryImageView;


    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_file_transfers);
        mApp=(ExpressApp) getApplication();

        if(getLanguage().equals("en")) {
            mExportHistoryImageView = (ImageView) findViewById(R.id.history_export);
        }
        else {
            mExportHistoryImageView = (ImageView) findViewById(R.id.history_export_fr);
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_title)).setText(R.string.file_transfers);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_10);
		mClearSale=(TextView) findViewById(R.id.file_transfer_clear_sales);
		mClearSale.setOnClickListener(mOnClearSales);
		mExportHistory = (TextView) findViewById(R.id.file_transfer_export_history);
		mExportHistory.setOnClickListener(mOnExportHistory);
    }

	public void updateProducts(View v)
	{
		try
		{
			SDCardUtilities.upgradeProductDatabase(mApp);
			makeToast(getString(R.string.import_successful));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			makeToast(e.getMessage());
		}
	}

    public void exportSettings(View v)
    {
        try
        {
            SDCardUtilities.exportSettingsAndProductDatabase(mApp);
			makeToast(getString(R.string.export_successful));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            makeToast(e.getMessage());
        }
    }

	public void importSettings(View v)
	{
		try
		{
			SDCardUtilities.importSettingsAndProductDatabase(mApp);
			makeToast(getString(R.string.import_successful));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			makeToast(e.getMessage());
		}
	}

	public void importTutorials(View v)
	{
		try
		{
			SDCardUtilities.importTutorialPack(mApp);
			makeToast(getString(R.string.import_successful));
		}
		catch (Exception e)
		{
			e.printStackTrace();
			makeToast(e.getMessage());
		}
	}

    void makeToast(String error)
    {
        Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
    }

	View.OnClickListener mOnClearSales = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			mApp.getDatabase().clearSale();
			//mApp.getSalesDatabase().clearAndLoad();
			mApp.getSalesDatabase().save();
		}
	};

    private void ExportLoadingBannerRequired() {
        mExportHistoryImageView.setVisibility(View.VISIBLE);
        Log.d("Export Banner", " VISIBLE");
    }

    private void ExportLoadingBannerDone() {
        mExportHistoryImageView.setVisibility(View.GONE);
        Log.d("Export Banner", " GONE");
    }

	View.OnClickListener mOnExportHistory = new View.OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
            new ExportDataTask().execute();
		}
	};

     private class ExportDataTask extends AsyncTask<Void,Void,Void> {
        private Exception e=null;

        // automatically done on worker thread (separate from UI thread)
        @Override
        protected Void doInBackground(Void... params) {
            try
            {
                SDCardUtilities.isUSBPresent(mApp);
            }
            catch (Exception e)
            {
                this.e = e;
                return null;
            }

            /* get data to export from database */
            ArrayList<DatabaseFault> faults=mApp.getDatabase().getFaults();
            SalesDatabase mySalesDb = mApp.getSalesDatabase();
            RowSale[] sales= mySalesDb.getRealSales();

            if (faults!=null)
            {
                for(DatabaseFault fault:faults)
                {
                }
            }

            ArrayList<String> salesData = new ArrayList<>();
            if(sales!=null)
            {
                for(RowSale sale:sales)
                {
                    String day = mySalesDb.getDate(sale.unixDayNumber);
                    String id = Integer.toString(sale.productId);
                    Double volume = finishedVolume(sale);
                    Double concVolume = productDayVolume(sale);
                    if(mAppManager.getTempUnits()==ApplicationManager.UNITS.METRIC) {
                        volume = ConversionFunctions.convertOztoMl(volume);
                        concVolume = ConversionFunctions.convertOztoMl(concVolume);
                    }
                    String volumes = String.format("%.0f, %.0f,", volume, concVolume);
                    salesData.add(day+","+id+","+volumes+"\n");
                }

                try
                {
                    SDCardUtilities.exportSalesDatabase(mApp, salesData);
                }
                catch (Exception e)
                {
                    this.e=e;
                }
            }

            return null;
        }

        // can use UI thread here
        protected void onPreExecute() {
            ExportLoadingBannerRequired();
        }

        // can use UI thread here
        protected void onPostExecute(Void result) {
            ExportLoadingBannerDone();
            if (e==null) {
                makeToast(getString(R.string.export_successful));
            }
            else {
                makeToast(e.getMessage());
            }
        }
    }
}
