package com.nestle.express.frontend_setup.activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.utils.Common;
import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.frontend_setup.view.MyGridView;


/**
 * Created by root on 16-11-14.
 */

public class OperatorSetUpActivity extends BaseActivity{
	final String TAG="OperatorSetUpActivity";
    private MyGridView mgv;

    private int[] res = {
            R.drawable.operator_breakout_02,R.drawable.operator_breakout_03,
            R.drawable.operator_breakout_04,R.drawable.operator_breakout_05,
            R.drawable.operator_breakout_06,R.drawable.operator_breakout_07,
            R.drawable.operator_breakout_08,R.drawable.operator_breakout_09,
            R.drawable.operator_breakout_10, R.drawable.operator_breakout_11,
            R.drawable.operator_breakout_12, R.drawable.operator_breakout_13,
            R.drawable.operator_breakout_14
            };

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_operator_access);
		Log.d(TAG, "");
    }

    @Override
    protected void initView() {
        mgv = (MyGridView) findViewById(R.id.mgv);
        findViewById(R.id.tv_back).setOnClickListener(this);
        ((TextView) findViewById(R.id.tv_title)).setText(R.string.operator_access_title);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_01);

    }

    @Override
    protected void initData() {
        mgv.setVertical(3);
        mgv.setHorizontal(5);
        String[] array = getResources().getStringArray(R.array.technician_screen_items);
        boolean[] booleen = SharedPreferencesUtil.getInstance().
                getEnalbleArray(Common.OPERATOR_ACCESS_ARRAY);
        int index = 0 ;
        for (int i = 0; i < array.length; i++) {
            MyGridView.GridItem item = new MyGridView.GridItem();
            item.setRes(res[index++]);
            item.setTitle(array[i]);
            item.setPosition(index);
            mgv.addCheckItem(item, booleen[i]);
        }
    }

    @Override
    protected void onPause() {
        boolean[] booleen = mgv.getCheckArray();
        SharedPreferencesUtil.getInstance().saveEnalbleArray(Common.OPERATOR_ACCESS_ARRAY, booleen);
        super.onPause();
    }

    public void setTime(View v)
	{
		startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
	}
}
