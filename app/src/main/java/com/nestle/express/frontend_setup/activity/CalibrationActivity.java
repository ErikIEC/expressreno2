package com.nestle.express.frontend_setup.activity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.CalibrationSlotView;

/**
 * Created by peng on 16/11/17.
 */
public class CalibrationActivity extends BaseActivity
{
    CalibrationSlotView mSlot1, mSlot2, mSlot3, mSlot4;

    @Override
    protected void initContentView() {
        setContentView(R.layout.activity_calibration);
        mSlot1 = (CalibrationSlotView) findViewById(R.id.calibration_slot_1);
        mSlot2 = (CalibrationSlotView) findViewById(R.id.calibration_slot_2);
        mSlot3 = (CalibrationSlotView) findViewById(R.id.calibration_slot_3);
        mSlot4 = (CalibrationSlotView) findViewById(R.id.calibration_slot_4);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {
        ((TextView)findViewById(R.id.tv_title)).setText(R.string.calibration);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_06);
        (findViewById(R.id.iv_icon)).setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        mSlot1.updateSettings();
        mSlot2.updateSettings();
        mSlot3.updateSettings();
        mSlot4.updateSettings();
    }
}
