package com.nestle.express.frontend_setup.utils;

import android.app.Activity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by peng on 2017/3/10.
 */

public class ActivityUtil {
    /**
     * Not thread safe
     */

    private static Set<Activity> mActivities = new HashSet<>();

    public static void addActivity(Activity activity){
        if(mActivities == null){
            mActivities = new HashSet<>();
        }
        mActivities.add(activity);
    }

    public static void delActivity(Activity activity){
        if(mActivities == null){
            mActivities = new HashSet<>();
        }
        mActivities.remove(activity);
    }

    public static void closeAllActivity(){
        for (Activity activity : mActivities) {
            activity.finish();
        }
    }



    public static int getSize(){
        return mActivities.size();
    }
}
