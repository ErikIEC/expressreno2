package com.nestle.express.frontend_setup.activity;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.frontend_setup.view.ProductChoiceSlotView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by peng on 16/12/1.
 */
public class ProductChoiceActivity extends BaseActivity {
    private TextView mTitleTv;
	ProductChoiceSlotView slot1, slot2, slot3, slot4;
    private ImageView mProductLoadingImageView;

        @Override
    protected void initContentView() {
        setContentView(R.layout.activity_product_choice);
    }

    @Override
    protected void initData() {
    }

    @Override
    protected void initView() {
        mTitleTv = (TextView)findViewById(R.id.tv_title);
        findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.technician_breakout_04);
        mTitleTv.setText(R.string.product_choice);
        if(getLanguage().equals("en")) {
            mProductLoadingImageView = (ImageView) findViewById(R.id.product_loading);
        }
        else {
            mProductLoadingImageView = (ImageView) findViewById(R.id.product_loading_fr);
        }

		slot1=(ProductChoiceSlotView) findViewById(R.id.product_choice_slot1);
		slot2=(ProductChoiceSlotView) findViewById(R.id.product_choice_slot2);
		slot3=(ProductChoiceSlotView) findViewById(R.id.product_choice_slot3);
		slot4=(ProductChoiceSlotView) findViewById(R.id.product_choice_slot4);

        ProductLoadingBannerRequired();
		mHandler.postDelayed(mRunnable, 1000);
    }

    private void initSpinners() {
    }

    @Override
    protected void onPause() {
        super.onPause();
        ((ExpressApp) getApplication()).getProductManager().save();
		mApp.getSalesDatabase().save();
    }


	Handler mHandler=new Handler();
	Runnable mRunnable=new Runnable()
	{
		@Override
		public void run()
		{
			slot1.setSlotAndInitialize(ProductSlot.SLOT1);
			slot2.setSlotAndInitialize(ProductSlot.SLOT2);
			slot3.setSlotAndInitialize(ProductSlot.SLOT3);
			slot4.setSlotAndInitialize(ProductSlot.SLOT4);
            ProductLoadingBannerDone();
		}
	};

    private void ProductLoadingBannerRequired() {
        mProductLoadingImageView.setVisibility(View.VISIBLE);
        Log.d("Product Loading Banner", " VISIBLE");
    }

    private void ProductLoadingBannerDone() {
        mProductLoadingImageView.setVisibility(View.GONE);
        Log.d("Product Loading Banner", " GONE");
    }
}
