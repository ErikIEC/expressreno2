package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by jason on 1/5/17.
 */
public class ConcentrateRinseSlotView extends FrameLayout
{
	ProductImageView mProductImage;
	TextView mEnableButton;
	VolumeStepperView mVolume;

	public ConcentrateRinseSlotView(Context context) {
		super(context);
	}

	public ConcentrateRinseSlotView(Context context, AttributeSet attrs) {
		super(context, attrs);
		commonInit(context, attrs);
	}

	public ConcentrateRinseSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		commonInit(context, attrs);
	}

	public void commonInit(Context context, AttributeSet attrs) {
		mApp=(ExpressApp) (((Activity) context).getApplication());
		mProductManager= mApp.getProductManager();

		View view = LayoutInflater.from(context).inflate(R.layout.view_concentrate_rinse_slot, this, true);
		mProductImage = (ProductImageView) view.findViewById(R.id.view_concentrate_rinse_image);
		mEnableButton = (TextView) view.findViewById(R.id.view_concentrate_rinse_enable);
		mVolume = (VolumeStepperView) view.findViewById(R.id.view_concentrate_rinse_volume);

		mEnableButton.setOnClickListener(mOnClick);
	}

	ExpressApp mApp;
	ProductManager mProductManager;
	NestleProduct mProduct;
	public void setSlotAndInitialize(ProductSlot slot)
	{
		mProductImage.setSlotAndInitialize(slot);
		mProduct=mProductManager.getSelectedProduct(slot);

		if(mProduct==null)
		{
			mEnableButton.setEnabled(false);
			mVolume.setEnabled(false);
			return;
		}

		mVolume.setValue(mProduct.getPostRinseVolume());

		mHandler.post(mRunnable);
	}

	public void saveProduct()
	{
		if (mProduct==null)
			return;

		mProduct.setPostRinseVolume(mVolume.getValue());
		mProductManager.updateSelectedProductSetting(mProduct);
	}

	OnClickListener mOnClick = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			if (mProduct.getPostRinseVolume()==0)
			{
				double volume=mProduct.getDefaultSetting().getPostRinseVolume();
				mProduct.setPostRinseVolume(volume);
			}
			else
				mProduct.setPostRinseVolume(0);

			mVolume.setValue(mProduct.getPostRinseVolume());

			mHandler.post(mRunnable);
		}
	};

	Handler mHandler=new Handler();
	Runnable mRunnable=new Runnable()
	{
		@Override
		public void run()
		{
			if (mProduct==null)
				return;

			if (mProduct.getPostRinseVolume()==0)
			{
				mVolume.setEnabled(false);
				mEnableButton.setPressed(false);
				mEnableButton.setAllCaps(true);
				mEnableButton.setText(R.string.off);
				mEnableButton.setTextSize(30.0f);
			}
			else
			{
				mVolume.setEnabled(true);
				mEnableButton.setPressed(true);
				mEnableButton.setAllCaps(true);
				mEnableButton.setText(R.string.on);
				mEnableButton.setTextSize(40.0f);
			}
		}
	};
}
