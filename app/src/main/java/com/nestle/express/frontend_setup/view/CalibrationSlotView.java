package com.nestle.express.frontend_setup.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.frontend_setup.R;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

/**
 * Created by peng on 16/11/30.
 */

public class CalibrationSlotView extends FrameLayout{
    final int TIME_WATER = 8000, TIME_CONCENTRATE = 8000, TIME_BRIX = 3500;
    TextView mTitleTv, mRatio, mBrix;
    ImageView mButtonWater, mButtonConcentrate, mButtonBrix, mButtonReset;
    StepperView mPumpSpeed;
    String mSettingTitle = "title";
    TextView mNumTv;
    ProductSlot mSlot;
    ProductManager mProductManager;
    MachineManager mMachineManager;
    NestleProduct mProduct;
    double mPumpOffset;

    public CalibrationSlotView(Context context) {
        super(context);
        commonInit(context, null);
    }

    public CalibrationSlotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        commonInit(context, attrs);
    }

    public CalibrationSlotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        commonInit(context, attrs);
    }

    public void commonInit(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_calibration_slot, this, true);
        mTitleTv = (TextView) view.findViewById(R.id.calibration_slot_title_tv);
        mRatio = (TextView) view.findViewById(R.id.calibration_slot_ratio);
        mBrix = (TextView) view.findViewById(R.id.calibration_slot_brix);
        mNumTv = (TextView) view.findViewById(R.id.calibration_slot_num_tv);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CalibrationSlotView);
        mSettingTitle = ta.getString(R.styleable.CalibrationSlotView_titleText);
        int slotNumber = ta.getInt(R.styleable.CalibrationSlotView_slotNumber, 0);
        ta.recycle();

        mNumTv.setText(Integer.toString(slotNumber));

        mButtonWater= (ImageView) view.findViewById(R.id.calibration_slot_water);
        mButtonConcentrate= (ImageView) view.findViewById(R.id.calibration_slot_concentrate);
        mButtonBrix= (ImageView) view.findViewById(R.id.calibration_slot_check_brix);
        mButtonReset= (ImageView) view.findViewById(R.id.calibration_slot_reset);
        mPumpSpeed = (StepperView) view.findViewById(R.id.calibration_slot_pump_speed);

		if (slotNumber==4)
			mSlot =ProductSlot.SLOT4;
		else if (slotNumber==3)
			mSlot =ProductSlot.SLOT3;
		else if (slotNumber==2)
			mSlot =ProductSlot.SLOT2;
		else //1
			mSlot =ProductSlot.SLOT1;

		ExpressApp app=(ExpressApp) (((Activity) context).getApplication());
		mProductManager=app.getProductManager();
		mMachineManager=app.getMachineManager();
		mProduct=mProductManager.getSelectedProduct(mSlot);

        if (mProduct==null)
        {
            mTitleTv.setText(context.getString(R.string.no_product));
            mBrix.setText("");
            mRatio.setText("");

            mPumpSpeed.setEnabled(false);
            mButtonWater.setEnabled(false);
            mButtonConcentrate.setEnabled(false);
            mButtonBrix.setEnabled(false);
            mButtonReset.setEnabled(false);

            return;
        }

        //mPumpSpeed.setValue(mProduct.getSpeedPump());
        mPumpSpeed.setMin(-15.0);
        mPumpSpeed.setMax(15.0);
        mPumpSpeed.setIncrement(1.0);
        mPumpOffset = (mProduct.getSpeedPump() - mProduct.getDefaultSpeedPump())/15.25;
        mPumpSpeed.setValue(mPumpOffset);
        mTitleTv.setText(mProduct.getFullName());
        mRatio.setText(mProduct.getRatioText());
        mBrix.setText(mProduct.getBrixText());

		//initializeSteppers(mProduct);

        mButtonWater.setOnClickListener(mDispenseWater);
        mButtonConcentrate.setOnClickListener(mDispenseConcentrate);
        mButtonBrix.setOnClickListener(mDispenseBrix);
		mButtonReset.setOnClickListener(mReset);
    }

	void initializeSteppers(NestleProduct product)
	{
		//mPumpSpeed.setValue(product.getSpeedPump());
        mPumpSpeed.setValue(mPumpOffset);
	}

    public void updateSettings()
    {
		if (mProduct==null)
			return;

        double configuredOffsetValue = (mProduct.getSpeedPump() - mProduct.getDefaultSpeedPump())/15.25;
        mPumpOffset = mPumpSpeed.getValue();    // get offset

        if(mPumpOffset != configuredOffsetValue) {
            double pumpSpeed = mProduct.getDefaultSpeedPump();
            pumpSpeed += (mPumpOffset * 15.25);
            mProduct.setSpeedPump(pumpSpeed);
            mProductManager.updateSelectedProductSetting(mProduct);
        }

        mMachineManager.calibrationStop(mSlot);
    }

    void lockButtons(int time)
    {
        mButtonWater.setEnabled(false);
        mButtonConcentrate.setEnabled(false);
        mButtonBrix.setEnabled(false);
        mButtonReset.setEnabled(false);

        mButtonWater.setAlpha(0.3f);
        mButtonConcentrate.setAlpha(0.3f);
        mButtonBrix.setAlpha(0.3f);
        mButtonReset.setAlpha(0.3f);

        mHandler.postDelayed(mStopDispense, time);
    }

    void stopAndUnlockButtons()
    {
        mButtonWater.setEnabled(true);
        mButtonConcentrate.setEnabled(true);
        mButtonBrix.setEnabled(true);
        mButtonReset.setEnabled(true);

        mButtonWater.setAlpha(1f);
        mButtonConcentrate.setAlpha(1f);
        mButtonBrix.setAlpha(1f);
        mButtonReset.setAlpha(1f);

        mMachineManager.calibrationStop(mSlot);
    }

    OnClickListener mDispenseWater = new OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            updateSettings();
            mMachineManager.calibrationWater(mSlot, mProduct);
            lockButtons(TIME_WATER);
        }
    };

    OnClickListener mDispenseConcentrate = new OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            updateSettings();
            mMachineManager.calibrationConcentrate(mSlot, mProduct);
            lockButtons(TIME_CONCENTRATE);
        }
    };

    OnClickListener mDispenseBrix = new OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            updateSettings();
            mMachineManager.calibrationBrix(mSlot, mProduct);
            lockButtons(TIME_BRIX);
        }
    };

	OnClickListener mReset = new OnClickListener()
	{
		@Override
		public void onClick(View view)
		{
			mPumpOffset = 0.0;
            mPumpSpeed.setValue(mPumpOffset);
            mProduct.resetSetting();
            mProductManager.updateSelectedProductSetting(mProduct);
		}
	};


    Handler mHandler = new Handler();
    Runnable mStopDispense = new Runnable()
    {
        @Override
        public void run()
        {
            stopAndUnlockButtons();
        }
    };
}
