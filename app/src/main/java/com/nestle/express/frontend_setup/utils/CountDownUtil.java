package com.nestle.express.frontend_setup.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;

import com.nestle.express.frontend_setup.activity.ServiceCenterActivity;


/**
 * Created by peng on 16-12-7.
 */

public class CountDownUtil {
    private static CountDownUtil mCountDownUtil;
    private CountDownTimer mTimer;

    public static CountDownUtil getInstance() {
        if (mCountDownUtil == null) {
            mCountDownUtil = new CountDownUtil();
        }
        return mCountDownUtil;
    }

    public void startCountDown(final Context mAppContext, int seconds) {
        stopCountDown();

        if (mTimer == null) {
            seconds *= 1000;
            mTimer = new CountDownTimer(seconds, seconds) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    Log.d("TIMEOUT", "Tech Screen Timeout ");
                    ActivityManager mActivityManager = (ActivityManager) mAppContext.getSystemService(Context.ACTIVITY_SERVICE);
                    String topActivityName = mActivityManager.getRunningTasks(1).get(0).topActivity.getClassName();
                    if (topActivityName.contains("ServiceCenterActivity")) {
                        stopCountDown();
                        ActivityUtil.closeAllActivity();
                    } else {
                        Intent intent = new Intent(mAppContext, ServiceCenterActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        mAppContext.startActivity(intent);
                    }

                }
            };
        }
        mTimer.cancel();
        mTimer.start();
    }

    public void stopCountDown() {
        if (mTimer != null) mTimer.cancel();
        mTimer = null;
    }

}
