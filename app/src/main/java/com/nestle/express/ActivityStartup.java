package com.nestle.express;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.nestle.express.frontend_ui.ActivityUI;
import com.nestle.express.iec_backend.ConversionFunctions;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.machine.MachineManager;

import java.text.SimpleDateFormat;

import static com.nestle.express.iec_backend.ExpressApp.STARTUP_FAILURE_CAUSE.UPDATE_AVAILABLE;

/**
 * Created by jason on 12/23/16.
 */
public class ActivityStartup extends ActivityFullScreen
{
	Context mContext=this;
	ExpressApp mApp;
	ApplicationManager mAppManager;
	MachineManager mMachine;

	boolean isCheckComplete;
	TextView mStatus, mTemperature, mMessage;
	String version;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_startup);
		mStatus=(TextView)findViewById(R.id.startup_status);
		mTemperature=(TextView)findViewById(R.id.startup_temperature);
		mMessage=(TextView)findViewById(R.id.startup_message);

		mApp=(ExpressApp) getApplication();
		mAppManager=mApp.getApplicationManager();
		mMachine=mApp.getMachineManager();

		version = mMachine.mAppVersion;//PackageUtilities.getPackageVersion(getPackageManager(), getPackageName());
		mMessage.setText(version);

		mHandler.postDelayed(mRunnable, 6000);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		mHandler.removeCallbacks(mRunnable);
	}

	Handler mHandler=new Handler();
	Runnable mRunnable=new Runnable()
	{
		@Override
		public void run()
		{
			isCheckComplete=true;
			mMessage.setText("");
			//mStatus
			//mTemperature
			//mMessage
			ExpressApp.STARTUP_FAILURE_CAUSE cause=mApp.getStartupFailureCase();

			Typeface fedraSans=Typeface.createFromAsset(getAssets(),"FedraSansStd-Book.otf");
			//mTemperature.setTypeface(fedraSans, Typeface.BOLD);
			try
			{
				mAppManager.setTempUnits(mAppManager.getUnits());       // add to set temp units to default units
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

            //cause = UPDATE_AVAILABLE;

			switch (cause)
			{
				case NO_ERROR:
					try
					{
						double temperature=mMachine.getStatus().getWater2();
						mTemperature.setText(getString(R.string.cooling)+" "+ConversionFunctions.formatTemperature(mAppManager, temperature));

						if (temperature<45.0)
							next();
					}
					catch (Exception e)
					{
						mTemperature.setText(getString(R.string.cooling));
					}

					mHandler.postDelayed(this, 1000);
					break;
                case UPDATE_AVAILABLE:
                    handleUpdate();
                    break;
				default:
				{
					mTemperature.setAllCaps(true);
					mTemperature.setText(R.string.startup_error);
					handleError(cause);
					findViewById(R.id.startup_skip).setAlpha(1.0f);
				}
			}
		}
	};


	void handleError(ExpressApp.STARTUP_FAILURE_CAUSE cause)
	{
		String timeStamp = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new java.util.Date());
		DialogInterface.OnClickListener listener=new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				return;
			}
		};

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle("Machine Error");
		alertDialogBuilder
				.setMessage("Version "+version+", on "+timeStamp+".\n"+
						"Take a picture of this error message and save it.\n"+
						cause.toString()+"\n"+mApp.getFailureMessage())
				.setNeutralButton("Okay", listener)
				.setCancelable(true);
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	void handleUpdate()
	{
		DialogInterface.OnClickListener listener=new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				return;
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage("Application Update found on USB stick, click OK to upgrade now?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					//if the user agrees to upgrade
					public void onClick(DialogInterface dialog, int id) {
					}
				})
				.setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// User cancelled the dialog
					}
				});
		//show the alert message
		builder.create().show();
	}


	long tapTimes[]=new long[3];
	public void skipCooldownSequence(View v)
	{
		tapTimes[2]=tapTimes[1];
		tapTimes[1]=tapTimes[0];
		tapTimes[0]=System.currentTimeMillis();

		if (tapTimes[0]-tapTimes[2]<3500)
			next();
	}

	void next()
	{
		mMachine.updateDriverSetpoints();

		Intent intent=new Intent(mContext, ActivityUI.class);
		//intent.putExtra("gitHash", BuildConfig.GitHash);

		startActivity(intent);
		finish();
	}
}
