package com.nestle.express;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by jason on 2/20/17.
 */
public class PackageUtilities
{
	public static String getPackageVersion(PackageManager manager, String packageName)
	{
		String s = "NULL";
		try{
			ApplicationInfo info = manager.getApplicationInfo(packageName, 0);
			ZipFile zipFile = new ZipFile(info.sourceDir);
			ZipEntry zipEntry = zipFile.getEntry("classes.dex");
			long time = zipEntry.getTime();

			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			s= dateFormat.format(new java.util.Date(time));
			zipFile.close();
		}
		catch(Exception e)
		{
		}
		return s;
	}
}
