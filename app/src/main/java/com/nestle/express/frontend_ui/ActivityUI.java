package com.nestle.express.frontend_ui;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.VideoView;


import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.iec_backend.ExpressApp;

import com.nestle.express.frontend_ui.nurun.activities.LocaleActivity;
import com.nestle.express.frontend_ui.nurun.utils.PageFadeTransitionUtils;


import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import com.nestle.express.frontend_ui.nurun.utils.PreferencesHelper;

public class ActivityUI extends LocaleActivity implements MediaPlayer.OnCompletionListener {
    /**
     * Views
     */
    private RelativeLayout mSceneRoot;
    private VideoView mVideoView;


    private boolean isFirstLaunch = true;
    long start;
    long end;
    /**
     * Handler and transition
     */

    private static final int PAGE_ONE = 0;
    private static final int PAGE_TWO = 1;
    private static final int PAGE_THREE = 2;
    private static final int PAGE_FOUR = 3;
    private static final int TO_MAIN = 4;
    private PageFadeTransitionUtils mPageUtils;
    private static final int DELAY_TRANSITION = 4300;

    private static final int DELAY_PRODUCT_VISIBLE = 3344;
    private static final int DEFAULT_PRODUCT_DISAPPEAR_DELAY = 2650;
    private static final int FIRST_PRODUCT_DISAPPEAR_DELAY = 2550;
    private static final int DELAY_TRANSTION_PRODUCT_ONE_TO_TWO = 500;
    private static final int DELAY_TRANSTION_PRODUCT_OTHERS = 670;
//    Blue bubble : 100 frames at 30 fps = 3.3334 seconds
//    Transition from blue to Orange : 15 frames at 30 fps : 0.5 second
//    Orange bubble : 100 frames at 30 fps = 3.3334 seconds
//    Transition from Orange to Yellow : 20 frames at 30 fps = 0.6667 second
//    Yellow  bubble : 100 frames at 30 fps = 3.3334 seconds
//    Transition from Yellow to Green : 20 frames at 30 fps = 0.6667 second
//    Green  bubble : 100 frames at 30 fps = 3.3334 seconds
//    Transition from Green to Blue : 20 frames at 30 fps =  0.6667 second
    /**
     * Products
     */
    private NestleProduct mProduct1, mProduct2, mProduct3, mProduct4;
    private ProductManager mProductManager;
    private MachineManager mMachineManager;
    //private String mCurrentVersionGitHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*try
        {
            Intent intent = getIntent();
            mCurrentVersionGitHash =intent.getStringExtra("gitHash");
        }
        catch (Exception e)
        {

        }*/


        initWindowFeature();
        setContentView(R.layout.activity_ui);
        mSceneRoot = (RelativeLayout) findViewById(R.id.sceneRootView);
        mSceneRoot.setLayerType(View.LAYER_TYPE_NONE, null);
        initProducts();
        initTransitionsViews(mSceneRoot);
        initVideoView();
//        mHandlerRunnable.post(runnableOne);
    }

    private void initWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    private void initProducts() {

        //Default locale was specified in ExpressApp according to the config value in DB
        //String defaultLanguageCode = Locale.getDefault().getLanguage();
        //PreferencesHelper.saveCurrentLocale(this, defaultLanguageCode);
        //defaultLanguageCode = (defaultLanguageCode.equals("es") ? PreferencesHelper.getDefaultLocale(this) : defaultLanguageCode);

        String defaultLanguageCode = SharedPreferencesUtil.getInstance().getLanguage();
        PreferencesHelper.saveCurrentLocale(this, defaultLanguageCode);

        //TODO Frontend : manage the currentLocale with the DatabaseLanguage to fetch the product in the specific language
        mProductManager = ((ExpressApp) getApplication()).getProductManager();
        mProduct1 = mProductManager.getSelectedProduct(ProductSlot.SLOT1, defaultLanguageCode);
        mProduct2 = mProductManager.getSelectedProduct(ProductSlot.SLOT2, defaultLanguageCode);
        mProduct3 = mProductManager.getSelectedProduct(ProductSlot.SLOT3, defaultLanguageCode);
        mProduct4 = mProductManager.getSelectedProduct(ProductSlot.SLOT4, defaultLanguageCode);

        mMachineManager = ((ExpressApp) getApplication()).getMachineManager();
        mMachineManager.exitDispenseMode();
    }

    private void initTransitionsViews(View view) {
        NestleProduct[] products = {mProduct1, mProduct2, mProduct3, mProduct4};
        mPageUtils = new PageFadeTransitionUtils(this, view, products);
        mSceneRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToMain();
            }
        });
    }

    private void initVideoView() {
        mVideoView = (VideoView) findViewById(R.id.idle_video_view);
        final String path = "android.resource://" + getPackageName() + "/" + R.raw.attract_loop_30fps_h264_wine;
//        final String path = "android.resource://" + getPackageName() + "/" + R.raw.odopod_attractl_loop;

        Uri uri = Uri.parse(path);

        mVideoView.setVideoURI(uri);
        mVideoView.setOnCompletionListener(this);

        mVideoView.setBackgroundColor(Color.WHITE); // Your color.
        mVideoView.start();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVideoView.setBackgroundColor(Color.TRANSPARENT);
                mHandlerRunnable.post(runnableOne);
            }
        });

        mVideoView.start();
    }

    private boolean mWentToMain = false;
    public void changeToMain() {
        mWentToMain = true;

        removeAllCallbacks();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //intent.putExtra("gitHash", mCurrentVersionGitHash);
        startActivity(intent);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
//        mHandler.sendEmptyMessage(TO_MAIN);
    }

    private void removeAllCallbacks() {
        mHandlerRunnable.removeCallbacks(runnableOne);
        mHandlerRunnable.removeCallbacks(runnableTwo);
        mHandlerRunnable.removeCallbacks(runnableThree);
        mHandlerRunnable.removeCallbacks(runnableFour);
    }

    @Override
    public void onActivityKeyDown(int keyCode, KeyEvent event) {
        super.onActivityKeyDown(keyCode, event);
        switch (keyCode){
            case KeyEvent.KEYCODE_M:
            case KeyEvent.KEYCODE_ENTER:
                if(!mWentToMain) {
                    changeToMain();
                }
            break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeAllCallbacks();
        mHandlerRunnable.removeCallbacksAndMessages(null);
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPageUtils.clearAssets();
        mPageUtils.clearAnimator();
    }

    int mCount = 0;

    private Handler mHandlerRunnable = new Handler();
    private Runnable runnableOne = new Runnable() {
        @Override
        public void run() {
            mHandlerRunnable.removeCallbacks(runnableOne);
            mMachineManager.exitDispenseMode();

            if(mCount > 3){
                changeToMain();
            }else{
                if(!mWentToMain) {
                    Log.d("PINGOUIN", "runnableOne");
                    mHandlerRunnable.postDelayed(runnableTwo, DELAY_PRODUCT_VISIBLE + DELAY_TRANSTION_PRODUCT_ONE_TO_TWO);
                    mPageUtils.transitionToFirstProduct((mWentToMain ? FIRST_PRODUCT_DISAPPEAR_DELAY : DEFAULT_PRODUCT_DISAPPEAR_DELAY));
                }
            }
        }
    };
    private Runnable runnableTwo = new Runnable() {
        @Override
        public void run() {
            mHandlerRunnable.removeCallbacks(runnableTwo);
            mMachineManager.exitDispenseMode();

            if(!mWentToMain) {
                Log.d("PINGOUIN", "runnableTwo");
                mHandlerRunnable.postDelayed(runnableThree, DELAY_PRODUCT_VISIBLE + DELAY_TRANSTION_PRODUCT_OTHERS);
                mPageUtils.transitionToSecondProduct(DEFAULT_PRODUCT_DISAPPEAR_DELAY);
            }
        }
    };
    private Runnable runnableThree = new Runnable() {
        @Override
        public void run() {
            mHandlerRunnable.removeCallbacks(runnableThree);
            mMachineManager.exitDispenseMode();

            if(!mWentToMain) {
                Log.d("PINGOUIN", "runnableThree");
                mHandlerRunnable.postDelayed(runnableFour, DELAY_PRODUCT_VISIBLE + DELAY_TRANSTION_PRODUCT_OTHERS);
                mPageUtils.transitionToThirdProduct(DEFAULT_PRODUCT_DISAPPEAR_DELAY);
            }
        }
    };
    private Runnable runnableFour = new Runnable() {
        @Override
        public void run() {
            mCount++;
            mHandlerRunnable.removeCallbacks(runnableFour);
            mMachineManager.exitDispenseMode();

            if(!mWentToMain) {
                Log.d("PINGOUIN", "runnableFour");
                mHandlerRunnable.postDelayed(runnableOne, DELAY_PRODUCT_VISIBLE + DELAY_TRANSTION_PRODUCT_OTHERS);
                mPageUtils.transitionToFourthProduct(DEFAULT_PRODUCT_DISAPPEAR_DELAY);
            }
        }
    };
}
