package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.util.Log;

import java.util.LinkedList;

public class AnimationUtils {

    private static LinkedList<ObjectAnimator> objectAnimators = new LinkedList();
    private static LinkedList<AnimatorSet> setAnimatitors = new LinkedList();

    public static ObjectAnimator ofFloat(Object object, String v, float... values) {
        try {
            ObjectAnimator first = objectAnimators.getFirst();
            first.setFloatValues(values);
            first.setTarget(object);
            first.setPropertyName(v);
            objectAnimators.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ObjectAnimator animator = ObjectAnimator.ofFloat(object, v, values);

        animator.addListener(objListener);
        return animator;
    }

    public static AnimatorSet getAnimatorSet(){
//        try {
//            AnimatorSet first = setAnimatitors.getFirst();
//            setAnimatitors.removeFirst();
//            return first;
//        } catch (Exception e) {
//
//        }
        AnimatorSet animator = new AnimatorSet();

        animator.addListener(setListener);
        return animator;

    }

    static Animator.AnimatorListener setListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            setAnimatitors.addLast((AnimatorSet) animation);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };
    static Animator.AnimatorListener objListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animation) {
            objectAnimators.addLast((ObjectAnimator) animation);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

}
