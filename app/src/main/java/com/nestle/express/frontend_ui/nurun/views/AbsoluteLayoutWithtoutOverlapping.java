package com.nestle.express.frontend_ui.nurun.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AbsoluteLayout;

/**
 * Created by jesserci on 16-10-17.
 */
public class AbsoluteLayoutWithtoutOverlapping extends AbsoluteLayout {

    public AbsoluteLayoutWithtoutOverlapping(Context context) {
        super(context);
    }

    public AbsoluteLayoutWithtoutOverlapping(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AbsoluteLayoutWithtoutOverlapping(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AbsoluteLayoutWithtoutOverlapping(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return true;
    }
}
