package com.nestle.express.frontend_ui.surface;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xumin on 2016/8/9.
 */
public class Layer extends BaseNode{
    private final Canvas canvas;
    private final Bitmap bitmap;
    private List<BaseNode> sprites = new ArrayList<>();
    private Rect rect = new Rect();

    private Bitmap background;

    private int color;

    public Layer(int width, int height) {
        super(width, height);
        bitmap = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);

    }

    public void addNode(BaseNode node){
        sprites.add(node);
    }
    public void setBackgroundColor(int color){
        this.color = color;
    }

    public void setBackgroundBitmap(Bitmap bitmap){
        background = bitmap;
        invalidate();
    }

    @Override
    public Bitmap getBitmap() {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        int sc = canvas.saveLayer(0, 0, 0 + getWidth(), 0 + getHeight(), null,
                Canvas.MATRIX_SAVE_FLAG |
                Canvas.CLIP_SAVE_FLAG |
                Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
                Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
                Canvas.CLIP_TO_LAYER_SAVE_FLAG);
        rect.set(0,0,getWidth(),getHeight());
        if (background != null){
            canvas.drawBitmap(background,null,rect,getPaint());
        }else {
            getPaint().setColor(color);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2, getPaint());
        }
//        canvas.drawBitmap(makeDst(getWidth() , getHeight()) , 0,0,getPaint());
        getPaint().setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
        for (BaseNode sprite:sprites) {
            Bitmap bitmap = sprite.getBitmap();
            int x = sprite.getPositionX();
            int y = sprite.getPositionY();
            rect.set(x,y,x + getWidth(),y + getHeight());
            if (bitmap != null){
                canvas.drawBitmap(bitmap,null,rect,sprite.getPaint());
            }
        }
        getPaint().setXfermode(null);
        canvas.restoreToCount(sc);
        return bitmap;

//        int sc = canvas.saveLayer(0, 0, 0 + getWidth(), 0 + getHeight(), null,
//                Canvas.MATRIX_SAVE_FLAG |
//                        Canvas.CLIP_SAVE_FLAG |
//                        Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
//                        Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
//                        Canvas.CLIP_TO_LAYER_SAVE_FLAG);
//
//        super.dispatchDraw(canvas);
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
//        canvas.drawBitmap(makeDst(getWidth(),getHeight()) , 0,0,paint);
//        paint.setXfermode(null);
//        canvas.restoreToCount(sc);
    }

    /**
     * 上切圆
     * @param w
     * @param h
     * @return
     */
    private Bitmap makeDst(int w, int h) {
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.TRANSPARENT);
        c.drawOval(new RectF(0, 0, w - 0, h - 0), p);
        return bm;
    }
}
