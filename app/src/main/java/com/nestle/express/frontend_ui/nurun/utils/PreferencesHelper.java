package com.nestle.express.frontend_ui.nurun.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.nestle.express.frontend_ui.nurun.activities.LocaleActivity;
import com.nestle.express.iec_backend.ExpressApp;


/**
 * Created by jesserci on 16-11-08.
 */

/**
 * Class to implement helper for preferences
 */
public final class PreferencesHelper {

    private static final String CURRENT_TIME_LOCALE_CHANGED = "CURRENT_TIME_LOCALE_CHANGED";
    private static final String DELAY_TO_RESET_DEFAULT_LOCALE = "DELAY_TO_RESET_DEFAULT_LOCALE";

    private static final String CURRENT_LOCALE = "CURRENT_LOCALE";

    //Default locale is set at the application level as it is fetched from DataBase
    private static final String DEFAULT_LOCALE = ExpressApp.DEFAULT_LOCALE;

    /**
     * Return the default shared preferences
     *
     * @param context : context of preferences
     * @return SharedPreferences : default shared preferences
     */
    static SharedPreferences getPrefs(Context context) {
            return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Return the editor of default shared preferences
     *
     * @param context : context of preferences
     * @return Editor : editor of default shared preferences
     */
    static SharedPreferences.Editor getPrefsEdit(Context context) {
        return getPrefs(context).edit();
    }


    /**
     * Return the default locale
     *
     * @param context : context of preferences
     * @return String : locale
     */
    public static String getDefaultLocale(Context context) {
        //todo get the current locale as default if null.
        return getPrefs(context).getString(DEFAULT_LOCALE, LocaleActivity.ENGLISH_ISO_CODE);
    }

    /**
     * Save the current locale
     *
     * @param context  : context of preferences
     * @param locale : value of default locale
     */
    public static void saveCurrentLocale(Context context, String locale) {
        SharedPreferences.Editor prefsEdit = getPrefsEdit(context);
        prefsEdit.putString(CURRENT_LOCALE, locale);
        prefsEdit.commit();
    }

    /**
     * Return the current locale
     *
     * @param context : context of preferences
     * @return String : locale
     */
    public static String getCurrentLocale(Context context) {
        return getPrefs(context).getString(CURRENT_LOCALE, LocaleActivity.ENGLISH_ISO_CODE);
    }

    /**
     * Save the current time after a locale change.
     *
     * @param context  : context of preferences
     * @param currentTimeInMillis : current time when the locale has been changed.
     */
    public static void saveCurrentTimeLocaleChanged(Context context, Long currentTimeInMillis) {
        SharedPreferences.Editor prefsEdit = getPrefsEdit(context);
        prefsEdit.putLong(CURRENT_TIME_LOCALE_CHANGED, currentTimeInMillis);
        prefsEdit.commit();
    }

    /**
     * Return the current time after a locale change.
     *
     * @param context : context of preferences
     * @return Long : the current time after a locale change.
     */
    public static Long getCurrentTimeLocaleChanged(Context context) {
        return getPrefs(context).getLong(CURRENT_TIME_LOCALE_CHANGED, 0);
    }

    /**
     * Return the delay before going back_btn to default locale.
     *
     * @param context : context of preferences
     * @return Long : delay before going back_btn to default locale.
     */
    public static int getDelayToResetDefaultLocale(Context context) {
        return getPrefs(context).getInt(DELAY_TO_RESET_DEFAULT_LOCALE, 0);
    }
}

