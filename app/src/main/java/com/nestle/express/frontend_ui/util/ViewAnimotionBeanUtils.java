package com.nestle.express.frontend_ui.util;

import android.graphics.PointF;
import android.view.View;

import com.nestle.express.frontend_ui.widget.ViewAnimotionBean;

import java.util.LinkedList;

/**
 * Created by xumin on 16/8/30.
 */
public class ViewAnimotionBeanUtils {

    private static LinkedList<ViewAnimotionBean> viewAnimotionBeans = new LinkedList();

    public static void addViewAnimotionBeans(ViewAnimotionBean bean) {
        viewAnimotionBeans.add(bean);
    }

    public static ViewAnimotionBean getViewAnimotionBean(View view , PointF newPointF , int direction, int radis, boolean isNeedScale , int offsetP , boolean isRotate) {
        try {
            ViewAnimotionBean first = viewAnimotionBeans.getFirst();
            first.setViewAnimotionBean(view , newPointF , direction , radis , isNeedScale );
            first.setOffsetP(offsetP);
            first.setRotate(isRotate);
            viewAnimotionBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction , radis , isNeedScale) ;
        viewAnimotionBean.setOffsetP(offsetP);
        viewAnimotionBean.setRotate(isRotate);
        return viewAnimotionBean ;
    }

    public static ViewAnimotionBean getViewAnimotionBean(View view , float endx , float endy , int direction, int radis) {
        PointF pointF = new PointF(endx , endy) ;
        try {
            ViewAnimotionBean first = viewAnimotionBeans.getFirst();
            first.setViewAnimotionBean(view , pointF , direction , radis , 0 );
            first.setOffsetP(2);
            first.setRotate(false);
            viewAnimotionBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , pointF , direction , radis , 0) ;
        viewAnimotionBean.setOffsetP(2);
        viewAnimotionBean.setRotate(false);
        return viewAnimotionBean ;
    }


    public static ViewAnimotionBean getViewAnimotionBean(View view , PointF newPointF , int direction, int radis) {
        try {
            ViewAnimotionBean first = viewAnimotionBeans.getFirst();
            first.setViewAnimotionBean(view , newPointF , direction , radis , false );
            first.setOffsetP(2);
            first.setRotate(false);
            viewAnimotionBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction , radis , false) ;
        viewAnimotionBean.setOffsetP(2);
        viewAnimotionBean.setRotate(false);
        return viewAnimotionBean ;
    }

    public static ViewAnimotionBean getViewAnimotionBean(View view ,PointF oldPointF, PointF newPointF , int direction, int radis, boolean isNeedScale) {
        try {
            ViewAnimotionBean first = viewAnimotionBeans.getFirst();
            first.setViewAnimotionBean(view , newPointF , direction , radis , isNeedScale);
            first.setOldPointF(oldPointF);
            viewAnimotionBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction , radis , isNeedScale) ;
        viewAnimotionBean.setOldPointF(oldPointF);
        viewAnimotionBeans.add(viewAnimotionBean) ;
        return viewAnimotionBean ;
    }

    public static ViewAnimotionBean getViewAnimotionBean(View view ,PointF oldPointF , PointF newPointF , int direction, int radis, boolean isNeedScale , int offsetP , boolean isRotate) {
        try {
            ViewAnimotionBean first = viewAnimotionBeans.getFirst();
            first.setViewAnimotionBean(view , newPointF , direction , radis , isNeedScale );
            first.setOldPointF(oldPointF);
            first.setOffsetP(offsetP);
            first.setRotate(isRotate);
            viewAnimotionBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction , radis , isNeedScale) ;
        viewAnimotionBean.setOldPointF(oldPointF);
        viewAnimotionBean.setOffsetP(offsetP);
        viewAnimotionBean.setRotate(isRotate);
        return viewAnimotionBean ;
    }
}
