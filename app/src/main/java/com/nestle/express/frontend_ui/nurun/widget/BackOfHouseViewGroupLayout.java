package com.nestle.express.frontend_ui.nurun.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.nurun.views.FedraSansFontTextView;

/**
 * Created by ricolive on 2016-11-14.
 */
public class BackOfHouseViewGroupLayout extends LinearLayout {


    public static final int DURATION = 300;

    public BackOfHouseViewGroupLayout(Context context) {
        super(context);
    }

    public BackOfHouseViewGroupLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BackOfHouseViewGroupLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void disableAllViewExcepted(int id) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (id != childchildView.getId()) {
                        childchildView.setAlpha(0.4f);
                        childchildView.setEnabled(false);
                    }
                }
            }
        }
    }

    public boolean hasButtonDisabled(){
        boolean isDisabled = false;
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (childchildView.isEnabled() == false) {
                        return true;
                    }
                }
            }
        }

        return isDisabled;
    }

    public void disableAllViewAndSetAplhaExcepted(int id) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    childchildView.setEnabled(false);
                    if (id != childchildView.getId()) {
                        childchildView.setAlpha(0.4f);
                    }
                }
            }
        }
    }

    public void enableGlassButton(int id){
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (id == childchildView.getId()) {
                        childchildView.setAlpha(1.0f);
                        childchildView.setEnabled(true);
                    }
                }
            }
        }
    }

    public void disableAllViewButtons() {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    childchildView.setEnabled(false);

                    if(childchildView instanceof FedraSansFontTextView){
                        childchildView.setBackground(getContext().getResources().getDrawable(R.drawable.shape_bottom_gray));
                    }
                }

                relativeView.setBackground(getContext().getResources().getDrawable(R.drawable.shape_bottom_gray));
            }
        }
    }

    public void showAllViewButtons() {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {

                relativeView.setVisibility(VISIBLE);
            }
        }
    }

    public void enableAllViewExcepted(SparseIntArray idArray) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (idArray.indexOfKey(childchildView.getId()) >= 0) {
                        childchildView.setAlpha(0.4f);
                    }
                }
            }
        }
    }

    public void disableViewById(int id) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (id == childchildView.getId()) {
                        childchildView.setAlpha(0.4f);
                    }
                }
            }
        }
    }

    public void showViewById(int id) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (id == childchildView.getId()) {
                        childchildView.setVisibility(View.VISIBLE);
                        relativeView.invalidate();
                    }
                }
            }
        }
        invalidate();
    }

    public void hideViewById(int id) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    if (id == childchildView.getId()) {
                        childchildView.setVisibility(View.GONE);
                        relativeView.invalidate();
                    }
                }

            }
        }
    }

    public void resetViewsToOriginalScale() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {

                relativeView.animate().scaleX(1.0f).setDuration(DURATION);
                relativeView.animate().scaleY(1.0f).setDuration(DURATION);

            }
        }
    }

    public void scaleGlassDown() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            if (relativeView instanceof LinearLayout) {
                relativeView.animate().scaleX(1.1f).setDuration(DURATION);
                relativeView.animate().scaleY(1.1f).setDuration(DURATION);
            }
        }
    }

    public void scalePourDown() {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);

            relativeView.animate().scaleX(1.1f).setDuration(DURATION);
            relativeView.animate().scaleY(1.1f).setDuration(DURATION);

        }
    }

    public void enableAllView() {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View relativeView = getChildAt(i);
            if (relativeView instanceof LinearLayout) {
                int childCount = ((LinearLayout) relativeView).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((LinearLayout) relativeView).getChildAt(j);
                    childchildView.setAlpha(1.0f);
                    childchildView.setEnabled(true);
                }
            }
        }

        resetViewsToOriginalScale();
    }
}
