package com.nestle.express.frontend_ui.util;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.Interpolator;

/**
 * Created by xumin on 16/8/11.
 */
public class PageAnimotionUtils {

    public static AnimatorSet getAnimotion(View view , float endX , float endY) {
        ObjectAnimator animator1 = AnimationUtils.ofFloat(view, "x", endX);
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "y",endY);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animator1.setObjectValues();
        animatorSet.playTogether(animator1, animator2);
        return animatorSet ;
    }

    public static AnimatorSet getAnimotion(final View view ,float startX , float startY , float endX , float endY) {
        //if(!(view instanceof ProductCircleViewGroup)){
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        //}
        ObjectAnimator animator1 = AnimationUtils.ofFloat(view, "x",startX, endX);
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "y",startY ,endY);
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animator1, animator2);
//        animatorSet.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                animatorSet.removeAllListeners();
//                if(view instanceof ProductCircleViewGroup){
//                    ((ProductCircleViewGroup)view).setLayerType(View.LAYER_TYPE_NONE, null);
//                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
        return animatorSet ;
    }

    public static AnimatorSet getAnimotion(View view , float endX , float endY , Interpolator interpolatorX , Interpolator interpolatorY) {
        ObjectAnimator animator1 = AnimationUtils.ofFloat(view, "x", endX);
        animator1.setInterpolator(interpolatorX);
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "y",endY);
        animator2.setInterpolator(interpolatorY);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animator1, animator2);

        return animatorSet ;
    }


    public static ObjectAnimator getAnimotionX (View view, float x , Interpolator interpolator) {
        ObjectAnimator animator1 = AnimationUtils.ofFloat(view, "x", x);
        animator1.setInterpolator(interpolator);
        return animator1 ;
    }

    public static ObjectAnimator getAnimotionY (View view, float y, Interpolator interpolator) {
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "y",y);
        animator2.setInterpolator(interpolator);
        return animator2 ;
    }

    public static AnimatorSet getAnimotionScale(View view , float endScaleX , float endScaleY) {
        ObjectAnimator animator1 = getAnimotionScaleX(view , endScaleX , true);
        ObjectAnimator animator2 = getAnimotionScaleY(view , endScaleY , true);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animator1, animator2);
        return animatorSet ;
    }

    public static AnimatorSet getAnimotionScale(View view , float endScaleX , float endScaleY, boolean flag) {
        ObjectAnimator animator1 = getAnimotionScaleX(view , endScaleX , false);
        ObjectAnimator animator2 = getAnimotionScaleY(view , endScaleY , false);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animator1, animator2);
        return animatorSet ;
    }

    public static ObjectAnimator getAnimotionScaleX (View view, float endScaleY , boolean flag) {
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "scaleY",view.getScaleY(),endScaleY);
        if (flag) {
            view.setPivotX(0);
        }
        return animator2 ;
    }

    public static ObjectAnimator getAnimotionScaleY (View view, float endScaleX , boolean flag) {
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "scaleX",view.getScaleY(),endScaleX);
        if (flag) {
            view.setPivotY(0);
        }
        return animator2 ;
    }

    public static ObjectAnimator getAnimotionRotation (View view, float radu) {
        ObjectAnimator animator2 = AnimationUtils.ofFloat(view, "rotation",radu);
        return animator2 ;
    }
}
