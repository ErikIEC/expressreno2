package com.nestle.express.frontend_ui.nurun.views;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;

/**
 * Created by jesserci on 16-10-06.
 */
public class SmallCircleView extends PositionView {

    public SmallCircleView(Context context) {
        super(context);
    }

    public SmallCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmallCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SmallCircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /*
    * We wont render this view under/over another one.
    * By setting this to false we ensure that this view won't have more rendering.
    * */
    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
