package com.nestle.express.frontend_ui.nurun.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.nestle.express.frontend_ui.nurun.utils.FontsCache;

/**
 * Created by Alex on 17-01-10.
 */
public class FedraSansFontTextView extends TextView {

    public FedraSansFontTextView(Context context, AttributeSet attrs){
        super(context, attrs);
        this.setTypeface(FontsCache.get(context, "fonts/fedra-sans-normal.otf"));
    }
}
