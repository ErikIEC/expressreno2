package com.nestle.express.frontend_ui.nurun.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.AbsoluteLayout;

import com.nestle.express.frontend_ui.R;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by xumin on 16/7/18.
 */
public class ViewGroupLayout extends ViewGroup {

    public static final int DURATION = 300;//1000;

    public ViewGroupLayout(Context context) {
        super(context);
        init();
    }

    public ViewGroupLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ViewGroupLayout);
        backgroundcolor = a.getColor(R.styleable.ViewGroupLayout_backgroundcolor, Color.BLACK) ;
        a.recycle();
        init();
    }

    public ViewGroupLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ViewGroupLayout, defStyleAttr, 0);
        backgroundcolor = a.getColor(R.styleable.ViewGroupLayout_backgroundcolor, Color.BLACK) ;
        a.recycle();
        init();
    }

    private void init () {
        paint = new Paint();
        paint.setFilterBitmap(false);
        setWillNotDraw(false);

        //圆环开始角度 -90° 正北方向
        mStartSweepValue = -90;
        //当前角度
        mCurrentAngle = 0;
        //当前百分比
        mCurrentPercent = 0;

        this.mTargetPercent = 100 ;
        mCurrentPercent = 0 ;
        mStokeWidth = 100 ;
        mCurrentAngle = 0 ;
        mStartSweepValue = -90;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize=MeasureSpec.getSize(widthMeasureSpec);
        int widthMode=MeasureSpec.getMode(widthMeasureSpec);
        int heightSize=MeasureSpec.getSize(heightMeasureSpec);
        int heightMode=MeasureSpec.getMode(heightMeasureSpec);
        this.setMeasuredDimension(widthSize, heightSize);
        int size = getChildCount();
        for (int x =0 ; x < size ; x++ ) {
            View view = getChildAt(x) ;
            measureChild(view, widthMeasureSpec, heightMeasureSpec);
        }
    }

    boolean mIsFirstLayout = true;
    @Override
    protected void onLayout(boolean b, int left, int top, int right, int bottom) {

        int size = getChildCount();
        for (int x =0 ; x < size ; x++ ) {
            View view = getChildAt(x) ;
//            if (x == 0) {
//                view.layout(0, 0, 100, 200);
//            } else {
                view.layout(0, 0, view.getMeasuredWidth() , view.getMeasuredHeight());
//            }

        }

        if(mIsFirstLayout) {
            mOriginalWidth = this.getLayoutParams().width;
            mOriginalHeight = this.getLayoutParams().height;

            mFinalResizeTranslationX = getContext().getResources().getDimension(R.dimen.dimen_110dp) - this.getX() + 18;
            mFinalResizeTranslationY = 0 - this.getY() - 49;

            mFinalScaleTranslationX = getContext().getResources().getDimension(R.dimen.dimen_235dp) - this.getX();
            mFinalScaleTranslationY = getContext().getResources().getDimension(R.dimen.dimen_115dp) - this.getY();

            mIsFirstLayout = false;
        }
    }

    private Paint paint ;
    private float mCurrentAngle;
    private float mStartSweepValue;
    private int mCurrentPercent, mTargetPercent;
    private int mStokeWidth = 100 ;
    private int backgroundcolor = Color.TRANSPARENT ;

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int sc = canvas.saveLayer(0, 0, 0 + getWidth(), 0 + getHeight(), null,
                Canvas.MATRIX_SAVE_FLAG |
                        Canvas.CLIP_SAVE_FLAG |
                        Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
                        Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
                        Canvas.CLIP_TO_LAYER_SAVE_FLAG);

        super.dispatchDraw(canvas);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawBitmap(makeDst(getWidth(),getHeight()) , 0,0,paint);
        paint.setXfermode(null);
        canvas.restoreToCount(sc);
//        int sc = canvas.saveLayer(0, 0, 0 + getWidth(), 0 + getHeight(), null,
//                Canvas.MATRIX_SAVE_FLAG |
//                        Canvas.CLIP_SAVE_FLAG |
//                        Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
//                        Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
//                        Canvas.CLIP_TO_LAYER_SAVE_FLAG);
//        canvas.drawBitmap(makeDst(getWidth(),getHeight()) , 0,0,paint);
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
//        super.dispatchDraw(canvas);
//        paint.setXfermode(null);
//        canvas.restoreToCount(sc);
    }

    /**
     * 上切圆
     * @param w
     * @param h
     * @return
     */
    private Bitmap makeDst(int w, int h) {
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(backgroundcolor);
        c.drawOval(new RectF(0, 0, w - 0, h - 0), p);
        return bm;
    }

    /**
     *内容的平移变换
     */
    public void startTransilation(boolean flag , int position) {
        int childViewId1 , childViewId2;
        if (position == 0) {
            childViewId1 = R.id.main_one_viewgroup_image1 ;
            childViewId2 = R.id.main_one_viewgroup_image2 ;
        } else if(position == 1) {
            childViewId1 = R.id.main_two_viewgroup_image1 ;
            childViewId2 = R.id.main_two_viewgroup_image2 ;
        } else if (position == 2) {
            childViewId1 = R.id.main_three_viewgroup_image1 ;
            childViewId2 = R.id.main_three_viewgroup_image2 ;
        } else {
            childViewId1 = R.id.main_four_viewgroup_image1 ;
            childViewId2 = R.id.main_four_viewgroup_image2 ;
        }

        int count = getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {

            View relativeView = getChildAt(i) ;

            if (relativeView instanceof AbsoluteLayout) {
                int relaCount = ((AbsoluteLayout) relativeView).getChildCount() ;
                for (int j =0 ; j < relaCount ; j++) {
                    View childchildView = ((AbsoluteLayout) relativeView).getChildAt(j);

                    //Animate glass
                    if (childchildView.getId() == childViewId1) {
                        PropertyValuesHolder mPropertyValuesHolderTranslationX = null ;
                        PropertyValuesHolder mPropertyValuesHolderTranslationY = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleX = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleY = null ;
                        if (flag) {
                            mPropertyValuesHolderTranslationX  = PropertyValuesHolder.ofFloat("translationX",0f, transilationGlassTranslationXFactor + 20);//+26
                            mPropertyValuesHolderTranslationY  = PropertyValuesHolder.ofFloat("translationY",0f, transilationGlassTranslationYFactor + 6);//-2

                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX", mScalingTransilationScalingFactor);
                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY", mScalingTransilationScalingFactor);
                        } else {
                            mPropertyValuesHolderTranslationX  = PropertyValuesHolder.ofFloat("translationX",childchildView.getX(), 0f);
                            mPropertyValuesHolderTranslationY  = PropertyValuesHolder.ofFloat("translationY",childchildView.getY(),0f);
                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",childchildView.getScaleX() , 1f);
                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",childchildView.getScaleY() , 1f);

                        }
                        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView  , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY ,mPropertyValuesHolderScaleX , mPropertyValuesHolderScaleY) ;
                        animator.setDuration(DURATION) ;
                        animator.start();
                    }
                    //Animate fruit
                    else if (childchildView.getId() == childViewId2) {
                        PropertyValuesHolder mPropertyValuesHolderTranslationX = null ;
                        PropertyValuesHolder mPropertyValuesHolderTranslationY = null ;

                        PropertyValuesHolder mPropertyValuesHolderScaleX = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleY = null ;
                        if (flag) {
                            mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",0f, transilationFruitTranslationXFactor - 33);//-28
                            mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY",0f, transilationFruitTranslationYFactor - 55);// -4//TODO: end animation

                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",childchildView.getScaleX(), mScalingTransilationScalingFactor);
                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",childchildView.getScaleY(), mScalingTransilationScalingFactor);

                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY ,mPropertyValuesHolderScaleX , mPropertyValuesHolderScaleY) ;
                            animator.setDuration(DURATION) ;
                            animator.start();
                        } else {
                            mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",0f);
                            mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY",0f);

                            mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", childchildView.getScaleX(), 1f);
                            mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", childchildView.getScaleY(), 1f);

                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY ,mPropertyValuesHolderScaleX , mPropertyValuesHolderScaleY) ;
                            animator.setDuration(DURATION) ;
                            animator.start();
                        }
                    }
                }
            }
        }
    }


    //viewGroupLayout's original dimensions when NutritionalInfoPanel is closed
    private float mOriginalWidth;
    private float mOriginalHeight;

    private float mScalingFactor = 3f;
    public float getScalingFactor(){
        return mScalingFactor;
    }
    private float mScalingTransilationScalingFactor = 0.17f;
    private float mResizeTransilationScalingFactor = 0.5f;

    //Translation values when resizing layout
    private float mFinalResizeTranslationX;
    private float mFinalResizeTranslationY;

    //Translation values when scaling layout
    private float mFinalScaleTranslationX;
    public float getmFinalScaleTranslationX(){
        return mFinalScaleTranslationX;
    }
    private float mFinalScaleTranslationY;
    public float getmFinalScaleTranslationY(){
        return mFinalScaleTranslationY;
    }

    //Translation values for the transilation animation of the glass ImageView
    private float transilationGlassTranslationXFactor = getContext().getResources().getDimension(R.dimen.dimen_030dp);
    private float transilationGlassTranslationYFactor = getContext().getResources().getDimension(R.dimen.dimen_065dp);

    //Translation values for the transilation animation of the fruit ImageView
    private float transilationFruitTranslationXFactor = getContext().getResources().getDimension(R.dimen.dimen_040dp);
    private float transilationFruitTranslationYFactor = getContext().getResources().getDimension(R.dimen.dimen_060dp);

    //OK so what's happening is that the viewGroupLayout is subject to a scaling and transition animation upon clicking "NutritionalInfoButton" to open "NutritionalInfoPanel"
        //The scaling animation appears smooth on the device but the viewGroupLayout's children views appear blurry once it is complete.
        //Hence the resize: scale completes and it is instantly canceled (setScale(1)) and an equivalent resize occurs.
        //The resize allows for a clear display of the viewGroupLayout's children views.
        //However, translation values of the viewGroupLayout need to be adjusted as they were first determined to complement the scaling.
    //Opening the NutritionalInfoPanel: toggling from scaling values to resize values at the end of scaling animation;
    //Closing the NutritionalInfoPanel: toggling from resize values to scaling values before scaling animation;
    public void scalingAndResizeToggle(boolean flag, final int position){

        final View glassImageView, fruitImageView, titleImageView, subtitleImageView, nutritionalInfoView;
        if (position == 0) {
            glassImageView = this.findViewById(R.id.main_one_viewgroup_image1);
            fruitImageView = this.findViewById(R.id.main_one_viewgroup_image2);
            titleImageView = this.findViewById(R.id.main_one_viewgroup_image3);
            subtitleImageView = this.findViewById(R.id.main_one_viewgroup_image6);
            nutritionalInfoView = this.findViewById(R.id.main_one_viewgroup_image4);
        } else if (position == 1) {
            glassImageView = this.findViewById(R.id.main_two_viewgroup_image1);
            fruitImageView = this.findViewById(R.id.main_two_viewgroup_image2);
            titleImageView = this.findViewById(R.id.main_two_viewgroup_image3);
            subtitleImageView = this.findViewById(R.id.main_two_viewgroup_image6);
            nutritionalInfoView = this.findViewById(R.id.main_two_viewgroup_image4);
        } else if (position == 2) {
            glassImageView = this.findViewById(R.id.main_three_viewgroup_image1);
            fruitImageView = this.findViewById(R.id.main_three_viewgroup_image2);
            titleImageView = this.findViewById(R.id.main_three_viewgroup_image3);
            subtitleImageView = this.findViewById(R.id.main_three_viewgroup_image6);
            nutritionalInfoView = this.findViewById(R.id.main_three_viewgroup_image4);
        } else {
            glassImageView = this.findViewById(R.id.main_four_viewgroup_image1);
            fruitImageView = this.findViewById(R.id.main_four_viewgroup_image2);
            titleImageView = this.findViewById(R.id.main_four_viewgroup_image3);
            subtitleImageView = this.findViewById(R.id.main_four_viewgroup_image6);
            nutritionalInfoView = this.findViewById(R.id.main_four_viewgroup_image4);
        }

//        Simpler fading in animation of the nutritional panel (more fluid)
        fadeInOutNutritionalPanelToggle(nutritionalInfoView, titleImageView, subtitleImageView, flag);

        //Scaling viewGroupLayout
        if(flag){
            setScaleX(mScalingFactor);
            setScaleY(mScalingFactor);

            setTranslationX(mFinalScaleTranslationX);
            setTranslationY(mFinalScaleTranslationY);

            this.getLayoutParams().width = Math.round(mOriginalWidth);
            this.getLayoutParams().height = Math.round(mOriginalHeight);

            glassImageView.setScaleX(mScalingTransilationScalingFactor);
            glassImageView.setScaleY(mScalingTransilationScalingFactor);
            glassImageView.setTranslationX(transilationGlassTranslationXFactor);
            glassImageView.setTranslationY(transilationGlassTranslationYFactor);

            fruitImageView.setScaleX(mScalingTransilationScalingFactor);
            fruitImageView.setScaleY(mScalingTransilationScalingFactor);
            fruitImageView.setTranslationX(transilationFruitTranslationXFactor);
            fruitImageView.setTranslationY(transilationFruitTranslationYFactor);

            this.requestLayout();
            startTransilation(false, position);
        }
        //Resizing viewGroupLayout
        else {
            setScaleX(1);
            setScaleY(1);

            setTranslationX(mFinalResizeTranslationX);
            setTranslationY(mFinalResizeTranslationY);

            getLayoutParams().width = Math.round(mOriginalWidth * mScalingFactor);
            getLayoutParams().height = Math.round(mOriginalHeight * mScalingFactor);
            this.requestLayout();

            glassImageView.setTranslationX(getContext().getResources().getDimension(R.dimen.dimen_50dp) - 20);//20dp
            glassImageView.setTranslationY(getContext().getResources().getDimension(R.dimen.dimen_10dp) + 100);//TODO: change glass final position
            glassImageView.setScaleX(mResizeTransilationScalingFactor);
            glassImageView.setScaleY(mResizeTransilationScalingFactor);

            fruitImageView.setTranslationX(getContext().getResources().getDimension(R.dimen.dimen_15dp) + 20);//40
            fruitImageView.setTranslationY(getContext().getResources().getDimension(R.dimen.dimen_40dp) + 30);//TODO: change fruit final position
            fruitImageView.setScaleX(mResizeTransilationScalingFactor);
            fruitImageView.setScaleY(mResizeTransilationScalingFactor);

//            Circular reveal of the nutritional panel (not very fluid)
//            showJieLouView(position);


            this.invalidate();
        }
    }

    private void fadeInOutNutritionalPanelToggle(
            final View nutritionalInfoView,
            final View titleImageView,
            final View subtitleImageView,
            boolean flag)
    {
        PropertyValuesHolder mPropertyValuesHolderAlpha = PropertyValuesHolder.ofFloat(
                "alpha",
                0f,
                1f
        );
        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(
                mPropertyValuesHolderAlpha
        );
        animator.setDuration(200);//first value is 500

        //Adjust the absolute horizontal (x) position of the subtitle dynamically in order to have it centered
//        int subtitleWidth = subtitleImageView.getWidth();
//        int bubbleWidth = Math.round(this.getWidth() * this.getScaleX());
//        subtitleImageView.setX((bubbleWidth - subtitleWidth)/2);

        if(flag){
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float fraction = valueAnimator.getAnimatedFraction();
                    nutritionalInfoView.setAlpha(1- fraction);
                    titleImageView.setAlpha(1- fraction);
                    subtitleImageView.setAlpha(1- fraction);
                }
            });
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    nutritionalInfoView.setVisibility(GONE);
                    titleImageView.setVisibility(GONE);
                    subtitleImageView.setVisibility(GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }
        else {

            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float fraction = valueAnimator.getAnimatedFraction();
                    nutritionalInfoView.setAlpha(fraction);
                    titleImageView.setAlpha(fraction);
                    subtitleImageView.setAlpha(fraction);
                }
            });
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    nutritionalInfoView.setVisibility(VISIBLE);
                    titleImageView.setVisibility(VISIBLE);
                    subtitleImageView.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animator) {

                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        }
        animator.start();
    }


    /**
     *打水的时候的平移动画
     */
    public void startDrinkTransilation(boolean flag , int position) {
        int childViewId1 = R.id.deep_purple1_image , childViewId2 = R.id.deep_purple1_image2;
        if (position == 0) {
            childViewId1 = R.id.main_one_viewgroup_image1 ;
            childViewId2 = R.id.main_one_viewgroup_image2 ;
        } else if(position == 1) {
            childViewId1 = R.id.main_two_viewgroup_image1 ;
            childViewId2 = R.id.main_two_viewgroup_image2 ;
        } else if (position == 2) {
            childViewId1 = R.id.main_three_viewgroup_image1 ;
            childViewId2 = R.id.main_three_viewgroup_image2 ;
        } else {
            childViewId1 = R.id.main_four_viewgroup_image1 ;
            childViewId2 = R.id.main_four_viewgroup_image2 ;
        }

        int count = getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {
            View relativeView = getChildAt(i) ;
            if (relativeView instanceof AbsoluteLayout) {
                int relaCount = ((AbsoluteLayout) relativeView).getChildCount() ;
                for (int j =0 ; j < relaCount ; j++) {
                    View childchildView = ((AbsoluteLayout) relativeView).getChildAt(j);
                    if (childchildView.getId() == childViewId1) {
                        PropertyValuesHolder mPropertyValuesHolderTranslationX = null ;
                        PropertyValuesHolder mPropertyValuesHolderTranslationY = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleX = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleY = null ;
                        if (flag) {
                            mPropertyValuesHolderTranslationX  = PropertyValuesHolder.ofFloat("translationX",0f, getContext().getResources().getDimension(R.dimen.dimen_30dp));
//                            mPropertyValuesHolderTranslationY  = PropertyValuesHolder.ofFloat("translationY",0f, getContext().getResources().getDimension(R.dimen.dimen_025dp));
//                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",1f, 0.55f);
//                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",1f, 0.55f);
                        } else {
                            Log.d("juice" , "onAnimationStart ------> else ") ;
                            mPropertyValuesHolderTranslationX  = PropertyValuesHolder.ofFloat("translationX",0f);
//                            mPropertyValuesHolderTranslationY  = PropertyValuesHolder.ofFloat("translationY",0f);
//                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",0.55f, 1f);
//                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",0.55f, 1f);
                        }
//                        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY ,mPropertyValuesHolderScaleX , mPropertyValuesHolderScaleY) ;
                        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX) ;
                        animator.setDuration(300) ;
                        animator.start();
                    } else if (childchildView.getId() == childViewId2) {
                        PropertyValuesHolder mPropertyValuesHolderTranslationX = null ;
                        PropertyValuesHolder mPropertyValuesHolderTranslationY = null ;

                        PropertyValuesHolder mPropertyValuesHolderScaleX = null ;
                        PropertyValuesHolder mPropertyValuesHolderScaleY = null ;
                        if (flag) {
                            mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",0f, getContext().getResources().getDimension(R.dimen.dimen_045dp));
                            mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY",0f, getContext().getResources().getDimension(R.dimen.dimen_5dp));

//                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",1f, 0.4f);
//                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",1f, 0.4f);

                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY) ;
                            animator.setDuration(300) ;
                            animator.start();
                        } else {
                            mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",0f);
                            mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY",0f);

//                            mPropertyValuesHolderScaleX  = PropertyValuesHolder.ofFloat("scaleX",0.4f, 1f);
//                            mPropertyValuesHolderScaleY  = PropertyValuesHolder.ofFloat("scaleY",0.4f, 1f);

//                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY) ;
//                            animator.setDuration(1000) ;
//                            animator.start();
                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY) ;
//                            ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(childchildView , mPropertyValuesHolderTranslationX , mPropertyValuesHolderTranslationY ,mPropertyValuesHolderScaleX , mPropertyValuesHolderScaleY) ;
                            animator.setDuration(300) ;
                            animator.start();
                        }
                    }
                }
            }
        }
    }

    /**
     * 标题的慢慢出现
     * @param fraction
     */
    public void setChildViewAlp(float fraction , boolean flag , int position) {
        int count = getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {
            View relativeView = getChildAt(i) ;
            if (relativeView instanceof AbsoluteLayout) {
                int relaCount = ((AbsoluteLayout) relativeView).getChildCount() ;
                for (int j =0 ; j < relaCount ; j++) {
                    View childchildView = ((AbsoluteLayout) relativeView).getChildAt(j);
                    int childViewId1 = R.id.deep_purple3_image , childViewId2 = R.id.deep_purple4_image, childViewId3;
                    if (position == 0) {
                        childViewId1 = R.id.main_one_viewgroup_image3 ;
                        childViewId2 = R.id.main_one_viewgroup_image4 ;
                        childViewId3 = R.id.main_one_viewgroup_image6 ;
                    } else if(position == 1) {
                        childViewId1 = R.id.main_two_viewgroup_image3 ;
                        childViewId2 = R.id.main_two_viewgroup_image4 ;
                        childViewId3 = R.id.main_two_viewgroup_image6 ;
                    } else if (position == 2) {
                        childViewId1 = R.id.main_three_viewgroup_image3 ;
                        childViewId2 = R.id.main_three_viewgroup_image4 ;
                        childViewId3 = R.id.main_three_viewgroup_image6 ;
                    } else {
                        childViewId1 = R.id.main_four_viewgroup_image3 ;
                        childViewId2 = R.id.main_four_viewgroup_image4 ;
                        childViewId3 = R.id.main_four_viewgroup_image6 ;
                    }

                    if (childchildView.getId() == childViewId1
                            || childchildView.getId() == childViewId3) {
                        childchildView.setAlpha(fraction);
                    }

                    if (!flag) {
                        if (childchildView.getId() == childViewId2) {
                            childchildView.setAlpha(fraction);
                        }
                    }
                }
            }
        }
    }

    /**
     * 内容揭露效果
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void showJieLouView (int position) {
        int[] childViewIDs;
        if (position == 0) {
            childViewIDs = new int[]{
                    R.id.main_one_viewgroup_image3,
                    R.id.main_one_viewgroup_image4,
                    R.id.main_one_viewgroup_image6
            };
        } else if (position == 1){
            childViewIDs = new int[]{
                    R.id.main_two_viewgroup_image3,
                    R.id.main_two_viewgroup_image4,
                    R.id.main_two_viewgroup_image6
            };
        } else if (position == 2) {
            childViewIDs = new int[]{
                    R.id.main_three_viewgroup_image3,
                    R.id.main_three_viewgroup_image4,
                    R.id.main_three_viewgroup_image6
            };
        } else {
            childViewIDs = new int[]{
                    R.id.main_four_viewgroup_image3,
                    R.id.main_four_viewgroup_image4,
                    R.id.main_four_viewgroup_image6
            };
        }

        int count = getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {
            View relativeView = getChildAt(i) ;
            if (relativeView instanceof AbsoluteLayout) {
                int relaCount = ((AbsoluteLayout) relativeView).getChildCount() ;
                for (int j =0 ; j < relaCount ; j++) {
                    View childchildView = ((AbsoluteLayout) relativeView).getChildAt(j);
                    if (Arrays.asList(childViewIDs).contains(childchildView.getId())) {
                        childchildView.setAlpha(1.0f);
                        Animator animator = ViewAnimationUtils.createCircularReveal(childchildView , childchildView.getWidth() / 2 , 0  , 0 ,childchildView.getHeight()) ;
                        animator.setDuration(1500);
                        animator.setInterpolator(new AccelerateDecelerateInterpolator());
                        animator.start();
                        break;
                    }
                }
            }
        }
    }

    public void setMyTransitionX (float x) {
        setTranslationX(x);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                childchildView.setTranslationX(-x);
            }
        }
    }

    public void setMyTransitionY (float y) {
        setTranslationY(y);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                childchildView.setTranslationY(-y);
            }
        }
    }


    public void setMyX (float addx) {
        setX((oldPoint.get(getId()).x + addx));
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                    if (oldPoint.containsKey(childchildView.getId())){
                        childchildView.setX((oldPoint.get(childchildView.getId()).x - addx));
                    }
            }
        }
    }


    public void setXAndMyChildX(float x) {
        setX(x);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                if (oldPoint.containsKey(childchildView.getId())){
                    childchildView.setX((oldPoint.get(childchildView.getId()).x + x));
                }
            }
        }
    }
    public void setYAndMyChildY(float y) {
        setY(y);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                if (oldPoint.containsKey(childchildView.getId())){
                    childchildView.setY(oldPoint.get(childchildView.getId()).y - (y-oldPoint.get(getId()).y));
                }
            }
        }
    }

    public void setMyY (float addY) {
        setY((oldPoint.get(getId()).y + addY));
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
//                if (childchildView.getId() == R.id.main_two_viewgroup_image1) {
//                    childchildView.setY(getContext().getResources().getDimension(R.dimen.dimen_60dp));
//                } else {
                if (oldPoint.containsKey(childchildView.getId()))
                    childchildView.setY((oldPoint.get(childchildView.getId()).y - addY));
//                }
            }
        }
    }

    public void setMyScaleX (float x) {
        setScaleX(x);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            childView1.setScaleX(1+(1-x));
//            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
//            for (int j=0 ; j< childCount; j++) {
//                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
//                childchildView.setScaleX(1+(1-x));
//            }
        }
    }

    public void setMyScaleY (float x) {
        setScaleY(x);
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            childView1.setScaleY(1+(1-x));
//            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
//            for (int j=0 ; j< childCount; j++) {
//                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
//                childchildView.setScaleY(1+(1-x));
//            }
        }
    }

    private AnimatorSet btnSexAnimatorSet ;

    private List<Animator> animators ;

    public void startAnim(long duration , int raduis ,int evlotion , int direction){

        initAnimotion(duration,raduis,evlotion,direction) ;
    }

    public void stopAnim() {
        if (btnSexAnimatorSet != null) {
            btnSexAnimatorSet.cancel();
        }

        if (animators!= null && animators.size() > 0) {
            for (int i = 0 ;  i < animators.size() ;i ++ ) {
                animators.get(i).cancel();
            }
        }
    }

    public void initAnimotion () {
        if (oldPoint.size() > 0) {
            oldPoint.clear();
        }

        oldPoint.put(getId() , new PointF(getX(),getY())) ;
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                PointF pointF = new PointF(childchildView.getX() , childchildView.getY()) ;
                oldPoint.put(childchildView.getId() , pointF) ;
            }
        }
    }

    public void putPosition (View view , float x , float y) {
        oldPoint.put(view.getId() , new PointF(x,y)) ;
    }

    private void initAnimotion(long duration , int raduis , int evolution , int deraction) {
        if (oldPoint.size() > 0) {
            oldPoint.clear();
        }

        oldPoint.put(getId() , new PointF(getX(),getY())) ;
        View childView1 = getChildAt(0) ;
        if (childView1 instanceof AbsoluteLayout) {
            int childCount = ((AbsoluteLayout)childView1).getChildCount() ;
            for (int j=0 ; j< childCount; j++) {
                View childchildView = ((AbsoluteLayout)childView1).getChildAt(j) ;
                PointF pointF = new PointF(childchildView.getX() , childchildView.getY()) ;
                oldPoint.put(childchildView.getId() , pointF) ;
            }
        }

        new Thread(new myRunnable(duration , raduis ,evolution , deraction)).start();
    }

    public void setChildeVisibilty(int visibilty) {
        int count = getChildCount() ;
        for (int i = 0 ; i < count ; i++) {
            View childeView = getChildAt(i) ;
            childeView.setVisibility(visibilty);
        }
    }

    private ArrayMap<Integer,PointF> oldPoint = new ArrayMap<>() ;

    private float Rudis ;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PointF pointF = (PointF) msg.obj;
//            setMyTransitionX(pointF.x);
//            setMyTransitionY(pointF.y);
            setMyX(pointF.x);
            setMyY(pointF.y);
        }
    } ;

    /**
     * move circle
     */
    private class myRunnable implements Runnable{

        final float minimumRadius=5;
        private long duration ;
        private float revolutions;
        private float raduis ;
        private boolean direction ;

        float angleModifier=0;
        float speedModifier=0;

        public myRunnable(long duration , int raduis, int revolutions, int dir) {
            Random rand = new Random();
            direction = rand.nextBoolean();//direction ; // 0==> Clockwise  1 ===> Counterclockwise
            angleModifier=(float) (4*Math.PI* rand.nextDouble());
            speedModifier=(float) (0.5+rand.nextDouble());
        }

        @Override
        public void run() {
            Log.d("JASON", "Run");

            PointF point = new PointF(raduis+minimumRadius , 0) ;
            Message.obtain(handler , 0 ,point).sendToTarget();


            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long startTime = System.currentTimeMillis() ;
            long endTime = 0 ;
            final float f0 = 30*9*speedModifier;
            float f = f0 ;

            float angle = 0;
            while (endTime - startTime < duration) {
                float angle_progress = (float) Math.pow(f/f0,2) ;
                float radius_progress = f/f0 ;
                angle = (float) (angle_progress * revolutions * 2* Math.PI) + angleModifier;

                if (direction)
                    angle=-angle;

                float r= minimumRadius+radius_progress * this.raduis;

                float x = (float) (r * Math.cos(angle)) ; //x==>getX() ;
                float y = (float) (r * Math.sin(angle)) ; //y==>getY();

                PointF pointF = new PointF(x , y) ;
                Message.obtain(handler , 0 ,pointF).sendToTarget();

                f-- ;
                if (f <= 30)
                    break;


                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                endTime = System.currentTimeMillis() ;
            }
        }
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
