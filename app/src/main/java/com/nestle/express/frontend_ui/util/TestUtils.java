package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

/**
 * Created by xumin on 16/8/13.
 */
public class TestUtils {

    public static void moveCirclePath(final View view , final float maxRadius, final float revolutions , final int dirction){

//        if (view instanceof ViewGroupLayout) {
//            ((ViewGroupLayout)view).initAnimotion();
//        } else if (view instanceof MyView) {
//            ((MyView)view).initAnimotion();
//        }
//
//        AnimatorSet animatorSet = PageAnimotionUtils.getAnimotion(view , view.getX() , view.getY() - maxRadius) ;
//        animatorSet.setDuration(800) ;
//        animatorSet.start();
//        animatorSet.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                moveCirclePath1(view , maxRadius , revolutions , dirction);
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });

    }

    //Try Radius=50 and revolutions=3
    private static void moveCirclePath1(final View view , final float maxRadius, final float revolutions , final int dirction)
    {
        ValueAnimator animation = ValueAnimator.ofFloat( 0, 1);
        animation.setDuration(10000);

        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation)
            {
	    	/*
	    		Progess is a value between 1.0 and 0.0
	    		it starts a 1.0 and decreases towards
	    		0 as the animation progresses.
	    		Because it is squared (^2) it starts fast
	    		and decelerates until it reaches the end
	    	*/
                float progress = (float) Math.pow(1 - (float)animation.getAnimatedValue(), 2);
                float radius = progress * maxRadius;
                float angle = (float) (progress * revolutions * 2.0 * Math.PI);

                int x = (int) (radius * Math.sin(angle));
                int y = (int) (radius * Math.cos(angle));
                if (dirction == 0) {
                    x = -x ;
                    y = -y ;
                }

                if ((float)animation.getAnimatedValue() < 0.1f) {
                    System.out.println("debug:(x,y) = " + x + "," + y + ",T="+progress);
                }


                if (view instanceof ViewGroupLayout) {
                    ((ViewGroupLayout)view).setMyX(x);
                    ((ViewGroupLayout)view).setMyY(y);
                } else if (view instanceof MyView){
                    ((MyView)view).setMyX(x);
                    ((MyView)view).setMyY(y);
                }

            }
        });

        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(main_one_viewgroup , "translationY" , maxRadius , 0);
//                objectAnimator.setDuration(600) ;
//                objectAnimator.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

}
