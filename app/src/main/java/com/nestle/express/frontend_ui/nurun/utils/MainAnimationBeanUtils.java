package com.nestle.express.frontend_ui.nurun.utils;

import android.graphics.PointF;
import android.view.View;

import java.util.LinkedList;

/**
 * Created by jesserci on 16-10-13.
 */
public class MainAnimationBeanUtils {

    private static LinkedList<MainAnimationBean> mMainAnimationBeans = new LinkedList();

    public static void addViewAnimotionBeans(MainAnimationBean bean) {
        mMainAnimationBeans.add(bean);
    }

    public static MainAnimationBean getViewAnimotionBean(View view, PointF newPointF, int direction, int radis, boolean isNeedScale, int offsetP, boolean isRotate) {
        try {
            MainAnimationBean first = mMainAnimationBeans.getFirst();
            first.setViewAnimotionBean(view, newPointF, direction, radis, isNeedScale, true);
            first.setOffsetP(offsetP);
            first.setRotate(isRotate);
            mMainAnimationBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        MainAnimationBean viewAnimotionBean = new MainAnimationBean(view, newPointF, direction, radis, isNeedScale);
        viewAnimotionBean.setOffsetP(offsetP);
        viewAnimotionBean.setRotate(isRotate);
        return viewAnimotionBean;
    }
    public static MainAnimationBean getViewAnimotionBean(View view, PointF newPointF, int direction, int radis, boolean isNeedScale, int offsetP, boolean isRotate, boolean isAnimateChildren) {
        try {
            MainAnimationBean first = mMainAnimationBeans.getFirst();
            first.setViewAnimotionBean(view, newPointF, direction, radis, isNeedScale, isAnimateChildren);
            first.setOffsetP(offsetP);
            first.setRotate(isRotate);
            mMainAnimationBeans.removeFirst();
            return first;
        } catch (Exception e) {

        }

        MainAnimationBean viewAnimotionBean = new MainAnimationBean(view, newPointF, direction, radis, isNeedScale, isAnimateChildren);
        viewAnimotionBean.setOffsetP(offsetP);
        viewAnimotionBean.setRotate(isRotate);
        return viewAnimotionBean;
    }
    
}
