package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.Keyframe;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.nestle.express.frontend_ui.nurun.views.ProductCircleViewGroup;

import java.util.LinkedList;

/**
 * Created by jesserci on 16-10-13.
 */
public class MainAnimationUtils {

    private static LinkedList<ValueAnimator> mValueAnimators = new LinkedList<>();

    public static ValueAnimator getValueAnimator(final MainAnimationBean mainAnimationBean, long duration) {
        if (!(mainAnimationBean.getView() instanceof ProductCircleViewGroup)) {
            mainAnimationBean.getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        ValueAnimator valueAnimator;
        UpdateListener listener = new UpdateListener();

        if (mValueAnimators.isEmpty()) {
            Keyframe kf0 = Keyframe.ofFloat(0, 0.0f);
            Keyframe kf1 = Keyframe.ofFloat(0.07f, 1.0f);
            Keyframe kf2 = Keyframe.ofFloat(0.8f, 1.5f);
            Keyframe kf3 = Keyframe.ofFloat(1.f, 1.5f);
            PropertyValuesHolder valuePropertyValues = PropertyValuesHolder.ofKeyframe("value", kf0, kf1, kf2, kf3);
            valueAnimator = ValueAnimator.ofPropertyValuesHolder(valuePropertyValues);

            valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            valueAnimator = mValueAnimators.getFirst();
            mValueAnimators.removeFirst();
        }

        valueAnimator.setDuration(duration);
        listener.setData(mainAnimationBean);
        valueAnimator.addUpdateListener(listener);

        return valueAnimator;
    }

    /**
     * 测试
     */
    static class UpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private MainAnimationBean bean;

        public void setData(MainAnimationBean mainAnimationBean) {
            this.bean = mainAnimationBean;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {

            if (animation.getAnimatedFraction() > 0.7f) {
                animation.cancel();
            }

            float valueAnimator = (float) animation.getAnimatedValue("value");
            if (valueAnimator <= 1.0f && bean.getView() instanceof ProductCircleViewGroup) {
                int offsetP = bean.getOffsetP();

                if (offsetP == 0) {
                    ((ProductCircleViewGroup) bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator * valueAnimator, bean.mAnimateChild);
                    ((ProductCircleViewGroup) bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator, bean.mAnimateChild);
                } else if (offsetP == 1) {
                    ((ProductCircleViewGroup) bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator, bean.mAnimateChild);
                    ((ProductCircleViewGroup) bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getOldPointF().y - bean.getView().getY())* valueAnimator* valueAnimator, bean.mAnimateChild);
                } else {
                    ((ProductCircleViewGroup) bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator, bean.mAnimateChild);
                    ((ProductCircleViewGroup) bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getOldPointF().y - bean.getView().getY()) * valueAnimator, bean.mAnimateChild);
                }
            } else {
                //1-1.5

                if (bean.isRotate()) {
                    float progress = (float) Math.pow((float) (1 - Math.abs(1 - valueAnimator) * 2), 2);  //PI 3/ 4
                    float radius = progress * bean.getRadis();
                    float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                    int x, y;

                    if (bean.getDirection() == 0) {
                        x = (int) (radius * Math.sin(angle));
                        y = (int) (radius * Math.cos(angle));
                    } else {
                        x = (int) (radius * Math.cos(angle));
                        y = (int) (radius * Math.sin(angle));
                    }


                    if (bean.getView() instanceof ProductCircleViewGroup) {
                        ((ProductCircleViewGroup) bean.getView()).setMyX(x, bean.mAnimateChild);
                        ((ProductCircleViewGroup) bean.getView()).setMyY(y, bean.mAnimateChild);
                    }

                    if (bean.isNeedScale()) {
                        ProductCircleViewGroup productCircleViewGroup = ((ProductCircleViewGroup) bean.getView());
                        if (valueAnimator < 1.05f) {
                            if (productCircleViewGroup != null && productCircleViewGroup.getScaleX() < 1.03f) {

                            }
                        } else if (valueAnimator > 1.05f && valueAnimator < 1.15f) {
                            if (productCircleViewGroup != null && productCircleViewGroup.getScaleX() > 1f) {

                            }
                        } else if (valueAnimator > 1.15f) {
                            if (productCircleViewGroup != null && productCircleViewGroup.getScaleX() < 1.05f) {

                            }
                        }
                    }

                }
            }
        }
    }
}
