package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.nestle.express.frontend_ui.nurun.views.SmallCircleView;
import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by xumin on 16/8/21.
 */
public class CircleAnimotionUtils {

    private static LinkedList<ValueAnimator> objectAnimators = new LinkedList<>();
    private static Map<ValueAnimator,UpdateListener> updateListeners = new HashMap<>();
    private static Map<ValueAnimator,UpdateListener1> updateListeners1 = new HashMap<>();

    /**
     * center circle animotion
     *
     * @param view
     * @param maxRadius
     * @param revolutions
     * @param dirction
     * @param duraction
     * @param isNeedScale
     */
    public static void moveCirclePath(final View view, final float maxRadius, final float revolutions, final int dirction, final long duraction, final boolean isNeedScale) {
        try {
            ValueAnimator first = objectAnimators.getFirst();
            objectAnimators.removeFirst();
            UpdateListener listener = updateListeners.get(first);
            listener.setData(isNeedScale,maxRadius,revolutions,view);
            first.start();
            return;
        } catch (Exception e) {

        }
        ValueAnimator animation = ValueAnimator.ofFloat(0, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1);
        animation.setDuration(duraction);
        UpdateListener listener = new UpdateListener();
        updateListeners.put(animation,listener);
        listener.setData(isNeedScale,maxRadius,revolutions,view);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animation.start();
    }

    static class UpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private boolean isNeedScale;
        private float maxRadius;
        private double revolutions;
        private View view;

        public void setData(boolean isNeedScale, float maxRadius, double revolutions, View view) {
            this.isNeedScale = isNeedScale;
            this.maxRadius = maxRadius;
            this.revolutions = revolutions;
            this.view = view;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (animation.getAnimatedFraction() > 0.6f) {
                animation.cancel();
            }

            if (isNeedScale) {
                float progress = (float) Math.pow((float) (1 - animation.getAnimatedFraction()), 2);  //PI 3/ 4
                float radius = progress * maxRadius;
                float angle = (float) (progress * ((11 * Math.PI / 6) + revolutions * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6
                int x, y;
                x = (int) (radius * Math.sin(angle));
                y = (int) (radius * Math.cos(angle));

                if (view instanceof ViewGroupLayout) {
                    ((ViewGroupLayout) view).setMyX(x);
                    ((ViewGroupLayout) view).setMyY(y);
//                    if (animation.getAnimatedFraction() < 0.05f) {
//                        if (view != null && view.getScaleX() < 1.02f) {
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() + 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() + 0.001f);
//                        }
//                    } else if (animation.getAnimatedFraction() > 0.05f && animation.getAnimatedFraction() < 0.2f) {
//                        if (view != null && view.getScaleX() > 0.95f) {
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() - 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() - 0.001f);
//                        }
//                    } else if (animation.getAnimatedFraction() > 0.2f) {
//                        if (view != null && view.getScaleX() < 1.05f) {
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() + 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() + 0.001f);
//                        }
//                    }
                }

            } else {
                float progress = (float) Math.pow((float) (1 - animation.getAnimatedFraction()), 2);  //PI 3/ 4
                float radius = progress * maxRadius;
                float angle = (float) (progress * ((4 * Math.PI / 3) + revolutions * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6
                int x, y;
                x = (int) (radius * Math.cos(angle));
                y = (int) (radius * Math.sin(angle));

                if (view instanceof ViewGroupLayout) {
                    ((ViewGroupLayout) view).setMyX(x);
                    ((ViewGroupLayout) view).setMyY(y);
//                    if (animation.getAnimatedFraction() < 0.05f) {
//                        if (view != null && view.getScaleX() < 1.02f) {
////                            view.setPivotX(view.getMeasuredWidth());
////                            view.setPivotY(0);
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() + 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() + 0.001f);
//                        }
//                    } else if (animation.getAnimatedFraction() > 0.05f && animation.getAnimatedFraction() < 0.2f) {
//                        if (view != null && view.getScaleX() > 0.95f) {
////                            view.setPivotX(view.getMeasuredWidth() / 2);
////                            view.setPivotY(view.getMeasuredHeight() / 2);
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() - 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() - 0.001f);
//                        }
//                    } else if (animation.getAnimatedFraction() > 0.2f) {
//                        if (view != null && view.getScaleX() < 1.05f) {
//                            ((ViewGroupLayout) view).setMyScaleX(view.getScaleX() + 0.001f);
//                            ((ViewGroupLayout) view).setMyScaleY(view.getScaleY() + 0.001f);
//                        }
//                    }
                }

            }
        }
    }

    public static void moveCircleLine(final View view, final float maxRadius, final float revolutions, final int dirction, final long duraction) {

        if (view instanceof ViewGroupLayout) {
            ((ViewGroupLayout) view).initAnimotion();
        } else if (view instanceof MyView) {
            ((MyView) view).initAnimotion();
        }

        PropertyValuesHolder mPropertyValuesHolderScaleX, mPropertyValuesHolderScaleY;

        if (dirction == 0) {
            mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("x", 0, (int) (-maxRadius / 2));
            mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("y", 0, (int) ((Math.sqrt(3) / 2) * maxRadius));
        } else {
            mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("x", 0, (int) ((Math.sqrt(3) / 2) * maxRadius));
            mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("y", 0, (int) (-maxRadius / 2));
        }


        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX, mPropertyValuesHolderScaleY);
        animator.setDuration(300);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float x = (float) animation.getAnimatedValue("x");
                float y = (float) animation.getAnimatedValue("y");
                if (view instanceof ViewGroupLayout) {
                    ((ViewGroupLayout) view).setMyX(x);
                    ((ViewGroupLayout) view).setMyY(y);
                } else if (view instanceof MyView) {
                    ((MyView) view).setMyX(x);
                    ((MyView) view).setMyY(y);
                }
            }
        });

        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                moveCirclePath(view, maxRadius, revolutions, dirction, duraction);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animator.start();
    }

    /**
     * center circle animotion
     *
     * @param view
     * @param maxRadius
     * @param revolutions
     * @param dirction
     * @param duraction
     * @param isNeedScale
     */
    public static ValueAnimator moveCirclePath1(final View view, final float maxRadius, final float revolutions, final int dirction, final long duraction, final boolean isNeedScale) {
        try {
            ValueAnimator first = objectAnimators.getFirst();
            objectAnimators.removeFirst();
            UpdateListener listener = updateListeners.get(first);
            listener.setData(isNeedScale,maxRadius,revolutions,view);
            return first;
        } catch (Exception e) {

        }
        ValueAnimator animation = ValueAnimator.ofFloat(0, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1);
        animation.setDuration(duraction);
        UpdateListener listener = new UpdateListener();
        updateListeners.put(animation,listener);
        listener.setData(isNeedScale,maxRadius,revolutions,view);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        return  animation ;
    }

    public static ValueAnimator moveCirclePath1(final View view, final float maxRadius, final float revolutions, final int dirction, final long duraction) {
        if (!(view instanceof ViewGroupLayout)) {
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        try {
            ValueAnimator first = objectAnimators.getFirst();
            first.setDuration(duraction) ;
            objectAnimators.removeFirst();
            UpdateListener1 listener = updateListeners1.get(first);
            listener.setData(view ,maxRadius,revolutions,dirction);
            return first;
        } catch (Exception e) {

        }

        ValueAnimator animation = ValueAnimator.ofFloat(0, 1);
        animation.setDuration(duraction);
        UpdateListener1 listener = new UpdateListener1();
        updateListeners1.put(animation,listener);
        listener.setData(view ,maxRadius,revolutions,dirction);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);
                if (!(view instanceof ViewGroupLayout)) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
//        animation.start();
        return animation ;
    }


    public static void moveCirclePath(final View view, final float maxRadius, final float revolutions, final int dirction, final long duraction) {
        if (!(view instanceof ViewGroupLayout)) {
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        try {
            ValueAnimator first = objectAnimators.getFirst();
            objectAnimators.removeFirst();
            UpdateListener1 listener = updateListeners1.get(first);
            listener.setData(view ,maxRadius,revolutions,dirction);
            first.start();
            return;
        } catch (Exception e) {

        }

        ValueAnimator animation = ValueAnimator.ofFloat(0, 1);
        animation.setDuration(duraction);
        UpdateListener1 listener = new UpdateListener1();
        updateListeners1.put(animation,listener);
        listener.setData(view ,maxRadius,revolutions,dirction);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);
                if (!(view instanceof ViewGroupLayout)) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animation.start();
    }

    static class UpdateListener1 implements ValueAnimator.AnimatorUpdateListener {
        private int dirction;
        private float maxRadius;
        private double revolutions;
        private View view;

        public void setData(View view,float maxRadius, float revolutions, int dirction) {
            this.dirction = dirction;
            this.maxRadius = maxRadius;
            this.revolutions = revolutions;
            this.view = view;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (animation.getAnimatedFraction() > 0.5f) {
                animation.cancel();
            }

            float progress = (float) Math.pow((float) (1 - animation.getAnimatedFraction()), 2);  //PI 3/ 4
            float radius = progress * maxRadius;
            float angle = (float) (progress * ((11 * Math.PI / 6) + revolutions * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

            int x, y;

            if (dirction == 0) {
                x = (int) (radius * Math.sin(angle));
                y = (int) (radius * Math.cos(angle));
            } else {
                x = (int) (radius * Math.cos(angle));
                y = (int) (radius * Math.sin(angle));
            }

            if (view instanceof ViewGroupLayout) {
                ((ViewGroupLayout) view).setMyX(x);
                ((ViewGroupLayout) view).setMyY(y);
            } else if (view instanceof MyView) {
                ((MyView) view).setMyX(x);
                ((MyView) view).setMyY(y);
            } else if (view instanceof SmallCircleView) {
                ((SmallCircleView) view).setMyX(x);
                ((SmallCircleView) view).setMyY(y);
        }
        }
    }

    public static int getTransleX(int raduis, int direction) {
        int x;
        if (direction == 0) {
            x = (int) (-raduis / 2.0f);
        } else {
            x = (int) ((Math.sqrt(3) / 2.0f) * raduis);
        }
        return x;
    }

    public static int getTransleY(int raduis, int direction) {
        int y;
        if (direction == 0) {
            y = (int) ((Math.sqrt(3) / 2.0f) * raduis);
        } else {
            y = (int) (-raduis / 2.0f);
        }
        return y;
    }

    public static void testMovePath(final View view , final int radis , float endX , float endY) {
        final PointF oldPointF = new PointF(view.getX() , view.getY()) ;
        final PointF newPointF = new PointF(endX , endY) ;
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1f,1.05f,1.1f,1.15f,1.2f,1.25f,1.3f,1.35f,1.4f,1.45f,1.5f) ;
        valueAnimator.setDuration(9000) ;
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float valueAnimator = (float) animation.getAnimatedValue();
                if (valueAnimator <= 1.0f) {
                    view.setX(oldPointF.x + (newPointF.x - oldPointF.x)*valueAnimator*valueAnimator);
//                    view.setX(oldPointF.x + (newPointF.x - oldPointF.x)*valueAnimator);
                    view.setY(oldPointF.y + (newPointF.y - oldPointF.y)*valueAnimator);
//                    view.setY(oldPointF.y + (newPointF.y - oldPointF.y)*valueAnimator*valueAnimator);
                } else {
                    //1-1.5
                    float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
                    float radius = progress * radis;
                    float angle = (float) (progress * ((11 * Math.PI / 6) + 3 * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                    int x, y;

//                    if (dirction == 0) {
                        x = (int) (radius * Math.sin(angle));
                        y = (int) (radius * Math.cos(angle));
//                    } else {
//                        x = (int) (radius * Math.cos(angle));
//                        y = (int) (radius * Math.sin(angle));
//                    }
                    if (view instanceof ViewGroupLayout) {
                        ((ViewGroupLayout) view).setMyX(x);
                        ((ViewGroupLayout) view).setMyY(y);
                    } else if (view instanceof MyView) {
                        ((MyView) view).setMyX(x);
                        ((MyView) view).setMyY(y);
                    }
                }

            }
        });

        valueAnimator.start();
    }

    /**
     *
     * @param view
     * @param radis
     * @param evalotion
     * @param direction
     * @param endX
     * @param endY
     */
    public static void testMovePath(final View view , final int radis , final int evalotion , final int direction, float endX , float endY) {
        final PointF oldPointF = new PointF(view.getX() , view.getY()) ;
        final PointF newPointF = new PointF(endX , endY) ;
        final ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1f,1.05f,1.1f,1.15f,1.2f,1.25f,1.3f,1.35f,1.4f,1.45f,1.5f) ;
        valueAnimator.setDuration(10000) ;
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float valueAnimator = (float) animation.getAnimatedValue();
                if (valueAnimator <= 1.0f) {
                    view.setX(oldPointF.x + (newPointF.x - oldPointF.x)*valueAnimator);
                    view.setY(oldPointF.y + (newPointF.y - oldPointF.y)*valueAnimator);
                } else {
                    //1-1.5
                    float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
                    float radius = progress * radis;
                    float angle = (float) (progress * ((11 * Math.PI / 6) + evalotion * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                    int x, y;

                    if (direction == 0) {
                    x = (int) (radius * Math.sin(angle));
                    y = (int) (radius * Math.cos(angle));
                    } else {
                        x = (int) (radius * Math.cos(angle));
                        y = (int) (radius * Math.sin(angle));
                    }
                    if (view instanceof ViewGroupLayout) {
                        ((ViewGroupLayout) view).setMyX(x);
                        ((ViewGroupLayout) view).setMyY(y);
                    } else if (view instanceof MyView) {
                        ((MyView) view).setMyX(x);
                        ((MyView) view).setMyY(y);
                    }
                }

            }
        });

        valueAnimator.start();
    }

}
