package com.nestle.express.frontend_ui.surface;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by xumin on 2016/8/9.
 */
public class Sprite extends BaseNode{

    private Canvas canvas;
    private Bitmap bitmap;
    private Bitmap mBitmap ;

    public Sprite(int width, int height) {
        super(width, height);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setAntiAlias(true);
        setPaint(paint);
        bitmap = Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        canvas.drawColor(Color.TRANSPARENT);
    }

    public void setmBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap ;
    }

    public void setColor(int color){
        getPaint().setColor(color);
    }
    @Override
    public Bitmap getBitmap() {
        if (mBitmap!=null) {
            return mBitmap ;
        }
        canvas.drawCircle(getWidth() / 2,getHeight() / 2 ,getWidth() / 2,getPaint());
        return bitmap;
    }
}
