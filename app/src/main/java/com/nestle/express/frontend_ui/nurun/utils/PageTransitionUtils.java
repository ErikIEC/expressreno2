package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.nurun.views.ProductCircleViewGroup;
import com.nestle.express.frontend_ui.nurun.views.SmallCircleView;
import com.nestle.express.frontend_ui.util.PageAnimotionUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jesserci on 16-10-06.
 */
public class PageTransitionUtils {

    private Context mContext;

    private AnimatorSet mMainAnimatorSet;
    private AnimatorSet mMainTransitionAnimatorSet;
    private AnimatorSet mMainCircleAnimatorSet;
    private boolean mIsSmallCircleAnimationStarted = false;
    private View mMainView;
    private SmallCircleView mCircleBlue,
            mCircleBlue1,
            mCircleBlue2,
            mCircleGreen,
            mCircleGreen1,
            mCircleOrange,
            mCircleOrange1,
            mCircleOrange2,
            mCircleOrange3;

    private int mCircleBlueRadius,
            mCircleBlue1Radius,
            mCircleBlue2Radius,
            mCircleGreenRadius,
            mCircleGreen1Radius,
            mCircleOrangeRadius,
            mCircleOrange1Radius,
            mCircleOrange2Radius,
            mCircleOrange3Radius;

    private CircleAnimationUtils.Direction mCircleBlueDirection,
            mCircleBlue1Direction,
            mCircleBlue2Direction,
            mCircleGreenDirection,
            mCircleGreen1Direction,
            mCircleOrangeDirection,
            mCircleOrange1Direction,
            mCircleOrange2Direction,
            mCircleOrange3Direction;


    private Map<PRODUCT_KEY, ProductViewHolder> mProductViewHolders = new HashMap<PRODUCT_KEY, ProductViewHolder>();


    private enum PRODUCT_KEY {
        PRODUCT_ONE,
        PRODUCT_TWO,
        PRODUCT_THREE,
        PRODUCT_FOUR
    }

    class ProductViewHolder {
        public ProductCircleViewGroup mProductViewGroup;
        public ImageView mProductJuice;
        public ImageView mProductFruit;
        public ImageView mProductTitle;
        public AbsoluteLayout mProductLayout;
        public ImageView mProductSubtitle;
        public AbsoluteLayout mChildLayout;

        ProductViewHolder(ProductCircleViewGroup productCircleViewGroup, ImageView productJuice, ImageView productFruit, ImageView productTitle, AbsoluteLayout productLayout, ImageView productSubtitle) {
            mProductViewGroup = productCircleViewGroup;
            mProductJuice = productJuice;
            mProductFruit = productFruit;
            mProductTitle = productTitle;
            mProductLayout = productLayout;
            mProductSubtitle = productSubtitle;
            mChildLayout = (AbsoluteLayout) mProductViewGroup.getChildAt(0);

            mProductViewGroup.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//            mChildLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//            mProductGlass.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//            mProductFruit.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//            mProductTitle.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//            mProductLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//            mProductSubtitle.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    private PointF mComingUpProductPrositionPointF;
    private PointF mEnteringProductPositionPointF;
    private PointF mCenterProductPositionPointF;
    private PointF mExitingProductPositionPointF;
    private PointF mCenterProductJuicePointF;
    private PointF mCenterProductFruitPointF;
    private PointF mCenterProductTitlePointF;
    private PointF mCenterProductLayoutPointF;
    private PointF mCenterProductSubtitlePointF;

    private enum Corner {TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT}

    public PageTransitionUtils(Context context, View view) {
        mContext = context;
        mMainView = view;
        initView(view);
        initRadiusAndDirection();
        initCenterProductPointF();
    }

    private void initView(View view) {
        mCircleBlue = (SmallCircleView) view.findViewById(R.id.circle_blue);
        mCircleBlue1 = (SmallCircleView) view.findViewById(R.id.circle_blue1);
        mCircleBlue2 = (SmallCircleView) view.findViewById(R.id.circle_blue2);
        mCircleGreen = (SmallCircleView) view.findViewById(R.id.circle_green);
        mCircleGreen1 = (SmallCircleView) view.findViewById(R.id.circle_green1);
        mCircleOrange = (SmallCircleView) view.findViewById(R.id.circle_orange);
        mCircleOrange1 = (SmallCircleView) view.findViewById(R.id.circle_orange1);
        mCircleOrange2 = (SmallCircleView) view.findViewById(R.id.circle_orange2);
        mCircleOrange3 = (SmallCircleView) view.findViewById(R.id.circle_orange3);

        mCircleBlue.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleBlue1.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleBlue2.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleGreen.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleGreen1.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleOrange.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleOrange1.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleOrange2.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mCircleOrange3.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        //Product one
        ProductViewHolder productOneViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_one_viewgroup),
                (ImageView) view.findViewById(R.id.product_one_juice),
                (ImageView) view.findViewById(R.id.product_one_fruit),
                (ImageView) view.findViewById(R.id.product_one_title),
                (AbsoluteLayout) view.findViewById(R.id.product_one_layout),
                (ImageView) view.findViewById(R.id.product_one_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_ONE, productOneViewHolder);

        //Product two
        ProductViewHolder productTwoViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_two_viewgroup),
                (ImageView) view.findViewById(R.id.product_two_juice),
                (ImageView) view.findViewById(R.id.product_two_fruit),
                (ImageView) view.findViewById(R.id.product_two_title),
                (AbsoluteLayout) view.findViewById(R.id.product_two_layout),
                (ImageView) view.findViewById(R.id.product_two_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_TWO, productTwoViewHolder);

        //Product three
        ProductViewHolder productThreeViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_three_viewgroup),
                (ImageView) view.findViewById(R.id.product_three_juice),
                (ImageView) view.findViewById(R.id.product_three_fruit),
                (ImageView) view.findViewById(R.id.product_three_title),
                (AbsoluteLayout) view.findViewById(R.id.product_three_layout),
                (ImageView) view.findViewById(R.id.product_three_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_THREE, productThreeViewHolder);

        //Product four
        ProductViewHolder productFourViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_four_viewgroup),
                (ImageView) view.findViewById(R.id.product_four_juice),
                (ImageView) view.findViewById(R.id.product_four_fruit),
                (ImageView) view.findViewById(R.id.product_four_title),
                (AbsoluteLayout) view.findViewById(R.id.product_four_layout),
                (ImageView) view.findViewById(R.id.product_four_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_FOUR, productFourViewHolder);
    }

    private void initRadiusAndDirection() {
        mCircleBlueRadius = 70;
        mCircleBlueDirection = CircleAnimationUtils.Direction.CLOCKWISE;
        mCircleBlue1Radius = 55;
        mCircleBlue1Direction = CircleAnimationUtils.Direction.COUNTERCLOCKWISE;
        mCircleBlue2Radius = 45;
        mCircleBlue2Direction = CircleAnimationUtils.Direction.CLOCKWISE;

        mCircleGreenRadius = 65;
        mCircleGreenDirection = CircleAnimationUtils.Direction.COUNTERCLOCKWISE;
        mCircleGreen1Radius = 50;
        mCircleGreen1Direction = CircleAnimationUtils.Direction.CLOCKWISE;

        mCircleOrangeRadius = 70;
        mCircleOrangeDirection = CircleAnimationUtils.Direction.CLOCKWISE;
        mCircleOrange1Radius = 80;
        mCircleOrange1Direction = CircleAnimationUtils.Direction.COUNTERCLOCKWISE;
        mCircleOrange2Radius = 60;
        mCircleOrange2Direction = CircleAnimationUtils.Direction.COUNTERCLOCKWISE;
        mCircleOrange3Radius = 45;
        mCircleOrange3Direction = CircleAnimationUtils.Direction.CLOCKWISE;
    }

    private void initCenterProductPointF() {
        mCenterProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        mCenterProductJuicePointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp));
        mCenterProductFruitPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp));
        mCenterProductTitlePointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        mCenterProductLayoutPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp));
        mCenterProductSubtitlePointF = new PointF(0, 0);
    }

    private void initMainAnimatorSet() {
        mMainAnimatorSet = new AnimatorSet();
        //mMainAnimatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        mMainTransitionAnimatorSet = new AnimatorSet();
        mMainTransitionAnimatorSet.setDuration(800);

        mMainCircleAnimatorSet = new AnimatorSet();
        //mMainCircleAnimatorSet.setDuration(7000);
    }

    private void initProductComingUpPosition(PRODUCT_KEY productKey) {
        mComingUpProductPrositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductViewGroup, mComingUpProductPrositionPointF.x, mComingUpProductPrositionPointF.y);
    }

    private void initProductEnteringPosition(PRODUCT_KEY productKey, Corner enteringCorner) {
        mEnteringProductPositionPointF = null;
        switch (enteringCorner) {
            case TOP_LEFT:
            case BOTTOM_LEFT:
                //not supported yet
                break;
            case TOP_RIGHT:
                mEnteringProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_500dp), mContext.getResources().getDimension(R.dimen.dimen_0300dp));
                break;
            case BOTTOM_RIGHT:
                mEnteringProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_500dp), mContext.getResources().getDimension(R.dimen.dimen_300dp));
                break;
        }
        if (mEnteringProductPositionPointF != null) {
            mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductViewGroup, mEnteringProductPositionPointF.x, mEnteringProductPositionPointF.y);
        }
    }

    private void initProductCenteringPosition(PRODUCT_KEY productKey) {

        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductViewGroup, mCenterProductPositionPointF.x, mCenterProductPositionPointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductJuice, mCenterProductJuicePointF.x, mCenterProductJuicePointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductFruit, mCenterProductFruitPointF.x, mCenterProductFruitPointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductTitle, mCenterProductTitlePointF.x, mCenterProductTitlePointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductLayout, mCenterProductLayoutPointF.x, mCenterProductLayoutPointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductSubtitle, mCenterProductSubtitlePointF.x, mCenterProductSubtitlePointF.y);
        mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mChildLayout, 0, 0);
    }

    private void initProductExitingPosition(PRODUCT_KEY productKey, Corner exitingCorner) {
        mExitingProductPositionPointF = null;
        switch (exitingCorner) {
            case TOP_LEFT:
                mExitingProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp));
                break;
            case BOTTOM_LEFT:
                mExitingProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_300dp));
                break;
            case TOP_RIGHT:
            case BOTTOM_RIGHT:
                //Not supported yet
                break;
        }
        if (mExitingProductPositionPointF != null) {
            mProductViewHolders.get(productKey).mProductViewGroup.putPosition(mProductViewHolders.get(productKey).mProductViewGroup, mExitingProductPositionPointF.x, mExitingProductPositionPointF.y);
        }
    }

    private void initCircleEndingPosition() {
        mCircleBlue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_400dp));
        mCircleBlue1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_355dp), mContext.getResources().getDimension(R.dimen.dimen_355dp));
        mCircleBlue2.putPosition(mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_430dp));
        mCircleGreen.putPosition(mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_400dp));
        mCircleGreen1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_500dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        mCircleOrange.putPosition(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_280dp));
        mCircleOrange1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_300dp));
        mCircleOrange2.putPosition(mContext.getResources().getDimension(R.dimen.dimen_550dp), mContext.getResources().getDimension(R.dimen.dimen_280dp));
        mCircleOrange3.putPosition(mContext.getResources().getDimension(R.dimen.dimen_460dp), mContext.getResources().getDimension(R.dimen.dimen_500dp));
    }

    public void transitionToProduct(final PRODUCT_KEY comingUpProductKey, PRODUCT_KEY enteringProductKey, Corner enteringCorner, final PRODUCT_KEY centeringProductKey,
                                    final PRODUCT_KEY exitingProductKey, Corner exitingCorner) {
        initMainAnimatorSet();
        initCircleEndingPosition();
        initProductComingUpPosition(comingUpProductKey);
        initProductEnteringPosition(enteringProductKey, enteringCorner);
        initProductCenteringPosition(centeringProductKey);
        initProductExitingPosition(exitingProductKey, exitingCorner);

        //Entering
        MainAnimationBean productEnteringBean = MainAnimationBeanUtils.getViewAnimotionBean(mProductViewHolders.get(enteringProductKey).mProductViewGroup, mEnteringProductPositionPointF, 1, 40, false, 0, true, false);
        ValueAnimator productEnteringBeanValueAnimator = MainAnimationUtils.getValueAnimator(productEnteringBean, 7000);
        //Centering
        MainAnimationBean productCenteringBean = MainAnimationBeanUtils.getViewAnimotionBean(mProductViewHolders.get(centeringProductKey).mProductViewGroup, mCenterProductPositionPointF, 1, 40, false, 0, true);
        ValueAnimator productCenteringBeanValueAnimator = MainAnimationUtils.getValueAnimator(productCenteringBean, 7000);
        //Exiting
        MainAnimationBean productExitingBean = MainAnimationBeanUtils.getViewAnimotionBean(mProductViewHolders.get(exitingProductKey).mProductViewGroup, mExitingProductPositionPointF, 1, 40, false, 0, true, false);
        ValueAnimator productExitingBeanValueAnimator = MainAnimationUtils.getValueAnimator(productExitingBean, 7000);

        //Second, we transition the views from the init position to the main position.
        //Small circle transitions
        AnimatorSet animatorSetCircleBlue = PageAnimotionUtils.getAnimotion(mCircleBlue,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_280dp) + CircleAnimationUtils.getTranslateX(mCircleBlueRadius, mCircleBlueDirection), mContext.getResources().getDimension(R.dimen.dimen_400dp) + CircleAnimationUtils.getTranslateY(mCircleBlueRadius, mCircleBlueDirection));
        AnimatorSet animatorSetCircleBlue1 = PageAnimotionUtils.getAnimotion(mCircleBlue1,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_355dp) + CircleAnimationUtils.getTranslateX(mCircleBlue1Radius, mCircleBlue1Direction), mContext.getResources().getDimension(R.dimen.dimen_355dp) + CircleAnimationUtils.getTranslateY(mCircleBlue1Radius, mCircleBlue1Direction));
        AnimatorSet animatorSetCircleBlue2 = PageAnimotionUtils.getAnimotion(mCircleBlue2,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_400dp) + CircleAnimationUtils.getTranslateX(mCircleBlue2Radius, mCircleBlue2Direction), mContext.getResources().getDimension(R.dimen.dimen_430dp) + CircleAnimationUtils.getTranslateY(mCircleBlue2Radius, mCircleBlue2Direction));

        AnimatorSet animatorSetCircleGreen = PageAnimotionUtils.getAnimotion(mCircleGreen,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_280dp) + CircleAnimationUtils.getTranslateX(mCircleGreenRadius, mCircleGreenDirection), mContext.getResources().getDimension(R.dimen.dimen_400dp) + CircleAnimationUtils.getTranslateY(mCircleGreenRadius, mCircleGreenDirection));
        AnimatorSet animatorSetCircleGreen1 = PageAnimotionUtils.getAnimotion(mCircleGreen1,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_500dp) + CircleAnimationUtils.getTranslateX(mCircleGreen1Radius, mCircleGreen1Direction), mContext.getResources().getDimension(R.dimen.dimen_130dp) + CircleAnimationUtils.getTranslateY(mCircleGreen1Radius, mCircleGreen1Direction));

        AnimatorSet animatorSetCircleOrange = PageAnimotionUtils.getAnimotion(mCircleOrange,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_450dp) + CircleAnimationUtils.getTranslateX(mCircleOrangeRadius, mCircleOrangeDirection), mContext.getResources().getDimension(R.dimen.dimen_280dp) + CircleAnimationUtils.getTranslateY(mCircleOrangeRadius, mCircleOrangeDirection));
        AnimatorSet animatorSetCircleOrange1 = PageAnimotionUtils.getAnimotion(mCircleOrange1,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_600dp) + CircleAnimationUtils.getTranslateX(mCircleOrange1Radius, mCircleOrange1Direction), mContext.getResources().getDimension(R.dimen.dimen_300dp) + CircleAnimationUtils.getTranslateY(mCircleOrange1Radius, mCircleOrange1Direction));
        AnimatorSet animatorSetCircleOrange2 = PageAnimotionUtils.getAnimotion(mCircleOrange2,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_550dp) + CircleAnimationUtils.getTranslateX(mCircleOrange2Radius, mCircleOrange2Direction), mContext.getResources().getDimension(R.dimen.dimen_280dp) + CircleAnimationUtils.getTranslateY(mCircleOrange2Radius, mCircleOrange2Direction));
        AnimatorSet animatorSetCircleOrange3 = PageAnimotionUtils.getAnimotion(mCircleOrange3,
                mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),
                mContext.getResources().getDimension(R.dimen.dimen_460dp) + CircleAnimationUtils.getTranslateX(mCircleOrange3Radius, mCircleOrange3Direction), mContext.getResources().getDimension(R.dimen.dimen_500dp) + CircleAnimationUtils.getTranslateY(mCircleOrange3Radius, mCircleOrange3Direction));

        //Product transition
        AnimatorSet animatorSetProductEntering = PageAnimotionUtils.getAnimotion(mProductViewHolders.get(enteringProductKey).mProductViewGroup,
                mProductViewHolders.get(enteringProductKey).mProductViewGroup.getX(),
                mProductViewHolders.get(enteringProductKey).mProductViewGroup.getY(),
                mEnteringProductPositionPointF.x + CircleAnimationUtils.getTranslateX(40, CircleAnimationUtils.Direction.COUNTERCLOCKWISE),
                mEnteringProductPositionPointF.y);

        final AnimatorSet animatorSetProductCentering = PageAnimotionUtils.getAnimotion(mProductViewHolders.get(centeringProductKey).mProductViewGroup,
                mProductViewHolders.get(centeringProductKey).mProductViewGroup.getX(),
                mProductViewHolders.get(centeringProductKey).mProductViewGroup.getY(),
                mCenterProductPositionPointF.x + CircleAnimationUtils.getTranslateX(40, CircleAnimationUtils.Direction.COUNTERCLOCKWISE),
                mCenterProductPositionPointF.y);
        ObjectAnimator centeringProductAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(centeringProductKey).mChildLayout, View.ALPHA, 0, 1);
        centeringProductAlphaAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                //centeringProductAlphaAnimation.removeAllListeners();
                mProductViewHolders.get(centeringProductKey).mProductViewGroup.setLayerType(View.LAYER_TYPE_NONE, null);
                mProductViewHolders.get(centeringProductKey).mChildLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mProductViewHolders.get(centeringProductKey).mChildLayout.setLayerType(View.LAYER_TYPE_NONE, null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSetProductCentering.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                mProductViewHolders.get(centeringProductKey).mChildLayout.setAlpha(1);
                //animatorSetProductCentering.removeAllListeners();
                //mProductViewHolders.get(centeringProductKey).mProductViewGroup.setLayerType(View.LAYER_TYPE_NONE, null);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });



        final AnimatorSet animatorSetProductExiting = PageAnimotionUtils.getAnimotion(mProductViewHolders.get(exitingProductKey).mProductViewGroup,
                mProductViewHolders.get(exitingProductKey).mProductViewGroup.getX(),
                mProductViewHolders.get(exitingProductKey).mProductViewGroup.getY(),
                mExitingProductPositionPointF.x + CircleAnimationUtils.getTranslateX(40, CircleAnimationUtils.Direction.COUNTERCLOCKWISE),
                mExitingProductPositionPointF.y);

        final ObjectAnimator exitingProductAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(exitingProductKey).mChildLayout, View.ALPHA, 1, 0);
        exitingProductAlphaAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                //exitingProductAlphaAnimation.removeAllListeners();
                //mProductViewHolders.get(exitingProductKey).mProductViewGroup.setLayerType(View.LAYER_TYPE_NONE, null);
                mProductViewHolders.get(exitingProductKey).mChildLayout.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mProductViewHolders.get(exitingProductKey).mChildLayout.setLayerType(View.LAYER_TYPE_NONE, null);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSetProductExiting.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
//                mProductViewHolders.get(exitingProductKey).mChildLayout.setAlpha(0);
                //animatorSetProductExiting.removeAllListeners();
//                mProductViewHolders.get(exitingProductKey).mProductViewGroup.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        AnimatorSet animatorSetProductComingUp = PageAnimotionUtils.getAnimotion(mProductViewHolders.get(comingUpProductKey).mProductViewGroup,
                mProductViewHolders.get(comingUpProductKey).mProductViewGroup.getX(),
                mProductViewHolders.get(comingUpProductKey).mProductViewGroup.getY(),
                mContext.getResources().getDimension(R.dimen.dimen_0500dp) + CircleAnimationUtils.getTranslateX(40, CircleAnimationUtils.Direction.COUNTERCLOCKWISE),
                mContext.getResources().getDimension(R.dimen.dimen_035dp));

        //mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_035dp)
        animatorSetProductComingUp.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mProductViewHolders.get(comingUpProductKey).mProductViewGroup.setMyX(mComingUpProductPrositionPointF.x, false);
                mProductViewHolders.get(comingUpProductKey).mProductViewGroup.setMyY(mComingUpProductPrositionPointF.y, false);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        AnimatorSet centeringSet = new AnimatorSet();
        centeringSet.playSequentially(animatorSetProductCentering/*, centeringProductAlphaAnimation*/);

        AnimatorSet exitingSet = new AnimatorSet();
        exitingSet.playSequentially(/*exitingProductAlphaAnimation,*/animatorSetProductExiting);

        mMainTransitionAnimatorSet.playTogether(
//                animatorSetCircleBlue,
//                animatorSetCircleBlue1,
//                animatorSetCircleBlue2,
//                animatorSetCircleGreen,
//                animatorSetCircleGreen1,
//                animatorSetCircleOrange,
//                animatorSetCircleOrange1,
//                animatorSetCircleOrange2,
//                animatorSetCircleOrange3,
//
////                ,
                animatorSetProductEntering,
                animatorSetProductCentering,
                animatorSetProductExiting,
                animatorSetProductComingUp
        );

        //Third, when the views are in main position we circle them on place
        //small circle circling
        ValueAnimator valueAnimatorCircleBlue = CircleAnimationUtils.moveCirclePath(mCircleBlue, mCircleBlueRadius, 5, mCircleBlueDirection, 7000);
        ValueAnimator valueAnimatorCircleBlue1 = CircleAnimationUtils.moveCirclePath(mCircleBlue1, mCircleBlue1Radius, 4, mCircleBlue1Direction, 7000);
        ValueAnimator valueAnimatorCircleBlue2 = CircleAnimationUtils.moveCirclePath(mCircleBlue2, mCircleBlue2Radius, 6, mCircleBlue2Direction, 7000);

        ValueAnimator valueAnimatorCircleGreen = CircleAnimationUtils.moveCirclePath(mCircleGreen, mCircleGreenRadius, 5, mCircleGreenDirection, 7000);
        ValueAnimator valueAnimatorCircleGreen1 = CircleAnimationUtils.moveCirclePath(mCircleGreen1, mCircleGreen1Radius, 3, mCircleGreen1Direction, 7000);

        ValueAnimator valueAnimatorCircleOrange = CircleAnimationUtils.moveCirclePath(mCircleOrange, mCircleOrangeRadius, 4, mCircleOrangeDirection, 7000);
        ValueAnimator valueAnimatorCircleOrange1 = CircleAnimationUtils.moveCirclePath(mCircleOrange1, mCircleOrange1Radius, 4, mCircleOrange1Direction, 7000);
        ValueAnimator valueAnimatorCircleOrange2 = CircleAnimationUtils.moveCirclePath(mCircleOrange2, mCircleOrange2Radius, 5, mCircleOrange2Direction, 7000);
        ValueAnimator valueAnimatorCircleOrange3 = CircleAnimationUtils.moveCirclePath(mCircleOrange3, mCircleOrange3Radius, 4, mCircleOrange3Direction, 7000);

        //product circling
        ValueAnimator valueAnimatorCircleProduct = CircleAnimationUtils.moveCirclePath(mProductViewHolders.get(centeringProductKey).mProductViewGroup, 40, 3, CircleAnimationUtils.Direction.COUNTERCLOCKWISE, 7000);


//        mMainCircleAnimatorSet.playTogether(
//                valueAnimatorCircleBlue,
//                valueAnimatorCircleBlue1,
//                valueAnimatorCircleBlue2,
//                valueAnimatorCircleGreen,
//                valueAnimatorCircleGreen1,
//                valueAnimatorCircleOrange,
//                valueAnimatorCircleOrange1,
//                valueAnimatorCircleOrange2,
//                valueAnimatorCircleOrange3,
//                productEnteringBeanValueAnimator,
//                productCenteringBeanValueAnimator,
//                productExitingBeanValueAnimator
////                ,
//                valueAnimatorCircleProductOne
//                valueAnimatorCircleProduct
//        );

        AnimatorSet alphaAnimatorSetStart = new AnimatorSet();
        alphaAnimatorSetStart.playTogether(exitingProductAlphaAnimation);

        AnimatorSet alphaAnimatorSetEnd = new AnimatorSet();
        alphaAnimatorSetEnd.playTogether(centeringProductAlphaAnimation);

        mMainAnimatorSet.playSequentially(
//                exitingProductAlphaAnimation,
                mMainTransitionAnimatorSet
//                ,
                //mMainCircleAnimatorSet,
//                centeringProductAlphaAnimation
                );

                mMainAnimatorSet.start();

        if(!mIsSmallCircleAnimationStarted){
//            mIsSmallCircleAnimationStarted = true;
//            animateImageView(mCircleBlue,true,3000);
//            animateImageView(mCircleBlue1,false,3000);
//            animateImageView(mCircleBlue2,true,3000);
//            animateImageView(mCircleGreen,false,3000);
//            animateImageView(mCircleGreen1,true,3000);
//            animateImageView(mCircleOrange,false,3000);
//            animateImageView(mCircleOrange1,true,3000);
//            animateImageView(mCircleOrange2,false,3000);
//            animateImageView(mCircleOrange3,true,3000);

        }
//
    }

    public void transitionToFirstProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_THREE, PRODUCT_KEY.PRODUCT_TWO, Corner.BOTTOM_RIGHT, PRODUCT_KEY.PRODUCT_ONE, PRODUCT_KEY.PRODUCT_FOUR, Corner.BOTTOM_LEFT);
    }

    public void transitionToSecondProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_FOUR, PRODUCT_KEY.PRODUCT_THREE, Corner.TOP_RIGHT, PRODUCT_KEY.PRODUCT_TWO, PRODUCT_KEY.PRODUCT_ONE, Corner.TOP_LEFT);
    }

    public void transitionToThirdProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_ONE, PRODUCT_KEY.PRODUCT_FOUR, Corner.BOTTOM_RIGHT, PRODUCT_KEY.PRODUCT_THREE, PRODUCT_KEY.PRODUCT_TWO, Corner.BOTTOM_LEFT);
    }

    public void transitionToFourthProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_TWO, PRODUCT_KEY.PRODUCT_ONE, Corner.TOP_RIGHT, PRODUCT_KEY.PRODUCT_FOUR, PRODUCT_KEY.PRODUCT_THREE, Corner.TOP_LEFT);
    }

    private void animateImageView(final View imgView, final boolean startUp, final int duration) {
        final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imgView, "translationY", startUp ? 0 : mMainView.getMeasuredHeight() - imgView.getMeasuredHeight(), startUp ? mMainView.getMeasuredHeight() - imgView.getMeasuredHeight() : 0);
        //final ObjectAnimator objectAnimatorx = ObjectAnimator.ofFloat(imgView, "translationX", startUp ? 0 : mMainView.getMeasuredWidth() - imgView.getMeasuredWidth(), startUp ? mMainView.getMeasuredWidth() - imgView.getMeasuredWidth() : 0);
        objectAnimator.setDuration(duration);
//        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imgView.clearAnimation();
                objectAnimator.removeAllListeners();
                animateImageView(imgView, !startUp, duration);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }
}
