package com.nestle.express.frontend_ui.nurun.utils;

import android.graphics.PointF;
import android.view.View;

/**
 * Created by jesserci on 16-10-13.
 */
public class MainAnimationBean {
    private View view;
    private PointF oldPointF;
    private PointF newPointF;
    private PointF newAngelPointF;
    private int direction;  // 0 顺时针  1 逆时针
    private int radis;
    private boolean isNeedScale;
    private long duraction;
    private int evlotion = 3; //默认三圈
    private float angel;//x sin 顺时针  x cos 逆时针   //(11 * Math.PI / 6)(cos g3/2 sin -1/2) 顺  or     (4 * Math.PI / 3)(cos -1/2 sin -g3/2) 逆
    private int offsetP; //0:x轴偏移  1:y轴偏移 2不偏移
    private boolean isRotate; //是否旋转

    public boolean mAnimateChild = true;
    public MainAnimationBean() {

    }

    public MainAnimationBean(View view, PointF newPointF, int direction, int radis, boolean isNeedScale) {
        this.view = view;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = isNeedScale;
        this.oldPointF = new PointF(view.getX(), view.getY());
        this.newAngelPointF = new PointF();
        if (direction == 0) {
            //顺时针
            angel = (float) (11 * Math.PI / 6.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            angel = (float) (4 * Math.PI / 3.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.sin(angel));
        }

    }

    public MainAnimationBean(View view, PointF newPointF, int direction, int radis, boolean isNeedScale, boolean isAnimateChildren) {
        this.view = view;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = isNeedScale;
        this.oldPointF = new PointF(view.getX(), view.getY());
        this.newAngelPointF = new PointF();
        if (direction == 0) {
            //顺时针
            angel = (float) (11 * Math.PI / 6.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            angel = (float) (4 * Math.PI / 3.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.sin(angel));
        }
        mAnimateChild = isAnimateChildren;
    }

    /**
     * ViewGruop 内部控件使用初始化
     *
     * @param view
     * @param newPointF
     * @param direction
     * @param radis
     * @param test
     */
    public MainAnimationBean(View view, PointF newPointF, int direction, int radis, int test) {
        this.view = view;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = false;
        this.oldPointF = new PointF(view.getX(), view.getY());
        this.newAngelPointF = new PointF();
        if (direction == 0) {
            //顺时针
            angel = (float) (11 * Math.PI / 6.f);
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            angel = (float) (4 * Math.PI / 3.f);
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.sin(angel));
        }

    }

    public void setViewAnimotionBean(View view, PointF newPointF, int direction, int radis, int test) {
        this.view = view;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = false;
        this.oldPointF = new PointF(view.getX(), view.getY());
        this.newAngelPointF = new PointF();
        if (direction == 0) {
            //顺时针
            angel = (float) (11 * Math.PI / 6.f);
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            angel = (float) (4 * Math.PI / 3.f);
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.sin(angel));
        }
    }

    public void setViewAnimotionBean(View view, PointF newPointF, int direction, int radis, boolean isNeedScale, boolean isAnimateChildren) {
        this.view = view;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = isNeedScale;
        this.oldPointF = new PointF(view.getX(), view.getY());
        this.newAngelPointF = new PointF();
        if (direction == 0) {
            //顺时针
            angel = (float) (11 * Math.PI / 6.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            angel = (float) (4 * Math.PI / 3.f);
            this.newAngelPointF.x = newPointF.x + (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y + (float) (radis * Math.sin(angel));
        }
        mAnimateChild = isAnimateChildren;
    }

    /**
     * ViewGroupLayout 子类专用的
     *
     * @param view
     * @param newPointF
     * @param direction
     * @param radis
     */
    public MainAnimationBean(View view, PointF newPointF, int direction, int radis) {
        this.view = view;
        this.newAngelPointF = new PointF();
        this.newPointF = newPointF;
        this.oldPointF = new PointF(view.getX(), view.getY());
        if (direction == 0) {
            //顺时针
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.sin(angel));
        }
    }

    /**
     * ViewGroupLayout 子类专用的
     *
     * @param view
     * @param newPointF
     * @param direction
     * @param radis
     */
    public MainAnimationBean(View view, PointF oldPointF, PointF newPointF, int direction, int radis) {
        this.view = view;
        this.newAngelPointF = new PointF();
        this.newPointF = newPointF;
        this.oldPointF = oldPointF;
        if (direction == 0) {
            //顺时针
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.sin(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.cos(angel));
        } else {
            //逆时针
            this.newAngelPointF.x = newPointF.x - (float) (radis * Math.cos(angel));
            this.newAngelPointF.y = newPointF.y - (float) (radis * Math.sin(angel));
        }
    }


    public MainAnimationBean(View view, PointF oldPointF, PointF newPointF, int direction, int radis, boolean isNeedScale, long duraction) {
        this.view = view;
        this.oldPointF = oldPointF;
        this.newPointF = newPointF;
        this.direction = direction;
        this.radis = radis;
        this.isNeedScale = isNeedScale;
        this.duraction = duraction;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public PointF getOldPointF() {
        return oldPointF;
    }

    public void setOldPointF(PointF oldPointF) {
        this.oldPointF = oldPointF;
    }

    public PointF getNewPointF() {
        return newPointF;
    }

    public void setNewPointF(PointF newPointF) {
        this.newPointF = newPointF;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getRadis() {
        return radis;
    }

    public void setRadis(int radis) {
        this.radis = radis;
    }

    public boolean isNeedScale() {
        return isNeedScale;
    }

    public void setNeedScale(boolean needScale) {
        isNeedScale = needScale;
    }

    public long getDuraction() {
        return duraction;
    }

    public void setDuraction(long duraction) {
        this.duraction = duraction;
    }

    public int getEvlotion() {
        return evlotion;
    }

    public void setEvlotion(int evlotion) {
        this.evlotion = evlotion;
    }

    public float getAngel() {
        return angel;
    }

    public void setAngel(float angel) {
        this.angel = angel;
    }

    public PointF getNewAngelPointF() {
        return newAngelPointF;
    }

    public void setNewAngelPointF(PointF newAngelPointF) {
        this.newAngelPointF = newAngelPointF;
    }

    public int getOffsetP() {
        return offsetP;
    }

    public void setOffsetP(int offsetP) {
        this.offsetP = offsetP;
    }

    public boolean isRotate() {
        return isRotate;
    }

    public void setRotate(boolean rotate) {
        isRotate = rotate;
    }
}
