package com.nestle.express.frontend_ui.surface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by xumin on 2016/8/9.
 */
public class SceneGL extends SurfaceView implements SurfaceHolder.Callback {

    private final Task task;
    private MyQueue<BaseNode> nodes = new MyQueue<>();

    private boolean flag = true;

    public SceneGL(Context context) {
        this(context,null);
    }

    public SceneGL(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        task = new Task(getHolder());
        new Thread(task).start();
    }

    public void addNode(BaseNode node){
        addNode(node,-1);
    }
    public void addNode(BaseNode node,int location){
        nodes.add(node);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        flag = true;

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        flag = false;
    }

    class Task implements Runnable{

        private SurfaceHolder holder;

        private Rect rect = new Rect();

        public Task(SurfaceHolder holder){
            this.holder = holder;
        }

        @Override
        public void run() {
            long pro = System.currentTimeMillis();
            Canvas canvas = null;
            while(true){
                if (flag) {
                    try {
                        SystemClock.sleep(4);
                        long tem = System.currentTimeMillis();
                        long offset = tem - pro;
                        canvas = holder.lockCanvas();
                        canvas.drawColor(Color.WHITE);
                        nodes.currToFirst();

                        while (true) {
                            BaseNode node = nodes.getNext();
                            if (node == null)
                                break;
                            Bitmap bitmap = node.getNextBitmap();
                            if (bitmap != null) {
                                rect.set(node.getPositionX(), node.getPositionY(), node.getPositionX() + node.getWidth(), node.getPositionY() + node.getHeight());
                                canvas.drawBitmap(bitmap, null, rect, node.getPaint());
                            }
                        }
                        pro = tem;

                    }catch (Exception e){
                        e.printStackTrace();
                    }finally {
                        if(canvas != null)
                            holder.unlockCanvasAndPost(canvas);
                    }
                    continue;
                }
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class MyGLRenderer implements GLSurfaceView.Renderer {
        private long pro ;
        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            pro = System.currentTimeMillis();
            try {
                task.notifyAll();
            }catch (Exception e){

            }


        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {

        }

        @Override
        public void onDrawFrame(GL10 gl) {
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            long tem = System.currentTimeMillis();
            long offset = tem - pro;
            nodes.currToFirst();

            while (true) {
                BaseNode node = nodes.getNext();
                if (node == null)
                    break;
                Bitmap bitmap = node.getNextBitmap();
                if (bitmap != null) {
//                    rect.set(node.getPositionX(), node.getPositionY(), node.getPositionX() + node.getWidth(), node.getPositionY() + node.getHeight());
//                    canvas.drawBitmap(bitmap, null, rect, node.getPaint());
                }
            }
            pro = tem;
        }

    }
}
