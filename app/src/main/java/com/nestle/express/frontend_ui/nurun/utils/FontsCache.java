package com.nestle.express.frontend_ui.nurun.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Created by jesserci on 17-02-03.
 */
public class FontsCache {

    private static Hashtable<String, Typeface> fontsCache = new Hashtable<String, Typeface>();

    public static Typeface get(Context context, String fontName) {
        Typeface typeface = fontsCache.get(fontName);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), fontName);
            } catch (Exception e) {
                return null;
            }
            fontsCache.put(fontName, typeface);
        }
        return typeface;
    }
}
