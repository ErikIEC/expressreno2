package com.nestle.express.frontend_ui.util;

import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.machine.MachineManager;

/**
 * Created by xumin on 16/8/24.
 */
public interface OnPourListener {
    void onPress(int position);
    void onPressOut(int position);
    void onDispenseEndPress(int position);
    void onConcentratePress(NestleProduct product, Runnable onDispenseFinish);
    void onSmallPress(NestleProduct product, Runnable onDispenseFinish);
    void onMediumPress(NestleProduct product, Runnable onDispenseFinish);
    void onLargePress(NestleProduct product, Runnable onDispenseFinish);
    void onTransitionToAttractiveLoopDelayed();
    void onRemoveAttractiveLoopDelayed();
    void onTransitionToDispenseDelayed();
    void onRemoveDispenseDelayed();
    void onSetPortion();
    void onSetPortionOut(int position, MachineManager.ProductSize productSize);
    void updateRinseRequired();
}
