package com.nestle.express.frontend_ui.nurun.utils;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.nurun.backend.ProductArtUtils;
import com.nestle.express.frontend_ui.nurun.widget.BackOfHouseViewGroupLayout;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;
import com.nestle.express.frontend_ui.util.OnPourListener;
import com.nestle.express.frontend_ui.widget.MyDrinkView;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by ricolive on 2016-10-12.
 */
public class ViewHelper implements View.OnClickListener, View.OnTouchListener, View.OnFocusChangeListener {
    public static final int NO_TOUCH = 0;
    public static final int ONE_TOUCH = 1;
    public static final int TWO_TOUCH = 2;
    public static final int DELAY_MILLIS = 300;
    public static final int DELAY_TOUCH_UP = 200;

    private int mTouchCount = NO_TOUCH;

    private final NestleProduct mProduct1, mProduct2, mProduct3, mProduct4;
    private NestleProduct mProducts[] = new NestleProduct[4];
    private ProductViewHolder mProductOneViewHolder, mProductTwoViewHolder, mProductThreeViewHolder, mProductFourViewHolder;

    private Context mContext;
    private ViewGroup mSceneRoot;
    private ApplicationManager.FRONTEND_STATE mFrontendState;
    private RinseStatus mRinseStatus;
    private ApplicationManager mAppManager;
    private OnPourListener mOnPourListener;

    private AbsoluteLayout container;
    private ImageView white_backgroud;

    private ArrayMap<PRODUCT_KEY, ProductViewHolder> mProductViewHolders = new ArrayMap<>();

    private SparseIntArray mActionButtonOneIdArray = new SparseIntArray();
    private SparseIntArray mActionButtonTwoIdArray = new SparseIntArray();
    private SparseIntArray mActionButtonThreeIdArray = new SparseIntArray();
    private SparseIntArray mActionButtonFourIdArray = new SparseIntArray();

    private Integer[] mActionButtonConcentrateIdArray;
    private Integer[] mActionButtonSmallIdArray;
    private Integer[] mActionButtonMediumIdArray;
    private Integer[] mActionButtonLargeIdArray;

    private SparseIntArray mTouchSimultaneous = new SparseIntArray();

    private List<View> mViewsFocusInOrder = new ArrayList<>();
    private List<ImageView> mCloseButtonFocusInOrder = new ArrayList<>();
    private boolean mIsBackofHouse = false;

    public void setIsBackofHouse(boolean isBackofHouse) {
        mIsBackofHouse = isBackofHouse;
    }

    private int mCurrentButtonUpId;

    private boolean mHasFocusPourView = false;

    private final Handler longPressHandler = new Handler();

    private Runnable mLongPressedRunnableOne = new Runnable() {
        public void run() {
            onPressButton(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).scaleButtonDown();
            animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
        }
    };


    private Runnable mLongPressedRunnableTwo = new Runnable() {
        public void run() {
            onPressButton(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).scaleButtonDown();
            animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
        }
    };


    private Runnable mLongPressedRunnableThree = new Runnable() {
        public void run() {
            onPressButton(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).scaleButtonDown();
            animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
        }
    };

    private Runnable mLongPressedRunnableFour = new Runnable() {
        public void run() {
            onPressButton(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).scaleButtonDown();
            animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
        }
    };

    private Runnable mLongPressedRunnableWater = new Runnable() {
        public void run() {
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).scaleButtonDown();
            AnimotionUtils.getInstance().addCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getAnimationDrinkType());
            updateAlphaToButtonsAndLines();
            mOnPourListener.onPress(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getPosition());
        }
    };

    private Runnable mOneButtonPressRunnable = new Runnable() {

        @Override
        public void run() {
            int id = mCurrentButtonUpId;
            Log.d("Log", "====== ACTION_UP ONE_TOUCH mTouchCount=" + mTouchCount);
            if (id == R.id.menu_one || id == R.id.menu_pour_one) {
                removeCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
            } else if (id == R.id.menu_two || id == R.id.menu_pour_two) {
                removeCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
            } else if (id == R.id.menu_three || id == R.id.menu_pour_three) {
                removeCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
            } else if (id == R.id.menu_four || id == R.id.menu_pour_four) {
                removeCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
            } else if (id == R.id.water) {
                AnimotionUtils.getInstance().removeCurrentAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getAnimationDrinkType());
            }
            disableAlphaToButtonsAndLines();
            longPressHandler.removeCallbacks(mOneButtonPressRunnable);
        }
    };

    private Runnable mNoButtonPressRunnable = new Runnable() {

        @Override
        public void run() {
            Log.d("Log", "====== ACTION_UP NO_TOUCH mTouchCount=" + mTouchCount);
            resetMenuToNoTouchState();
            longPressHandler.removeCallbacks(mNoButtonPressRunnable);
        }
    };


    public ViewHelper(ViewGroup sceneRoot, Context context, OnPourListener onPourListener, NestleProduct product1, NestleProduct product2, NestleProduct product3, NestleProduct product4) {

        mSceneRoot = sceneRoot;
        mContext = context;
        mOnPourListener = onPourListener;
        mProduct1 = product1;
        mProduct2 = product2;
        mProduct3 = product3;
        mProduct4 = product4;
        mProducts = new NestleProduct[]{product1, product2, product3, product4};

        mAppManager = ((ExpressApp) context.getApplicationContext()).getApplicationManager();
        mFrontendState = mAppManager.getFrontendState();
        mRinseStatus = mAppManager.getRinseStatus();
    }

    public ViewHelper(ViewGroup sceneRoot, Context context, OnPourListener onPourListener, NestleProduct[] products, NestleProduct product1, NestleProduct product2, NestleProduct product3, NestleProduct product4) {

        mSceneRoot = sceneRoot;
        mContext = context;
        mOnPourListener = onPourListener;
        mProduct1 = product1;
        mProduct2 = product2;
        mProduct3 = product3;
        mProduct4 = product4;
        mProducts = products;//new NestleProduct[]{product1, product2, product3, product4};

        mAppManager = ((ExpressApp) context.getApplicationContext()).getApplicationManager();
        mFrontendState = mAppManager.getFrontendState();
        mRinseStatus = mAppManager.getRinseStatus();
    }

    public void initViewHelper() {

        container = (AbsoluteLayout) mSceneRoot.findViewById(R.id.container);
        white_backgroud = (ImageView) mSceneRoot.findViewById(R.id.white_backgroud);

        white_backgroud.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_ONE)) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
                } else if (AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_TWO)) {
                    Log.d("TEST---", "WHITE BG CLICK");
                    Log.d("TEST---", "Enabled? " + v.isEnabled());
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
                } else if (AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_THREE)) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
                } else if (AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR)) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
                }
            }
        });

        //Product 1
        ViewGroupLayout productOneViewGroup = (ViewGroupLayout) mSceneRoot.findViewById(R.id.main_one_viewgroup);
        ImageView productOneGlass = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image1);
        ImageView productOneFruit = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image2);
        ImageView productOneTitle = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image3);
        ImageView productOneSubtitle = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image6);
        ImageView productOneNutritionalInfo = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image4);

        MyDrinkView drink_1 = (MyDrinkView) mSceneRoot.findViewById(R.id.drink_1);

        final RelativeLayout productOneNutritionalInfoButton = (RelativeLayout) mSceneRoot.findViewById(R.id.main_small_one);
        ImageView productOneNutritionalInfoButtonImageView = (ImageView) mSceneRoot.findViewById(R.id.main_small_one_image_view);
        ImageView productOneCloseNutritionalInfoButton = (ImageView) mSceneRoot.findViewById(R.id.main_one_viewgroup_image5);
        productOneCloseNutritionalInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(white_backgroud.isEnabled()==true) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
                    AccessibilityUtils.setFocusOrder(getViewsFocusOrder());
                    productOneNutritionalInfoButton.requestFocus();
                }
            }
        });
        mCloseButtonFocusInOrder.add(productOneCloseNutritionalInfoButton);
        View line1 = mSceneRoot.findViewById(R.id.line1);

        //Product 2
        ViewGroupLayout productTwoViewGroup = (ViewGroupLayout) mSceneRoot.findViewById(R.id.main_two_viewgroup);
        ImageView productTwoGlass = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image1);
        ImageView productTwoFruit = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image2);
        ImageView productTwoTitle = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image3);
        ImageView productTwoSubtitle = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image6);
        ImageView productTwoNutritionalInfo = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image4);

        MyDrinkView drink_2 = (MyDrinkView) mSceneRoot.findViewById(R.id.drink_2);

        final RelativeLayout productTwoNutritionalInfoButton = (RelativeLayout) mSceneRoot.findViewById(R.id.main_small_two);
        ImageView productTwoNutritionalInfoButtonImageView = (ImageView) mSceneRoot.findViewById(R.id.main_small_two_image_view);
        ImageView productTwoCloseNutritionalInfoButton = (ImageView) mSceneRoot.findViewById(R.id.main_two_viewgroup_image5);
        productTwoCloseNutritionalInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(white_backgroud.isEnabled()==true) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
                    AccessibilityUtils.setFocusOrder(getViewsFocusOrder());
                    productTwoNutritionalInfoButton.requestFocus();
                }
            }
        });
        mCloseButtonFocusInOrder.add(productTwoCloseNutritionalInfoButton);
        View line2 = mSceneRoot.findViewById(R.id.line2);

        //Product 3
        ViewGroupLayout productThreeViewGroup = (ViewGroupLayout) mSceneRoot.findViewById(R.id.main_three_viewgroup);
        ImageView productThreeGlass = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image1);
        ImageView productThreeFruit = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image2);
        ImageView productThreeTitle = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image3);
        ImageView productThreeSubtitle = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image6);
        ImageView productThreeNutritionalInfo = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image4);

        MyDrinkView drink_3 = (MyDrinkView) mSceneRoot.findViewById(R.id.drink_3);

        final RelativeLayout productThreeNutritionalInfoButton = (RelativeLayout) mSceneRoot.findViewById(R.id.main_small_three);
        ImageView productThreeNutritionalInfoButtonImageView = (ImageView) mSceneRoot.findViewById(R.id.main_small_three_image_view);
        ImageView productThreeCloseNutritionalInfoButton = (ImageView) mSceneRoot.findViewById(R.id.main_three_viewgroup_image5);
        productThreeCloseNutritionalInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(white_backgroud.isEnabled()==true) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
                    AccessibilityUtils.setFocusOrder(getViewsFocusOrder());
                    productThreeNutritionalInfoButton.requestFocus();
                }
            }
        });
        mCloseButtonFocusInOrder.add(productThreeCloseNutritionalInfoButton);
        View line3 = mSceneRoot.findViewById(R.id.line3);

        //Product 4
        ViewGroupLayout productFourViewGroup = (ViewGroupLayout) mSceneRoot.findViewById(R.id.main_four_viewgroup);
        ImageView productFourGlass = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image1);
        ImageView productFourFruit = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image2);
        ImageView productFourTitle = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image3);
        ImageView productFourSubtitle = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image6);
        ImageView productFourNutritionalInfo = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image4);

        MyDrinkView drink_4 = (MyDrinkView) mSceneRoot.findViewById(R.id.drink_4);

        final RelativeLayout productFourNutritionalInfoButton = (RelativeLayout) mSceneRoot.findViewById(R.id.main_small_four);
        ImageView productFourNutritionalInfoButtonImageView = (ImageView) mSceneRoot.findViewById(R.id.main_small_four_image_view);
        ImageView productFourCloseNutritionalInfoButton = (ImageView) mSceneRoot.findViewById(R.id.main_four_viewgroup_image5);
        productFourCloseNutritionalInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(white_backgroud.isEnabled()==true) {
                    animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
                    AccessibilityUtils.setFocusOrder(getViewsFocusOrder());
                    productFourNutritionalInfoButton.requestFocus();
                }
            }
        });
        mCloseButtonFocusInOrder.add(productFourCloseNutritionalInfoButton);
        View line4 = mSceneRoot.findViewById(R.id.line4);

        //Product water
        View lineWater = mSceneRoot.findViewById(R.id.lineWater);
        TextView mProductWaterButton = (TextView) mSceneRoot.findViewById(R.id.water);

        AbsoluteLayout containerMenuLayout = (AbsoluteLayout) mSceneRoot.findViewById(R.id.container_menu_layout);

        if (isBackOfTheHouse()) {

            //inflate Stub buttons to back of house
            View.inflate(mContext, R.layout.menu_back_house_view, containerMenuLayout);

            BackOfHouseViewGroupLayout mMenuBackOfHouseViewGroupOne = (BackOfHouseViewGroupLayout) mSceneRoot.findViewById(R.id.menu_viewgroup_one);

            ImageView menuConcentrateGlassOne = (ImageView) mMenuBackOfHouseViewGroupOne.findViewById(R.id.menu_concentrate_glass_one);
            ImageView menuSmallGlassOne = (ImageView) mMenuBackOfHouseViewGroupOne.findViewById(R.id.menu_small_glass_one);
            ImageView menuMediumGlassOne = (ImageView) mMenuBackOfHouseViewGroupOne.findViewById(R.id.menu_medium_glass_one);
            ImageView menuLargeGlassOne = (ImageView) mMenuBackOfHouseViewGroupOne.findViewById(R.id.menu_large_glass_one);
            TextView menuPourOne =  (TextView) mMenuBackOfHouseViewGroupOne.findViewById((hasMoreThanOneButton(mProduct1))?R.id.menu_pour_one:R.id.menu_one);
            menuPourOne.setVisibility(View.VISIBLE);
            TextView productOnePourButtonTextView = (TextView) mMenuBackOfHouseViewGroupOne.findViewById(R.id.menu_one_text_view);
            mProductOneViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_ONE_POSITION, AnimotionUtils.ANIMOTION_DRINK_ONE, AnimotionUtils.ANIMOTION_SHOCONTENT_ONE, line1, this,
                    productOneViewGroup, drink_1, productOneNutritionalInfoButton, productOneNutritionalInfoButtonImageView,
                    productOneGlass, productOneFruit, productOneTitle, productOneSubtitle, productOneNutritionalInfo, menuPourOne, menuConcentrateGlassOne, menuSmallGlassOne,
                    menuMediumGlassOne, menuLargeGlassOne, productOnePourButtonTextView, mMenuBackOfHouseViewGroupOne);

            BackOfHouseViewGroupLayout mMenuBackOfHouseViewGroupTwo = (BackOfHouseViewGroupLayout) mSceneRoot.findViewById(R.id.menu_viewgroup_two);

            ImageView menuConcentrateGlassTwo = (ImageView) mMenuBackOfHouseViewGroupTwo.findViewById(R.id.menu_concentrate_glass_two);
            ImageView menuSmallGlassTwo = (ImageView) mMenuBackOfHouseViewGroupTwo.findViewById(R.id.menu_small_glass_two);
            ImageView menuMediumGlassTwo = (ImageView) mMenuBackOfHouseViewGroupTwo.findViewById(R.id.menu_medium_glass_two);
            ImageView menuLargeGlassTwo = (ImageView) mMenuBackOfHouseViewGroupTwo.findViewById(R.id.menu_large_glass_two);
            TextView menuPourTwo = (TextView) mMenuBackOfHouseViewGroupTwo.findViewById((hasMoreThanOneButton(mProduct2))?R.id.menu_pour_two:R.id.menu_two);
            menuPourTwo.setVisibility(View.VISIBLE);
            TextView productTwoPourButtonTextView = (TextView) mMenuBackOfHouseViewGroupTwo.findViewById(R.id.menu_two_text_view);
            mProductTwoViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_TWO_POSITION, AnimotionUtils.ANIMOTION_DRINK_TWO, AnimotionUtils.ANIMOTION_SHOCONTENT_TWO, line2, this,
                    productTwoViewGroup, drink_2, productTwoNutritionalInfoButton, productTwoNutritionalInfoButtonImageView,
                    productTwoGlass, productTwoFruit, productTwoTitle, productTwoSubtitle, productTwoNutritionalInfo, menuPourTwo, menuConcentrateGlassTwo, menuSmallGlassTwo,
                    menuMediumGlassTwo, menuLargeGlassTwo, productTwoPourButtonTextView, mMenuBackOfHouseViewGroupTwo);

            BackOfHouseViewGroupLayout mMenuBackOfHouseViewGroupThree = (BackOfHouseViewGroupLayout) mSceneRoot.findViewById(R.id.menu_viewgroup_three);

            ImageView menuConcentrateGlassThree = (ImageView) mMenuBackOfHouseViewGroupThree.findViewById(R.id.menu_concentrate_glass_three);
            ImageView menuSmallGlassThree = (ImageView) mMenuBackOfHouseViewGroupThree.findViewById(R.id.menu_small_glass_three);
            ImageView menuMediumGlassThree = (ImageView) mMenuBackOfHouseViewGroupThree.findViewById(R.id.menu_medium_glass_three);
            ImageView menuLargeGlassThree = (ImageView) mMenuBackOfHouseViewGroupThree.findViewById(R.id.menu_large_glass_three);
            TextView menuPourThree = (TextView) mMenuBackOfHouseViewGroupThree.findViewById((hasMoreThanOneButton(mProduct3))?R.id.menu_pour_three:R.id.menu_three);
            menuPourThree.setVisibility(View.VISIBLE);
            TextView productThreePourButtonTextView = (TextView) mMenuBackOfHouseViewGroupThree.findViewById(R.id.menu_three_text_view);
            mProductThreeViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_THREE_POSITION, AnimotionUtils.ANIMOTION_DRINK_THREE, AnimotionUtils.ANIMOTION_SHOCONTENT_THREE, line3, this,
                    productThreeViewGroup, drink_3, productThreeNutritionalInfoButton, productThreeNutritionalInfoButtonImageView,
                    productThreeGlass, productThreeFruit, productThreeTitle, productThreeSubtitle, productThreeNutritionalInfo, menuPourThree,
                    menuConcentrateGlassThree, menuSmallGlassThree, menuMediumGlassThree, menuLargeGlassThree, productThreePourButtonTextView, mMenuBackOfHouseViewGroupThree);

            BackOfHouseViewGroupLayout mMenuBackOfHouseViewGroupFour = (BackOfHouseViewGroupLayout) mSceneRoot.findViewById(R.id.menu_viewgroup_four);

            ImageView menuConcentrateGlassFour = (ImageView) mMenuBackOfHouseViewGroupFour.findViewById(R.id.menu_concentrate_glass_four);
            ImageView menuSmallGlassFour = (ImageView) mMenuBackOfHouseViewGroupFour.findViewById(R.id.menu_small_glass_four);
            ImageView menuMediumGlassFour = (ImageView) mMenuBackOfHouseViewGroupFour.findViewById(R.id.menu_medium_glass_four);
            ImageView menuLargeGlassFour = (ImageView) mMenuBackOfHouseViewGroupFour.findViewById(R.id.menu_large_glass_four);
            TextView menuPourFour = (TextView) mMenuBackOfHouseViewGroupFour.findViewById((hasMoreThanOneButton(mProduct4))?R.id.menu_pour_four:R.id.menu_four);
            menuPourFour.setVisibility(View.VISIBLE);
            TextView productFourPourButtonTextView = (TextView) mMenuBackOfHouseViewGroupFour.findViewById(R.id.menu_four_text_view);
            mProductFourViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_FOUR_POSITION, AnimotionUtils.ANIMOTION_DRINK_FOUR, AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR, line4, this,
                    productFourViewGroup, drink_4, productFourNutritionalInfoButton, productFourNutritionalInfoButtonImageView,
                    productFourGlass, productFourFruit, productFourTitle, productFourSubtitle, productFourNutritionalInfo, menuPourFour,
                    menuConcentrateGlassFour, menuSmallGlassFour, menuMediumGlassFour, menuLargeGlassFour, productFourPourButtonTextView, mMenuBackOfHouseViewGroupFour);



            if(mProduct1 != null && mProduct1.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
                mViewsFocusInOrder.add(productOneNutritionalInfoButtonImageView);
                if(mProduct1.getButtonSetup().isConcentrate()){
                    addViewFocusInOrder(mProduct1, menuConcentrateGlassOne, MachineManager.ProductSize.CONCENTRATE);
                }else{
                    addViewFocusInOrder(mProduct1, menuSmallGlassOne, MachineManager.ProductSize.SMALL);
                }
                addViewFocusInOrder(mProduct1, menuMediumGlassOne, MachineManager.ProductSize.MEDIUM);
                addViewFocusInOrder(mProduct1, menuLargeGlassOne, MachineManager.ProductSize.LARGE);
                mViewsFocusInOrder.add(menuPourOne);
            }

            if(mProduct2 != null && mProduct2.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
                mViewsFocusInOrder.add(productTwoNutritionalInfoButtonImageView);
                if(mProduct2.getButtonSetup().isConcentrate()) {
                    addViewFocusInOrder(mProduct2, menuConcentrateGlassTwo, MachineManager.ProductSize.CONCENTRATE);
                }else {
                    addViewFocusInOrder(mProduct2, menuSmallGlassTwo, MachineManager.ProductSize.SMALL);
                }
                addViewFocusInOrder(mProduct2, menuMediumGlassTwo, MachineManager.ProductSize.MEDIUM);
                addViewFocusInOrder(mProduct2, menuLargeGlassTwo, MachineManager.ProductSize.LARGE);
                mViewsFocusInOrder.add(menuPourTwo);
            }

            mViewsFocusInOrder.add(mProductWaterButton);

            if(mProduct3 != null && mProduct3.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
                mViewsFocusInOrder.add(productThreeNutritionalInfoButtonImageView);
                if (mProduct3.getButtonSetup().isConcentrate()) {
                    addViewFocusInOrder(mProduct3, menuConcentrateGlassThree, MachineManager.ProductSize.CONCENTRATE);
                } else {
                    addViewFocusInOrder(mProduct3, menuSmallGlassThree, MachineManager.ProductSize.SMALL);
                }
                addViewFocusInOrder(mProduct3, menuMediumGlassThree, MachineManager.ProductSize.MEDIUM);
                addViewFocusInOrder(mProduct3, menuLargeGlassThree, MachineManager.ProductSize.LARGE);
                mViewsFocusInOrder.add(menuPourThree);
            }

            if(mProduct4 != null && mProduct4.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
                mViewsFocusInOrder.add(productFourNutritionalInfoButtonImageView);
                if (mProduct1 != null && mProduct4.getButtonSetup().isConcentrate()) {
                    addViewFocusInOrder(mProduct4, menuConcentrateGlassFour, MachineManager.ProductSize.CONCENTRATE);
                } else {
                    addViewFocusInOrder(mProduct4, menuSmallGlassFour, MachineManager.ProductSize.SMALL);
                }
                addViewFocusInOrder(mProduct4, menuMediumGlassFour, MachineManager.ProductSize.MEDIUM);
                addViewFocusInOrder(mProduct4, menuLargeGlassFour, MachineManager.ProductSize.LARGE);
                mViewsFocusInOrder.add(menuPourFour);
            }

        } else {//If CLIENT or RINSE UI

            //Inflate Stub buttons to user
            View.inflate(mContext, R.layout.menu_pour_view, containerMenuLayout);


            TextView productOnePourButton = (TextView) mSceneRoot.findViewById(R.id.menu_one);
            mProductOneViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_ONE_POSITION, AnimotionUtils.ANIMOTION_DRINK_ONE, AnimotionUtils.ANIMOTION_SHOCONTENT_ONE, line1, productOnePourButton, this,
                    productOneViewGroup, drink_1, productOneNutritionalInfoButton, productOneNutritionalInfoButtonImageView,
                    productOneGlass, productOneFruit, productOneTitle, productOneSubtitle, productOneNutritionalInfo);

            TextView productTwoPourButton = (TextView) mSceneRoot.findViewById(R.id.menu_two);
            mProductTwoViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_TWO_POSITION, AnimotionUtils.ANIMOTION_DRINK_TWO, AnimotionUtils.ANIMOTION_SHOCONTENT_TWO, line2, productTwoPourButton, this,
                    productTwoViewGroup, drink_2, productTwoNutritionalInfoButton, productTwoNutritionalInfoButtonImageView,
                    productTwoGlass, productTwoFruit, productTwoTitle, productTwoSubtitle, productTwoNutritionalInfo);

            TextView productThreePourButton = (TextView) mSceneRoot.findViewById(R.id.menu_three);
            mProductThreeViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_THREE_POSITION, AnimotionUtils.ANIMOTION_DRINK_THREE, AnimotionUtils.ANIMOTION_SHOCONTENT_THREE, line3, productThreePourButton, this,
                    productThreeViewGroup, drink_3, productThreeNutritionalInfoButton, productThreeNutritionalInfoButtonImageView,
                    productThreeGlass, productThreeFruit, productThreeTitle, productThreeSubtitle, productThreeNutritionalInfo);

            TextView productFourPourButton = (TextView) mSceneRoot.findViewById(R.id.menu_four);
            mProductFourViewHolder = new ProductViewHolder(AnimotionUtils.PRODUCT_FOUR_POSITION, AnimotionUtils.ANIMOTION_DRINK_FOUR, AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR, line4, productFourPourButton, this,
                    productFourViewGroup, drink_4, productFourNutritionalInfoButton, productFourNutritionalInfoButtonImageView,
                    productFourGlass, productFourFruit, productFourTitle, productFourSubtitle, productFourNutritionalInfo);

            if(mProduct1 != null && (mProduct1.getStatus() != NestleProduct.ProductStatus.SOLD_OUT && mProduct1.getStatus() != NestleProduct.ProductStatus.FAULT)) {
                mViewsFocusInOrder.add(productOneNutritionalInfoButtonImageView);
                mViewsFocusInOrder.add(productOnePourButton);
            }


            if(mProduct2 != null && (mProduct2.getStatus() != NestleProduct.ProductStatus.SOLD_OUT && mProduct2.getStatus() != NestleProduct.ProductStatus.FAULT)) {
                mViewsFocusInOrder.add(productTwoNutritionalInfoButtonImageView);

                mViewsFocusInOrder.add(productTwoPourButton);
            }

            mViewsFocusInOrder.add(mProductWaterButton);

            if(mProduct3 != null && (mProduct3.getStatus() != NestleProduct.ProductStatus.SOLD_OUT && mProduct3.getStatus() != NestleProduct.ProductStatus.FAULT)) {
                mViewsFocusInOrder.add(productThreeNutritionalInfoButtonImageView);

                mViewsFocusInOrder.add(productThreePourButton);
            }

            if(mProduct4 != null && (mProduct4.getStatus() != NestleProduct.ProductStatus.SOLD_OUT && mProduct4.getStatus() != NestleProduct.ProductStatus.FAULT)) {
                mViewsFocusInOrder.add(productFourNutritionalInfoButtonImageView);

                mViewsFocusInOrder.add(productFourPourButton);
            }
        }

        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_ONE, mProductOneViewHolder);
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_TWO, mProductTwoViewHolder);
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_THREE, mProductThreeViewHolder);
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_FOUR, mProductFourViewHolder);

        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_WATER, new ProductViewHolder(AnimotionUtils.WATER_POSITION, AnimotionUtils.DRINK_WATER, AnimotionUtils.DRINK_WATER, lineWater, mProductWaterButton, this));

        container.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (container != null) {
                            int count = container.getChildCount();
                            for (int i = 0; i < count; i++) {
                                View view = container.getChildAt(i);
                                AnimotionUtils.getInstance().putViewPosition(view);
                            }

                            AnimotionUtils.getInstance().setPutValue(true);
                        }
                    }
                });

        initAssets();
    }

    private void setFocusToCloseButton(ImageView productCloseNutritionalInfoButton) {
        productCloseNutritionalInfoButton.setFocusable(true);
        productCloseNutritionalInfoButton.setNextFocusLeftId(productCloseNutritionalInfoButton.getId());
        productCloseNutritionalInfoButton.setNextFocusRightId(productCloseNutritionalInfoButton.getId());
    }

    public void disabebleAllButtons(){
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).disableAllButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getNutritionalInfoButtonImageView().setEnabled(false);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).disableAllButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).getNutritionalInfoButtonImageView().setEnabled(false);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).disableAllButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).getNutritionalInfoButtonImageView().setEnabled(false);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).disableAllButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).getNutritionalInfoButtonImageView().setEnabled(false);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).lockPourButton();
    }

    //Prod
    public void initActionButtonsId() {
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).showAllGlassButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).showAllGlassButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).showAllGlassButtons();
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).showAllGlassButtons();

        if (isBackOfTheHouse()) {

            if (mProduct1 != null) {

                if(hasMoreThanOneButton(mProduct1)) {
                    mActionButtonOneIdArray.append(R.id.menu_pour_one, R.id.menu_pour_one);
                }else{
                    mActionButtonOneIdArray.append(R.id.menu_one, R.id.menu_one);
                }

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).isSmallOrConcentrate(mProduct1);
                if (mProduct1.getButtonSetup().isConcentrate()) {
                    mActionButtonOneIdArray.append(R.id.menu_concentrate_glass_one, R.id.menu_concentrate_glass_one);
                }else{
                    if (mProduct1.getButtonSetup().isSmall()) {
                        mActionButtonOneIdArray.append(R.id.menu_small_glass_one, R.id.menu_small_glass_one);
                    }else{
                        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).hideGlassButtonById(R.id.menu_small_glass_one);
                    }
                }

                if (mProduct1.getButtonSetup().isMedium()) {
                    mActionButtonOneIdArray.append(R.id.menu_medium_glass_one, R.id.menu_medium_glass_one);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).hideGlassButtonById(R.id.menu_medium_glass_one);
                }
                if (mProduct1.getButtonSetup().isLarge()) {
                    mActionButtonOneIdArray.append(R.id.menu_large_glass_one, R.id.menu_large_glass_one);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).hideGlassButtonById(R.id.menu_large_glass_one);
                }
            }

            if (mProduct2 != null) {

                if(hasMoreThanOneButton(mProduct2)) {
                    mActionButtonTwoIdArray.append(R.id.menu_pour_two, R.id.menu_pour_two);
                }else{
                    mActionButtonTwoIdArray.append(R.id.menu_two, R.id.menu_two);
                }

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).isSmallOrConcentrate(mProduct2);
                if (mProduct2.getButtonSetup().isConcentrate()) {
                    mActionButtonTwoIdArray.append(R.id.menu_concentrate_glass_two, R.id.menu_concentrate_glass_two);
                }else{
                    if (mProduct2.getButtonSetup().isSmall()) {
                        mActionButtonTwoIdArray.append(R.id.menu_small_glass_two, R.id.menu_small_glass_two);
                    }else{
                        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).hideGlassButtonById(R.id.menu_small_glass_two);
                    }
                }

                if (mProduct2.getButtonSetup().isMedium()) {
                    mActionButtonTwoIdArray.append(R.id.menu_medium_glass_two, R.id.menu_medium_glass_two);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).hideGlassButtonById(R.id.menu_medium_glass_two);
                }
                if (mProduct2.getButtonSetup().isLarge()) {
                    mActionButtonTwoIdArray.append(R.id.menu_large_glass_two, R.id.menu_large_glass_two);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).hideGlassButtonById(R.id.menu_large_glass_two);
                }
            }

            if (mProduct3 != null) {

                if(hasMoreThanOneButton(mProduct3)) {
                    mActionButtonThreeIdArray.append(R.id.menu_pour_three, R.id.menu_pour_three);
                }else{
                    mActionButtonThreeIdArray.append(R.id.menu_three, R.id.menu_three);
                }

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).isSmallOrConcentrate(mProduct3);
                if (mProduct3.getButtonSetup().isConcentrate()) {
                    mActionButtonThreeIdArray.append(R.id.menu_concentrate_glass_three, R.id.menu_concentrate_glass_three);
                }else{
                    if (mProduct3.getButtonSetup().isSmall()) {
                        mActionButtonThreeIdArray.append(R.id.menu_small_glass_three, R.id.menu_small_glass_three);
                    }else{
                        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).hideGlassButtonById(R.id.menu_small_glass_three);
                    }
                }

                if (mProduct3.getButtonSetup().isMedium()) {
                    mActionButtonThreeIdArray.append(R.id.menu_medium_glass_three, R.id.menu_medium_glass_three);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).hideGlassButtonById(R.id.menu_medium_glass_three);
                }
                if (mProduct3.getButtonSetup().isLarge()) {
                    mActionButtonThreeIdArray.append(R.id.menu_large_glass_three, R.id.menu_large_glass_three);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).hideGlassButtonById(R.id.menu_large_glass_three);
                }
            }



            if (mProduct4 != null) {

                if(hasMoreThanOneButton(mProduct4)) {
                    mActionButtonFourIdArray.append(R.id.menu_pour_four, R.id.menu_pour_four);
                }else{
                    mActionButtonFourIdArray.append(R.id.menu_four, R.id.menu_four);
                }

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).isSmallOrConcentrate(mProduct4);
                if (mProduct4.getButtonSetup().isConcentrate()) {
                    mActionButtonFourIdArray.append(R.id.menu_concentrate_glass_four, R.id.menu_concentrate_glass_four);
                }else{
                    if (mProduct4.getButtonSetup().isSmall()) {
                        mActionButtonFourIdArray.append(R.id.menu_small_glass_four, R.id.menu_small_glass_four);
                    }else{
                        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).hideGlassButtonById(R.id.menu_small_glass_four);
                    }
                }

                if (mProduct4.getButtonSetup().isMedium()) {
                    mActionButtonFourIdArray.append(R.id.menu_medium_glass_four, R.id.menu_medium_glass_four);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).hideGlassButtonById(R.id.menu_medium_glass_four);
                }
                if (mProduct4.getButtonSetup().isLarge()) {
                    mActionButtonFourIdArray.append(R.id.menu_large_glass_four, R.id.menu_large_glass_four);
                }else{
                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).hideGlassButtonById(R.id.menu_large_glass_four);
                }
            }

            mActionButtonConcentrateIdArray = new Integer[]{new Integer(R.id.menu_concentrate_glass_one), new Integer(R.id.menu_concentrate_glass_two), new Integer(R.id.menu_concentrate_glass_three), new Integer(R.id.menu_concentrate_glass_four)};
            mActionButtonSmallIdArray = new Integer[]{new Integer(R.id.menu_small_glass_one), new Integer(R.id.menu_small_glass_two), new Integer(R.id.menu_small_glass_three), new Integer(R.id.menu_small_glass_four)};
            mActionButtonMediumIdArray = new Integer[]{new Integer(R.id.menu_medium_glass_one), new Integer(R.id.menu_medium_glass_two), new Integer(R.id.menu_medium_glass_three), new Integer(R.id.menu_medium_glass_four)};
            mActionButtonLargeIdArray = new Integer[]{new Integer(R.id.menu_large_glass_one), new Integer(R.id.menu_large_glass_two), new Integer(R.id.menu_large_glass_three), new Integer(R.id.menu_large_glass_four)};
        } else {
            mActionButtonOneIdArray.append(R.id.menu_one, R.id.menu_one);
            mActionButtonTwoIdArray.append(R.id.menu_two, R.id.menu_two);
            mActionButtonThreeIdArray.append(R.id.menu_three, R.id.menu_three);
            mActionButtonFourIdArray.append(R.id.menu_four, R.id.menu_four);
        }

    }


    private boolean containsButtonOneId(int id) {
        return (mActionButtonOneIdArray.indexOfKey(id) >= 0);
    }

    private boolean containsButtonTwoId(int id) {
        return (mActionButtonTwoIdArray.indexOfKey(id) >= 0);
    }

    private boolean containsButtonThreeId(int id) {
        return (mActionButtonThreeIdArray.indexOfKey(id) >= 0);
    }

    private boolean containsButtonFourId(int id) {
        return (mActionButtonFourIdArray.indexOfKey(id) >= 0);
    }

    private void initAssets() {
        if (!mProductViewHolders.isEmpty()) {
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).setAssets(mProduct1);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).setAssets(mProduct2);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).setAssets(mProduct3);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).setAssets(mProduct4);
        }
    }

    public void initAssetsAfterAnimation() {
        if (!mProductViewHolders.isEmpty()) {
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).setAssetsAfterAnimation(mProduct1);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).setAssetsAfterAnimation(mProduct2);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).setAssetsAfterAnimation(mProduct3);
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).setAssetsAfterAnimation(mProduct4);

        }
    }

    private long onclickTime = 0;

    private long nowTime = 0;

    public void setNowTime(long time) {
        nowTime = time;
    }

    @Override
    public void onClick(View view) {

        if (mTouchCount != NO_TOUCH) {
            return;
        }

        setNowTime(System.currentTimeMillis());

        int id = view.getId();

        if (AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_ONE) && view.getId() != R.id.main_small_one_image_view
                || AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_TWO) && view.getId() != R.id.main_small_two_image_view
                || AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_THREE) && view.getId() != R.id.main_small_three_image_view
                || AnimotionUtils.getInstance().containsAnimotionType(AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR) && view.getId() != R.id.main_small_four_image_view) {
            return;
        }

        if (System.currentTimeMillis() - onclickTime < 1000) {
            return;
        } else {
            onclickTime = System.currentTimeMillis();
        }
        Log.d("TEST---", "Is view enables " + view.isEnabled());
        Log.d("TEST---", "View's is " + view.getResources().getResourceName(view.getId()));
        AccessibilityUtils.clearFocusOrder(getViewsFocusOrder());
        if (id == R.id.main_small_one_image_view && mProduct1 != null && mProduct1.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
            setFocusToCloseButton(mCloseButtonFocusInOrder.get(0));
            animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
            mCloseButtonFocusInOrder.get(0).requestFocus();
        } else if (id == R.id.main_small_two_image_view && mProduct2 != null && mProduct2.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
            setFocusToCloseButton(mCloseButtonFocusInOrder.get(1));
            animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
            mCloseButtonFocusInOrder.get(1).requestFocus();
        } else if (id == R.id.main_small_three_image_view && mProduct3 != null && mProduct3.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
            setFocusToCloseButton(mCloseButtonFocusInOrder.get(2));
            animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
            mCloseButtonFocusInOrder.get(2).requestFocus();
        } else if (id == R.id.main_small_four_image_view && mProduct4 != null && mProduct4.getStatus() == NestleProduct.ProductStatus.DISPENSE) {
            setFocusToCloseButton(mCloseButtonFocusInOrder.get(3));
            animotionJuiceInfo(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
            mCloseButtonFocusInOrder.get(3).requestFocus();
        }

    }

    private boolean mIsShownNutritionalInfo = false;

    private void animotionJuiceInfo(ProductViewHolder currentProductViewHolder) {

        mOnPourListener.onTransitionToAttractiveLoopDelayed();
        mOnPourListener.onTransitionToDispenseDelayed();

        if (AnimotionUtils.getInstance().containsAnimotionType(currentProductViewHolder.getAnimationShowContentType())
                && mIsShownNutritionalInfo) {
            //hide nutritional info
            mIsShownNutritionalInfo = false;
            AnimotionUtils.getInstance().animotionRightCancel(mContext, currentProductViewHolder.getViewGroupLayout(), container, white_backgroud, currentProductViewHolder.getPosition());
            AnimotionUtils.getInstance().animotionRightSmallCancel(mContext, currentProductViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getNutritionalInfoButtonImageView(), container, currentProductViewHolder.getPosition());
            AnimotionUtils.getInstance().clearCurrentAnimotionType();
            disableAlphaToButtonsAndLines();

        } else if (!mIsShownNutritionalInfo) {
            //showing nutritional info
            mIsShownNutritionalInfo = true;
            if (AnimotionUtils.getInstance().getCurrentAnimotionTypeSize() == AnimotionUtils.MAIN_SCREEN) {

                for (int i = 0; i < mProductViewHolders.size(); i++) {
                    PRODUCT_KEY key = mProductViewHolders.keyAt(i);
                    // get the object by the key.
                    ProductViewHolder productViewHolder = mProductViewHolders.get(key);

                    productViewHolder.enableAlphaToButtonAndLine();
                }
            }

            AnimotionUtils.getInstance().animotionRight(mContext, currentProductViewHolder.getViewGroupLayout(), container, white_backgroud, currentProductViewHolder.getPosition());
            AnimotionUtils.getInstance().animotionRightSmall(mContext, currentProductViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getNutritionalInfoButtonImageView(), container);
        }
    }

    private void resetMenuToNoTouchState() {
        Log.d("Log", "====== ACTION_UP NO_TOUCH mTouchCount=" + mTouchCount);
        for (int i = 0; i < mProductViewHolders.size(); i++) {
            PRODUCT_KEY key = mProductViewHolders.keyAt(i);

            ProductViewHolder productViewHolder = mProductViewHolders.get(key);
            productViewHolder.disableAlphaToButtonAndLine();
            if (productViewHolder.getAnimationDrinkType() != AnimotionUtils.DRINK_WATER) {
                AnimotionUtils.getInstance().resetView(productViewHolder.getViewGroupLayout());

                AnimotionUtils.getInstance().resetView(productViewHolder.getNutritionalInfoButton());

                AnimotionUtils.getInstance().resetView(productViewHolder.getDrink());

                AnimotionUtils.getInstance().resetView(productViewHolder.getLine());

                if (AnimotionUtils.getInstance().containsAnimotionType(productViewHolder.getAnimationDrinkType())) {
                    productViewHolder.getViewGroupLayout().startTransilation(false, productViewHolder.getPosition());
                    Log.d("RESET", "====== ACTION_UP NO_TOUCH mTouchCount=" + mTouchCount);
                }
            }

            productViewHolder.disableAlphaToButtonAndLine();
            productViewHolder.enableAllGlassButtons();
        }
        AnimotionUtils.getInstance().clearCurrentAnimotionType();
    }

    @Override
    synchronized public boolean onTouch(View view, MotionEvent event) {

        setNowTime(System.currentTimeMillis());

        int id = view.getId();

        event.setSource(id);

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:

                incrementTouchCount(id);

                removeCallBack();

                this.mOnPourListener.onRemoveAttractiveLoopDelayed();

                if ((mTouchSimultaneous.indexOfKey(id) >= 0)) {

                    if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS && mTouchCount > 1) {
                        decrementTouchCount(id);
                        return false;
                    }

                    if (containsButtonOneId(id)) {

                        if (mProduct1 == null) {
                            decrementTouchCount(id);
                            return false;
                        }

                        //In default frontend state, if the product's status is not dispense then dispensing is prevented
                        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                && mProduct1.getStatus() != NestleProduct.ProductStatus.DISPENSE) {
                            decrementTouchCount(id);
                            break;
                        }

                        //Free pour or rinsing
                        if (id == R.id.menu_one || id == R.id.menu_pour_one) {

                            //Free pour or manual rinsing
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                    || (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE
                                    && mRinseStatus.getRinseStatus(ProductSlot.values()[0]) == RinseStatus.RINSE_STATUS.MANUAL_RINSE)) {
                                if(mHasFocusPourView){
                                    longPressHandler.postDelayed(mLongPressedRunnableOne, 0);
                                }else{
                                    longPressHandler.postDelayed(mLongPressedRunnableOne, DELAY_MILLIS);
                                }

                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).disableAllGlassButtonsExcepted(id);
                            }
                            //Any other rinse status
                            else if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
                                Log.d("RINSE", "TOUCH rinse 1");
                                startRinsing(0);
                            }
                        } else { //Back of the house or set portions screen


                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                                //if pressed
                                if(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).hasDisabledButton()){
                                    mOnPourListener.onDispenseEndPress(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getPosition());
                                }else{
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).scaleGlassButtonDown();
                                    if (id == R.id.menu_concentrate_glass_one) {
                                        mOnPourListener.onConcentratePress(mProduct1, createDispenseFinish(PRODUCT_KEY.PRODUCT_ONE, id));
                                        Log.d("ENABLE","==> isConcentrate = "+mProduct1.getButtonSetup().isConcentrate());
                                    } else if (id == R.id.menu_small_glass_one) {
                                        mOnPourListener.onSmallPress(mProduct1, createDispenseFinish(PRODUCT_KEY.PRODUCT_ONE, id));
                                    } else if (id == R.id.menu_medium_glass_one) {
                                        mOnPourListener.onMediumPress(mProduct1, createDispenseFinish(PRODUCT_KEY.PRODUCT_ONE, id));
                                        Log.d("ENABLE", "==> isMedium = " + mProduct1.getButtonSetup().isMedium());
                                    } else if (id == R.id.menu_large_glass_one) {
                                        mOnPourListener.onLargePress(mProduct1, createDispenseFinish(PRODUCT_KEY.PRODUCT_ONE, id));
                                        Log.d("ENABLE", "==> isLarge = " + mProduct1.getButtonSetup().isLarge());
                                    }
                                    animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE));
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).disableAllGlassButtonsExcepted(id);
                                }


                            } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                                longPressHandler.postDelayed(mLongPressedRunnableOne, DELAY_MILLIS);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).disableAllGlassButtonsExcepted(id);
                            }
                        }

                    } else if (containsButtonTwoId(id)) {

                        //In default frontend state, if the product's status is not dispense then dispensing is prevented
                        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                && mProduct2.getStatus() != NestleProduct.ProductStatus.DISPENSE) {
                            decrementTouchCount(id);
                            break;
                        }

                        //Free pour or rinsing
                        if (id == R.id.menu_two || id == R.id.menu_pour_two) {
                            //Free pour or manual rinsing
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                    || (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE
                                    && mRinseStatus.getRinseStatus(ProductSlot.values()[1]) == RinseStatus.RINSE_STATUS.MANUAL_RINSE)) {
                                if(mHasFocusPourView) {
                                    longPressHandler.postDelayed(mLongPressedRunnableTwo, 0);
                                }else{
                                    longPressHandler.postDelayed(mLongPressedRunnableTwo, DELAY_MILLIS);
                                }
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).disableAllGlassButtonsExcepted(id);
                            }
                            //Any other rinse status
                            else if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
                                Log.d("RINSE", "TOUCH rinse 2");
                                startRinsing(1);
                            }
                        } else { //Back of the house or set portions screen
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                                //if pressed
                                if(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).hasDisabledButton()){
                                    mOnPourListener.onDispenseEndPress(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).getPosition());
                                }else {
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).scaleGlassButtonDown();
                                    if (id == R.id.menu_concentrate_glass_two) {
                                        mOnPourListener.onConcentratePress(mProduct2, createDispenseFinish(PRODUCT_KEY.PRODUCT_TWO, id));
                                        Log.d("ENABLE", "==> isConcentrate = " + mProduct2.getButtonSetup().isConcentrate());
                                    } else if (id == R.id.menu_small_glass_two) {
                                        mOnPourListener.onSmallPress(mProduct2, createDispenseFinish(PRODUCT_KEY.PRODUCT_TWO, id));
                                        Log.d("ENABLE", "==> isSmall = " + mProduct2.getButtonSetup().isSmall());
                                    } else if (id == R.id.menu_medium_glass_two) {
                                        mOnPourListener.onMediumPress(mProduct2, createDispenseFinish(PRODUCT_KEY.PRODUCT_TWO, id));
                                        Log.d("ENABLE", "==> isMedium = " + mProduct2.getButtonSetup().isMedium());
                                    } else if (id == R.id.menu_large_glass_two) {
                                        mOnPourListener.onLargePress(mProduct2, createDispenseFinish(PRODUCT_KEY.PRODUCT_TWO, id));
                                        Log.d("ENABLE", "==> isLarge = " + mProduct2.getButtonSetup().isLarge());
                                    }
                                    animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO));
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).disableAllGlassButtonsExcepted(id);
                                }
                            } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                                longPressHandler.postDelayed(mLongPressedRunnableTwo, DELAY_MILLIS);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).disableAllGlassButtonsExcepted(id);
                            }
                        }
                    } else if (containsButtonThreeId(id)) {

                        //In default frontend state, if the product's status is not dispense then dispensing is prevented
                        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                && mProduct3.getStatus() != NestleProduct.ProductStatus.DISPENSE) {
                            decrementTouchCount(id);
                            break;
                        }

                        //Free pour or rinsing
                        if (id == R.id.menu_three || id == R.id.menu_pour_three) {
                            Log.d("RINSE", "1- TOUCH rinse 3");
                            //Free pour or manual rinsing
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                    || (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE
                                    && mRinseStatus.getRinseStatus(ProductSlot.values()[2]) == RinseStatus.RINSE_STATUS.MANUAL_RINSE)) {
                                if(mHasFocusPourView) {
                                    longPressHandler.postDelayed(mLongPressedRunnableThree, 0);
                                }else{
                                    longPressHandler.postDelayed(mLongPressedRunnableThree, DELAY_MILLIS);
                                }
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).disableAllGlassButtonsExcepted(id);
                            }
                            //Any other rinse status
                            else if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
                                Log.d("RINSE", "2- TOUCH rinse 3");
                                startRinsing(2);
                            }
                        } else { //Back of the house or set portions screen
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                                //if pressed
                                if(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).hasDisabledButton()){
                                    mOnPourListener.onDispenseEndPress(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).getPosition());
                                }else {
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).scaleGlassButtonDown();
                                    if (id == R.id.menu_concentrate_glass_three) {
                                        mOnPourListener.onConcentratePress(mProduct3, createDispenseFinish(PRODUCT_KEY.PRODUCT_THREE, id));
                                        Log.d("ENABLE", "==> isConcentrate = " + mProduct3.getButtonSetup().isConcentrate());
                                    } else if (id == R.id.menu_small_glass_three) {
                                        mOnPourListener.onSmallPress(mProduct3, createDispenseFinish(PRODUCT_KEY.PRODUCT_THREE, id));
                                        Log.d("ENABLE", "==> isSmall = " + mProduct3.getButtonSetup().isSmall());
                                    } else if (id == R.id.menu_medium_glass_three) {
                                        mOnPourListener.onMediumPress(mProduct3, createDispenseFinish(PRODUCT_KEY.PRODUCT_THREE, id));
                                        Log.d("ENABLE", "==> isMedium = " + mProduct3.getButtonSetup().isMedium());
                                    } else if (id == R.id.menu_large_glass_three) {
                                        mOnPourListener.onLargePress(mProduct3, createDispenseFinish(PRODUCT_KEY.PRODUCT_THREE, id));
                                        Log.d("ENABLE", "==> isLarge = " + mProduct3.getButtonSetup().isLarge());
                                    }
                                    animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE));
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).disableAllGlassButtonsExcepted(id);
                                }
                            } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                                longPressHandler.postDelayed(mLongPressedRunnableThree, DELAY_MILLIS);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).disableAllGlassButtonsExcepted(id);
                            }
                        }
                    } else if (containsButtonFourId(id)) {

                        //In default frontend state, if the product's status is not dispense then dispensing is prevented
                        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                && mProduct4.getStatus() != NestleProduct.ProductStatus.DISPENSE) {
                            decrementTouchCount(id);
                            break;
                        }

                        //Free pour or rinsing
                        if (id == R.id.menu_four || id == R.id.menu_pour_four) {
                            Log.d("RINSE", "1- TOUCH rinse 4");
                            //Free pour or manual rinsing
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT
                                    || (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE
                                    && mRinseStatus.getRinseStatus(ProductSlot.values()[3]) == RinseStatus.RINSE_STATUS.MANUAL_RINSE)) {
                                if(mHasFocusPourView) {
                                    longPressHandler.postDelayed(mLongPressedRunnableFour, 0);
                                }else{
                                    longPressHandler.postDelayed(mLongPressedRunnableFour, DELAY_MILLIS);
                                }
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).disableAllGlassButtonsExcepted(id);
                            }
                            //Any other rinse status
                            else if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
                                Log.d("RINSE", "TOUCH rinse 4");
                                startRinsing(3);
                            }
                        } else { //Back of the house or set portions screen
                            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                                //if pressed
                                if(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).hasDisabledButton()){
                                    mOnPourListener.onDispenseEndPress(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).getPosition());
                                }else {
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).scaleGlassButtonDown();
                                    if (id == R.id.menu_concentrate_glass_four) {
                                        mOnPourListener.onConcentratePress(mProduct4, createDispenseFinish(PRODUCT_KEY.PRODUCT_FOUR, id));
                                        Log.d("ENABLE", "==> isConcentrate = " + mProduct4.getButtonSetup().isConcentrate());
                                    } else if (id == R.id.menu_small_glass_four) {
                                        mOnPourListener.onSmallPress(mProduct4, createDispenseFinish(PRODUCT_KEY.PRODUCT_FOUR, id));
                                        Log.d("ENABLE", "==> isSmall = " + mProduct4.getButtonSetup().isSmall());
                                    } else if (id == R.id.menu_medium_glass_four) {
                                        mOnPourListener.onMediumPress(mProduct4, createDispenseFinish(PRODUCT_KEY.PRODUCT_FOUR, id));
                                        Log.d("ENABLE", "==> isMedium = " + mProduct4.getButtonSetup().isMedium());
                                    } else if (id == R.id.menu_large_glass_four) {
                                        mOnPourListener.onLargePress(mProduct4, createDispenseFinish(PRODUCT_KEY.PRODUCT_FOUR, id));
                                        Log.d("ENABLE", "==> isLarge = " + mProduct4.getButtonSetup().isLarge());
                                    }
                                    animationMenuActionDown(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR));
                                    mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).disableAllGlassButtonsExcepted(id);
                                }
                            } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                                longPressHandler.postDelayed(mLongPressedRunnableFour, DELAY_MILLIS);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).disableAllGlassButtonsExcepted(id);
                            }
                        }
                    } else if (id == R.id.water) {
                        if(mHasFocusPourView) {
                            longPressHandler.postDelayed(mLongPressedRunnableWater, 0);
                        }else{
                            longPressHandler.postDelayed(mLongPressedRunnableWater, DELAY_MILLIS);
                        }
                    }
                }

                break;

            case MotionEvent.ACTION_UP:

                decrementTouchCount(id);

                if (mTouchCount < TWO_TOUCH) {

                    setNowTime(System.currentTimeMillis());

                    if (containsButtonOneId(id)) {

                        longPressHandler.removeCallbacks(mLongPressedRunnableOne);
                        Log.d("log", "Long Press detected; halting propagation of motion event");

                        if (id == R.id.menu_one || id == R.id.menu_pour_one
                                || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                            if(!mHasFocusPourView){
                                animationMenuActionUp(PRODUCT_KEY.PRODUCT_ONE, id);
                                onPressOut(PRODUCT_KEY.PRODUCT_ONE, id);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).enableAllGlassButtons();
                            }

                        }

                    } else if (containsButtonTwoId(id)) {
                        longPressHandler.removeCallbacks(mLongPressedRunnableTwo);
                        Log.d("log", "Long Press detected; halting propagation of motion event");

                        if (id == R.id.menu_two || id == R.id.menu_pour_two
                                || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                            if(!mHasFocusPourView) {
                                animationMenuActionUp(PRODUCT_KEY.PRODUCT_TWO, id);
                                onPressOut(PRODUCT_KEY.PRODUCT_TWO, id);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).enableAllGlassButtons();
                            }
                        }

                    } else if (containsButtonThreeId(id)) {
                        longPressHandler.removeCallbacks(mLongPressedRunnableThree);
                        Log.d("log", "Long Press detected; halting propagation of motion event");

                        if (id == R.id.menu_three || id == R.id.menu_pour_three
                                || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                            if(!mHasFocusPourView) {
                                animationMenuActionUp(PRODUCT_KEY.PRODUCT_THREE, id);
                                onPressOut(PRODUCT_KEY.PRODUCT_THREE, id);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).enableAllGlassButtons();
                            }
                        }

                    } else if (containsButtonFourId(id)) {
                        longPressHandler.removeCallbacks(mLongPressedRunnableFour);
                        Log.d("log", "Long Press detected; halting propagation of motion event");

                        if (id == R.id.menu_four || id == R.id.menu_pour_four
                                || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                            if(!mHasFocusPourView) {
                                animationMenuActionUp(PRODUCT_KEY.PRODUCT_FOUR, id);
                                onPressOut(PRODUCT_KEY.PRODUCT_FOUR, id);
                                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).enableAllGlassButtons();
                            }
                        }

                    } else if (id == R.id.water) {
                        if(!mHasFocusPourView) {
                            longPressHandler.removeCallbacks(mLongPressedRunnableWater);
                            mOnPourListener.onPressOut(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getPosition());
                            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).enableAllGlassButtons();
                        }
                    }

                    switch (mTouchCount) {

                        case NO_TOUCH:
                            if(!mHasFocusPourView) {
                                removeCallBack();
                                longPressHandler.removeCallbacks(mNoButtonPressRunnable);
                                longPressHandler.removeCallbacks(mOneButtonPressRunnable);
                                longPressHandler.postDelayed(mNoButtonPressRunnable, DELAY_TOUCH_UP);
                                mOnPourListener.onTransitionToAttractiveLoopDelayed();
                                mOnPourListener.onTransitionToDispenseDelayed();
                            }
                            break;

                        case ONE_TOUCH:
                                mCurrentButtonUpId = id;
                                removeCallBack();
                                longPressHandler.postDelayed(mOneButtonPressRunnable, DELAY_TOUCH_UP);
                            break;
                    }

                }
                break;
        }

        return true;
    }

    public void stopPourPressAnimation(){

        if(mHasFocusPourView) {
            if ((mCurrentViewFocusId == R.id.menu_one) || (mCurrentViewFocusId == R.id.menu_pour_one)) {
                animationMenuActionUp(PRODUCT_KEY.PRODUCT_ONE, mCurrentViewFocusId);
                onPressOut(PRODUCT_KEY.PRODUCT_ONE, mCurrentViewFocusId);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).enableAllGlassButtons();
                startNoButtonPressAnimation();
            } else if ((mCurrentViewFocusId == R.id.menu_two) || (mCurrentViewFocusId == R.id.menu_pour_two)) {
                animationMenuActionUp(PRODUCT_KEY.PRODUCT_TWO, mCurrentViewFocusId);
                onPressOut(PRODUCT_KEY.PRODUCT_TWO, mCurrentViewFocusId);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).enableAllGlassButtons();
                startNoButtonPressAnimation();
            } else if ((mCurrentViewFocusId == R.id.menu_three) || (mCurrentViewFocusId == R.id.menu_pour_three)) {
                animationMenuActionUp(PRODUCT_KEY.PRODUCT_THREE, mCurrentViewFocusId);
                onPressOut(PRODUCT_KEY.PRODUCT_THREE, mCurrentViewFocusId);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).enableAllGlassButtons();
                startNoButtonPressAnimation();
            } else if ((mCurrentViewFocusId == R.id.menu_four) || (mCurrentViewFocusId == R.id.menu_pour_four)) {
                animationMenuActionUp(PRODUCT_KEY.PRODUCT_FOUR, mCurrentViewFocusId);
                onPressOut(PRODUCT_KEY.PRODUCT_FOUR, mCurrentViewFocusId);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).enableAllGlassButtons();
                startNoButtonPressAnimation();
            } else if (mCurrentViewFocusId == R.id.water) {
                longPressHandler.removeCallbacks(mLongPressedRunnableWater);
                mOnPourListener.onPressOut(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getPosition());
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).enableAllGlassButtons();
                startNoButtonPressAnimation();
            }
        }

    }

    private void startNoButtonPressAnimation() {
        Log.d("KEYBOARD", "==== stopPourPressAnimation");
        longPressHandler.removeCallbacks(mNoButtonPressRunnable);
        longPressHandler.removeCallbacks(mOneButtonPressRunnable);
        longPressHandler.postDelayed(mNoButtonPressRunnable, DELAY_TOUCH_UP);
        mOnPourListener.onTransitionToAttractiveLoopDelayed();
        mOnPourListener.onTransitionToDispenseDelayed();
    }

    private void removeCallBack() {
        longPressHandler.removeCallbacks(mLongPressedRunnableOne);
        longPressHandler.removeCallbacks(mLongPressedRunnableTwo);
        longPressHandler.removeCallbacks(mLongPressedRunnableThree);
        longPressHandler.removeCallbacks(mLongPressedRunnableFour);
        longPressHandler.removeCallbacks(mLongPressedRunnableWater);
        longPressHandler.removeCallbacks(mOneButtonPressRunnable);
    }

    private Runnable createDispenseFinish(final PRODUCT_KEY animationType, final int id) {
        Runnable onDispenseFinish = new Runnable() {

            @Override
            public void run() {
                dispenseFinishAnimation(id, animationType);
            }
        };

        return onDispenseFinish;
    }

    private void dispenseFinishAnimation(int id, PRODUCT_KEY animationType) {
        decrementTouchCountAfterTimer(id);
        animationMenuActionUp(animationType, id);
        mProductViewHolders.get(animationType).enableAllGlassButtons();

        switch (mTouchCount) {

            case NO_TOUCH:
                longPressHandler.removeCallbacks(mNoButtonPressRunnable);
                longPressHandler.removeCallbacks(mOneButtonPressRunnable);
                longPressHandler.postDelayed(mNoButtonPressRunnable, DELAY_TOUCH_UP);
                mOnPourListener.onTransitionToAttractiveLoopDelayed();
                mOnPourListener.onTransitionToDispenseDelayed();
                break;

            case ONE_TOUCH:
                removeCurrentAnimotionType(mProductViewHolders.get(animationType));
                disableAlphaToButtonsAndLines();
                break;
        }
    }

    synchronized private void updateAlphaToButtonsAndLines() {
        for (int i = 0; i < mProductViewHolders.size(); i++) {
            PRODUCT_KEY key = mProductViewHolders.keyAt(i);
            ProductViewHolder productViewHolder = mProductViewHolders.get(key);
            if (AnimotionUtils.getInstance().containsAnimotionType(productViewHolder.getAnimationDrinkType())) {
                productViewHolder.disableAlphaToButtonAndLine();
            } else if(AnimotionUtils.getInstance().getCurrentAnimotionTypeSize() > 1) {
                productViewHolder.enableAlphaToButtonAndLine();
            }
        }
    }

    synchronized private void disableAlphaToButtonsAndLines() {
        for (int i = 0; i < mProductViewHolders.size(); i++) {
            PRODUCT_KEY key = mProductViewHolders.keyAt(i);
            ProductViewHolder productViewHolder = mProductViewHolders.get(key);
            productViewHolder.disableAlphaToButtonAndLine();
        }
    }

    synchronized private void removeCurrentAnimotionType(ProductViewHolder productViewHolder) {

        AnimotionUtils.getInstance().removeCurrentAnimotionType(productViewHolder.getAnimationDrinkType());
        for (int i = 0; i < mProductViewHolders.size(); i++) {
            PRODUCT_KEY key = mProductViewHolders.keyAt(i);

            ProductViewHolder currentProductViewHolder = mProductViewHolders.get(key);

            if (currentProductViewHolder.getDrink() != null)
                currentProductViewHolder.getDrink().stopAnim();

            if (currentProductViewHolder.getAnimationDrinkType() != productViewHolder.getAnimationDrinkType() && AnimotionUtils.getInstance().containsAnimotionType(currentProductViewHolder.getAnimationDrinkType())) {

                AnimotionUtils.getInstance().animotionOnlyOneDrink(mContext, currentProductViewHolder.getViewGroupLayout(), currentProductViewHolder.getDrink(), currentProductViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition(), true, currentProductViewHolder.getLine());
                AnimotionUtils.getInstance().animotionOnlyOneDrinkSmall(mContext, currentProductViewHolder.mNutritionalInfoButton, currentProductViewHolder.getPosition());

                for (int y = 0; y < mProductViewHolders.size(); y++) {
                    // get the object by the key.
                    PRODUCT_KEY productKey = mProductViewHolders.keyAt(y);
                    ProductViewHolder otherProductViewHolder = mProductViewHolders.get(productKey);

                    if (otherProductViewHolder.getAnimationDrinkType() != currentProductViewHolder.getAnimationDrinkType()) {

                        AnimotionUtils.getInstance().animotionDrinkOther(mContext, otherProductViewHolder.getViewGroupLayout(), otherProductViewHolder.getDrink(), otherProductViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition(), otherProductViewHolder.getLine());
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext, otherProductViewHolder.getViewGroupLayout(), otherProductViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition());

                    }
                }

            }
        }

    }

    synchronized private void animationMenuActionUp(PRODUCT_KEY animationType, int id) {
        mProductViewHolders.get(animationType).getDrink().stopAnim();
    }

    private void onPressOut(PRODUCT_KEY animationType, int id) {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
            MachineManager.ProductSize productSize = null;
            if (Arrays.asList(mActionButtonSmallIdArray).contains(id)) {
                productSize = MachineManager.ProductSize.SMALL;
            }else if (Arrays.asList(mActionButtonConcentrateIdArray).contains(id)) {
                productSize = MachineManager.ProductSize.CONCENTRATE;
            } else if (Arrays.asList(mActionButtonMediumIdArray).contains(id)) {
                productSize = MachineManager.ProductSize.MEDIUM;
            } else if (Arrays.asList(mActionButtonLargeIdArray).contains(id)) {
                productSize = MachineManager.ProductSize.LARGE;
            }
            Log.d("SET_PORTION", "onSetPortionOut");
            this.mOnPourListener.onPressOut(mProductViewHolders.get(animationType).getPosition());
            this.mOnPourListener.onSetPortionOut(mProductViewHolders.get(animationType).getPosition(), productSize);

        } else {
            Log.d("SET_PORTION", "ONPOUR");
            this.mOnPourListener.onPressOut(mProductViewHolders.get(animationType).getPosition());
        }
    }

    synchronized private void animationMenuActionDown(ProductViewHolder currentProductViewHolder) {

        if (AnimotionUtils.getInstance().getCurrentAnimotionTypeSize() == 0 || (AnimotionUtils.getInstance().getCurrentAnimotionTypeSize() == 1 && AnimotionUtils.getInstance().containsAnimotionType(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_WATER).getAnimationDrinkType()))) {
            for (int i = 0; i < mProductViewHolders.size(); i++) {
                PRODUCT_KEY key = mProductViewHolders.keyAt(i);
                // get the object by the key.
                ProductViewHolder productViewHolder = mProductViewHolders.get(key);

                if (productViewHolder.getAnimationDrinkType() == currentProductViewHolder.getAnimationDrinkType()) {
                    AnimotionUtils.getInstance().animotionOnlyOneDrink(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getDrink(), productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition(), true, productViewHolder.getLine());
                    AnimotionUtils.getInstance().animotionOnlyOneDrinkSmall(mContext, productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition());
                } else {
                    if (!AnimotionUtils.getInstance().containsAnimotionType(productViewHolder.getAnimationDrinkType())) {
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getDrink(), productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition(), productViewHolder.getLine());
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition());
                    }
                }
            }
        } else {
            for (int i = 0; i < mProductViewHolders.size(); i++) {
                PRODUCT_KEY key = mProductViewHolders.keyAt(i);
                // get the object by the key.
                ProductViewHolder productViewHolder = mProductViewHolders.get(key);

                if (productViewHolder.getAnimationDrinkType() == currentProductViewHolder.getAnimationDrinkType() || (AnimotionUtils.getInstance().containsAnimotionType(productViewHolder.getAnimationDrinkType()) && productViewHolder.getAnimationDrinkType() != AnimotionUtils.DRINK_WATER)) {
                    AnimotionUtils.getInstance().animotionDrink(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getDrink(), productViewHolder.getNutritionalInfoButton(), productViewHolder.getPosition(), false, productViewHolder.getLine());
                    AnimotionUtils.getInstance().animotionDrinkSmall(mContext, productViewHolder.getNutritionalInfoButton(), productViewHolder.getPosition());
                } else {
                    AnimotionUtils.getInstance().animotionDrinkOther(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getDrink(), productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition(), productViewHolder.getLine());
                    AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext, productViewHolder.getViewGroupLayout(), productViewHolder.getNutritionalInfoButton(), currentProductViewHolder.getPosition());
                }
            }
        }
        AnimotionUtils.getInstance().addCurrentAnimotionType(currentProductViewHolder.getAnimationDrinkType());
        updateAlphaToButtonsAndLines();
    }

    private void onPressButton(ProductViewHolder currentProductViewHolder) {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
            Log.d("SET_PORTION", "onSetPortion");
            this.mOnPourListener.onPress(currentProductViewHolder.getPosition());
            this.mOnPourListener.onSetPortion();

        } else {
            this.mOnPourListener.onPress(currentProductViewHolder.getPosition());
        }
    }

    synchronized private void decrementTouchCountAfterTimer(int id) {

        if ((mTouchSimultaneous.indexOfKey(id) >= 0)) {
            mTouchSimultaneous.delete(id);
        }

        mTouchCount = mTouchSimultaneous.size();
    }

    synchronized private void decrementTouchCount(int id) {

        if (containsButtonOneId(id) || containsButtonTwoId(id) || containsButtonThreeId(id) || containsButtonFourId(id) || id == R.id.water) {

            if (id == R.id.menu_one || id == R.id.menu_pour_one
                    || id == R.id.menu_two || id == R.id.menu_pour_two
                    || id == R.id.menu_three || id == R.id.menu_pour_three
                    || id == R.id.menu_four || id == R.id.menu_pour_four
                    || id == R.id.water || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {


                if ((mTouchSimultaneous.indexOfKey(id) >= 0)) {
                    mTouchSimultaneous.delete(id);
                }
            }

            mTouchCount = mTouchSimultaneous.size();
        }

    }

    synchronized private void incrementTouchCount(int id) {
        if (containsButtonOneId(id) || containsButtonTwoId(id) || containsButtonThreeId(id) || containsButtonFourId(id) || id == R.id.water) {

            if (mTouchSimultaneous.size() < 2) {
                mTouchSimultaneous.append(id, id);
            }

            mTouchCount = mTouchSimultaneous.size();
        }
    }

    public View[] getViewsFocusOrder() {
        View[] views = new View[0];
        return mViewsFocusInOrder.toArray(views);
    }

    static final class RinseCallback implements Runnable
    {
        ProductViewHolder mProductViewHolder;
        MachineManager mMachineManager;
        OnPourListener mListener;
        NestleProduct mProduct;
        public RinseCallback(final NestleProduct product, final ProductViewHolder productViewHolder, final OnPourListener listener, final MachineManager machineManager)
        {
            mListener=listener;
            mMachineManager=machineManager;
            mProduct=product;
            mProductViewHolder=productViewHolder;
        }

        @Override
        public void run()
        {
            mProductViewHolder.hidePourButton();
            mListener.updateRinseRequired();
            mMachineManager.rinseEnd(mProduct);
            mListener.onTransitionToDispenseDelayed();
        }
    }

    public void startRinsing(int position) {

        MachineManager machineManager = ((ExpressApp) mContext.getApplicationContext()).getMachineManager();

        Runnable onFinishCallback = null;
        ProductViewHolder productViewHolder = null;
        switch (position) {
            case 0:
                onFinishCallback = new RinseCallback(mProduct1, mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE), mOnPourListener, machineManager);
                productViewHolder = mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE);
                break;
            case 1:
                onFinishCallback = new RinseCallback(mProduct2, mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO), mOnPourListener, machineManager);
                productViewHolder = mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO);
                break;
            case 2:
                onFinishCallback = new RinseCallback(mProduct3, mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE), mOnPourListener, machineManager);
                productViewHolder = mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE);
                break;
            case 3:
                onFinishCallback = new RinseCallback(mProduct4, mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR), mOnPourListener, machineManager);
                productViewHolder = mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR);
                break;
        }

        if(productViewHolder != null && productViewHolder.isPourButtonEnabled()){
            productViewHolder.disablePourButton();
            machineManager.rinseStart(mProducts[position], mRinseStatus.getRinseStatus(ProductSlot.values()[position]), onFinishCallback);
            mOnPourListener.onRemoveDispenseDelayed();
        }

        Log.d("RINSE","=== startRinsing");

    }

    public void disableProductsIfError() {
        //if(mProductViewHolders != null && !mProductViewHolders.isEmpty()) {
            int position = 0;
        Log.d("DISABLE_PRODUCTS", " disableProductsIfError ");
            for (NestleProduct product : mProducts) {

                ProductViewHolder productViewHolder = mProductViewHolders.get(PRODUCT_KEY.values()[position]);
                if (productViewHolder != null) {
                    Log.d("DISABLE_PRODUCTS", " productViewHolder ");

                    if (product != null) {

                        NestleProduct.ProductStatus productStatus = product.getStatus();

                        if (productStatus != NestleProduct.ProductStatus.DISPENSE) {
                            Log.d("DISABLE_PRODUCTS", " product status != dispense ");
                            productViewHolder.disableProductOnError(productStatus);
                        } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                            Log.d("DISABLE_PRODUCTS", " product status == dispense ");
                            productViewHolder.enableProductNoError();
                        }
                    } else {
                        productViewHolder.disableProductOnNull();
                    }
                }

                position++;
            }
        //}
    }

    public void disableProductsIfError(NestleProduct[] products) {
        //if(mProductViewHolders != null && !mProductViewHolders.isEmpty()) {
        int position = 0;
        Log.d("DISABLE_PRODUCTS", " disableProductsIfError ");
        for (NestleProduct product : products) {

            ProductViewHolder productViewHolder = mProductViewHolders.get(PRODUCT_KEY.values()[position]);
            if (productViewHolder != null) {
                Log.d("DISABLE_PRODUCTS", " productViewHolder ");

                if (product != null) {

                    NestleProduct.ProductStatus productStatus = product.getStatus();

                    if (productStatus != NestleProduct.ProductStatus.DISPENSE) {
                        Log.d("DISABLE_PRODUCTS", " product status != dispense ");
                        productViewHolder.disableProductOnError(productStatus);
                    } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                        Log.d("DISABLE_PRODUCTS", " product status == dispense ");
                        productViewHolder.enableProductNoError();
                    }
                } else {
                    productViewHolder.disableProductOnNull();
                }
            }

            position++;
        }
        //}
    }

    public void disableProductsIfError(NestleProduct[] products, String lang) {
        //if(mProductViewHolders != null && !mProductViewHolders.isEmpty()) {
        int position = 0;
        Log.d("DISABLE_PRODUCTS", " disableProductsIfError ");
        for (NestleProduct product : products) {

            ProductViewHolder productViewHolder = mProductViewHolders.get(PRODUCT_KEY.values()[position]);
            if (productViewHolder != null) {
                Log.d("DISABLE_PRODUCTS", " productViewHolder ");

                if (product != null) {

                    NestleProduct.ProductStatus productStatus = product.getStatus();

                    if (productStatus != NestleProduct.ProductStatus.DISPENSE) {
                        Log.d("DISABLE_PRODUCTS", " product status != dispense ");
                        productViewHolder.disableProductOnError(productStatus);
                    } else if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                        Log.d("DISABLE_PRODUCTS", " product status == dispense ");
                        productViewHolder.enableProductNoError(lang);
                    }
                } else {
                    productViewHolder.disableProductOnNull();
                }
            }

            position++;
        }
        //}
    }


    public void disableButtonIfRinseRequired(){
        for(Map.Entry<PRODUCT_KEY, ProductViewHolder> entry : mProductViewHolders.entrySet()){
            ProductViewHolder productViewHolder = entry.getValue();
            productViewHolder.disableRinseButtonOnFrontendDefault();
            //Log.d("RINSE","@@@@disableRinseButtonOnFrontendDefault");
        }
    }

    public void addViewFocusInOrder(View view) {
        Log.d("focus", "add focus");
        mViewsFocusInOrder.add(view);
    }

    public void addViewFocusInOrder(NestleProduct product, View object, MachineManager.ProductSize productSize){
        if(product != null){
            switch (productSize) {
                case CONCENTRATE:
                    if(product.getButtonSetup().isConcentrate()){
                        mViewsFocusInOrder.add(object);
                    }
                    break;
                case SMALL:
                    if(product.getButtonSetup().isSmall()){
                        mViewsFocusInOrder.add(object);
                    }
                    break;
                case MEDIUM:
                    if(product.getButtonSetup().isMedium()){
                        mViewsFocusInOrder.add(object);
                    }
                    break;
                case LARGE:
                    if(product.getButtonSetup().isLarge()){
                        mViewsFocusInOrder.add(object);
                    }
                    break;
            }
        }

    }

    public void clearViewsFocusInOrder() {
        mViewsFocusInOrder.clear();
    }

    private int mCurrentViewFocusId;

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Log.d("KEYBOARD"," View = "+v.getId()+" hasFocus = "+hasFocus);

        mCurrentViewFocusId = v.getId();

        if(hasFocus == true){

            if(containsButtonOneId(mCurrentViewFocusId) || containsButtonTwoId(mCurrentViewFocusId) || containsButtonThreeId(mCurrentViewFocusId) || containsButtonFourId(mCurrentViewFocusId) || mCurrentViewFocusId == R.id.water ){
                mHasFocusPourView = true;
            }


        }else{
            mHasFocusPourView = false;
        }

    }

    public void startFocus(){
        if(!mHasFocusPourView) {
            if(mViewsFocusInOrder!=null && mViewsFocusInOrder.size() > 1 )
            mViewsFocusInOrder.get(1).requestFocus();
        }
    }

    private enum PRODUCT_KEY {
        PRODUCT_ONE,
        PRODUCT_TWO,
        PRODUCT_THREE,
        PRODUCT_FOUR,
        PRODUCT_WATER
    }

    private class ProductViewHolder {

        private int mPosition;
        private int mAnimationDrinkType;
        private int mAnimationShowContentType;
        private boolean mIsPourButtonEnabled = true;
        private ViewGroupLayout mViewGroupLayout;
        private RelativeLayout mNutritionalInfoButton;
        private ImageView mNutritionalInfoButtonImage;
        private MyDrinkView mDrink;
        private View mLine;
        private TextView mPourTextButton;
        private TextView mSmallPourTextButton;
        private ImageView mConcentrateGlassButton;
        private ImageView mSmallGlassButton;
        private ImageView mMediumGlassButton;
        private ImageView mLargeGlassButton;
        private BackOfHouseViewGroupLayout mBackOfHouseViewGroupLayout;
        private ImageView mProductGlass;
        private ImageView mProductFruit;
        private ImageView mProductTitle;
        private ImageView mProductSubtitle;
        private ImageView mProductNutritionalInfo;
        private SparseIntArray mDisableButtonsIdArray = new SparseIntArray();

        private Drawable mProductGlassDrawable;
        private Drawable mProductNutritionalInfoButtonDrawable;

        public ProductViewHolder(int position, int animationDrinkType, int animationShowContentType, View line, TextView pourTextButton, ViewHelper listener) {
            mPosition = position;
            mAnimationDrinkType = animationDrinkType;
            mAnimationShowContentType = animationShowContentType;
            mLine = line;
            mPourTextButton = pourTextButton;
            if (mPourTextButton != null) {
                mPourTextButton.setOnTouchListener(listener);
                mPourTextButton.setOnFocusChangeListener(listener);
            }

            //When in RINSE frontend state with a rinse status that is not 'MANUAL_RINSE', only pour buttons associated with a product of rinse state are visible
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
                if (position == 4) {//position 4 is for water dispenser
                    mLine.setVisibility(View.GONE);
                    mPourTextButton.setVisibility(View.GONE);
                } else if (mRinseStatus.getRinseStatus(ProductSlot.values()[position]) == RinseStatus.RINSE_STATUS.MANUAL_RINSE) {
                    mPourTextButton.setText(R.string.menu_rinse);
                    Log.d("RINSE","== RINSE manual position ="+position+1);
                } else if (mRinseStatus.getRinseStatus(ProductSlot.values()[position]) == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE) {
                    mPourTextButton.setText(R.string.menu_rinse);
                    Log.d("RINSE","== RINSE manual position ="+position+1);
                }else if (mRinseStatus.getRinseStatus(ProductSlot.values()[position]) == RinseStatus.RINSE_STATUS.DAILY_RINSE) {
                    mPourTextButton.setText(R.string.menu_rinse);
                    Log.d("RINSE","== RINSE manual position ="+position+1);
                } else {
                    mPourTextButton.setVisibility(View.GONE);
                }
            }

        }

        //User UI ProductViewHolder
        public ProductViewHolder(int position, int animationDrinkType, int animationShowContentType, View line, TextView pourTextButton, ViewHelper listener,
                                 ViewGroupLayout viewGroupLayout, MyDrinkView drink, RelativeLayout nutritionalInfoButton, ImageView nutritionalInfoButtonImageView,
                                 ImageView glass, ImageView fruit, ImageView title, ImageView subtitle, ImageView nutritionalInfo) {
            this(position, animationDrinkType, animationShowContentType, line, pourTextButton, listener);

            mViewGroupLayout = viewGroupLayout;
            mDrink = drink;
            mNutritionalInfoButton = nutritionalInfoButton;
            mNutritionalInfoButtonImage = nutritionalInfoButtonImageView;
            mNutritionalInfoButtonImage.setOnClickListener(listener);
            mProductGlass = glass;
            mProductFruit = fruit;
            mProductTitle = title;
            mProductSubtitle = subtitle;
            mProductNutritionalInfo = nutritionalInfo;
        }

        //Back of the house ProductViewHolder
        public ProductViewHolder(int position, int animationDrinkType, int animationShowContentType, View line, ViewHelper listener, ViewGroupLayout viewGroupLayout, MyDrinkView drink, RelativeLayout nutritionalInfoButton, ImageView nutritionalInfoButtonImageView,
                                 ImageView glass, ImageView fruit, ImageView title, ImageView subtitle, ImageView nutritionalInfo,
                                 TextView smallPourTextButton, ImageView concentrateGlassButton, ImageView smallGlassButton, ImageView mediumGlassButton, ImageView largeGlassButton, TextView pourButtonTextView, BackOfHouseViewGroupLayout backOfHouseViewGroupLayout) {
            this(position, animationDrinkType, animationShowContentType, line, pourButtonTextView, listener, viewGroupLayout, drink, nutritionalInfoButton, nutritionalInfoButtonImageView, glass, fruit, title, subtitle, nutritionalInfo);

            mBackOfHouseViewGroupLayout = backOfHouseViewGroupLayout;
            mSmallPourTextButton = smallPourTextButton;
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mSmallPourTextButton.setVisibility(View.GONE);
            } else {
                mSmallPourTextButton.setOnTouchListener(listener);
                mSmallPourTextButton.setOnFocusChangeListener(listener);
            }
            mConcentrateGlassButton = concentrateGlassButton;
            mConcentrateGlassButton.setOnTouchListener(listener);
            mConcentrateGlassButton.setOnFocusChangeListener(listener);
            mSmallGlassButton = smallGlassButton;
            mSmallGlassButton.setOnTouchListener(listener);
            mSmallGlassButton.setOnFocusChangeListener(listener);
            mMediumGlassButton = mediumGlassButton;
            mMediumGlassButton.setOnTouchListener(listener);
            mMediumGlassButton.setOnFocusChangeListener(listener);
            mLargeGlassButton = largeGlassButton;
            mLargeGlassButton.setOnTouchListener(listener);
            mLargeGlassButton.setOnFocusChangeListener(listener);
        }

        public void setAssets(NestleProduct product) {

            if (product != null) {
                mProductGlassDrawable = ProductArtUtils.getProductBitmapDrawable(
                        mContext,
                        product.getProductArt().getGlassPath(),
                        ProductArtUtils.ASSET.GLASS
                );
                mProductNutritionalInfoButtonDrawable = ProductArtUtils.getProductBitmapDrawable(
                        mContext,
                        product.getProductArt().getNutritionalInfoButtonPath(),
                        ProductArtUtils.ASSET.NUTRITIONAL_INFO_BUTTON
                );

                if (mViewGroupLayout != null) {
                    mViewGroupLayout.setBackground(
                            ProductArtUtils.getProductBitmapDrawable(
                                    mContext,
                                    product.getProductArt().getCircularBGPath(),
                                    ProductArtUtils.ASSET.CIRCULAR_BG
                            )
                    );
                }

                if (mProductGlass != null)
                    mProductGlass.setImageDrawable(
                            mProductGlassDrawable
                    );

                if (mProductFruit != null)
                    mProductFruit.setImageDrawable(
                            ProductArtUtils.getProductBitmapDrawable(
                                    mContext,
                                    product.getProductArt().getFruitPath(),
                                    ProductArtUtils.ASSET.FRUIT
                            )
                    );

                if (mNutritionalInfoButtonImage != null)
                    mNutritionalInfoButtonImage.setImageDrawable(
                            mProductNutritionalInfoButtonDrawable
                    );

                NestleProduct.ProductStatus productStatus = product.getStatus();
                if (productStatus != NestleProduct.ProductStatus.DISPENSE) {
                    disableProductOnError(productStatus);
                }
            }
        }

        public void setAssetsAfterAnimation(NestleProduct product) {
            if (product != null) {
                if (mProductTitle != null)
                    mProductTitle.setImageDrawable(
                            ProductArtUtils.getProductBitmapDrawable(
                                    mContext,
                                    product.getProductArt().getTitlePath(),
                                    ProductArtUtils.ASSET.TITLE
                            )
                    );

                if (mProductSubtitle != null) {
                    mProductSubtitle.setImageDrawable(
                            ProductArtUtils.getProductBitmapDrawable(
                                    mContext,
                                    product.getProductArt().getSubtitlePath(),
                                    ProductArtUtils.ASSET.SUBTITLE
                            )
                    );
                }

                if (mProductNutritionalInfo != null)
                    mProductNutritionalInfo.setImageDrawable(
                            ProductArtUtils.getProductBitmapDrawable(
                                    mContext,
                                    product.getProductArt().getNutritionalInfoPath(),
                                    ProductArtUtils.ASSET.NUTRITIONAL_INFO
                            )
                    );
            }
        }

        public void clearAssets() {


            if (mViewGroupLayout != null) {
                mViewGroupLayout.setBackground(null);
            }

            if (mProductGlass != null)
                mProductGlass.setImageDrawable(null);

            if (mProductFruit != null)
                mProductFruit.setImageDrawable(null);

            if (mProductTitle != null)
                mProductTitle.setImageDrawable(null);

            if (mProductSubtitle != null) {
                mProductSubtitle.setImageDrawable(null);
            }

            if (mProductNutritionalInfo != null)
                mProductNutritionalInfo.setImageDrawable(null);

            if (mNutritionalInfoButtonImage != null)
                mNutritionalInfoButtonImage.setImageDrawable(null);
        }

        public void disableProductOnError(NestleProduct.ProductStatus productStatus) {
            if (mPourTextButton != null) {

                if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                    disablePourButton();
                    mNutritionalInfoButtonImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.small_title_make_another_selection));
                }

                if(productStatus == null)
                    Log.d("DISABLE_PRODUCTS"," product status=null ");

                switch (productStatus) {
                    case FAULT:
                        Log.d("DISABLE_PRODUCTS"," product status=FAULT ");
                        mPourTextButton.setText(R.string.menu_fault);
                        break;
                    case RINSE:
                        Log.d("DISABLE_PRODUCTS"," product status=RINSE ");
                        //When in RINSE frontend state with a rinse status that is not 'MANUAL_RINSE', only pour buttons associated with a product of rinse state are visible
                        mPourTextButton.setVisibility(View.VISIBLE);
                        mPourTextButton.setText(R.string.menu_rinse);
                        break;
                    case SOLD_OUT:
                        Log.d("DISABLE_PRODUCTS"," product status= SOLD_OUT");
                        mPourTextButton.setText(R.string.menu_sold_out);
                        break;
                }

                if (isBackOfTheHouse()) {
                    ViewGroup row = (ViewGroup) mPourTextButton.getParent();
                    for (int itemPos = 0; itemPos < row.getChildCount(); itemPos++) {
                        View view = row.getChildAt(itemPos);
                        view.setVisibility(View.GONE);
                    }
                    mPourTextButton.setVisibility(View.VISIBLE);
                }
            }
        }

        public void disableRinseButtonOnFrontendDefault(){

            if(mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT && mPosition < 4){
                Log.d("RINSE","--- position "+mPosition);
                RinseStatus.RINSE_STATUS status =  mRinseStatus.getRinseStatus(ProductSlot.values()[mPosition]);
                if (status == RinseStatus.RINSE_STATUS.DAILY_RINSE || status  == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE){
                    disablePourButton();
                    mPourTextButton.setEnabled(false);
                    mPourTextButton.setVisibility(View.VISIBLE);
                    mPourTextButton.setText(R.string.menu_rinse);
                    if (isBackOfTheHouse()) {
                        ViewGroup row = (ViewGroup) mPourTextButton.getParent();
                        for (int itemPos = 0; itemPos < row.getChildCount(); itemPos++) {
                            View view = row.getChildAt(itemPos);
                            view.setVisibility(View.GONE);
                        }
                        mPourTextButton.setVisibility(View.VISIBLE);
                        mPourTextButton.setEnabled(false);
                        Log.d("RINSE", "--- position " + mPosition+" enable false");
                    }
                }
            }

            if(mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE && mPosition < 4){
                RinseStatus.RINSE_STATUS status =  mRinseStatus.getRinseStatus(ProductSlot.values()[mPosition]);
                if (status != RinseStatus.RINSE_STATUS.ALLERGEN_RINSE && status != RinseStatus.RINSE_STATUS.DAILY_RINSE){
                    mPourTextButton.setVisibility(View.GONE);
                }
            }
            //Log.d("RINSE","--- showRinseButtonOnFrontendDefault ---");
        }

        public void disableProductOnNull() {
            if (mPourTextButton != null) {

                disablePourButton();

                mProductGlass.setImageDrawable(mContext.getResources().getDrawable(R.drawable.glass));
                mProductFruit.setVisibility(View.GONE);
                mNutritionalInfoButtonImage.setImageDrawable(mContext.getResources().getDrawable(R.drawable.small_title_make_another_selection));
                mPourTextButton.setText(R.string.menu_null);

                if (isBackOfTheHouse()) {
                    ViewGroup row = (ViewGroup) mPourTextButton.getParent();
                    for (int itemPos = 0; itemPos < row.getChildCount(); itemPos++) {
                        View view = row.getChildAt(itemPos);
                        view.setVisibility(View.GONE);
                    }
                    mPourTextButton.setVisibility(View.VISIBLE);
                }
            }
        }

        public void isSmallOrConcentrate(NestleProduct product){

            if(product.getButtonSetup().isConcentrate()){
                mConcentrateGlassButton.setVisibility(View.VISIBLE);
                mSmallGlassButton.setVisibility(View.GONE);
                Log.d("ENTROU", "CONCENTRATE VISIBLE");
            }else{
                mConcentrateGlassButton.setVisibility(View.GONE);
                mSmallGlassButton.setVisibility(View.VISIBLE);
                Log.d("ENTROU", "CONCENTRATE GONE");
            }
        }

        public void disablePourButton() {
            mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.shape_bottom_gray));
            mIsPourButtonEnabled = false;
        }

        public void lockPourButton() {
            mPourTextButton.setEnabled(false);
            mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.shape_bottom_gray));
        }

        public void hidePourButton() {
            mPourTextButton.setVisibility(View.GONE);
        }

        public boolean isPourButtonEnabled() {
            return mIsPourButtonEnabled;
        }

        public void enableProductNoError() {

            if (mPourTextButton != null && mPourTextButton.isEnabled() == true) {

                mPourTextButton.setText(R.string.menu_text);

                //if(lang.equals("en"))
                    mPourTextButton.setTextSize((float)60);
                //else
                //    mPourTextButton.setTextSize((float)40);

                mProductFruit.setVisibility(View.VISIBLE);
                mProductGlass.setImageDrawable(mProductGlassDrawable);
                mNutritionalInfoButtonImage.setImageDrawable(mProductNutritionalInfoButtonDrawable);

                switch (mPosition) {
                    case AnimotionUtils.PRODUCT_ONE_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_one));
                        break;
                    case AnimotionUtils.PRODUCT_TWO_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_two));
                        break;
                    case AnimotionUtils.PRODUCT_THREE_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_three));
                        break;
                    case AnimotionUtils.PRODUCT_FOUR_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_four));
                        break;
                }

                if (isBackOfTheHouse()) {
                    mPourTextButton.setVisibility(View.GONE);
                }
            }
        }

        public void enableProductNoError(String lang) {

            if (mPourTextButton != null && mPourTextButton.isEnabled() == true) {

                mPourTextButton.setText(R.string.menu_text);

                if(lang.equals("en"))
                    mPourTextButton.setTextSize((float)60);
                else if(lang.equals("es"))
                    mPourTextButton.setTextSize((float)38);
                else {
                    mPourTextButton.setTextSize((float) 43);
                    mPourTextButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                }

                mProductFruit.setVisibility(View.VISIBLE);
                mProductGlass.setImageDrawable(mProductGlassDrawable);
                mNutritionalInfoButtonImage.setImageDrawable(mProductNutritionalInfoButtonDrawable);

                switch (mPosition) {
                    case AnimotionUtils.PRODUCT_ONE_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_one));
                        break;
                    case AnimotionUtils.PRODUCT_TWO_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_two));
                        break;
                    case AnimotionUtils.PRODUCT_THREE_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_three));
                        break;
                    case AnimotionUtils.PRODUCT_FOUR_POSITION:
                        mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.selector_pour_four));
                        break;
                }

                if (isBackOfTheHouse()) {
                    mPourTextButton.setVisibility(View.GONE);
                    if(hasMoreThanOneButton(mProducts[getPosition()])==false){                      // if no portion buttons selected
                        if(lang.equals("en"))
                            mSmallPourTextButton.setTextSize((float)60);
                        else if(lang.equals("es"))
                            mSmallPourTextButton.setTextSize((float)38);
                        else {
                            mSmallPourTextButton.setTextSize((float) 43);
                            mSmallPourTextButton.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                        }
                    }
                }
            }
        }

        public int getPosition() {
            return mPosition;
        }

        public int getAnimationDrinkType() {
            return mAnimationDrinkType;
        }

        public ViewGroupLayout getViewGroupLayout() {
            return mViewGroupLayout;
        }

        public RelativeLayout getNutritionalInfoButton() {
            return mNutritionalInfoButton;
        }

        public ImageView getNutritionalInfoButtonImageView() {
            return mNutritionalInfoButtonImage;
        }

        public MyDrinkView getDrink() {
            return mDrink;
        }

        public View getLine() {
            return mLine;
        }

        public int getAnimationShowContentType() {
            return mAnimationShowContentType;
        }

        public void enableAlphaToButtonAndLine() {
            this.mLine.setAlpha(0.0f);
            if (this.mPourTextButton != null)
                this.mPourTextButton.setAlpha(0.4f);

            if (mBackOfHouseViewGroupLayout != null)
                mBackOfHouseViewGroupLayout.setAlpha(0.4f);
        }

        public void disableAlphaToButtonAndLine() {
            this.mLine.setAlpha(1.0f);
            if (this.mPourTextButton != null)
                this.mPourTextButton.setAlpha(1.0f);

            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.setAlpha(1.0f);
            }

        }

        public void disableAllGlassButtonsExcepted(int id) {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.disableAllViewExcepted(id);
            }
        }

        public void disableAllGlassButtons(int id) {
            if (mBackOfHouseViewGroupLayout != null)
                mBackOfHouseViewGroupLayout.disableAllViewAndSetAplhaExcepted(id);
        }

        public void enableGlassButton(int id){
            if (mBackOfHouseViewGroupLayout != null)
                mBackOfHouseViewGroupLayout.enableGlassButton(id);
        }

        public boolean hasDisabledButton(){
            boolean isDisabled = false;
            if (mBackOfHouseViewGroupLayout != null) {
                isDisabled = mBackOfHouseViewGroupLayout.hasButtonDisabled();
            }
            return isDisabled;
        }

        public void disableAllButtons() {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.disableAllViewButtons();
            }else{
                mPourTextButton.setBackground(mContext.getResources().getDrawable(R.drawable.shape_bottom_gray));
                mPourTextButton.setEnabled(false);
            }
        }

        public void disableGlassButtonById(int id) {
            if (mBackOfHouseViewGroupLayout != null) {
                addDisableButtonId(id);
                mBackOfHouseViewGroupLayout.disableViewById(id);
            }
        }

        public void hideGlassButtonById(int id) {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.hideViewById(id);
            }
        }

        public void showAllGlassButtons() {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.showAllViewButtons();

            } else if (mPourTextButton != null) {
                mPourTextButton.setVisibility(View.VISIBLE);
            }
        }

        public void enableAllGlassButtons() {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.enableAllView();
                mBackOfHouseViewGroupLayout.enableAllViewExcepted(mDisableButtonsIdArray);

            } else if (mPourTextButton != null) {
                mPourTextButton.animate().scaleX(1.0f).setDuration(300);
                mPourTextButton.animate().scaleY(1.0f).setDuration(300);
            }
        }

        public void scaleButtonDown() {
            if (mBackOfHouseViewGroupLayout != null) {
                mBackOfHouseViewGroupLayout.scalePourDown();
            } else if (mPourTextButton != null) {
                mPourTextButton.animate().scaleX(1.1f).setDuration(300);
                mPourTextButton.animate().scaleY(1.1f).setDuration(300);
            }
        }

        public void scaleGlassButtonDown() {
            if (mBackOfHouseViewGroupLayout != null)
                mBackOfHouseViewGroupLayout.scaleGlassDown();
        }

        private void addDisableButtonId(int id) {
            mDisableButtonsIdArray.append(id, id);
        }

    }

    public boolean isBackOfTheHouse() {

        if(mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE){
            return false;
        }
        //This is the correct way for check the back of the house condition
        return (mIsBackofHouse || mProduct1.getButtonSetup().isConcentrate() || mProduct1.getButtonSetup().isSmall() || mProduct1.getButtonSetup().isMedium() || mProduct1.getButtonSetup().isLarge()
                || mProduct2.getButtonSetup().isConcentrate() || mProduct2.getButtonSetup().isSmall() || mProduct2.getButtonSetup().isMedium() || mProduct2.getButtonSetup().isLarge()
                || mProduct3.getButtonSetup().isConcentrate() || mProduct3.getButtonSetup().isSmall() || mProduct3.getButtonSetup().isMedium() || mProduct3.getButtonSetup().isLarge()
                || mProduct4.getButtonSetup().isConcentrate() || mProduct4.getButtonSetup().isSmall() || mProduct4.getButtonSetup().isMedium() || mProduct4.getButtonSetup().isLarge());

        //This is only to simulate this situation for QA
        //return mIsBackofHouse;
    }

    public boolean hasMoreThanOneButton(NestleProduct product){

        if (product == null) {
            return false;
        }

        return (product.getButtonSetup().isConcentrate() || product.getButtonSetup().isSmall() || product.getButtonSetup().isMedium() || product.getButtonSetup().isLarge());
    }


    public void clearAssets() {
        if (!mProductViewHolders.isEmpty()) {
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).clearAssets();
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_TWO).clearAssets();
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_THREE).clearAssets();
            mProductViewHolders.get(PRODUCT_KEY.PRODUCT_FOUR).clearAssets();
        }
    }
}
