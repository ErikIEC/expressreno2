package com.nestle.express.frontend_ui.transition;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;

import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.Transition;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.TransitionValues;

public class ChangeTransition extends Transition {
    private static final String PROPNAME_NAME1 = "android:transition:translationx";
    private static final String PROPNAME_NAME2 = "android:transition:translationy";
//    private static final String PROPNAME_NAME3 = "android:transition:translationz";
    private static final String FIELD_NAME1 = "translationX";
    private static final String FIELD_NAME2 = "translationY";
//    private static final String FIELD_NAME3 = "translationZ";

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        transitionValues.values.put(PROPNAME_NAME1, transitionValues.view.getTranslationX());
        transitionValues.values.put(PROPNAME_NAME2, transitionValues.view.getTranslationY());
//        transitionValues.values.put(PROPNAME_NAME3, transitionValues.view.getTranslationZ());
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        transitionValues.values.put(PROPNAME_NAME1, transitionValues.view.getTranslationX());
        transitionValues.values.put(PROPNAME_NAME2, transitionValues.view.getTranslationY());
//        transitionValues.values.put(PROPNAME_NAME3, transitionValues.view.getTranslationZ());
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues,
                                   TransitionValues endValues) {
        if (startValues == null || endValues == null) {
            return null;
        }
        final View view = endValues.view;
        final float startValueX = (Float) startValues.values.get(PROPNAME_NAME1);
        final float startValueY = (Float) startValues.values.get(PROPNAME_NAME2);
//        float startValueZ = (Float) startValues.values.get(PROPNAME_NAME3);
        final float endValueX = (Float) endValues.values.get(PROPNAME_NAME1);
        final float endValueY = (Float) endValues.values.get(PROPNAME_NAME2);
//        float endValueZ = (Float) endValues.values.get(PROPNAME_NAME3);
        AnimatorSet animatorSet = new AnimatorSet();
        if (startValueX != endValueX) {
            view.setTranslationX(startValueX);

            ObjectAnimator animator = ObjectAnimator.ofFloat(view, FIELD_NAME1, startValueX, endValueX) ;
//            animator.setInterpolator(new LinearInterpolator());

//            ValueAnimator valueAnimator = ValueAnimator.ofFloat(startValueX , endValueX) ;
//            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                @Override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    float value = (float) animation.getAnimatedFraction();
//                    view.setTranslationX((endValueX - startValueX) *value);
//                }
//            });

            animatorSet.playTogether(animator);
        }
        if (startValueY != endValueY) {
            view.setTranslationY(startValueY);

            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(view, FIELD_NAME2, startValueY, endValueY ) ;
//            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
//            ValueAnimator valueAnimator = ValueAnimator.ofFloat(startValueX , endValueX) ;
//            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                @Override
//                public void onAnimationUpdate(ValueAnimator animation) {
//                    float value = (float) animation.getAnimatedFraction();
//                    view.setTranslationY((endValueY - startValueY)*value*value);
//                }
//            });

            animatorSet.playTogether(objectAnimator);
        }
//        if (startValueZ != endValueZ) {
//            view.setTranslationZ(startValueZ);
//            animatorSet.playTogether(ObjectAnimator.ofFloat(view, FIELD_NAME3, startValueZ, endValueZ));
//        }
        if(animatorSet.getChildAnimations()!=null && animatorSet.getChildAnimations().size()>0){
            return animatorSet;
        }

//        ValueAnimator animator = ValueAnimator.ofFloat() ;
//        return animator ;
        return null;
    }
}
