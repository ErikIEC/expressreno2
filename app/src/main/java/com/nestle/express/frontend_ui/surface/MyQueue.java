package com.nestle.express.frontend_ui.surface;

/**
 * Created by xumin on 2016/8/26.
 */
public class MyQueue<T> {

    private Node<T> first;

    private Node<T> last;

    private Node<T> curr,next;

    public T getFirst(){
        return first.t;
    }

    public T getLast(){
        return last.t;
    }

    public boolean hasNext(){
        return curr == null && curr.next == null ? false : true;
    }

    public void currToFirst(){
        curr = first;
    }
    public T getNext(){
        Node<T> next = curr;
        if (next == null)
            return null;
        curr = next.next;
        return next.t;
    }

    public void add(T t){

        Node<T> node = new Node<>();
        node.t = t;
        if (first == null){
            first = node;
            last = node;
            return;
        }
        last.next = node;
        last = node;
    }

    public void remove(T t){
        Node tem = first;
        while(true){
            if (tem.t == t){
                Node pre = tem.pre;
                Node next = tem.next;
                pre.next = next;
                next.pre = pre;
                return;
            }
            tem = tem.next;
            if (tem == null)
                return;
        }
    }

    private class Node<T>{
        Node pre;
        Node next;
        T t;
    }
}
