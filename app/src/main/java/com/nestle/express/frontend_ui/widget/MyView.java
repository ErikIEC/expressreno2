package com.nestle.express.frontend_ui.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xumin on 16/6/7.
 */
public class MyView extends View {


    private AnimatorSet btnSexAnimatorSet ;

    private List<Animator> animators ;

    public MyView(Context context) {
        super(context);
//        init();
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
//        init();
    }

    private void init() {
//        startAnim();
    }

    public void startAnim(){
        floatAnim(this , 1000);

//        moveCirclePath(50, 3);
    }


    /**
     * small circle
     * Try Radius=50 and revolutions=3
     * This is the portiom control the rotation of the bubble,
     //Jason, please try to modify and generate the right code
     * @param maxRadius
     * @param revolutions
     */
    protected void moveCirclePath(final float maxRadius, final float revolutions)
    {
        ValueAnimator animation = ValueAnimator.ofFloat((float) 105, 15);
        animation.setDuration(10000);

        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation)
            {
	    	/*
	    		Progess is a value between 1.0 and 0.0
	    		it starts a 1.0 and decreases towards
	    		0 as the animation progresses.
	    		Because it is squared (^2) it starts fast
	    		and decelerates until it reaches the end
	    	*/
                float fraction = (float) animation.getAnimatedFraction();
                float value = (float)animation.getAnimatedValue() ;

                float progress = (float) Math.pow((value/105), 2);
                float radius = progress * maxRadius;
                float angle = (float) (progress * revolutions * 2.0 * Math.PI);

                int x = (int) (radius * Math.cos(angle));
                int y = (int) (radius * Math.sin(angle));
                setTranslationX(x);
                setTranslationY(y);
            }
        });

        animation.setInterpolator(new DecelerateInterpolator());
        animation.setStartDelay(1000);
        animation.start();
    }

    public void stopAnim() {
        if (btnSexAnimatorSet != null) {
            btnSexAnimatorSet.cancel();
        }

        if (animators!= null && animators.size() > 0) {
            for (int i = 0 ;  i < animators.size() ;i ++ ) {
                animators.get(i).cancel();
            }
        }
    }
    private void floatAnim(View view, int delay){

//        animators = new ArrayList<>();
//
//        float offsetX = (float) (5 - (Math.random() * 5));
//        ObjectAnimator translationXAnim = ObjectAnimator.ofFloat(view, "translationX", -offsetX,offsetX - 1,-offsetX - 1,offsetX - 2,-offsetX - 2,offsetX - 3,-offsetX - 3,offsetX - 4,-offsetX - 4);
//        translationXAnim.setDuration(1500 + (long) (Math.random() * 1000));
//        translationXAnim.setRepeatCount(1);//无限循环
//        translationXAnim.setRepeatMode(ValueAnimator.INFINITE);//
//
////        BounceInterpolator bounceInterpolator = new BounceInterpolator() ;
//
//        DecelerateInterpolator overshootInterpolator = new DecelerateInterpolator(1);
////        translationXAnim.setInterpolator(overshootInterpolator);
//
//        translationXAnim.start();
//        animators.add(translationXAnim);
//        float offsetY = (float) (5 - (Math.random() * 5));
//        ObjectAnimator translationYAnim = ObjectAnimator.ofFloat(view, "translationY", -offsetY,offsetY - 1,-offsetY - 1,offsetY - 2,-offsetY - 2,offsetY - 3,-offsetY - 3,offsetY - 4,-offsetY - 4);
//        translationYAnim.setDuration(1500 + (long) (Math.random() * 1000));
//        translationYAnim.setRepeatCount(1);
//        translationYAnim.setRepeatMode(ValueAnimator.INFINITE);
////        translationYAnim.setInterpolator(overshootInterpolator);
//        translationYAnim.start();
//        animators.add(translationYAnim);
//
//        btnSexAnimatorSet = new AnimatorSet();
//        btnSexAnimatorSet.playTogether(animators);
//        btnSexAnimatorSet.setInterpolator(overshootInterpolator);
//        btnSexAnimatorSet.setStartDelay(delay);
//        btnSexAnimatorSet.start();



        animators = new ArrayList<>();

//        ObjectAnimator scaleXAnim = ObjectAnimator.ofFloat(view, "scaleX",1f,0.90f,1f,0.90f,1f);
//        scaleXAnim.setDuration(8000);
//        scaleXAnim.setRepeatMode(ValueAnimator.INFINITE);//
        DecelerateInterpolator overshootInterpolator = new DecelerateInterpolator(1);
//        animators.add(scaleXAnim);
//
//        ObjectAnimator scaleYAnim = ObjectAnimator.ofFloat(view, "scaleY",1f,0.90f,1f,0.90f,1f);
//        scaleYAnim.setDuration(8000);
//        scaleYAnim.setRepeatMode(ValueAnimator.INFINITE);//
//        animators.add(scaleYAnim);


        float offsetX = (float) (80 - (Math.random() * 80));
        ObjectAnimator translationXAnim = ObjectAnimator.ofFloat(view, "translationX",0,offsetX - 5,-offsetX - 5,offsetX - 10,-offsetX - 10,offsetX - 15,-offsetX - 15,offsetX - 20,-offsetX - 20f , 0f);
        translationXAnim.setDuration(12000);
//        translationXAnim.setRepeatCount(1);//无限循环
        translationXAnim.setRepeatMode(ValueAnimator.INFINITE);//

//        BounceInterpolator bounceInterpolator = new BounceInterpolator() ;

//        translationXAnim.setInterpolator(overshootInterpolator);

        translationXAnim.start();
        animators.add(translationXAnim);
        float offsetY = (float) (50 - (Math.random() * 50));
        ObjectAnimator translationYAnim = ObjectAnimator.ofFloat(view, "translationY", 0f, -offsetY,offsetY - 5,-offsetY - 5,offsetY - 10,-offsetY - 10,offsetY - 15,-offsetY - 15,offsetY - 20,-offsetY - 20 , 0);
        translationYAnim.setDuration(10000);
//        translationYAnim.setRepeatCount(1);
        translationYAnim.setRepeatMode(ValueAnimator.INFINITE);
//        translationYAnim.setInterpolator(overshootInterpolator);
        animators.add(translationYAnim);

        btnSexAnimatorSet = new AnimatorSet();
        btnSexAnimatorSet.playTogether(animators);
        btnSexAnimatorSet.setInterpolator(overshootInterpolator);
        btnSexAnimatorSet.setStartDelay(delay);
        btnSexAnimatorSet.start();
    }

    private PointF pointF ;

    public void initAnimotion(){
        pointF = new PointF(getX() , getY()) ;
    }

    public void putPosition (float x , float y) {
        pointF = new PointF(x ,y) ;
    }

    public void setMyX(float addX) {
        setX(pointF.x + addX);
    }

    public void setMyY(float addY) {
        setY(pointF.y + addY);
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
