package com.nestle.express.frontend_ui.transition;

import android.view.animation.AccelerateDecelerateInterpolator;

import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.ChangeBounds;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.TransitionSet;

/**
 *Created by xumin on 16/6/7.
 */
public class TransitionPlayer1 extends TransitionSet {
    public final ChangeBounds changeBounds = new ChangeBounds();
    public final ChangeAlpha changeAlpha = new ChangeAlpha();
    public final ChangeRotate changeRotate = new ChangeRotate();
    public final ChangeScale changeScale = new ChangeScale();
    public final ChangeTransition changeTransition = new ChangeTransition();
    public final ChangeBackground changeBackground = new ChangeBackground();
//    public final ChangeTextColor changeTextColor = new ChangeTextColor();
//    public final Fade fadeOut = new Fade(Fade.OUT);
//    public final Fade fadeIn = new Fade(Fade.IN);

    public TransitionPlayer1() {

        setOrdering(TransitionSet.ORDERING_TOGETHER);
        setDuration(1000);

        addTransition(changeBounds);
        addTransition(changeAlpha);
        addTransition(changeRotate);
        addTransition(changeScale);
        addTransition(changeTransition);
        addTransition(changeBackground);
//        addTransition(changeTextColor);

//        addTransition(fadeOut);
//        addTransition(fadeIn);

        setInterpolator(new AccelerateDecelerateInterpolator());

//        setInterpolator(new MyMoveInterpolator(3 ,0.20f));

//        setInterpolator(new OvershootInterpolator());
    }

    @Override
    public TransitionSet addTarget(int targetId) {
        return super.addTarget(targetId);
    }
}
