package com.nestle.express.frontend_ui.util;

import android.animation.TypeEvaluator;
import android.graphics.PointF;

/**
 * Created by xumin on 16/8/12.
 */
public class BezierEvaluator implements TypeEvaluator<PointF> {

//    private PointF points[];
//
//    public BezierEvaluator(PointF... points) {
//        if (points.length != 4) {
//            throw new IllegalArgumentException("只演示三次方贝赛尔曲线");
//        }
//        this.points = points;
//    }
//
//    @Override
//    public PointF evaluate(float fraction, PointF startValue, PointF endValue) {
//        // B(t) = P0 * (1-t)^3 + 3 * P1 * t * (1-t)^2 + 3 * P2 * t^2 * (1-t) + P3 * t^3
//
//        float t = fraction;
//        float one_t = 1.0f - t;
//
//        PointF P0 = points[0];
//        PointF P1 = points[1];
//        PointF P2 = points[2];
//        PointF P3 = points[3];
//
//        float x = (float) (P0.x * Math.pow(one_t, 3) + 3 * P1.x * t * Math.pow(one_t, 2) + 3 * P2.x * Math.pow(t, 2) * one_t + P3.x * Math.pow(t, 3));
//        float y = (float) (P0.y * Math.pow(one_t, 3) + 3 * P1.y * t * Math.pow(one_t, 2) + 3 * P2.y * Math.pow(t, 2) * one_t + P3.y * Math.pow(t, 3));
//
//        PointF pointF = new PointF(x, y);
//
//        return pointF;
//    }

    private PointF pointF1;//途径的两个点
    private PointF pointF2;

    public BezierEvaluator(PointF pointF1, PointF pointF2) {
        this.pointF1 = pointF1;
        this.pointF2 = pointF2;
    }

    @Override
    public PointF evaluate(float time, PointF startValue,
                           PointF endValue) {

        float timeLeft = 1.0f - time;
        PointF point = new PointF();//结果

        PointF point0 = (PointF) startValue;//起点

        PointF point3 = (PointF) endValue;//终点
        //代入公式
        point.x = timeLeft * timeLeft * timeLeft * (point0.x)
                + 3 * timeLeft * timeLeft * time * (pointF1.x)
                + 3 * timeLeft * time * time * (pointF2.x)
                + time * time * time * (point3.x);

        point.y = timeLeft * timeLeft * timeLeft * (point0.y)
                + 3 * timeLeft * timeLeft * time * (pointF1.y)
                + 3 * timeLeft * time * time * (pointF2.y)
                + time * time * time * (point3.y);
        return point;
    }

}