package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.widget.ViewAnimotionBean;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xumin on 16/8/18.
 */
public class PageNewUtils {
    private Context mContext ;
    private View mView ;

    private MyView purple1 , purple , green , blue , deep_purple , orange , green1 , orange1;
    public ImageView main_one_viewgroup_image1 , main_one_viewgroup_image2 , main_one_viewgroup_image3 , main_one_viewgroup_image4
            ,main_two_viewgroup_image1 , main_two_viewgroup_image2 , main_two_viewgroup_image3 , main_two_viewgroup_image4
            ,main_three_viewgroup_image1 , main_three_viewgroup_image2 , main_three_viewgroup_image3 , main_three_viewgroup_image4
            ,main_four_viewgroup_image1 , main_four_viewgroup_image2 , main_four_viewgroup_image3 , main_four_viewgroup_image4;
    public AbsoluteLayout main_one_viewgroup_layout ,main_two_viewgroup_layout,main_three_viewgroup_layout,main_four_viewgroup_layout;
    public ViewGroupLayout main_one_viewgroup , main_two_viewgroup ,main_three_viewgroup , main_four_viewgroup;

    public PageNewUtils(Context context , View view ) {
            this.mContext = context ;
            this.mView = view ;
            initView(view);
    }

    private void initView (View view) {
        purple1 = (MyView)view.findViewById(R.id.purple1) ;
        purple = (MyView)view.findViewById(R.id.purple) ;
//        green = (MyView)view.findViewById(R.id.green) ;
        blue = (MyView)view.findViewById(R.id.blue) ;
        deep_purple = (MyView)view.findViewById(R.id.deep_purple) ;
        orange = (MyView)view.findViewById(R.id.orange) ;
        green1 = (MyView)view.findViewById(R.id.green1) ;
        orange1 = (MyView)view.findViewById(R.id.orange1) ;

        main_one_viewgroup = (ViewGroupLayout) view.findViewById(R.id.main_one_viewgroup);
        main_two_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_two_viewgroup) ;
        main_three_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_three_viewgroup) ;
        main_four_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_four_viewgroup) ;

        main_one_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_one_viewgroup_layout) ;
        main_two_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_two_viewgroup_layout) ;
        main_three_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_three_viewgroup_layout) ;
        main_four_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_four_viewgroup_layout) ;

        main_one_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image1) ;
        main_one_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image2) ;
        main_one_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image3) ;
        main_one_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image4) ;

        main_two_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image1) ;
        main_two_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image2) ;
        main_two_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image3) ;
        main_two_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image4) ;

        main_three_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image1) ;
        main_three_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image2) ;
        main_three_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image3) ;
        main_three_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image4) ;

        main_four_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image1) ;
        main_four_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image2) ;
        main_four_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image3) ;
        main_four_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image4) ;
    }

    public void testMove() {
        setPostionOne();
        initPostionTwo() ;
        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 0 , 40 , true , 0 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, 0,0 , 40) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),0 , 40) ;

        ViewAnimotionBean viewAnimotionBean6  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple1 , mContext.getResources().getDimension(R.dimen.dimen_080dp),mContext.getResources().getDimension(R.dimen.dimen_080dp) ,0 , 0) ;
        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple , mContext.getResources().getDimension(R.dimen.dimen_050dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp) ,0 , 0) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_0300dp) ) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 1 , 35 , false , 1 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF,1 , 40 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean10 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_460dp), mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_440dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean12 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean14 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_460dp),0 , 0) ;

        ViewAnimotionBean viewAnimotionBean15 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp) , mContext.getResources().getDimension(R.dimen.dimen_150dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean16 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_280dp),mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean17 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_300dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean18 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange , mContext.getResources().getDimension(R.dimen.dimen_300dp)  , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        // end animotion
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_080dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        ViewAnimotionBean viewAnimotionBean19 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 35 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean20 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_40dp) , mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        ViewAnimotionBean viewAnimotionBean21 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange ,oldPointF, newPointF , 1 , 30 , false , 1 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0425dp) ) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp)) ;
        ViewAnimotionBean viewAnimotionBean22 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 1 , 25 , false , 1 , true) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 20000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 20000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 20000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 20000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 20000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 20000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 20000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 20000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 20000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 20000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 20000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 20000 , null) ;
        ValueAnimator valueAnimator12 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean12, 20000 , null) ;
        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 20000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 20000 , null) ;
        ValueAnimator valueAnimator15 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean15, 20000 , null) ;
        ValueAnimator valueAnimator16 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean16, 20000 , null) ;
        ValueAnimator valueAnimator17 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean17, 20000 , null) ;
        ValueAnimator valueAnimator18 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean18, 20000 , null) ;

        ValueAnimator valueAnimator19 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean19, 19200 , null) ;
        valueAnimator19.setStartDelay(800);
        ValueAnimator valueAnimator20 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean20, 19200 , null) ;
        valueAnimator20.setStartDelay(800);
        ValueAnimator valueAnimator21 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean21, 19200 , null) ;
        valueAnimator21.setStartDelay(800);
        ValueAnimator valueAnimator22 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean22, 19200 , null) ;
        valueAnimator22.setStartDelay(800);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(valueAnimator,valueAnimator1,valueAnimator2,valueAnimator3,valueAnimator4
                                ,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9
                                ,valueAnimator10,valueAnimator11,valueAnimator12,valueAnimator13,valueAnimator14
                                ,valueAnimator15,valueAnimator16,valueAnimator17,valueAnimator18,valueAnimator19
                                ,valueAnimator20,valueAnimator21,valueAnimator22);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.e("SOLON" , "animation is end ...") ;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                Log.e("SOLON" , "animation is cancel ...") ;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void onOneState() {

    }

    public void initPosition(ArrayList<ViewAnimotionBean> list) {
        for (ViewAnimotionBean bean : list) {
            bean.initPosition();
        }
    }

    public ViewAnimotionBean getSonViewAniotionBean(View view , float endx , float endY,int direction,int radis ) {
        PointF newPointF = new PointF(endx , endY) ;
        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction ,radis) ;
        return viewAnimotionBean ;
    }

    /**
     * 第一个页面切换动画
     */
    public void pageOneAnimotion() {
        setPostionOne();
        initPostionTwo() ;
        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 0 , 40 , true , 0 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, 0,0 , 40) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),0 , 40) ;

        ViewAnimotionBean viewAnimotionBean6  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple1 , mContext.getResources().getDimension(R.dimen.dimen_080dp),mContext.getResources().getDimension(R.dimen.dimen_080dp) ,0 , 0) ;
        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple , mContext.getResources().getDimension(R.dimen.dimen_050dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp) ,0 , 0) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_0300dp) ) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 1 , 35 , false , 1 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF,1 , 40 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean10 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_460dp), mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_440dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean12 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean14 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_460dp),0 , 0) ;

        ViewAnimotionBean viewAnimotionBean15 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0400dp) , mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean16 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_280dp),mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean17 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_300dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean18 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange , mContext.getResources().getDimension(R.dimen.dimen_300dp)  , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        // end animotion
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_080dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        ViewAnimotionBean viewAnimotionBean19 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 35 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean20 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_40dp) , mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        ViewAnimotionBean viewAnimotionBean21 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange ,oldPointF, newPointF , 1 , 30 , false , 1 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0425dp) ) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp)) ;
        ViewAnimotionBean viewAnimotionBean22 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 1 , 25 , false , 1 , true) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        ValueAnimator valueAnimator12 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean12, 15000 , null) ;
        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 15000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 15000 , null) ;
        ValueAnimator valueAnimator15 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean15, 800 , null) ;
        ValueAnimator valueAnimator16 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean16, 800 , null) ;
        ValueAnimator valueAnimator17 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean17, 800 , null) ;
        ValueAnimator valueAnimator18 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean18, 800 , null) ;

        ValueAnimator valueAnimator19 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean19, 14200 , null) ;
//        valueAnimator19.setStartDelay(800);
        ValueAnimator valueAnimator20 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean20, 14200 , null) ;
//        valueAnimator20.setStartDelay(800);
        ValueAnimator valueAnimator21 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean21, 14200 , null) ;
//        valueAnimator21.setStartDelay(800);
        ValueAnimator valueAnimator22 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean22, 14200 , null) ;
//        valueAnimator22.setStartDelay(800);

        AnimatorSet animatorSet19 = new AnimatorSet() ;
        animatorSet19.playTogether(valueAnimator15,valueAnimator16,valueAnimator17,valueAnimator18);
        animatorSet19.setDuration(800) ;
        AnimatorSet animatorSet20 = new AnimatorSet() ;
        animatorSet20.playTogether(valueAnimator19,valueAnimator20,valueAnimator21,valueAnimator22);
        animatorSet20.setDuration(14000) ;
        AnimatorSet animatorSet21 = new AnimatorSet();
        animatorSet21.playSequentially(animatorSet19 );

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(valueAnimator,valueAnimator1,valueAnimator2,valueAnimator3,valueAnimator4
                ,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9
                ,valueAnimator10,valueAnimator11,valueAnimator12,valueAnimator13,valueAnimator14
                ,animatorSet19);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                pageTwoToThreeAniomotion();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void setPostionOne() {
        main_four_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.INVISIBLE);

        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_two_viewgroup_image1 , R.dimen.dimen_0300dp , R.dimen.dimen_330dp) ;
        setViewPosition(main_two_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_400dp) ;
        setViewPosition(main_two_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_355dp) ;
        setViewPosition(main_two_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_450dp) ;
        setViewPosition(main_two_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_two_viewgroup.setChildeVisibilty(View.VISIBLE);
        main_two_viewgroup.setMyScaleX(1.0f);
        main_two_viewgroup.setMyScaleY(1.0f);

    }

    private void initPostionTwo() {
        main_one_viewgroup.putPosition(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp) , mContext.getResources().getDimension(R.dimen.dimen_0300dp));
        main_four_viewgroup.putPosition(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition( mContext.getResources().getDimension(R.dimen.dimen_060dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp));
        orange.putPosition(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_330dp));
        main_three_viewgroup.putPosition(main_three_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp));

        main_two_viewgroup.putPosition(main_two_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp) , mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_two_viewgroup.putPosition(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;

        main_two_viewgroup.putPosition(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_image4 , 0, 0) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }


    /**
     * 第一个页面结束的时候调用的方法
     */
    private void pageEndAnimotion() {
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_080dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 35 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean1 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_40dp) , mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        ViewAnimotionBean viewAnimotionBean2 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange ,oldPointF, newPointF , 1 , 60 , false , 0 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0425dp) ) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp)) ;
        ViewAnimotionBean viewAnimotionBean3 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 1 , 80 , false , 1 , true) ;

        ArrayList arrayList = new ArrayList() ;
        arrayList.add(viewAnimotionBean) ;
        arrayList.add(viewAnimotionBean1);
        arrayList.add(viewAnimotionBean2) ;
        arrayList.add(viewAnimotionBean3) ;

        List<Animator> arrayList1 = new ArrayList() ;
        for (int i = 0 ; i < arrayList.size() ; i ++ ) {
            ValueAnimator valueAnimator = null ;

            valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000 , null) ;

            arrayList1.add(valueAnimator) ;
        }

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(17000);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(arrayList1);
        animatorSet.start();
    }

    /**
     * 第二个页面动画
     *
     *
     * 修改说明：移动距离增加一部分为 圆周运动的偏移量 这样就不会造成抖动
     *
     * initPostionThree 为我们初始化没有带偏移量的值的设定
     *
     */
    public void pageTwoToThreeAniomotion() {
        setPostionTwo() ;
        initPostionThree() ;
        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0280dp),mContext.getResources().getDimension(R.dimen.dimen_240dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 1 , 70 , false , 1 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),1 , 70) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF , 0 , 40 , true , 0 , true) ;
        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image4 , 0, 0,1 , 40) ;
        ViewAnimotionBean viewAnimotionBean11  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean12  = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_0150dp), mContext.getResources().getDimension(R.dimen.dimen_100dp),0 , 0) ;
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_150dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 ,oldPointF, newPointF , 0 , 40 , false , 2 , true) ;
        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_540dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        ViewAnimotionBean viewAnimotionBean14 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange1 ,oldPointF, newPointF , 0 , 70 , false , 2 , true) ;
        ViewAnimotionBean viewAnimotionBean15  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_0500dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean16  = ViewAnimotionBeanUtils.getViewAnimotionBean(orange , mContext.getResources().getDimension(R.dimen.dimen_25dp), mContext.getResources().getDimension(R.dimen.dimen_450dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean17  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_0500dp),0 , 0) ;
        //end
        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0120dp) , mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        ViewAnimotionBean viewAnimotionBean18 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 80 , false , 2 , true) ;


        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_900dp) , mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;
        ViewAnimotionBean viewAnimotionBean19 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 0 , 80 , false , 2 , true) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        ValueAnimator valueAnimator12 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean12, 15000 , null) ;
        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 15000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 15000 , null) ;
        ValueAnimator valueAnimator15 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean15, 15000 , null) ;
        ValueAnimator valueAnimator16 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean16, 15000 , null) ;
        ValueAnimator valueAnimator17 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean17, 15000 , null) ;

        //end
        ValueAnimator valueAnimator18 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean18, 14200 , null) ;
        valueAnimator18.setStartDelay(800);
        ValueAnimator valueAnimator19 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean19, 14200 , null) ;
        valueAnimator19.setStartDelay(800);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(valueAnimator,valueAnimator1,valueAnimator2,valueAnimator3,valueAnimator4
                ,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9
                ,valueAnimator10,valueAnimator11,valueAnimator12,valueAnimator13,valueAnimator14
                ,valueAnimator15,valueAnimator16,valueAnimator17,valueAnimator18,valueAnimator19
                );
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                    pageThree();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void setPostionTwo() {
        main_four_viewgroup.setChildeVisibilty(View.VISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.INVISIBLE);

//        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
//        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
//        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
//        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
//        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_three_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_three_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_three_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_three_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_three_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_four_viewgroup.setMyScaleX(1f);
        main_four_viewgroup.setMyScaleY(1f);
    }

    private void initPostionThree() {
        main_two_viewgroup.putPosition(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0280dp) , mContext.getResources().getDimension(R.dimen.dimen_240dp) );
        orange1.putPosition( mContext.getResources().getDimension(R.dimen.dimen_540dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));
        green1.putPosition( mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_060dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));
        main_three_viewgroup.putPosition(main_three_viewgroup,mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp));

        main_four_viewgroup.putPosition(main_four_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp) , mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_four_viewgroup.putPosition(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image4 , 0, 0) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第二个页面动画结束的动画
     */
    public void pageTwoEndAnimotion() {
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0120dp) , mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 80 , false , 2 , true) ;


        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_900dp) , mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;
        ViewAnimotionBean viewAnimotionBean1 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 0 , 80 , false , 2 , true) ;

        ArrayList arrayList = new ArrayList() ;
        arrayList.add(viewAnimotionBean) ;
        arrayList.add(viewAnimotionBean1) ;
        List<Animator> arrayList1 = new ArrayList() ;
        for (int i = 0 ; i < arrayList.size() ; i ++ ) {
            ValueAnimator valueAnimator = null ;
            valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000 , null) ;
            arrayList1.add(valueAnimator) ;
        }

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(17000);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(arrayList1);
        animatorSet.start();
    }

    /**
     * 第三个页面动画
     */
    public void pageThree() {

        setPostionThree() ;

        initPostionFour();


        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , pointF , 0 , 40 , true , 0 , true) ;

        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image4 , 0, 0,0 , 40) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),0 , 40) ;

        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0250dp),mContext.getResources().getDimension(R.dimen.dimen_280dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF , 0 , 55 , false , 0 , true) ;

        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),0 , 55) ;


        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_650dp) , mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;

        ViewAnimotionBean viewAnimotionBean14  = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_0150dp), mContext.getResources().getDimension(R.dimen.dimen_30dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean15  = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 , mContext.getResources().getDimension(R.dimen.dimen_0100dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean16  = ViewAnimotionBeanUtils.getViewAnimotionBean(orange1 , mContext.getResources().getDimension(R.dimen.dimen_500dp), mContext.getResources().getDimension(R.dimen.dimen_0200dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean17  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_500dp),0 , 0) ;

        ViewAnimotionBean viewAnimotionBean18  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_0340dp), mContext.getResources().getDimension(R.dimen.dimen_90dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean19  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_0340dp), mContext.getResources().getDimension(R.dimen.dimen_80dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean20 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_0200dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean21  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean22 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_0200dp), mContext.getResources().getDimension(R.dimen.dimen_30dp),0 , 0) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;

        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 15000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 15000 , null) ;
        ValueAnimator valueAnimator15 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean15, 15000 , null) ;
        ValueAnimator valueAnimator16 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean16, 15000 , null) ;
        ValueAnimator valueAnimator17 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean17, 15000 , null) ;
        ValueAnimator valueAnimator18 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean18, 15000 , null) ;

        ValueAnimator valueAnimator19 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean19, 15000 , null) ;
        ValueAnimator valueAnimator20 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean20, 15000 , null) ;
        ValueAnimator valueAnimator21 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean21, 15000 , null) ;
        ValueAnimator valueAnimator22 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean22, 15000 , null) ;

        //end
        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_010dp)) ;
        ViewAnimotionBean viewAnimotionBean23 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 0 , 50 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        ViewAnimotionBean viewAnimotionBean24 = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;
        ValueAnimator valueAnimator23 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean23, 14200 , null) ;
        valueAnimator23.setStartDelay(800);
        ValueAnimator valueAnimator24 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean24, 14200 , null) ;
        valueAnimator24.setStartDelay(800);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(valueAnimator,valueAnimator1,valueAnimator2,valueAnimator3,valueAnimator4
                ,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9
                ,valueAnimator10,valueAnimator11,valueAnimator13,valueAnimator14
                ,valueAnimator15,valueAnimator16,valueAnimator17,valueAnimator18,valueAnimator19
                ,valueAnimator20,valueAnimator21,valueAnimator22,valueAnimator23,valueAnimator24
        );
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                onFourAnimotion() ;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void setPostionThree() {
        main_one_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_two_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.VISIBLE);

//        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
//        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
//        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
//        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
//        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_four_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_four_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_four_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_four_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_four_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_three_viewgroup.setMyScaleX(1f);
        main_three_viewgroup.setMyScaleY(1f);
    }

    private void initPostionFour() {
        main_four_viewgroup.putPosition(main_four_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_0250dp), mContext.getResources().getDimension(R.dimen.dimen_280dp) );
        main_one_viewgroup.putPosition(main_one_viewgroup,mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_010dp));
        green1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_550dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));

        main_three_viewgroup.putPosition(main_three_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_three_viewgroup.putPosition(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image4 , 0, 0) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第三个页面动画结束的动画
     */
    public void onThreeEndAnimotion () {

        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_010dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 0 , 50 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
        ViewAnimotionBean viewAnimotionBean1 = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;
        ArrayList arrayList = new ArrayList() ;
        arrayList.add(viewAnimotionBean) ;
        arrayList.add(viewAnimotionBean1) ;
        List<Animator> arrayList1 = new ArrayList() ;
        for (int i = 0 ; i < arrayList.size() ; i ++ ) {
            ValueAnimator valueAnimator = null ;
            valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000 , null) ;
            arrayList1.add(valueAnimator) ;
        }

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(17000);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(arrayList1);
        animatorSet.start();
    }

    /**
     * 第四个页面动画
     */
    public void onFourAnimotion() {
        setPostionFour();
        initPositionFive();


        ViewAnimotionBean viewAnimotionBean  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_240dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean2 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean4 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image4 , 0,  mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean5 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_layout ,mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),0 , 0) ;


        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 0 , 40 , true , 0 , true) ;

        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 40) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, 0,1 , 40) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),1 , 40) ;

        ViewAnimotionBean viewAnimotionBean12  = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean13  = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 , mContext.getResources().getDimension(R.dimen.dimen_550dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),0 , 0) ;

        ViewAnimotionBean viewAnimotionBean14  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_500dp),0 , 0) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        ValueAnimator valueAnimator12 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean12, 15000 , null) ;
        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 15000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 15000 , null) ;

        //end
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_030dp), mContext.getResources().getDimension(R.dimen.dimen_030dp)) ;
        ViewAnimotionBean viewAnimotionBean15 = ViewAnimotionBeanUtils.getViewAnimotionBean(purple ,oldPointF, newPointF , 0 , 55 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_50dp), mContext.getResources().getDimension(R.dimen.dimen_20dp)) ;
        ViewAnimotionBean viewAnimotionBean16 = ViewAnimotionBeanUtils.getViewAnimotionBean(purple1 ,oldPointF, newPointF , 1 , 30 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_600dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_300dp)) ;
        ViewAnimotionBean viewAnimotionBean17 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple ,oldPointF, newPointF , 0 , 55 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_900dp) , mContext.getResources().getDimension(R.dimen.dimen_600dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        ViewAnimotionBean viewAnimotionBean18 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_900dp) , mContext.getResources().getDimension(R.dimen.dimen_600dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_220dp)) ;
        ViewAnimotionBean viewAnimotionBean19 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_0400dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_500dp), mContext.getResources().getDimension(R.dimen.dimen_0250dp)) ;
        ViewAnimotionBean viewAnimotionBean20 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;

        ValueAnimator valueAnimator15 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean15, 14200 , null) ;
        valueAnimator15.setStartDelay(800);
        ValueAnimator valueAnimator16 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean16, 14200 , null) ;
        valueAnimator16.setStartDelay(800);
        ValueAnimator valueAnimator17 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean17, 14200 , null) ;
        valueAnimator17.setStartDelay(800);
        ValueAnimator valueAnimator18 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean18, 14200 , null) ;
        valueAnimator18.setStartDelay(800);
        ValueAnimator valueAnimator19 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean19, 14200 , null) ;
        valueAnimator19.setStartDelay(800);
        ValueAnimator valueAnimator20 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean20, 14200 , null) ;
        valueAnimator20.setStartDelay(800);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(valueAnimator,valueAnimator1,valueAnimator2,valueAnimator3,valueAnimator4
                ,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9
                ,valueAnimator10,valueAnimator11,valueAnimator12,valueAnimator13,valueAnimator14
                ,valueAnimator15,valueAnimator16,valueAnimator17,valueAnimator18,valueAnimator19
                ,valueAnimator20
        );
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                initAllPosition();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void setPostionFour() {
        main_one_viewgroup.setChildeVisibilty(View.VISIBLE);
        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_one_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_one_viewgroup.setMyScaleX(1f);
        main_one_viewgroup.setMyScaleY(1f);
    }

    public void initPositionFive() {
        main_two_viewgroup.putPosition(main_two_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_500dp) , mContext.getResources().getDimension(R.dimen.dimen_0250dp));
        purple1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_50dp), mContext.getResources().getDimension(R.dimen.dimen_20dp));
        purple.putPosition(mContext.getResources().getDimension(R.dimen.dimen_030dp), mContext.getResources().getDimension(R.dimen.dimen_030dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_220dp));
        deep_purple.putPosition(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_300dp));

        main_one_viewgroup.putPosition(main_one_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_one_viewgroup.putPosition(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image4 , 0, 0) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第四个页面动画结束的动画
     */
    public void onFourEnd () {
//        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
//        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_010dp)) ;
//        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 0 , 50 , false , 2 , true) ;
//
//        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
//        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_550dp), mContext.getResources().getDimension(R.dimen.dimen_10dp)) ;
//        ViewAnimotionBean viewAnimotionBean1 = ViewAnimotionBeanUtils.getViewAnimotionBean(green1 ,oldPointF, newPointF , 1 , 55 , false , 2 , true) ;

        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(purple , mContext.getResources().getDimension(R.dimen.dimen_0100dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(purple1 , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_50dp) + CircleAnimotionUtils.getTransleX(30,1), mContext.getResources().getDimension(R.dimen.dimen_20dp)+CircleAnimotionUtils.getTransleY(30,1)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_530dp) + CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_300dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_220dp)+CircleAnimotionUtils.getTransleY(55,0)) ;

        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_0400dp) , mContext.getResources().getDimension(R.dimen.dimen_500dp) + CircleAnimotionUtils.getTransleX(55,1), mContext.getResources().getDimension(R.dimen.dimen_0250dp)+ CircleAnimotionUtils.getTransleY(55,1)) ;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet1 , animatorSet2 , animatorSet3 , animatorSet4 , animatorSet5 , animatorSet6);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,55 , 3 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple1 ,30 , 2 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple ,55 , 2 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(blue ,55 , 3 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(deep_purple ,20 , 3 , 0 , 10000);
                initAllPosition();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    /**
     * 重置一下每一个view的坐标
     */
    public void initAllPosition() {
//        setViewPosition(purple , R.dimen.dimen_030dp , R.dimen.dimen_030dp) ;
//        setViewPosition(purple1 , R.dimen.dimen_50dp , R.dimen.dimen_20dp) ;
//        setViewPosition(blue , R.dimen.dimen_580dp , R.dimen.dimen_220dp) ;
//        setViewPosition(deep_purple , R.dimen.dimen_500dp , R.dimen.dimen_330dp) ;
        setViewPosition(orange , R.dimen.dimen_580dp , R.dimen.dimen_330dp) ;
        setViewPosition(green1 , R.dimen.dimen_0100dp , R.dimen.dimen_0100dp) ;
        setViewPosition(orange1 , R.dimen.dimen_0100dp , R.dimen.dimen_0100dp) ;
        setViewPosition(main_four_viewgroup , R.dimen.dimen_650dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_three_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_50dp) ;
        setViewPosition(main_three_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_30dp) ;
        setViewPosition(main_three_viewgroup_image4 , 0 , R.dimen.dimen_300dp) ;
        setViewPosition(main_three_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_090dp) ;
        setViewPosition(main_three_viewgroup_image2 , R.dimen.dimen_0340dp , R.dimen.dimen_80dp) ;
        setViewPosition(main_three_viewgroup , R.dimen.dimen_0300dp , R.dimen.dimen_240dp) ;
        setViewPosition(main_four_viewgroup_image1 , R.dimen.dimen_280dp , R.dimen.dimen_0240dp) ;
        setViewPosition(main_four_viewgroup_image2 , R.dimen.dimen_240dp , R.dimen.dimen_0100dp) ;
        setViewPosition(main_four_viewgroup_image3 , R.dimen.dimen_420dp , R.dimen.dimen_0200dp) ;
        setViewPosition(main_four_viewgroup_layout , R.dimen.dimen_420dp , R.dimen.dimen_0120dp) ;
        setViewPosition(main_four_viewgroup_image4 , 0 , R.dimen.dimen_0300dp) ;
//        setViewPosition(main_two_viewgroup , R.dimen.dimen_500dp , R.dimen.dimen_0250dp) ;
        setViewPosition(main_two_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_370dp) ;
        setViewPosition(main_two_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_450dp) ;
        setViewPosition(main_two_viewgroup_image4 , 0 , R.dimen.dimen_300dp) ;
        setViewPosition(main_two_viewgroup_image1 , R.dimen.dimen_0200dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_two_viewgroup_image2 , R.dimen.dimen_0100dp , R.dimen.dimen_300dp) ;
    }

    /**
     * 设置点的坐标
     * @param view
     * @param demisX
     * @param demisY
     */
    private void setViewPosition(View view , int demisX , int demisY) {
        view.setX(demisX==0?0:mContext.getResources().getDimension(demisX));
        view.setY(demisY==0?0:mContext.getResources().getDimension(demisY));
    }

    private void setViewScale(View view , float scaleX, float scaleY) {
        view.setScaleX(scaleX);
        view.setScaleY(scaleY);
    }

    /**
     * 初始化的第一个页面的抖动
     */
    public void circleView () {
        CircleAnimotionUtils.moveCirclePath(main_one_viewgroup ,70 , 3 , 1 , 10000);

        CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,40 , 3 ,0, 10000 , true);

        CircleAnimotionUtils.moveCirclePath(main_four_viewgroup ,80 , 3 ,0, 10000);

        CircleAnimotionUtils.moveCirclePath(blue ,80 , 3 , 1 , 9000);
        CircleAnimotionUtils.moveCirclePath(orange ,60 , 3 , 1 , 10000 );
        CircleAnimotionUtils.moveCirclePath(main_three_viewgroup ,80 , 3 , 1 , 9000 );

    }
}
