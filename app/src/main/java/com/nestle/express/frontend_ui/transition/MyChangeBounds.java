package com.nestle.express.frontend_ui.transition;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.Transition;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.TransitionValues;

/**
 * Created by xumin on 16/8/8.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MyChangeBounds extends Transition {
    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {
        Animator changeBounds = super.createAnimator(sceneRoot, startValues, endValues);
        if (startValues == null || endValues == null || changeBounds == null)
            return null;

        changeBounds.setDuration(300);
        changeBounds.setInterpolator(AnimationUtils.loadInterpolator(sceneRoot.getContext(),
                android.R.interpolator.fast_out_slow_in));
        return changeBounds;
    }

    @Override
    public void captureStartValues(TransitionValues transitionValues) {

    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {

    }

}
