package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.nurun.backend.ProductArtUtils;
import com.nestle.express.frontend_ui.nurun.views.ProductCircleViewGroup;
import com.nestle.express.iec_backend.product.structure.NestleProduct;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jesserci on 16-10-06.
 */
public class PageFadeTransitionUtils {

    private final NestleProduct mProducts[] = new NestleProduct[4];
    private Context mContext;

    private AnimatorSet mMainAnimatorSet;

    private Map<PRODUCT_KEY, ProductViewHolder> mProductViewHolders = new HashMap<>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableBackgrounds = new HashMap<>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableGlass = new HashMap<>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableFruits = new HashMap<>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableTitles = new HashMap<>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableSubtitles = new HashMap<>();
    private ObjectAnimator exitingProductAlphaAnimation;
    private ObjectAnimator centeringProductAlphaAnimation;

    private enum PRODUCT_KEY {
        PRODUCT_ONE,
        PRODUCT_TWO,
        PRODUCT_THREE,
        PRODUCT_FOUR
    }

    class ProductViewHolder {
        private ProductCircleViewGroup mProductViewGroup;
        private ImageView mProductGlass;
        private ImageView mProductFruit;
        private ImageView mProductTitle;
        private AbsoluteLayout mProductLayout;
        private ImageView mProductSubtitle;
        private RelativeLayout mChildLayout;

        ProductViewHolder(ProductCircleViewGroup productCircleViewGroup, ImageView productGlass, ImageView productFruit, ImageView productTitle, AbsoluteLayout productLayout, ImageView productSubtitle) {
            mProductViewGroup = productCircleViewGroup;
            mProductGlass = productGlass;
            mProductFruit = productFruit;
            mProductTitle = productTitle;
            mProductLayout = productLayout;
            mProductSubtitle = productSubtitle;
            mChildLayout = (RelativeLayout) mProductViewGroup.getChildAt(0);
        }

        public ProductCircleViewGroup getProductViewGroup() {
            return mProductViewGroup;
        }

        public ImageView getProductGlass() {
            return mProductGlass;
        }

        public ImageView getProductFruit() {
            return mProductFruit;
        }

        public ImageView getProductTitle() {
            return mProductTitle;
        }

        public AbsoluteLayout getProductLayout() {
            return mProductLayout;
        }

        public ImageView getProductSubtitle() {
            return mProductSubtitle;
        }

        public RelativeLayout getChildLayout() {
            return mChildLayout;
        }
    }

    public PageFadeTransitionUtils(Context context, View view, NestleProduct[] products) {
        mContext = context;
        for (int i = 0; i < 4; i++) {
            mProducts[i] = products[i];
        }
        initView(view);
    }

    private void initView(View view) {
        //Product one
        ProductViewHolder productOneViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_one_viewgroup),
                (ImageView) view.findViewById(R.id.product_one_juice),
                (ImageView) view.findViewById(R.id.product_one_fruit),
                (ImageView) view.findViewById(R.id.product_one_title),
                (AbsoluteLayout) view.findViewById(R.id.product_one_layout),
                (ImageView) view.findViewById(R.id.product_one_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_ONE, productOneViewHolder);

        int i = 0;
        for (PRODUCT_KEY productKey : PRODUCT_KEY.values()) {
            if (mProducts[i] != null) {
                mProductDrawableGlass.put(productKey, ProductArtUtils.getProductBitmapDrawable(mContext, mProducts[i].getProductArt().getGlassPath(), ProductArtUtils.ASSET.GLASS));
                mProductDrawableFruits.put(productKey, ProductArtUtils.getProductBitmapDrawable(mContext, mProducts[i].getProductArt().getFruitPath(), ProductArtUtils.ASSET.FRUIT));
                mProductDrawableTitles.put(productKey, ProductArtUtils.getProductBitmapDrawable(mContext, mProducts[i].getProductArt().getTitlePath(), ProductArtUtils.ASSET.TITLE));
                mProductDrawableSubtitles.put(productKey, ProductArtUtils.getProductBitmapDrawable(mContext, mProducts[i].getProductArt().getSubtitleAlignedPath(), ProductArtUtils.ASSET.SUBTITLE_ALIGNED));
            }
            i++;
        }

        checkProductsForErrors();
    }

    private void checkProductsForErrors() {
        int i = 0;
        for (PRODUCT_KEY productKey : PRODUCT_KEY.values()) {
            if (mProducts[i] == null) {
                disableProductOnNull(productKey);
            }
            i++;
        }
    }

    private void disableProductOnNull(PRODUCT_KEY productKey) {
        mProductDrawableGlass.put(productKey, mContext.getResources().getDrawable(R.drawable.glass));
        mProductDrawableFruits.put(productKey, null);
        mProductDrawableTitles.put(productKey, null);
        mProductDrawableSubtitles.put(productKey, mContext.getResources().getDrawable(R.drawable.null_subtitle));
    }


    private void initMainAnimatorSet() {
        mMainAnimatorSet = new AnimatorSet();
        if (exitingProductAlphaAnimation != null
                && centeringProductAlphaAnimation != null) {
            clearAnimator();
        }

    }


    public void transitionToProduct(final PRODUCT_KEY centeringProductKey, int startExitDelay) {
        initMainAnimatorSet();

        exitingProductAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mChildLayout, View.ALPHA, 1, 0);

        Log.d("PINGOUIN", "Start Exit Delay = " + startExitDelay);
        exitingProductAlphaAnimation.setStartDelay(startExitDelay);

        centeringProductAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mChildLayout, View.ALPHA, 0, 1);
        centeringProductAlphaAnimation.setStartDelay(50);

        centeringProductAlphaAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductViewGroup().setBackground(mProductDrawableBackgrounds.get(centeringProductKey));
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductGlass().setImageDrawable(mProductDrawableGlass.get(centeringProductKey));
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductFruit().setImageDrawable(mProductDrawableFruits.get(centeringProductKey));
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductTitle().setImageDrawable(mProductDrawableTitles.get(centeringProductKey));
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductSubtitle().setImageDrawable(mProductDrawableSubtitles.get(centeringProductKey));

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductGlass().setAlpha(0f);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductFruit().setAlpha(0f);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
//
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        ObjectAnimator delayedGlassAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductGlass(), View.ALPHA, 0, 1);
        delayedGlassAlphaAnimation.setStartDelay(500);

        ObjectAnimator delayedFruitAlphaAnimation = ObjectAnimator.ofFloat(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductFruit(), View.ALPHA, 0, 1);
        delayedFruitAlphaAnimation.setStartDelay(900);

        mMainAnimatorSet.playTogether(
                centeringProductAlphaAnimation
                ,
                exitingProductAlphaAnimation,
                delayedGlassAlphaAnimation,
                delayedFruitAlphaAnimation
        );

        mMainAnimatorSet.start();

    }

    public void transitionToFirstProduct(int startExitDelay) {
        Log.d("PINGOUIN", "One Start Exit Delay = " + startExitDelay);
        transitionToProduct(PRODUCT_KEY.PRODUCT_ONE, startExitDelay);
    }

    public void transitionToSecondProduct(int startExitDelay) {
        Log.d("PINGOUIN", "TWO Start Exit Delay = " + startExitDelay);
        transitionToProduct(PRODUCT_KEY.PRODUCT_TWO, startExitDelay);
    }

    public void transitionToThirdProduct(int startExitDelay) {
        Log.d("PINGOUIN", "THREE Start Exit Delay = " + startExitDelay);
        transitionToProduct(PRODUCT_KEY.PRODUCT_THREE, startExitDelay);
    }

    public void transitionToFourthProduct(int startExitDelay) {
        Log.d("PINGOUIN", "FOUR Start Exit Delay = " + startExitDelay);
        transitionToProduct(PRODUCT_KEY.PRODUCT_FOUR, startExitDelay);
    }

    public void clearAssets() {
        //Clear HashMap
        mProductDrawableBackgrounds.clear();
        mProductDrawableGlass.clear();
        mProductDrawableFruits.clear();
        mProductDrawableTitles.clear();
        mProductDrawableSubtitles.clear();

        //Clear views drawable / bitmap
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductViewGroup().setBackground(null);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductGlass().setImageDrawable(null);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductFruit().setImageDrawable(null);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductTitle().setImageDrawable(null);
        mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).getProductSubtitle().setImageDrawable(null);
    }

    public void clearAnimator() {
        if (mMainAnimatorSet != null && mMainAnimatorSet.getListeners() != null)
            mMainAnimatorSet.getListeners().clear();
        if (exitingProductAlphaAnimation != null && exitingProductAlphaAnimation.getListeners() != null)
            exitingProductAlphaAnimation.getListeners().clear();
        if (centeringProductAlphaAnimation != null && centeringProductAlphaAnimation.getListeners() != null)
            centeringProductAlphaAnimation.getListeners().clear();
    }
}
