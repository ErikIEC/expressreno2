package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.Animator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.widget.MyDrinkView;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricolive on 2016-10-20.
 */
public class AnimotionUtils {

    public static final int MAIN_SCREEN = 0 ;
    //Product info
    public static final int ANIMOTION_SHOCONTENT_ONE = 1 ;
    public static final int ANIMOTION_SHOCONTENT_TWO = 2 ;
    public static final int ANIMOTION_SHOCONTENT_THREE = 3 ;
    public static final int ANIMOTION_SHOCONTENT_FOUR = 4 ;
    //Drink selection
    public static final int ANIMOTION_DRINK_ONE = 5 ;
    public static final int ANIMOTION_DRINK_TWO = 6 ;
    public static final int ANIMOTION_DRINK_THREE = 7 ;
    public static final int ANIMOTION_DRINK_FOUR = 8 ;
    public static final int DRINK_WATER = 9 ;
    //Positions
    public static final int PRODUCT_ONE_POSITION = 0;
    public static final int PRODUCT_TWO_POSITION = 1;
    public static final int PRODUCT_THREE_POSITION = 2;
    public static final int PRODUCT_FOUR_POSITION = 3;
    public static final int WATER_POSITION = 4;

    //private int currentAnimotinType = MAIN_SCREEN ;
    private List<Integer> currentAnimotionType = new ArrayList<>() ;

    private Map<Integer,Float> yMap = new HashMap<Integer, Float>();

    private Map<Integer,Float> xMap = new HashMap<Integer, Float>();
    private static AnimotionUtils animotionUtils = new AnimotionUtils();

    private boolean isPutValue = false ;

    //private long animotionTime = 800 ;
    private long animotionTime = 600 ;
    private long drinkAnimotionTime = 800 ;

    private double scaleFactor = 3;

    private AnimotionUtils(){}

    public boolean containsAnimotionType(int animotionType){
        return this.currentAnimotionType.contains(animotionType);
    }

    public void addCurrentAnimotionType(int currentAnimotinType){
        this.currentAnimotionType.add(currentAnimotinType);
    }

    public void clearCurrentAnimotionType(){
        this.currentAnimotionType.clear();
    }

    public void removeCurrentAnimotionType(int currentAnimotinType){
        this.currentAnimotionType.remove(Integer.valueOf(currentAnimotinType));
    }

    public int getCurrentAnimotionTypeSize(){
        return this.currentAnimotionType.size();
    }

    public static AnimotionUtils getInstance() {
        return animotionUtils ;
    }

    public void putViewPosition(View view) {
        if (isPutValue) {
            return ;
        }
        if (view!= null) {
            xMap.put(view.getId() , view.getX()) ;
            yMap.put(view.getId() , view.getY()) ;
        }
    }

    public void setPutValue(boolean flag) {
        this.isPutValue = flag ;
    }

    /**
     *
     * @param mContext
     * @param viewGroupLayout
     * @param container
     */
    synchronized public void animotionRight (final Context mContext , final ViewGroupLayout viewGroupLayout , final AbsoluteLayout container , final ImageView backGroud , final int position) {

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", 1f,3f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", 1f,3f);
        Log.d("test" , "viewX:" + viewGroupLayout.getX() + "  ,viewY:" + viewGroupLayout.getY()) ;

        //Translation values when scaling layout
        float viewGroupLayoutFinalScaleTranslationX = (mContext.getResources().getDimension(R.dimen.dimen_235dp) + 140) - viewGroupLayout.getX();
        float viewGroupLayoutFinalScaleTranslationY = (mContext.getResources().getDimension(R.dimen.dimen_115dp) + 97) - viewGroupLayout.getY();

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat(
                "translationX",
                viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()),
                viewGroupLayoutFinalScaleTranslationX
        );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat(
                "translationY",
                viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()),
                viewGroupLayoutFinalScaleTranslationY
        );

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(
                mPropertyValuesHolderTranslationX,
                mPropertyValuesHolderTranslationY,
                mPropertyValuesHolderScaleX,
                mPropertyValuesHolderScaleY
        );
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                viewGroupLayout.setChildViewAlp(fraction, true, position);
                backGroud.setAlpha(fraction);

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                viewGroupLayout.setTranslationX(animatorValueTranslationX);
                viewGroupLayout.setTranslationY(animatorValueTranslationY);
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                //The background is used to prevent clicks occurring on any other elements of the UI than the 'close nutritional panel' button
                backGroud.setVisibility(View.VISIBLE);
                backGroud.setEnabled(false);

                container.bringChildToFront(backGroud);

                container.bringChildToFront(viewGroupLayout);
                viewGroupLayout.startTransilation(
                        true,
                        position
                );
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("test", "x6:" + viewGroupLayout.getX() + ",   " + viewGroupLayout.getY());

                if (position == 0) {
                    addCurrentAnimotionType(ANIMOTION_SHOCONTENT_ONE);
                } else if (position == 1) {
                    addCurrentAnimotionType(ANIMOTION_SHOCONTENT_TWO);
                } else if (position == 2) {
                    addCurrentAnimotionType(ANIMOTION_SHOCONTENT_THREE);
                } else {
                    addCurrentAnimotionType(ANIMOTION_SHOCONTENT_FOUR);
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        //From scaling to resizing values for a clearer display (see function scalingAndResizeToggle for more info)
                        viewGroupLayout.scalingAndResizeToggle(false, position);
                        backGroud.setEnabled(true);
                    }
                }, 300);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    /**
     *
     * @param mContext
     * @param nutritionalInfoButtonLayout
     * @param nutritionalInfoButtonImage
     * @param container
     */
    synchronized public void animotionRightSmall (Context mContext , final RelativeLayout nutritionalInfoButtonLayout, final ImageView nutritionalInfoButtonImage, final AbsoluteLayout container) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", nutritionalInfoButtonLayout.getScaleX(),0.5f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", nutritionalInfoButtonLayout.getScaleY(),0.5f);

        float a = mContext.getResources().getDimension(R.dimen.dimen_25dp) -  yMap.get(nutritionalInfoButtonLayout.getId()) ;
        float b = mContext.getResources().getDimension(R.dimen.dimen_25dp) -  xMap.get(nutritionalInfoButtonLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",nutritionalInfoButtonLayout.getX() - xMap.get(nutritionalInfoButtonLayout.getId()), b );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", nutritionalInfoButtonLayout.getY() - yMap.get(nutritionalInfoButtonLayout.getId()) , a);

        ValueAnimator animator = ValueAnimator.ofInt(0,1);
//                ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;
                nutritionalInfoButtonLayout.setAlpha(1-fraction);
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(nutritionalInfoButtonLayout);
                Log.d("TEST---", "DISABLING");
                nutritionalInfoButtonImage.setEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("juice", "x1:" + nutritionalInfoButtonLayout.getX() + ",   " + nutritionalInfoButtonLayout.getY());
                nutritionalInfoButtonLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    /**
     *
     * @param mContext
     * @param viewGroupLayout
     * @param container
     */
    synchronized public void animotionRightCancel (Context mContext , final ViewGroupLayout viewGroupLayout , final AbsoluteLayout container , final ImageView backGroud , final int position) {

        float viewGroupLayoutFinalScaleX = viewGroupLayout.getScalingFactor();
        float viewGroupLayoutFinalScaleY = viewGroupLayout.getScalingFactor();
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayoutFinalScaleX, 1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayoutFinalScaleY,1.0f);

        float viewGroupLayoutFinalScaleTranslationX = viewGroupLayout.getmFinalScaleTranslationX();
        float viewGroupLayoutFinalScaleTranslationY = viewGroupLayout.getmFinalScaleTranslationY();
        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",viewGroupLayoutFinalScaleTranslationX, 0f);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", viewGroupLayoutFinalScaleTranslationY , 0f);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(drinkAnimotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

//                viewGroupLayout.setChildViewAlp(1 - fraction , false , position);
                backGroud.setAlpha(1 - fraction);

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                viewGroupLayout.setTranslationX(animatorValueTranslationX);
                viewGroupLayout.setTranslationY(animatorValueTranslationY);

                Log.d("juice", "transtlateX:" + animatorValueTranslationX) ;
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(viewGroupLayout);

                //From resizing to scaling values for a clearer display (see function scalingAndResizeToggle for more info)
                viewGroupLayout.scalingAndResizeToggle(true, position);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("juice","x6:" + viewGroupLayout.getX() + ",   " + viewGroupLayout.getY());
                //setCurrentAnimotinType(MAIN_SCREEN);
                //Note: Verify if call clear or remove
                backGroud.setVisibility(View.GONE);
                clearCurrentAnimotionType();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    /**
     *
     * @param mContext
     * @param nutritionalInfoButtonLayout
     * @param nutritionalInfoButtonImage
     * @param container
     */
    synchronized public void animotionRightSmallCancel (Context mContext , final RelativeLayout nutritionalInfoButtonLayout, final ImageView nutritionalInfoButtonImage, final AbsoluteLayout container , final int position) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", 0.5f, 1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", 0.5f, 1.0f);

        float startX , startY ;

        startX = mContext.getResources().getDimension(R.dimen.dimen_450dp) - xMap.get(nutritionalInfoButtonLayout.getId());
        startY = mContext.getResources().getDimension(R.dimen.dimen_25dp) - yMap.get(nutritionalInfoButtonLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, 0 );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , 0 );

        ValueAnimator animator = ValueAnimator.ofInt(0,1);
//                ValueAnimator.ofPropertyValuesHolder(
//                mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,
//                mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(drinkAnimotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;
                nutritionalInfoButtonLayout.setAlpha(fraction);
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(nutritionalInfoButtonLayout);
                nutritionalInfoButtonLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                nutritionalInfoButtonImage.setEnabled(true);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    synchronized public void animotionDrink (final Context mContext , final ViewGroupLayout viewGroupLayout , final MyDrinkView drinkView , final RelativeLayout image , final int position , final boolean isReapt , final View line) {
        //NOTE: temporaly code
        if(position == WATER_POSITION){
            return;
        }

 //       PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.2f ,1.4f,1.2f , 1.4f , 1.2f);
 //       PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.2f ,1.4f,1.2f , 1.4f , 1.2f);

        //PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.0f ,1.1f , 1.0f);
        //PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.0f ,1.1f , 1.0f);

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.0f);

        PropertyValuesHolder mPropertyValuesDrinkHolderScaleX = PropertyValuesHolder.ofFloat("scaleXDrink", viewGroupLayout.getScaleX(),0.95f);
        PropertyValuesHolder mPropertyValuesDrinkHolderScaleY = PropertyValuesHolder.ofFloat("scaleYDrink", viewGroupLayout.getScaleY(),0.95f);


        float startX ,startY;


        Log.d("juice" , "x9:" + viewGroupLayout.getX() + ",y9:" + viewGroupLayout.getY() + ",  " + xMap.get(viewGroupLayout.getId())) ;
        startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, 0);//a
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , 0);//b
        ValueAnimator animator ;
        if (!isReapt) {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY, mPropertyValuesDrinkHolderScaleX, mPropertyValuesDrinkHolderScaleY);
        } else {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY, mPropertyValuesDrinkHolderScaleX, mPropertyValuesDrinkHolderScaleY);
        }

        animator.setDuration(drinkAnimotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");

                float animatorValueScaleXDrink =  (float) valueAnimator.getAnimatedValue("scaleXDrink");
                float animatorValueScaleYDrink = (float) valueAnimator.getAnimatedValue("scaleYDrink");
                if (!isReapt) {
                    float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                    float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                    viewGroupLayout.setTranslationX(animatorValueTranslationX);
                    viewGroupLayout.setTranslationY(animatorValueTranslationY);

                    drinkView.setTranslationX(animatorValueTranslationX);
                    drinkView.setTranslationY(animatorValueTranslationY);

                    line.setAlpha(fraction);
                    Log.d("juice", "scaleX:" + animatorValueScaleX + ", x :" + animatorValueTranslationX) ;
                }

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                drinkView.setScaleX(animatorValueScaleXDrink);
                drinkView.setScaleY(animatorValueScaleYDrink);

            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                drinkView.setAlpha(1.0f);
                if (!(position == 0 && containsAnimotionType(ANIMOTION_DRINK_ONE)
                        || position == 1 && containsAnimotionType(ANIMOTION_DRINK_TWO)
                        || position == 2 && containsAnimotionType(ANIMOTION_DRINK_THREE)
                        || position == 3 && containsAnimotionType(ANIMOTION_DRINK_FOUR))) {
                    viewGroupLayout.startDrinkTransilation(true , position);
                }
                drinkView.startAnim();

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }


    synchronized public void animotionOnlyOneDrink (final Context mContext , final ViewGroupLayout viewGroupLayout , final MyDrinkView drinkView , final RelativeLayout image , final int position , final boolean isReapt , final View line) {
        //NOTE: temporaly code
        if(position == WATER_POSITION){
            return;
        }

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.15f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.15f);

        PropertyValuesHolder mPropertyValuesDrinkHolderScaleX = PropertyValuesHolder.ofFloat("scaleXDrink", viewGroupLayout.getScaleX(),1.08f);
        PropertyValuesHolder mPropertyValuesDrinkHolderScaleY = PropertyValuesHolder.ofFloat("scaleYDrink", viewGroupLayout.getScaleY(),1.08f);

        float  startX ,startY;

        Log.d("juice" , "x9:" + viewGroupLayout.getX() + ",y9:" + viewGroupLayout.getY() + ",  " + xMap.get(viewGroupLayout.getId())) ;
        startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, 0);//a
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , 0);//b
        ValueAnimator animator ;

        Log.d("log","]]]]]]]]] isReapt = "+isReapt);

        if (!isReapt) {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY,mPropertyValuesDrinkHolderScaleX,mPropertyValuesDrinkHolderScaleY);
        } else {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY,mPropertyValuesDrinkHolderScaleX,mPropertyValuesDrinkHolderScaleY);
        }

        animator.setDuration(drinkAnimotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");

                float animatorValueScaleXDrink =  (float) valueAnimator.getAnimatedValue("scaleXDrink");
                float animatorValueScaleYDrink = (float) valueAnimator.getAnimatedValue("scaleYDrink");
                if (!isReapt) {
                    float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                    float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                    viewGroupLayout.setTranslationX(animatorValueTranslationX);
                    viewGroupLayout.setTranslationY(animatorValueTranslationY);

                    drinkView.setTranslationX(animatorValueTranslationX);
                    drinkView.setTranslationY(animatorValueTranslationY);

                    line.setAlpha(fraction);
                    Log.d("juice" , "scaleX:" + animatorValueScaleX + ", x :" + animatorValueTranslationX) ;
                }

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                drinkView.setScaleX(animatorValueScaleXDrink);
                drinkView.setScaleY(animatorValueScaleYDrink);

            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                drinkView.setAlpha(1.0f);
                if (!(position == 0 && containsAnimotionType(ANIMOTION_DRINK_ONE)
                        || position == 1 && containsAnimotionType(ANIMOTION_DRINK_TWO)
                        || position == 2 && containsAnimotionType(ANIMOTION_DRINK_THREE)
                        || position == 3 && containsAnimotionType(ANIMOTION_DRINK_FOUR))) {
                    viewGroupLayout.startDrinkTransilation(true , position);
                }
                drinkView.startAnim();

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    synchronized public void animotionDrinkOther (final Context mContext , final ViewGroupLayout viewGroupLayout , final MyDrinkView drinkView , final  RelativeLayout image , final int position , final View line) {

        //NOTE: temporaly code
        if(viewGroupLayout == null){
            return;
        }

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),0.8f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),0.8f);

        float a , b ;
        if (position == 0) {
            if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }

        } else if (position == 1) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            }
        } else if (position == 2) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }
        } else {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }
        }

        float startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        float startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, a);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , b);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                viewGroupLayout.setTranslationX(animatorValueTranslationX);
                viewGroupLayout.setTranslationY(animatorValueTranslationY);

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                drinkView.setTranslationX(animatorValueTranslationX);
                drinkView.setTranslationY(animatorValueTranslationY);

                drinkView.setScaleX(animatorValueScaleX);
                drinkView.setScaleY(animatorValueScaleY);



            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                Log.d("juice", "onAnimationStart ------>");
                if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false, 0);
                } else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false, 1);
                } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false, 2);
                } else {
                    viewGroupLayout.startDrinkTransilation(false, 3);
                }

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    synchronized public void resetView(final View view) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", view.getScaleX(),1f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", view.getScaleY(),1f);

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",view.getX() - xMap.get(view.getId()), 0);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", view.getY() - yMap.get(view.getId()) , 0);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);

        animator.setDuration(drinkAnimotionTime / 2) ;

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                view.setTranslationX(animatorValueTranslationX);
                view.setTranslationY(animatorValueTranslationY);

                view.setScaleX(animatorValueScaleX);
                view.setScaleY(animatorValueScaleY);
            }
        });

        animator.start();
    }

    synchronized public void animotionDrinkSmall (Context context , final RelativeLayout imageView , int position) {

        //NOTE: temporaly code
        if(position == WATER_POSITION){
            return;
        }

        //PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", imageView.getScaleX(),1.0f ,1.2f,1.1f, 1.0f);
        //PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", imageView.getScaleY(),1.0f ,1.2f,1.1f, 1.0f);

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", imageView.getScaleX(),1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", imageView.getScaleY(),1.0f);

        float a , b , startX ,startY;
        startX = 0 ;startY = 0 ;
        if (position == 0) {
            a = context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_110dp) -  xMap.get(imageView.getId()) ;
        } else if (position == 1) {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_250dp) -  xMap.get(imageView.getId())  ;
        } else if (position == 2) {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_400dp) -  xMap.get(imageView.getId()) ;
        } else {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId());
            b = 0;//context.getResources().getDimension(R.dimen.dimen_550dp) -  xMap.get(imageView.getId()) ;
        }

        startX = imageView.getX() - xMap.get(imageView.getId()) ;
        startY = imageView.getY() - yMap.get(imageView.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, b);//b
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , a);//a

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);

        animator.setDuration(animotionTime / 2) ;

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                imageView.setTranslationX(animatorValueTranslationX);
                imageView.setTranslationY(animatorValueTranslationY);

                imageView.setScaleX(animatorValueScaleX);
                imageView.setScaleY(animatorValueScaleY);
            }
        });

        animator.start();
    }

    synchronized public void animotionDrinkSmallCancel(Context mContext , final ViewGroupLayout viewGroupLayout , final RelativeLayout image , int position) {

        //NOTE: temporaly code
        if(image == null){
            return;
        }

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", image.getScaleX(),0.8f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", image.getScaleY(),0.8f);

        float a , b ;

        if (position == 0) {
            if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }

        } else if (position == 1) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            }
        } else if (position == 2) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }
        } else {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }
        }

        float startX = image.getX() - xMap.get(image.getId()) ;
        float startY = image.getY() - yMap.get(image.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, a);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , b);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                image.setTranslationX(animatorValueTranslationX);
                image.setTranslationY(animatorValueTranslationY);

                image.setScaleX(animatorValueScaleX);
                image.setScaleY(animatorValueScaleY);
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    synchronized public void animotionOnlyOneDrinkSmall (Context context , final RelativeLayout imageView , int position) {

        //NOTE: temporaly code
        if(position == WATER_POSITION){
            return;
        }

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", imageView.getScaleX(),1.05f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", imageView.getScaleY(),1.05f);

        float a , b , startX ,startY;
        startX = 0 ;startY = 0 ;
        if (position == 0) {
            a = context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_110dp) -  xMap.get(imageView.getId()) ;
        } else if (position == 1) {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_250dp) -  xMap.get(imageView.getId())  ;
        } else if (position == 2) {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = 0;//context.getResources().getDimension(R.dimen.dimen_400dp) -  xMap.get(imageView.getId()) ;
        } else {
            a = 0;//context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId());
            b = 0;//context.getResources().getDimension(R.dimen.dimen_550dp) -  xMap.get(imageView.getId()) ;
        }

        startX = imageView.getX() - xMap.get(imageView.getId()) ;
        startY = imageView.getY() - yMap.get(imageView.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, b);//b
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , a);//a

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);

        animator.setDuration(drinkAnimotionTime / 2) ;

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction();

                float animatorValueScaleX = (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX = (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                imageView.setTranslationX(animatorValueTranslationX);
                imageView.setTranslationY(animatorValueTranslationY);

                imageView.setScaleX(animatorValueScaleX);
                imageView.setScaleY(animatorValueScaleY);
            }
        });

        animator.start();
    }


    public class ResizeAnimation extends Animation {
        private View mView;
        private float mToHeight;
        private float mFromHeight;


        private float mToWidth;
        private float mFromWidth;

        private int i;


        public ResizeAnimation(View v, float fromWidth, float fromHeight, float toWidth, float toHeight) {
            mToHeight = toHeight;
            mToWidth = toWidth;
            mFromHeight = fromHeight;
            mFromWidth = fromWidth;
            mView = v;
            setDuration(animotionTime);
            i = 0;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height = (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
            float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
            ViewGroup.LayoutParams p = mView.getLayoutParams();
            p.height = (int) height;
            p.width = (int) width;

            mView.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }

}
