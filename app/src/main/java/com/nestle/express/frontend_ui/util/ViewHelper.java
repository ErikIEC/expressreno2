package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.transition.TransitionPlayer1;
import com.nestle.express.frontend_ui.transition.TransitionPlayer2;
import com.nestle.express.frontend_ui.transition.TransitionPlayer3;
import com.nestle.express.frontend_ui.transition.TransitionPlayer4;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.Scene;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.TransitionManager;
import com.nestle.express.frontend_ui.widget.MyDrinkView;
import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

/**
 * Created by xumin on 16/6/16.
 */
public class ViewHelper implements View.OnClickListener , View.OnTouchListener{
    private Context mContext;
    private ViewGroup mSceneRoot;

    //第一个界面当前的页码
    private int currentLayoutIndex = 0;
    private int toIndex = 0 ;
    //第一期动画切换的布局数组
    int[] layouts = new int[]{R.layout.mydemo_intro_scene1, R.layout.mydemo_intro_scene2,
            R.layout.mydemo_intro_scene3 , R.layout.mydemo_intro_scene4};

//    int[] layouts = new int[]{ R.layout.mydemo_intro_scene1 ,R.layout.mydemo_intro_scene2 };

//    int[] layouts = new int[]{R.layout.test1, R.layout.test2};
    //动画控制器
    TransitionPlayer1 transitionPlayer = new TransitionPlayer1();

    TransitionPlayer2 transitionPlayer1 = new TransitionPlayer2();

    private Scene scene;

    int[] titleImages = new int[] {R.drawable.title_one , R.drawable.title_two } ;

    int[] contentImages = new int[] {R.drawable.wenan , R.drawable.wenan02 ,R.drawable.wenan03 } ;

    private ImageHelper mImageHelper ;

    private static final long DRINK_RETURN_TIME = 2000 ; // Animate water recovery time

    private OnPourListener mOnPourListener ;

    public ViewHelper(ViewGroup mSceneRoot , Context context , OnPourListener onPourListener) {
        this.mSceneRoot = mSceneRoot;
        this.mContext = context ;
        this.mImageHelper = new ImageHelper(context) ;
        this.mOnPourListener = onPourListener ;
        currentLayoutIndex = AnimotionUtils.MAIN_SCREEN ;
    }

    public void initView () {
        View view = View.inflate(mContext, layouts[currentLayoutIndex], mSceneRoot);
        startMoveAnimotion(0) ;
    }


    private boolean isConsever = false ;
    public void preTransition(){
        mSceneRoot.removeAllViews();

        if (isConsever) {
            toIndex = currentLayoutIndex - 1 ;

            if (toIndex < 0) {
                toIndex = currentLayoutIndex + 1 ;
                isConsever = false ;
            }
        } else {
            toIndex = currentLayoutIndex + 1 ;

            if (toIndex > layouts.length - 1) {
                toIndex = currentLayoutIndex - 1 ;
                isConsever = true ;
            }
        }


        Log.d("test" , "toIndex : " + toIndex) ;
        //lock current layout
        View view = View.inflate(mContext, layouts[currentLayoutIndex], mSceneRoot);
        //pre next layout
        scene = Scene.getSceneForLayout(mSceneRoot, layouts[toIndex], mContext);

        scene.setEnterAction(new Runnable() {
            @Override
            public void run() {
                Log.d("test" , "enterAction:" + currentLayoutIndex) ;
                startMoveAnimotion(toIndex) ;
                mImageHelper.clearMemory();
                System.gc();
            }
        });

        mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
//                transitionPlayer.addTarget(R.id.main_one_viewgroup) ;
//                transitionPlayer.addTarget(R.id.main_two_viewgroup) ;
//                transitionPlayer.addTarget(R.id.main_three_viewgroup) ;
//                transitionPlayer.addTarget(R.id.main_four_viewgroup) ;
                TransitionManager.go(scene, transitionPlayer);
                Log.d("test" , "enterAction11111") ;
                return false;
            }
        });
        currentLayoutIndex = toIndex;
    }

    public boolean isLast() {
        if (toIndex == 2) {
            return true ;
        }
        return false ;
    }

    public void initPreViewActivity() {
        final Scene scene = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_test, mContext);

        scene.setEnterAction(new Runnable() {
            @Override
            public void run() {
//                mImageHelper.clearMemory();
//                System.gc();
//                changeMainContent() ;
            }
        });

        mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                TransitionManager.go(scene, transitionPlayer);
                return false;
            }
        });
    }


    public void initPreAnimation(){
        mSceneRoot.removeAllViews();

        //lock current layout
        View view = View.inflate(mContext, R.layout.mydemo_intro_scene5, mSceneRoot);

        currentLayoutIndex = 0;
        //pre next layout
        final Scene scene = Scene.getSceneForLayout(mSceneRoot, layouts[currentLayoutIndex], mContext);

        mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                TransitionManager.go(scene, transitionPlayer);
                return false;
            }
        });
    }

    public void initViewHelp() {
        View view = View.inflate(mContext, R.layout.mydemo_intro_scene5, mSceneRoot);
        changeMainContent() ;
    }


    public void preAnimotion2Transition(){
        mSceneRoot.removeAllViews();
////
////        //lock current layout
        View view = View.inflate(mContext, R.layout.mydemo_test, mSceneRoot);
        //pre next layout
        final Scene scene = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene5, mContext);

        scene.setEnterAction(new Runnable() {
            @Override
            public void run() {
//                mImageHelper.clearMemory();
//                System.gc();
//                changeMainContent() ;
            }
        });

        scene.setExitAction(new Runnable() {
            @Override
            public void run() {

            }
        });

        mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                TransitionManager.go(scene, transitionPlayer1);
                return false;
            }
        });
        currentLayoutIndex = 0;
        toIndex = 0 ;
    }


    public void preAnimotionMainTransition(int index){
        if (index == 0){
            mSceneRoot.removeAllViews();
            //lock current layout
            View view = View.inflate(mContext, layouts[currentLayoutIndex], mSceneRoot);
            //pre next layout
            final Scene scene = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene51, mContext);

            scene.setEnterAction(new Runnable() {
                @Override
                public void run() {
//                    mImageHelper.clearMemory();
//                    System.gc();
//                changeMainContent() ;
                }
            });

            mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    TransitionManager.go(scene, transitionPlayer1);
                    return false;
                }
            });
        } else {
            mSceneRoot.removeAllViews();
            //lock current layout
            View view = View.inflate(mContext,  R.layout.mydemo_intro_scene51, mSceneRoot);
            //pre next layout
            final Scene scene = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene52, mContext);

            scene.setEnterAction(new Runnable() {
                @Override
                public void run() {
//                    mImageHelper.clearMemory();
//                    System.gc();
//                changeMainContent() ;
                }
            });

            mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    TransitionManager.go(scene, transitionPlayer1);
                    return false;
                }
            });
        }

        currentLayoutIndex = 0;
    }

    private long onclickTime = 0 ;

    private long nowTime = 0 ;

    public void setNowTime(long time) {
        nowTime = time ;
    }

    public long getNowTime() {
        return nowTime ;
    }

    @Override
    public void onClick(View view) {

        setNowTime(System.currentTimeMillis());
        handler.removeMessages(1);

        int id = view.getId() ;
        if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_ONE && view.getId() != R.id.main_small_one
                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_TWO && view.getId() != R.id.main_small_two
                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_THREE && view.getId() != R.id.main_small_three
                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR && view.getId() != R.id.main_small_four) {
            return ;
        }

        //禁止连续点击
        if (System.currentTimeMillis() - onclickTime < 1000) {
            return ;
        } else {
            onclickTime = System.currentTimeMillis() ;
        }

        if (id == R.id.main_one_viewgroup) {


        } else if (id == R.id.main_two_viewgroup) {

        }else if (id == R.id.main_three_viewgroup) {

        }else if (id == R.id.main_four_viewgroup) {

        }else if (id == R.id.main_small_one) {
            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_ONE) {
                Log.d("juice" , "id" + main_one_viewgroup.getId() +"fX:" + main_one_viewgroup.getX() +", " + main_one_viewgroup.getY()) ;
                AnimotionUtils.getInstance().animotionRightCancel(mContext , main_one_viewgroup , container , white_backgroud , 0);
                AnimotionUtils.getInstance().animotionRightSmallCancel(mContext , main_small_one , container , 0);
            } else {
                Log.d("juice" , "id" + main_one_viewgroup.getId() +"fX:" + main_one_viewgroup.getX() +", " + main_one_viewgroup.getY()) ;
                if (AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_TWO
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_THREE) {
                    AnimotionUtils.getInstance().resetView(main_two_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_three_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_four_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_small_two);
                    AnimotionUtils.getInstance().resetView(main_small_three);
                    AnimotionUtils.getInstance().resetView(main_small_four);

                    AnimotionUtils.getInstance().resetView(drink_2);
                    AnimotionUtils.getInstance().resetView(drink_3);
                    AnimotionUtils.getInstance().resetView(drink_4);

                    line1.setAlpha(1.0f);
                    line2.setAlpha(1.0f);
                    line3.setAlpha(1.0f);
                    line4.setAlpha(1.0f);

                    main_two_viewgroup.startDrinkTransilation(false , 1);
                    main_three_viewgroup.startDrinkTransilation(false , 2);
                    main_four_viewgroup.startDrinkTransilation(false , 3);
                }

                AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_SHOCONTENT_ONE);

                AnimotionUtils.getInstance().animotionRight(mContext , main_one_viewgroup , container , white_backgroud , 0);
                AnimotionUtils.getInstance().animotionRightSmall(mContext , main_small_one , container);
            }
        }else if (id == R.id.main_small_two) {
            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_TWO) {
                AnimotionUtils.getInstance().animotionRightCancel(mContext , main_two_viewgroup , container , white_backgroud , 1);
                AnimotionUtils.getInstance().animotionRightSmallCancel(mContext , main_small_two , container , 1);
            } else {
                if (AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_ONE
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_THREE) {
                    AnimotionUtils.getInstance().resetView(main_one_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_three_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_four_viewgroup);

                    AnimotionUtils.getInstance().resetView(main_small_one);
                    AnimotionUtils.getInstance().resetView(main_small_three);
                    AnimotionUtils.getInstance().resetView(main_small_four);

                    AnimotionUtils.getInstance().resetView(drink_1);
                    AnimotionUtils.getInstance().resetView(drink_3);
                    AnimotionUtils.getInstance().resetView(drink_4);

                    line1.setAlpha(1.0f);
                    line2.setAlpha(1.0f);
                    line3.setAlpha(1.0f);
                    line4.setAlpha(1.0f);

                    main_one_viewgroup.startDrinkTransilation(false , 0);
                    main_three_viewgroup.startDrinkTransilation(false , 2);
                    main_four_viewgroup.startDrinkTransilation(false , 3);
                }

                AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_SHOCONTENT_TWO);
                AnimotionUtils.getInstance().animotionRight(mContext , main_two_viewgroup , container , white_backgroud , 1);
                AnimotionUtils.getInstance().animotionRightSmall(mContext , main_small_two , container);
            }
        }else if (id == R.id.main_small_three) {
            //第三个小圆
            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_THREE) {
                AnimotionUtils.getInstance().animotionRightCancel(mContext , main_three_viewgroup , container , white_backgroud , 2);
                AnimotionUtils.getInstance().animotionRightSmallCancel(mContext , main_small_three , container , 2);
            } else {
                if (AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_ONE
                        ||AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_TWO) {
                    AnimotionUtils.getInstance().resetView(main_one_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_two_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_four_viewgroup);

                    AnimotionUtils.getInstance().resetView(main_small_one);
                    AnimotionUtils.getInstance().resetView(main_small_two);
                    AnimotionUtils.getInstance().resetView(main_small_four);

                    AnimotionUtils.getInstance().resetView(drink_1);
                    AnimotionUtils.getInstance().resetView(drink_2);
                    AnimotionUtils.getInstance().resetView(drink_4);

                    line1.setAlpha(1.0f);
                    line2.setAlpha(1.0f);
                    line3.setAlpha(1.0f);
                    line4.setAlpha(1.0f);

                    main_one_viewgroup.startDrinkTransilation(false , 0);
                    main_two_viewgroup.startDrinkTransilation(false , 1);
                    main_four_viewgroup.startDrinkTransilation(false , 3);
                }
                AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_SHOCONTENT_THREE);
                AnimotionUtils.getInstance().animotionRight(mContext , main_three_viewgroup , container , white_backgroud , 2);
                AnimotionUtils.getInstance().animotionRightSmall(mContext , main_small_three , container);
            }

        }else if (id == R.id.main_small_four) {
            //第四个小圆
            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR) {
                AnimotionUtils.getInstance().animotionRightCancel(mContext, main_four_viewgroup, container, white_backgroud, 3);
                AnimotionUtils.getInstance().animotionRightSmallCancel(mContext, main_small_four, container, 3);
            } else {
                if (AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_THREE
                        || AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_ONE
                        || AnimotionUtils.getInstance().getCurrentAnimotinType() != AnimotionUtils.ANIMOTION_SHOCONTENT_TWO) {
                    AnimotionUtils.getInstance().resetView(main_one_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_two_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_three_viewgroup);

                    AnimotionUtils.getInstance().resetView(main_small_one);
                    AnimotionUtils.getInstance().resetView(main_small_two);
                    AnimotionUtils.getInstance().resetView(main_small_three);

                    AnimotionUtils.getInstance().resetView(drink_1);
                    AnimotionUtils.getInstance().resetView(drink_2);
                    AnimotionUtils.getInstance().resetView(drink_3);

                    line1.setAlpha(1.0f);
                    line2.setAlpha(1.0f);
                    line3.setAlpha(1.0f);
                    line4.setAlpha(1.0f);

                    main_one_viewgroup.startDrinkTransilation(false, 0);
                    main_two_viewgroup.startDrinkTransilation(false, 1);
                    main_three_viewgroup.startDrinkTransilation(false, 2);
                }
                AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR);
                AnimotionUtils.getInstance().animotionRight(mContext, main_four_viewgroup, container, white_backgroud, 3);
                AnimotionUtils.getInstance().animotionRightSmall(mContext, main_small_four, container);
            }

        }
//        } else if (id == R.id.green) {
//            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_ONE) {
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_one_viewgroup , 0);
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_one_viewgroup ,drink_1,main_small_one , 0 , true , line1) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_one , 0);
////                AnimotionUtils.getInstance().animotionDrink(mContext , main_one_viewgroup , 0) ;
//            } else {
//
//                //打水下面第一个
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_one_viewgroup ,drink_1,main_small_one, 0 , false , line1) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_one , 0);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup ,drink_2 ,main_small_two , 0 , line2) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 0);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup ,drink_3 ,main_small_three , 0 , line3) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 0);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four ,  0 , line4) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 0);
//            }
//        } else if (id == R.id.red) {
//
//            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_TWO) {
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_two_viewgroup , drink_2 ,main_small_two , 1 , true , line2) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_two , 1);
//            } else {
//                //打水下面第二个
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_two_viewgroup ,drink_2 , main_small_two ,  1 , false , line2) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_two , 1);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 1 , line1) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 1);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup ,drink_3 ,main_small_three , 1 , line3) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 1);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 1 , line4) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 1);
//            }
//        } else if (id == R.id.deep_purple) {
//            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_THREE) {
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_three_viewgroup , drink_3 , main_small_three , 2 , true , line3) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_three , 2);
//            } else {
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_one_viewgroup , 2);
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_two_viewgroup , 2) ;
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_three_viewgroup , 2) ;
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_four_viewgroup , 2) ;
//
//                //打水下面第二个
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_three_viewgroup ,drink_3 , main_small_three , 2 , false , line3) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_three , 2);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 2 , line1) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 2);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup , drink_2 ,main_small_two ,2 , line2) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 2);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 2 , line4) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 2);
//            }
//
//        } else if (id == R.id.orange) {
//            if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_FOUR) {
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 3 ,true , line4) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_four , 3);
//            } else {
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_one_viewgroup , 3);
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_two_viewgroup , 3) ;
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_three_viewgroup , 3) ;
////                AnimotionUtils.getInstance().animtionCancel(mContext , main_four_viewgroup , 3) ;
//
//                //打水下面第二个
//                AnimotionUtils.getInstance().animotionDrink(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 3 , false , line4) ;
//                AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_four , 3);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 3 , line1) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 3);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup , drink_2 ,main_small_two ,3 , line2) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 3);
//                AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup , drink_3 , main_small_three ,3 , line3) ;
//                AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 3);
//            }
//        }
    }


    public void startMoveAnimotion (int index) {
        int count = mSceneRoot.getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {
            View childView = mSceneRoot.getChildAt(i) ;
            if (childView instanceof AbsoluteLayout) {
                int count1 = ((AbsoluteLayout) childView).getChildCount() ;
                for (int j = 0 ; j < count1 ; j ++) {
                    View childchildView = ((AbsoluteLayout) childView).getChildAt(j) ;
                    if (childchildView instanceof MyView) {
                        float x = (float) Math.random() ;
                        moveCirclePath(childchildView ,55 , 3 , x>0.5?1:0);
                    }

                    if (index == 1) {
                        if (childchildView.getId() == R.id.main_two_viewgroup) {
                            moveCirclePath(childchildView ,55 , 3 , 1);
                        }
                    } else if (index == 2 && childchildView.getId() == R.id.main_four_viewgroup) {
                        moveCirclePath(childchildView ,55 , 3 , 1);
//                            ((ViewGroupLayout) childchildView).startAnim(10000 , 55 , 3 , 0);
                    } else if (index == 3 && childchildView.getId() == R.id.main_three_viewgroup) {
//                            ((ViewGroupLayout) childchildView).startAnim(10000 , 55 , 3 , 0);
                        moveCirclePath(childchildView ,55 , 3 , 1);
                    } else {
//                            ((ViewGroupLayout) childchildView).startAnim(40 , 50 , 1000 , 3);
                        moveCirclePath(childchildView ,55 , 3 , 0);
                    }
                    }
                }
            }
    }

    //Try Radius=50 and revolutions=3
    protected void moveCirclePath(final View view , final float maxRadius, final float revolutions , final int dirction)
    {

//        if (view instanceof ViewGroupLayout) {
//            ((ViewGroupLayout)view).initAnimotion();
//        } else if (view instanceof MyView) {
//            ((MyView)view).initAnimotion();
//        }

        ValueAnimator animation = ValueAnimator.ofFloat( 0,1);
        animation.setDuration(15000);

        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation)
            {
                float animateValue = (float) animation.getAnimatedValue();

                if (animateValue > 0.6f ) {
                    animation.cancel();
                }

//               if ((float)animation.getAnimatedValue() > 0.2 && (float)animation.getAnimatedValue()<0.6) {
//                   animateValue = 1f - animateValue ;
//               } else if ((float)animation.getAnimatedValue() < 0.2) {
//                   animation.cancel();
//               }

	    	/*
	    		Progess is a value between 1.0 and 0.0
	    		it starts a 1.0 and decreases towards
	    		0 as the animation progresses.
	    		Because it is squared (^2) it starts fast
	    		and decelerates until it reaches the end
	    	*/
                //0-0.5-0
                float progress = (float) Math.pow(1 - animateValue, 2);
                float radius = progress * maxRadius;   //0 --R --0
                float angle = (float) (progress * revolutions * 2.0 * Math.PI);

                int x = (int) (radius * Math.sin(angle));
                int y = (int) (radius * Math.cos(angle));

                if (dirction == 0) {
                    x = -x ;
                    y = -y ;
                }

//                ((ViewGroupLayout)view).setMyTransitionX(x);
//                ((ViewGroupLayout)view).setMyTransitionY(y);

                if (view instanceof ViewGroupLayout) {
                    ((ViewGroupLayout)view).setMyTransitionX(x);
                ((ViewGroupLayout)view).setMyTransitionY(y);
                } else if (view instanceof MyView){
                    view.setTranslationX(x);
                    view.setTranslationY(y);
                }
            }
        });

        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(main_one_viewgroup , "translationY" , maxRadius , 0);
//                objectAnimator.setDuration(600) ;
//                objectAnimator.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void startAnimotion() {
//        if (animationDrawable1!=null) {
//            animationDrawable1.start();
//        }
    }

    public void stopAnimotion () {
        if (animationDrawable1!=null && animationDrawable1.isRunning()) {
            animationDrawable1.stop();
        }
    }

    public void changeMainContent () {
        container = (AbsoluteLayout)mSceneRoot.findViewById(R.id.container) ;
        main_one_viewgroup = (ViewGroupLayout)mSceneRoot.findViewById(R.id.main_one_viewgroup) ;
        drink_1 = (MyDrinkView)mSceneRoot.findViewById(R.id.drink_1) ;
        drink_1.setColor(mContext.getResources().getColor(R.color.color_deep_blue1));
        drink_2 = (MyDrinkView)mSceneRoot.findViewById(R.id.drink_2) ;
        drink_2.setColor(mContext.getResources().getColor(R.color.color_e7b10c));
        drink_3 = (MyDrinkView)mSceneRoot.findViewById(R.id.drink_3) ;
        drink_3.setColor(mContext.getResources().getColor(R.color.color_deep_green));
        drink_4 = (MyDrinkView)mSceneRoot.findViewById(R.id.drink_4) ;
        drink_4.setColor(mContext.getResources().getColor(R.color.color_pink_yellow));
        main_two_viewgroup = (ViewGroupLayout)mSceneRoot.findViewById(R.id.main_two_viewgroup) ;
        white_backgroud = (ImageView)mSceneRoot.findViewById(R.id.white_backgroud) ;
        main_three_viewgroup = (ViewGroupLayout)mSceneRoot.findViewById(R.id.main_three_viewgroup) ;
        main_four_viewgroup = (ViewGroupLayout)mSceneRoot.findViewById(R.id.main_four_viewgroup) ;
        main_small_one = (ImageView)mSceneRoot.findViewById(R.id.main_small_one) ;
        main_small_two = (ImageView)mSceneRoot.findViewById(R.id.main_small_two) ;
        main_small_three = (ImageView)mSceneRoot.findViewById(R.id.main_small_three) ;
        main_small_four = (ImageView)mSceneRoot.findViewById(R.id.main_small_four) ;

        line1 = (View)mSceneRoot.findViewById(R.id.line1) ;
        line2 = (View)mSceneRoot.findViewById(R.id.line2) ;
        line3 = (View)mSceneRoot.findViewById(R.id.line3) ;
        line4 = (View)mSceneRoot.findViewById(R.id.line4) ;

//        main_one_viewgroup_image2 = (ImageView)mSceneRoot.findViewById(R.id.main_one_viewgroup_image2) ;
//        main_one_viewgroup_image2.setImageResource(R.drawable.animotion_orange);
//        animationDrawable1 = (AnimationDrawable) main_one_viewgroup_image2.getDrawable();
//        animationDrawable.start();



        mSceneRoot.findViewById(R.id.main_one_viewgroup).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_two_viewgroup).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_three_viewgroup).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_four_viewgroup).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_small_one).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_small_two).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_small_three).setOnClickListener(this);
        mSceneRoot.findViewById(R.id.main_small_four).setOnClickListener(this);

//        mSceneRoot.findViewById(R.id.green).setOnClickListener(this);
//        mSceneRoot.findViewById(R.id.red).setOnClickListener(this);
//        mSceneRoot.findViewById(R.id.deep_main_three_viewgroup).setOnClickListener(this);
//        mSceneRoot.findViewById(R.id.orange).setOnClickListener(this);

        mSceneRoot.findViewById(R.id.green).setOnTouchListener(this);
        mSceneRoot.findViewById(R.id.red).setOnTouchListener(this);
        mSceneRoot.findViewById(R.id.deep_purple).setOnTouchListener(this);
        mSceneRoot.findViewById(R.id.orange).setOnTouchListener(this);

        container.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                       if (container!=null) {
                           int count = container.getChildCount() ;
                           for (int i = 0 ; i < count ; i++) {
                               View view = container.getChildAt(i) ;
                               AnimotionUtils.getInstance().putViewPosition(view);
                           }

                           AnimotionUtils.getInstance().setPutValue(true);
                       }
                    }
                });
//
    }


    public void startDrink() {
        int count = mSceneRoot.getChildCount() ;
        for (int i = 0 ; i < count ; i ++) {
            View childView = mSceneRoot.getChildAt(i) ;
        }
    }

    private AbsoluteLayout container ;
    private ViewGroupLayout main_one_viewgroup ,main_two_viewgroup , main_three_viewgroup , main_four_viewgroup;
    private ImageView main_small_one , main_small_two , main_small_three , main_small_four ,white_backgroud ;
    private MyDrinkView drink_1 , drink_2 , drink_3 , drink_4;
    private AnimationDrawable animationDrawable1 ,animationDrawable2 , animationDrawable3 , animationDrawable4;
    private View line1, line2, line3, line4 ;

    private ImageView main_one_viewgroup_image , main_one_viewgroup_image2 ,deep_main_three_viewgroup3_image ,deep_main_three_viewgroup4_image ,main_two_viewgroup_image ,main_two_viewgroup_image2
            ,main_two_viewgroup_image3 , main_two_viewgroup_image4 ,main_three_viewgroup_image ,main_three_viewgroup_image2 ,main_three_viewgroup_image3
            ,main_three_viewgroup_image4,main_four_viewgroup_image , main_four_viewgroup_image2 , main_four_viewgroup_image3 , main_four_viewgroup_image4 ;

    private Boolean isTouch = false ;

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        setNowTime(System.currentTimeMillis());

        synchronized (isTouch) {
            if (event.getAction()== MotionEvent.ACTION_DOWN && isTouch) {
                return true ;
            }
            isTouch = true ;
        }

        int id = view.getId() ;

//        if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_ONE && view.getId() != R.id.main_small_one
//                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_TWO && view.getId() != R.id.main_small_two
//                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_THREE && view.getId() != R.id.main_small_three
//                || AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_SHOCONTENT_FOUR && view.getId() != R.id.main_small_four) {
//            return false;
//        }



        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                //        //禁止连续点击
//                if (System.currentTimeMillis() - onclickTime < 1000) {
//                     return true;
//                } else {
//                     onclickTime = System.currentTimeMillis() ;
//                }

                if (id == R.id.green) {

                    if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_ONE) {
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_one_viewgroup ,drink_1,main_small_one , 0 , true , line1) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_one , 0);
                    } else {

                        //打水下面第一个
//                        AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_DRINK_ONE);
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_one_viewgroup ,drink_1,main_small_one, 0 , false , line1) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_one , 0);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup ,drink_2 ,main_small_two , 0 , line2) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 0);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup ,drink_3 ,main_small_three , 0 , line3) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 0);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four ,  0 , line4) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 0);
                    }
                    this.mOnPourListener.onPress(0);
                } else if (id == R.id.red) {
                    if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_TWO) {
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_two_viewgroup , drink_2 ,main_small_two , 1 , true , line2) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_two , 1);
                    } else {

                        //打水下面第二个
//                        AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_DRINK_TWO);
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_two_viewgroup ,drink_2 , main_small_two ,  1 , false , line2) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_two , 1);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 1 , line1) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 1);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup ,drink_3 ,main_small_three , 1 , line3) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 1);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 1 , line4) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 1);
                    }
                    this.mOnPourListener.onPress(1);
                } else if (id == R.id.deep_purple) {
                    if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_THREE) {
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_three_viewgroup , drink_3 , main_small_three , 2 , true , line3) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_three , 2);
                    } else {

                        //打水下面第二个
//                        AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_DRINK_THREE);
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_three_viewgroup ,drink_3 , main_small_three , 2 , false , line3) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_three , 2);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 2 , line1) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 2);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup , drink_2 ,main_small_two ,2 , line2) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 2);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 2 , line4) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_four_viewgroup , main_small_four , 2);
                    }
                    this.mOnPourListener.onPress(2);
                } else if (id == R.id.orange) {
                    if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_FOUR) {
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 3 ,true , line4) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_four , 3);
                    } else {

                        //打水下面第二个
//                        AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.ANIMOTION_DRINK_FOUR);
                        AnimotionUtils.getInstance().animotionDrink(mContext , main_four_viewgroup ,drink_4 ,main_small_four , 3 , false , line4) ;
                        AnimotionUtils.getInstance().animotionDrinkSmall(mContext , main_small_four , 3);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_one_viewgroup ,drink_1 ,main_small_one , 3 , line1) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_one_viewgroup , main_small_one , 3);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_two_viewgroup , drink_2 ,main_small_two ,3 , line2) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_two_viewgroup , main_small_two , 3);
                        AnimotionUtils.getInstance().animotionDrinkOther(mContext , main_three_viewgroup , drink_3 , main_small_three ,3 , line3) ;
                        AnimotionUtils.getInstance().animotionDrinkSmallCancel(mContext , main_three_viewgroup , main_small_three , 3);
                    }

                    this.mOnPourListener.onPress(3);
                }
                break ;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                synchronized (isTouch) {
                    isTouch = false ;
                }
                setNowTime(System.currentTimeMillis());
                if (id == R.id.green) {
                    drink_1.stopAnim();
                    drink_1.setAlpha(0.0f);
                    this.mOnPourListener.onPressOut(0);
                } else if (id == R.id.red) {

                    drink_2.stopAnim();
                    drink_2.setAlpha(0.0f);
                    this.mOnPourListener.onPressOut(1);
                } else if (id == R.id.deep_purple) {
                    drink_3.stopAnim();
                    drink_3.setAlpha(0.0f);
                    this.mOnPourListener.onPressOut(2);
                } else if (id == R.id.orange) {
                    drink_4.stopAnim();
                    drink_4.setAlpha(0.0f);
                    this.mOnPourListener.onPressOut(3);
                }

                handler.removeMessages(1);
                handler.sendEmptyMessageDelayed(1 , DRINK_RETURN_TIME) ;
                break;
        }

        return true;
    }

    private TransitionPlayer3 transitionPlayer3 = new TransitionPlayer3() ;
    private TransitionPlayer4 transitionPlayer4 = new TransitionPlayer4() ;

    public void loadMain(int i) {
        if (i == 0) {
            mSceneRoot.removeAllViews();
//            //lock current layout
            View view = View.inflate(mContext, layouts[currentLayoutIndex], mSceneRoot);
            if (currentLayoutIndex == 0) {
                final Scene scene1 = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene50_1, mContext);
                mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                        TransitionManager.go(scene1 , transitionPlayer3);
                        return false;
                    }
                });
            } else {
                final Scene scene1 = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene50, mContext);
                mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                        TransitionManager.go(scene1 , transitionPlayer3);
                        return false;
                    }
                });
            }
        } else if (i == 1) {
            final Scene scene1 = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene51, mContext);

            mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    TransitionManager.go(scene1 , transitionPlayer3);
                    return false;
                }
            });
        }else if(i == 2) {
            final Scene scene1 = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene53, mContext);

            mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    TransitionManager.go(scene1 , transitionPlayer3);
                    return false;
                }
            });
        } else {
            final Scene scene1 = Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene5, mContext);
            mSceneRoot.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mSceneRoot.getViewTreeObserver().removeOnPreDrawListener(this);
                    TransitionManager.go(scene1 , transitionPlayer4);
                    return false;
                }
            });
        }


    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (System.currentTimeMillis() - getNowTime() > DRINK_RETURN_TIME) {
                    AnimotionUtils.getInstance().resetView(main_one_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_two_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_three_viewgroup);
                    AnimotionUtils.getInstance().resetView(main_four_viewgroup);

                    AnimotionUtils.getInstance().resetView(main_small_one);
                    AnimotionUtils.getInstance().resetView(main_small_two);
                    AnimotionUtils.getInstance().resetView(main_small_three);
                    AnimotionUtils.getInstance().resetView(main_small_four);

                    AnimotionUtils.getInstance().resetView(drink_1);
                    AnimotionUtils.getInstance().resetView(drink_2);
                    AnimotionUtils.getInstance().resetView(drink_3);
                    AnimotionUtils.getInstance().resetView(drink_4);

                    if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_ONE) {
                        main_one_viewgroup.startTransilation(false , 0);
                    } else if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_TWO) {
                        main_two_viewgroup.startTransilation(false , 1);
                    } else if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_THREE) {
                        main_three_viewgroup.startTransilation(false , 2);
                    } else if (AnimotionUtils.getInstance().getCurrentAnimotinType() == AnimotionUtils.ANIMOTION_DRINK_FOUR){
                        main_four_viewgroup.startTransilation(false , 3);
                    }

                    AnimotionUtils.getInstance().setCurrentAnimotinType(AnimotionUtils.MAIN_SCREEN);

                    line1.setAlpha(1);
                    line2.setAlpha(1);
                    line3.setAlpha(1);
                    line4.setAlpha(1);
                }
            }


        }
    } ;
}
