package com.nestle.express.frontend_ui.nurun.utils;

import android.view.View;

/**
 * Created by jesserci on 16-11-09.
 */
public class AccessibilityUtils {

    public static void setFocusOrder(View... views) {
        if (views != null) {
            for (int i = 0; i < views.length; i++) {
                //Set focusable
                views[i].setFocusable(true);
                views[i].setFocusableInTouchMode(false);

                //Focus Left
                if ((i - 1) < 0) {
                    views[i].setNextFocusLeftId(views[views.length - 1].getId());
                } else {
                    views[i].setNextFocusLeftId(views[i - 1].getId());
                }

                //Focus right
                if ((i + 1) < views.length) {
                    views[i].setNextFocusRightId(views[i + 1].getId());
                } else {
                    views[i].setNextFocusRightId(views[0].getId());
                }
            }
        }
    }

    public static void clearFocusOrder(View... views) {
        if (views != null) {
            for (int i = 0; i < views.length; i++) {
                //Set focusable
                views[i].setFocusable(false);
                views[i].clearFocus();
            }
        }
    }
}
