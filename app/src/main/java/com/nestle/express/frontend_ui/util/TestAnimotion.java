package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.Keyframe;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.util.Log;
import android.view.View;

import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.widget.ViewAnimotionBean;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by xumin on 16/9/6.
 */
public class TestAnimotion {
    public static ValueAnimator testValueAnimotor1 (final ViewAnimotionBean bean ) {
        //10800
        Keyframe kf0 = Keyframe.ofFloat(0, 0.0f);
        Keyframe kf1 = Keyframe.ofFloat(800/10800, 1.0f);
        Keyframe kf2 = Keyframe.ofFloat(1.f, 1.5f);
        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe("value" ,kf0, kf1, kf2);
        ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(pvhRotation) ;

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue("value");
                if (value < 1.0f) {
                    Log.e("SOLON" , "value < 1.0 ....") ;
                    bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*value);
                    bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*value);
                } else {
                    Log.e("SOLON" , "value > 1.0 ....") ;
                    float progress = (float) Math.pow((float) (1 - Math.abs(1-value)*2), 2);  //PI 3/ 4
                    float radius = progress * bean.getRadis();
                    float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                    int x, y;

                    if (bean.getDirection() == 0) {
                        x = (int) (radius * Math.sin(angle));
                        y = (int) (radius * Math.cos(angle));
                    } else {
                        x = (int) (radius * Math.cos(angle));
                        y = (int) (radius * Math.sin(angle));
                    }


                    if (bean.getView() instanceof MyView) {
                        ((MyView)bean.getView()).setMyX(x);
                        ((MyView)bean.getView()).setMyY(y);
                    } else if (bean.getView() instanceof ViewGroupLayout) {
                        ((ViewGroupLayout)bean.getView()).setMyX(x);
                        ((ViewGroupLayout)bean.getView()).setMyY(y);
                    }
                }
            }
        });
        valueAnimator.setDuration(10800) ;
//        valueAnimator.setInterpolator(new DecelerateInterpolator());
//        valueAnimator.start();
        return valueAnimator ;
    }

    public static void testValueAnimotor () {
        Keyframe kf0 = Keyframe.ofFloat(0, 0.0f);
        Keyframe kf1 = Keyframe.ofFloat(0.2f, 1.0f);
        Keyframe kf2 = Keyframe.ofFloat(1.f, 1.5f);
        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe("value" ,kf0, kf1, kf2);
        ValueAnimator valueAnimator = ValueAnimator.ofPropertyValuesHolder(pvhRotation) ;

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (float) animation.getAnimatedValue("value");
                if (value < 1.0f) {
                    Log.e("SOLON" , "value < 1.0 ....") ;
                } else {
                    Log.e("SOLON" , "value > 1.0 ....") ;
                }
            }
        });
        valueAnimator.setDuration(1000) ;
        valueAnimator.start();
    }

    private static LinkedList<ValueAnimator> objectAnimators = new LinkedList<>();
    private static Map<ValueAnimator,UpdateListener5> updateListeners5 = new HashMap<>();

    public static ValueAnimator getValueAnimator (final ViewAnimotionBean viewAnimotionBean , long duraction , MyAnimotionUtils.OnAnimotionStateListener stateListener) {
        if (!(viewAnimotionBean.getView() instanceof ViewGroupLayout)) {
            viewAnimotionBean.getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        try {
            ValueAnimator first = objectAnimators.getFirst();
            first.setDuration(duraction) ;
            objectAnimators.removeFirst();
            UpdateListener5 listener = updateListeners5.get(first);
            listener.setData(viewAnimotionBean , stateListener);
//            first.start();
            return first;
        } catch (Exception e) {

        }
        Keyframe kf0 = Keyframe.ofFloat(0, 0.0f);
        Keyframe kf1 = Keyframe.ofFloat(0.07f, 1.0f);
        Keyframe kf2 = Keyframe.ofFloat(0.8f, 1.5f);
        Keyframe kf3 = Keyframe.ofFloat(1.f, 1.5f);
//        kf2.setInterpolator(new DecelerateInterpolator());
        PropertyValuesHolder valuePropertyValues = PropertyValuesHolder.ofKeyframe("value" ,kf0, kf1, kf2,kf3);
        ValueAnimator animation = ValueAnimator.ofPropertyValuesHolder(valuePropertyValues) ;
        animation.setDuration(duraction);
        UpdateListener5 listener = new UpdateListener5();
        updateListeners5.put(animation,listener);
        listener.setData(viewAnimotionBean , stateListener);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);

                if (!(viewAnimotionBean.getView() instanceof ViewGroupLayout)) {
                    viewAnimotionBean.getView().setLayerType(View.LAYER_TYPE_NONE, null);
                    ViewAnimotionBeanUtils.addViewAnimotionBeans(viewAnimotionBean);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        return animation ;
    }

    /**
     * 测试
     */
    static class UpdateListener5 implements ValueAnimator.AnimatorUpdateListener{
        private ViewAnimotionBean bean ;
        private boolean isPull = false ;
        private MyAnimotionUtils.OnAnimotionStateListener stateListener ;

        public void setData(ViewAnimotionBean viewAnimotionBean , MyAnimotionUtils.OnAnimotionStateListener stateListener) {
            this.bean = viewAnimotionBean ;
            isPull = false ;
            this.stateListener = stateListener ;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {

//            if (animation.getAnimatedFraction() > 0.7f) {
//                animation.cancel();
//            }

            float valueAnimator = (float) animation.getAnimatedValue("value");
            if (valueAnimator <= 1.0f && bean.getView() instanceof ViewGroupLayout) {
                int offsetP = bean.getOffsetP() ;
                if (((ViewGroupLayout)bean.getView()).getTag() != null && ((ViewGroupLayout)bean.getView()).getTag().equals("TEST")){
                    Log.i("PINGOUIN", "TEST animation bean.getOldPointF().y : "+ bean.getOldPointF().y);
                    Log.i("PINGOUIN", "TEST animation bean.getNewAngelPointF().y : "+ bean.getNewAngelPointF().y);
                    Log.i("PINGOUIN", "TEST animation valueAnimator : "+ valueAnimator);
                    Log.i("PINGOUIN", "TEST animation bean.getView().getY() : "+ bean.getView().getY());
                    Log.i("PINGOUIN", "TEST animation CALC X : "+ bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator* valueAnimator);
                    Log.i("PINGOUIN", "TEST animation CALC Y : "+ bean.getOldPointF().y + ((bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator));
                }
                if (offsetP == 0) {
                    ((ViewGroupLayout)bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator * valueAnimator);
                    ((ViewGroupLayout)bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator);
//                    ((ViewGroupLayout)bean.getView()).setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator);
                } else if (offsetP == 1) {
                    ((ViewGroupLayout)bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator);
                    ((ViewGroupLayout)bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getOldPointF().y - bean.getView().getY()));
//                    ((ViewGroupLayout)bean.getView()).setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator * valueAnimator);
                } else {
                    ((ViewGroupLayout)bean.getView()).setXAndMyChildX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x) * valueAnimator);
                    ((ViewGroupLayout)bean.getView()).setYAndMyChildY(bean.getOldPointF().y + (bean.getOldPointF().y - bean.getView().getY()));
//                    ((ViewGroupLayout)bean.getView()).setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y) * valueAnimator);
                }
            } else {
//                //1-1.5
                if (!isPull && valueAnimator > 1.05f) {
                    isPull = true ;
                    if (this.stateListener!=null)
                        this.stateListener.OnAnimotionState();
                }
                if (bean.isRotate()) {
                    float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
                    float radius = progress * bean.getRadis();
                    float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                    int x, y;

                    if (bean.getDirection() == 0) {
                        x = (int) (radius * Math.sin(angle));
                        y = (int) (radius * Math.cos(angle));
                    } else {
                        x = (int) (radius * Math.cos(angle));
                        y = (int) (radius * Math.sin(angle));
                    }


                    if (bean.getView() instanceof MyView) {
                        ((MyView)bean.getView()).setMyX(x);
                        ((MyView)bean.getView()).setMyY(y);
                    } else if (bean.getView() instanceof ViewGroupLayout) {
                        ((ViewGroupLayout)bean.getView()).setMyX(x);
                        ((ViewGroupLayout)bean.getView()).setMyY(y);
                    }

                        if (bean.isNeedScale()) {
                            ViewGroupLayout viewGroupLayout = ((ViewGroupLayout) bean.getView()) ;
                            if (valueAnimator < 1.05f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.03f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.003f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.003f);
                                }
                            } else if (valueAnimator > 1.05f && valueAnimator < 1.15f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() > 1f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() - 0.0025f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() - 0.0025f);
                                }
                            } else if (valueAnimator > 1.15f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.05f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.001f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.001f);
                                }
                            }
                        }

                }
            }
        }
    }
}
