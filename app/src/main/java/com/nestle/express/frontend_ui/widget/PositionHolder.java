package com.nestle.express.frontend_ui.widget;

/**
 * Created by xumin on 16/8/12.
 */
public class PositionHolder {
    private float mX;
    private float mY;

    public PositionHolder(float x, float y) {
        mX = x;
        mY = y;
    }

    public float getX() {
        return mX;
    }

    public void setX(float x) {
        mX = x;
    }

    public float getY() {
        return mY;
    }

    public void setY(float y) {
        mY = y;
    }
}
