package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.ValueAnimator;
import android.view.View;

import com.nestle.express.frontend_ui.nurun.views.PositionInterface;
import com.nestle.express.frontend_ui.nurun.views.ProductCircleViewGroup;

import java.util.LinkedList;

/**
 * Created by jesserci on 16-10-06.
 */
public class CircleAnimationUtils {

    private static LinkedList<ValueAnimator> mValueAnimators = new LinkedList<>();

    public enum Direction {CLOCKWISE, COUNTERCLOCKWISE}

    public static ValueAnimator moveCirclePath(final View view, final float maxRadius, final float revolutions, final Direction direction, final long duration) {
        if ((view instanceof ProductCircleViewGroup)) {
            view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        ValueAnimator valueAnimator;
        UpdateListener listener = new UpdateListener();

        if (mValueAnimators.isEmpty()) {
            valueAnimator = ValueAnimator.ofFloat(0, 1);
//            valueAnimator.addListener(new Animator.AnimatorListener() {
//                @Override
//                public void onAnimationStart(Animator animation) {
//
//                }
//
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    ((ValueAnimator) animation).removeAllUpdateListeners();
//
//                    mValueAnimators.addLast((ValueAnimator) animation);
//                    if (!(view instanceof ProductCircleViewGroup)) {
//                        view.setLayerType(View.LAYER_TYPE_NONE, null);
//                    }
//                }
//
//                @Override
//                public void onAnimationCancel(Animator animation) {
//                }
//
//                @Override
//                public void onAnimationRepeat(Animator animation) {
//                }
//            });
            //valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            valueAnimator = mValueAnimators.getFirst();
            mValueAnimators.removeFirst();
        }

        valueAnimator.setDuration(duration);
        listener.setData(view, maxRadius, revolutions, direction);
        valueAnimator.addUpdateListener(listener);


        return valueAnimator;
    }

    static class UpdateListener implements ValueAnimator.AnimatorUpdateListener {
        private Direction direction;
        private float maxRadius;
        private double revolutions;
        private View view;

        public void setData(View view, float maxRadius, float revolutions, Direction direction) {
            this.direction = direction;
            this.maxRadius = maxRadius;
            this.revolutions = revolutions;
            this.view = view;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            //TODO why ?!
//            if (animation.getAnimatedFraction() > 0.8f) {
//                animation.cancel();
//            }

            float progress = (float) Math.pow((float) (1 - animation.getAnimatedFraction()), 2);  //PI 3/ 4
            float radius = progress * maxRadius;
            float angle = (float) (progress * ((11 * Math.PI / 6) + revolutions * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

            int x, y;

            if (direction == Direction.CLOCKWISE) {
                x = (int) (radius * Math.sin(angle));
                y = (int) (radius * Math.cos(angle));
            } else {
                x = (int) (radius * Math.cos(angle));
                y = (int) (radius * Math.sin(angle));
            }

            if (view instanceof PositionInterface) {
                ((ProductCircleViewGroup) view).setMyX(x, false);
                ((ProductCircleViewGroup) view).setMyY(y, false);
            }
        }
    }

    public static int getTranslateX(int radius, Direction direction) {
        int x;
        if (direction == Direction.CLOCKWISE) {
            x = (int) (-radius / 2.0f);
        } else {
            x = (int) ((Math.sqrt(3) / 2.0f) * radius);
        }
        return x;
    }

    public static int getTranslateY(int radius, Direction direction) {
        int y;
        if (direction == Direction.CLOCKWISE) {
            y = (int) ((Math.sqrt(3) / 2.0f) * radius);
        } else {
            y = (int) (-radius / 2.0f);
        }
        return y;
    }
}
