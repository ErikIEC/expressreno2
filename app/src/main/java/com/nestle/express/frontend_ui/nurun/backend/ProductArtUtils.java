package com.nestle.express.frontend_ui.nurun.backend;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.nestle.express.frontend_ui.R;

/**
 * Created by jesserci on 16-11-03.
 */
public class ProductArtUtils {

    public enum ASSET {GLASS, FRUIT, TITLE, SUBTITLE, SUBTITLE_ALIGNED, NUTRITIONAL_INFO, NUTRITIONAL_INFO_BUTTON, CIRCULAR_BG}

    public static Drawable getProductBitmapDrawable(Context context, String path, ASSET asset) {
        Drawable bitmapDrawable = BitmapDrawable.createFromPath(path);
        if (bitmapDrawable != null) {
            return bitmapDrawable;
        } else {
            return getApplicationDefaultBitmapDrawable(context, asset);
        }
    }

    //TODO set default asset
    public static Drawable getApplicationDefaultBitmapDrawable(Context context, ASSET asset) {
        switch (asset) {
            case GLASS:
                Log.e("ProductArtUtils", "GLASS not found, default error GLASS picked");
                return ContextCompat.getDrawable(context, R.drawable.juice_beizi_orange);
            case FRUIT:
                Log.e("ProductArtUtils", "FRUIT not found, default error FRUIT picked");
                return ContextCompat.getDrawable(context, R.drawable.juice_guo_orange);
            case TITLE:
                Log.e("ProductArtUtils", "TITLE not found, default error TITLE picked");
                return ContextCompat.getDrawable(context, R.drawable.title_one);
            case SUBTITLE:
                Log.e("ProductArtUtils", "SUBTITLE not found, default error SUBTITLE picked");
                return ContextCompat.getDrawable(context, R.drawable.wenan);
            case SUBTITLE_ALIGNED:
                Log.e("ProductArtUtils", "SUBTITLE_ALIGNED not found, default error SUBTITLE_ALIGNED picked");
                return ContextCompat.getDrawable(context, R.drawable.wenan);
            case NUTRITIONAL_INFO:
                Log.e("ProductArtUtils", "NUTRITIONAL_INFO not found, default error NUTRITIONAL_INFO picked");
                return ContextCompat.getDrawable(context, R.drawable.right_content);
            case NUTRITIONAL_INFO_BUTTON:
                Log.e("ProductArtUtils", "NUTRITIONAL_INFO_BUTTON not found, default error NUTRITIONAL_INFO_BUTTON picked");
                return ContextCompat.getDrawable(context, R.drawable.small_title_one);
            case CIRCULAR_BG:
                Log.e("ProductArtUtils", "CIRCULAR_BG not found, default error CIRCULAR_BG picked");
                return ContextCompat.getDrawable(context, R.drawable.yuan1);
            default:
                return null;
        }
    }
}
