package com.nestle.express.frontend_ui.surface;

import android.graphics.Bitmap;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xumin on 2016/8/9.
 */
public class BaseNode {

    private int width, height;

    private Paint paint = new Paint();

    private float anchorX = 0;

    private float anchorY = 0;

    private float scaleX = 1, scaleY = 1;

    private int positionX, positionY;

    private Bitmap bitmap = null;

    public BaseNode(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public final Bitmap getNextBitmap() {
        return bitmap;
    }

    public void invalidate() {
        bitmap = getBitmap();
    }

    final public int getWidth() {
        return (int) (width * getScaleX());
    }

    final public void setWidth(int width) {
        this.width = width;
    }

    final public int getHeight() {
        return (int) (height * getScaleY());
    }

    final public void setHeight(int height) {
        this.height = height;
    }

    final public Paint getPaint() {
        return paint;
    }

    final public void setPaint(Paint paint) {
        this.paint = paint;
    }

    final public float getAnchorX() {
        return anchorX;
    }

    final public void setAnchorX(float anchorX) {
        this.anchorX = anchorX;
    }

    final public float getAnchorY() {
        return anchorY;
    }

    final public void setAnchorY(float anchorY) {
        this.anchorY = anchorY;
    }

    final public float getScaleX() {
        return scaleX;
    }

    final public void setScaleX(float scaleX) {
        this.scaleX = scaleX;
    }

    final public float getScaleY() {
        return scaleY;
    }

    final public void setScaleY(float scaleY) {
        this.scaleY = scaleY;
    }

    final public int getPositionX() {
        return (int) (positionX - (getWidth() * 1.0f * getAnchorX()));
    }

    final public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    final public int getPositionY() {
        return (int) (positionY - (getHeight() * 1.0f * getAnchorY()));
    }

    final public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    final public void setPosition(int positionX, int positionY) {
        this.positionY = positionY;
        this.positionX = positionX;
    }
}
