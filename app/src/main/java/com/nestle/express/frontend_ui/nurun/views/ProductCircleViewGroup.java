package com.nestle.express.frontend_ui.nurun.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.RelativeLayout;

/**
 * Created by jesserci on 16-10-11.
 */
public class ProductCircleViewGroup extends RelativeLayout implements PositionInterface {

    private Paint mPaint;
    private int mBackgroundColor = Color.TRANSPARENT;
    private ArrayMap<Integer, PointF> mOldPointFs = new ArrayMap<>();
    private Paint mPaintDst;
    private Bitmap bm;


    public ProductCircleViewGroup(Context context) {
        super(context);
        init();
    }

    public ProductCircleViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProductCircleViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ProductCircleViewGroup(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setFilterBitmap(false);

        mPaintDst = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaintDst.setColor(mBackgroundColor);
        setWillNotDraw(false);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(widthSize, heightSize);
        int childCount = getChildCount();
        for (int x = 0; x < childCount; x++) {
            View view = getChildAt(x);
            measureChild(view, widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int size = getChildCount();
        for (int x = 0; x < size; x++) {
            View view = getChildAt(x);
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        }
    }

    /*@Override
    protected void dispatchDraw(Canvas canvas) {
        int sc = canvas.saveLayer(0, 0, 0 + getWidth(), 0 + getHeight(), null,
                Canvas.MATRIX_SAVE_FLAG |
                        Canvas.CLIP_SAVE_FLAG |
                        Canvas.HAS_ALPHA_LAYER_SAVE_FLAG |
                        Canvas.FULL_COLOR_LAYER_SAVE_FLAG |
                        Canvas.CLIP_TO_LAYER_SAVE_FLAG);
        canvas.drawBitmap(makeDst(getWidth(), getHeight()), 0, 0, mPaint);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        super.dispatchDraw(canvas);
        mPaint.setXfermode(null);
        canvas.restoreToCount(sc);
    }*/

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }

    /**
     * 上切圆
     *
     * @param w
     * @param h
     * @return
     */
    private Bitmap makeDst(int w, int h) {
        if (bm == null) {
            bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bm);
            c.drawOval(new RectF(0, 0, w, h), mPaintDst);
        }
        return bm;
    }

    @Override
    public void putPosition(View view, float x, float y) {
        mOldPointFs.put(view.getId(), new PointF(x, y));
    }

    @Override
    public void putPosition(float x, float y) {
        mOldPointFs.put(this.getId(), new PointF(x, y));
    }

    @Override
    public void setMyX(float addX) {
        setX((mOldPointFs.get(getId()).x + addX));
        if (true) {
            {
                View childView1 = getChildAt(0);
                if (childView1 instanceof AbsoluteLayout) {
                    int childCount = ((AbsoluteLayout) childView1).getChildCount();
                    for (int j = 0; j < childCount; j++) {
                        View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                        if (mOldPointFs.containsKey(childchildView.getId())) {
                            childchildView.setX((mOldPointFs.get(childchildView.getId()).x - addX));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void setMyY(float addY) {
        setY((mOldPointFs.get(getId()).y + addY));
        if (true) {
            View childView1 = getChildAt(0);
            if (childView1 instanceof AbsoluteLayout) {
                int childCount = ((AbsoluteLayout) childView1).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                    if (mOldPointFs.containsKey(childchildView.getId())) {
                        childchildView.setY((mOldPointFs.get(childchildView.getId()).y - addY));
                    }
                }
            }
        }
    }


    public void setXAndMyChildX(float x, boolean animateChildren) {
        setX(x);
        if (false) {
            View childView1 = getChildAt(0);
            if (childView1 instanceof AbsoluteLayout) {
                int childCount = ((AbsoluteLayout) childView1).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                    if (mOldPointFs.containsKey(childchildView.getId())) {
                        childchildView.setX((mOldPointFs.get(childchildView.getId()).x - ((x - mOldPointFs.get(getId()).x))));
                    }
                }
            }
        }
    }

    public void setYAndMyChildY(float y, boolean animateChildren) {
        setY(y);
        if (false) {
            View childView1 = getChildAt(0);
            if (childView1 instanceof AbsoluteLayout) {
                int childCount = ((AbsoluteLayout) childView1).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                    if (mOldPointFs.containsKey(childchildView.getId())) {
                        childchildView.setY(mOldPointFs.get(childchildView.getId()).y - (y - mOldPointFs.get(getId()).y));
                    }
                }
            }
        }
    }

    public void setMyScaleX(float x) {
        setScaleX(x);
        View childView1 = getChildAt(0);
        if (childView1 instanceof AbsoluteLayout) {
            childView1.setScaleX(1 + (1 - x));
        }
    }

    public void setMyScaleY(float y) {
        setScaleY(y);
        View childView1 = getChildAt(0);
        if (childView1 instanceof AbsoluteLayout) {
            childView1.setScaleY(1 + (1 - y));
        }
    }

    public void setMyX(float addX, boolean mAnimateChild) {
        setX((mOldPointFs.get(getId()).x + addX));
        AbsoluteLayout childView1 = (AbsoluteLayout) getChildAt(0);
//        childView1.setX(mOldPointFs.get(childView1.getId()).x - addX);
        if (false) {
            {
                if (childView1 instanceof AbsoluteLayout) {
                    int childCount = ((AbsoluteLayout) childView1).getChildCount();
                    for (int j = 0; j < childCount; j++) {
                        View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                        if (mOldPointFs.containsKey(childchildView.getId())) {
                            childchildView.setX((mOldPointFs.get(childchildView.getId()).x - addX));
                        }
                    }
                }
            }
        }
    }

    public void setMyY(float addY, boolean mAnimateChild) {
        setY((mOldPointFs.get(getId()).y + addY));
        AbsoluteLayout childView1 = (AbsoluteLayout) getChildAt(0);
//        childView1.setY(mOldPointFs.get(childView1.getId()).y - addY);
        if (false) {
            if (childView1 instanceof AbsoluteLayout) {
                int childCount = ((AbsoluteLayout) childView1).getChildCount();
                for (int j = 0; j < childCount; j++) {
                    View childchildView = ((AbsoluteLayout) childView1).getChildAt(j);
                    if (mOldPointFs.containsKey(childchildView.getId())) {
                        childchildView.setY((mOldPointFs.get(childchildView.getId()).y - addY));
                    }
                }
            }
        }
    }
}
