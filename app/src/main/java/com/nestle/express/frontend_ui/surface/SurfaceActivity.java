package com.nestle.express.frontend_ui.surface;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.nestle.express.frontend_ui.R;

/**
 * Created by xumin on 2016/8/9.
 */
public class SurfaceActivity extends Activity {
    Layer layer ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Scene scene = new Scene(this);

//        final Layer layer = new Layer((int)getResources().getDimension(R.dimen.dimen_430dp),(int)getResources().getDimension(R.dimen.dimen_430dp)) ;
//        layer.setPosition((int)getResources().getDimension(R.dimen.dimen_200dp) , (int)getResources().getDimension(R.dimen.dimen_200dp));
//        layer.setBackgroundBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.yuan1));
//
//        Sprite sprite = new Sprite(400,400) ;
//        sprite.setColor(Color.RED);
//        sprite.setPosition(100,100);
        setContentView(scene);
//
//        layer.addNode(sprite);
//        scene.addNode(layer);
//
//        scene.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ObjectAnimator objectAnimator = ObjectAnimator.ofInt(layer,"positionX",0,500);
//                ObjectAnimator objectAnimatorY = ObjectAnimator.ofInt(layer,"positionY",0,500);
//                AnimatorSet animatorSet = new AnimatorSet();
//                animatorSet.playTogether(objectAnimator , objectAnimatorY);
//                animatorSet.setDuration(1000);
//                animatorSet.start();;
//            }
//        });

//        Scene scene = new Scene(this) ;
//
//        layer = new Layer(500,500) ;
//        layer.setPosition(200,100);
//        layer.invalidate();
//        layer.setBackgroundColor(Color.GREEN);
//
//        layer.setBackgroundBitmap(AssertUtils.getImageFromAssetsFile(this , "1/yuan1.png" , (int)getResources().getDimension(R.dimen.dimen_430dp),(int)getResources().getDimension(R.dimen.dimen_430dp)));
//
//        Sprite sprite = new Sprite(400,400) ;
//        sprite.invalidate();
//        sprite.setColor(Color.RED);
//        sprite.setPosition(100,100);
//
//
//        layer.addNode(sprite);
//
//        scene.addNode(layer);
//
//        setContentView(scene);
//
//        scene.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ObjectAnimator objectAnimatorX = ObjectAnimator.ofInt(layer , "positionX" , 0 , 500) ;
//                objectAnimatorX.setDuration(1000) ;
//                objectAnimatorX.start();
////                ObjectAnimator objectAnimatorY = ObjectAnimator.ofInt(layer , "positionY" , 0 , 500) ;
////                AnimatorSet animatorSet = new AnimatorSet();
////                animatorSet.playTogether(objectAnimatorX , objectAnimatorY);
////                animatorSet.setDuration(1000) ;
////                animatorSet.start();
//            }
//        });

        initView(scene);
    }

    public void initView (Scene scene) {
        Layer main_one_viewGroup = new Layer((int)getResources().getDimension(R.dimen.dimen_430dp),(int)getResources().getDimension(R.dimen.dimen_430dp)) ;
        main_one_viewGroup.setPosition((int)getResources().getDimension(R.dimen.dimen_100dp) , (int)getResources().getDimension(R.dimen.dimen_030dp));
        main_one_viewGroup.setBackgroundBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.yuan1));

        Sprite main_one_viewgroup_image1 = new Sprite((int)getResources().getDimension(R.dimen.dimen_110dp),(int)getResources().getDimension(R.dimen.dimen_250dp)) ;
        main_one_viewgroup_image1.setPosition((int)getResources().getDimension(R.dimen.dimen_90dp),(int)getResources().getDimension(R.dimen.dimen_110dp));
        main_one_viewgroup_image1.setmBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.juice_guo_putao));

        main_one_viewGroup.addNode(main_one_viewgroup_image1);

        scene.addNode(main_one_viewGroup);
    }
}
