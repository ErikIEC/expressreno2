package com.nestle.express.frontend_ui.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

public class ImageHelper {

    private Context mContext ;
    public ImageHelper (Context context) {
        this.mContext = context ;
        Glide.get(getContext()).getBitmapPool().setSizeMultiplier(0.2f);
        Glide.get(getContext()).setMemoryCategory(MemoryCategory.LOW);
    }

    private Context getContext() {
        return this.mContext;
    }

    private RequestManager getRequestManager() {

        return Glide.with(getContext());
    }

    public void displayImage(int resourceId, ImageView iv) {
        iv.setImageResource(resourceId);
    }

    /**
     * 本地图片
     *
     * @param uri 图片地址
     * @param iv
     */
    public void displayImage(Uri uri, ImageView iv) {
        getRequestManager().load(uri).diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter().dontAnimate().into(iv);
    }
    /**
     * 本地图片
     *
     * @param path 图片地址
     * @param iv
     */
    public void disPlayImageResourse(String path, ImageView iv) {
        getRequestManager().load(path).diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter().dontAnimate().into(iv);
    }
    /**
     * 本地图片
     *
     * @param resourseId 资源地址
     * @param iv
     */
    public void disPlayImageResourse (int resourseId , ImageView iv) {
        getRequestManager().load(resourseId).diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter().dontAnimate().into(iv);
    }

    public void displayImageWithDefault(String url, ImageView iv, int resourceId) {
        getRequestManager().load(url).thumbnail(0.2f).diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(resourceId)
                .fitCenter().dontAnimate().into(iv);
//        ImageUtils.with(getContext()).load(url).error(resourceId).into(iv);
    }

    public void clearMemory() {
        Glide.get(getContext()).clearMemory();
    }

    public void loadImageViewBack (final ImageView iv , int resourId) {
        getRequestManager().load(resourId).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                iv.setBackgroundResource(resource);
            }
        }) ;
    }

}
