package com.nestle.express.frontend_ui.widget;

import android.view.View;

/**
 * Created by xumin on 16/8/12.
 */
public class ViewHolder {
    private View view;
    public ViewHolder(View view){
        this.view = view;
    }

    //看到这个是不是感觉跟第一种方法一样的感脚，只是封装的不同
    public void setXY(PositionHolder xyHolder) {
        this.view.setX(xyHolder.getX());
        this.view.setY(xyHolder.getY());
    }

    public PositionHolder getXY() {
        return new PositionHolder(view.getX(), view.getY());
    }
}
