package com.nestle.express.frontend_ui.nurun.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.nestle.express.frontend_ui.nurun.utils.PreferencesHelper;
import com.nestle.express.iec_backend.ExpressApp;

import java.util.Locale;

/**
 * Created by jesserci on 16-11-08.
 */
public abstract class LocaleActivity extends Activity {

    //TODO-Alex: uncomment below
    private final int mDelayToResetDefaultLocale = 1000 * 60 * 1;
    //TODO-Alex: remove below
//    private final int mDelayToResetDefaultLocale = 10000;

    static public final String FRENCH_ISO_CODE = "fr";
    static public final String ENGLISH_ISO_CODE = "en";
    static public final String SPANISH_ISO_CODE = "es";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (PreferencesHelper.getCurrentTimeLocaleChanged(this) != 0L
                && (PreferencesHelper.getCurrentTimeLocaleChanged(this) + Long.valueOf(PreferencesHelper.getDelayToResetDefaultLocale(this))) <= System.currentTimeMillis()) {
            if (!PreferencesHelper.getDefaultLocale(this).isEmpty()) {
                ExpressApp.setLocale(this, new Locale(PreferencesHelper.getDefaultLocale(this)));
                PreferencesHelper.saveCurrentTimeLocaleChanged(this, 0L);
                PreferencesHelper.saveCurrentLocale(this, PreferencesHelper.getDefaultLocale(this));
            }
        }
    }

    private void setLocaleValues(Locale locale) {
        PreferencesHelper.saveCurrentLocale(this, locale.getLanguage());
        ///PreferencesHelper.saveCurrentTimeLocaleChanged(this, System.currentTimeMillis());
        // PreferencesHelper.saveDelayToResetDefaultLocale(this, mDelayToResetDefaultLocale);
    }

    public void onEnglish(View view) {
        Locale locale = new Locale(ENGLISH_ISO_CODE);
        onLocaleSelected(locale);
    }

    public void onFrench(View view) {
        Locale locale = new Locale(FRENCH_ISO_CODE);
        onLocaleSelected(locale);
    }

    public void onSpanish(View view) {
        Locale locale = new Locale(SPANISH_ISO_CODE);
        onLocaleSelected(locale);
    }

    protected void onLocaleSelected(Locale locale) {
        if (locale != null
                && !locale.getLanguage().equalsIgnoreCase(PreferencesHelper.getCurrentLocale(this))) {
            setLocaleValues(locale);

            ExpressApp.setLocale(this, locale);
            Intent intent = new Intent(getApplicationContext(), this.getClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        onActivityKeyDown(keyCode, event);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        onActivityKeyUp(keyCode, event);
        return super.onKeyUp(keyCode, event);
    }

    public void onActivityKeyDown(int keyCode, KeyEvent event){

    }


    public void onActivityKeyUp(int keyCode, KeyEvent event){

    }
}
