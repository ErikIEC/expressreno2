package com.nestle.express.frontend_ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

import com.nestle.express.iec_backend.ExpressApp;

/**
 * Created by estev on 4/12/2017.
 */

public class MyVideoView extends VideoView {
    public MyVideoView(Context context){super(ExpressApp.getApp());}

    public MyVideoView(Context context, AttributeSet attrs){super(ExpressApp.getApp(), attrs);}

    public MyVideoView(Context context, AttributeSet attrs, int defStyleAttr){
        super(ExpressApp.getApp(), attrs, defStyleAttr);
    }
}
