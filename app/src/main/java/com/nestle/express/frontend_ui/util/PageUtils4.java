package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.widget.ViewAnimotionBean;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xumin on 16/8/18.
 */
public class PageUtils4 {
    private Context mContext ;
    private View mView ;

    private MyView purple1 , purple , green , blue , deep_purple , orange , green1 , orange1;
    public ImageView main_one_viewgroup_image1 , main_one_viewgroup_image2 , main_one_viewgroup_image3 , main_one_viewgroup_image4
            ,main_two_viewgroup_image1 , main_two_viewgroup_image2 , main_two_viewgroup_image3 , main_two_viewgroup_image4
            ,main_three_viewgroup_image1 , main_three_viewgroup_image2 , main_three_viewgroup_image3 , main_three_viewgroup_image4
            ,main_four_viewgroup_image1 , main_four_viewgroup_image2 , main_four_viewgroup_image3 , main_four_viewgroup_image4;
    public AbsoluteLayout main_one_viewgroup_layout ,main_two_viewgroup_layout,main_three_viewgroup_layout,main_four_viewgroup_layout;
    public ViewGroupLayout main_one_viewgroup , main_two_viewgroup ,main_three_viewgroup , main_four_viewgroup;

    public PageUtils4(Context context , View view ) {
            this.mContext = context ;
            this.mView = view ;
            initView(view);
    }

    private void initView (View view) {
        purple1 = (MyView)view.findViewById(R.id.purple1) ;
        purple = (MyView)view.findViewById(R.id.purple) ;
//        green = (MyView)view.findViewById(R.id.green) ;
        blue = (MyView)view.findViewById(R.id.blue) ;
        deep_purple = (MyView)view.findViewById(R.id.deep_purple) ;
        orange = (MyView)view.findViewById(R.id.orange) ;
        green1 = (MyView)view.findViewById(R.id.green1) ;
        orange1 = (MyView)view.findViewById(R.id.orange1) ;

        main_one_viewgroup = (ViewGroupLayout) view.findViewById(R.id.main_one_viewgroup);
        main_two_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_two_viewgroup) ;
        main_three_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_three_viewgroup) ;
        main_four_viewgroup = (ViewGroupLayout)view.findViewById(R.id.main_four_viewgroup) ;

        main_one_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_one_viewgroup_layout) ;
        main_two_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_two_viewgroup_layout) ;
        main_three_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_three_viewgroup_layout) ;
        main_four_viewgroup_layout = (AbsoluteLayout)view.findViewById(R.id.main_four_viewgroup_layout) ;

        main_one_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image1) ;
        main_one_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image2) ;
        main_one_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image3) ;
        main_one_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_one_viewgroup_image4) ;

        main_two_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image1) ;
        main_two_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image2) ;
        main_two_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image3) ;
        main_two_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_two_viewgroup_image4) ;

        main_three_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image1) ;
        main_three_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image2) ;
        main_three_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image3) ;
        main_three_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_three_viewgroup_image4) ;

        main_four_viewgroup_image1 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image1) ;
        main_four_viewgroup_image2 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image2) ;
        main_four_viewgroup_image3 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image3) ;
        main_four_viewgroup_image4 = (ImageView)view.findViewById(R.id.main_four_viewgroup_image4) ;

//        circleView() ;

    }

    public void testMove() {
//        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
//        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 0 , 80 , true , 0 , true) ;
//        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
//        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 80) ;
//        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),0 , 80) ;
//        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),0 , 80) ;
//        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, 0,0 , 80) ;
//        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),0 , 80) ;
//
//        ViewAnimotionBean viewAnimotionBean6  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple1 , mContext.getResources().getDimension(R.dimen.dimen_080dp),mContext.getResources().getDimension(R.dimen.dimen_080dp) ,0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(purple , mContext.getResources().getDimension(R.dimen.dimen_050dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp) ,0 , 0) ;
//        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_0300dp) ) ;
//        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 1 , 70 , false , 1 , true) ;
//        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
//        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF,0 , 80 , false , 2 , true) ;
//
//        ViewAnimotionBean viewAnimotionBean10 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_460dp), mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_440dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean12 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean14 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_460dp),0 , 0) ;
//
//        ViewAnimotionBean viewAnimotionBean15 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp) , mContext.getResources().getDimension(R.dimen.dimen_150dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean16 = ViewAnimotionBeanUtils.getViewAnimotionBean(blue , mContext.getResources().getDimension(R.dimen.dimen_280dp),mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean17 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_300dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
//        ViewAnimotionBean viewAnimotionBean18 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange , mContext.getResources().getDimension(R.dimen.dimen_300dp)  , mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
//
//
//        ArrayList arrayList = new ArrayList() ;
//        arrayList.add(viewAnimotionBean) ;
//        arrayList.add(viewAnimotionBean1) ;
//        arrayList.add(viewAnimotionBean2) ;
//        arrayList.add(viewAnimotionBean3) ;
//        arrayList.add(viewAnimotionBean4) ;
//        arrayList.add(viewAnimotionBean5) ;
//        arrayList.add(viewAnimotionBean6) ;
//        arrayList.add(viewAnimotionBean7) ;
//        arrayList.add(viewAnimotionBean8) ;
//        arrayList.add(viewAnimotionBean9) ;
//
//        arrayList.add(viewAnimotionBean10) ;
//        arrayList.add(viewAnimotionBean11) ;
//        arrayList.add(viewAnimotionBean12) ;
//        arrayList.add(viewAnimotionBean13) ;
//        arrayList.add(viewAnimotionBean14) ;
//
//        arrayList.add(viewAnimotionBean15) ;
//        arrayList.add(viewAnimotionBean16) ;
//        arrayList.add(viewAnimotionBean17) ;
//        arrayList.add(viewAnimotionBean18) ;
//
//        initPostionTwo() ;
//
//        List<Animator> arrayList1 = new ArrayList() ;
//        for (int i = 0 ; i < arrayList.size() ; i ++ ) {
//            ValueAnimator valueAnimator = null ;
//            if (i==0) {
//                valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000, new MyAnimotionUtils.OnAnimotionStateListener() {
//                    @Override
//                    public void OnAnimotionState() {
//                        onOneState();
//                    }
//                }) ;
//            } else {
//                valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000 , null) ;
//            }
//            arrayList1.add(valueAnimator) ;
//        }
//
//        AnimatorSet animatorSet = new AnimatorSet();
//        animatorSet.setDuration(20000);
//        animatorSet.setInterpolator(new DecelerateInterpolator());
//        animatorSet.playTogether(arrayList1);
////        animatorSet.playTogether(MyAnimotionUtils.getValueAnimator(viewAnimotionBean , 20000) , MyAnimotionUtils.getValueAnimator(viewAnimotionBean1 , 20000),MyAnimotionUtils.getValueAnimator(viewAnimotionBean8 , 20000));
//        animatorSet.start();
        pageOneAnimotion();
    }

    public void onOneState() {
        PointF oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_080dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        PointF newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_060dp), mContext.getResources().getDimension(R.dimen.dimen_140dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(blue ,oldPointF, newPointF , 1 , 35 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean1 = ViewAnimotionBeanUtils.getViewAnimotionBean(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_40dp) , mContext.getResources().getDimension(R.dimen.dimen_400dp),0 , 0) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp)) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        ViewAnimotionBean viewAnimotionBean2 = ViewAnimotionBeanUtils.getViewAnimotionBean(orange ,oldPointF, newPointF , 1 , 60 , false , 0 , true) ;

        oldPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0425dp) ) ;
        newPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp)) ;
        ViewAnimotionBean viewAnimotionBean3 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup ,oldPointF, newPointF , 1 , 80 , false , 1 , true) ;

        ArrayList arrayList = new ArrayList() ;
        arrayList.add(viewAnimotionBean) ;
        arrayList.add(viewAnimotionBean1);
        arrayList.add(viewAnimotionBean2) ;
        arrayList.add(viewAnimotionBean3) ;

        List<Animator> arrayList1 = new ArrayList() ;
        for (int i = 0 ; i < arrayList.size() ; i ++ ) {
            ValueAnimator valueAnimator = null ;

            valueAnimator = MyAnimotionUtils.getValueAnimator((ViewAnimotionBean) arrayList.get(i), 5000 ,null) ;

            arrayList1.add(valueAnimator) ;
        }

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(17000);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether(arrayList1);
//        animatorSet.playTogether(MyAnimotionUtils.getValueAnimator(viewAnimotionBean , 20000) , MyAnimotionUtils.getValueAnimator(viewAnimotionBean1 , 20000),MyAnimotionUtils.getValueAnimator(viewAnimotionBean8 , 20000));
        animatorSet.start();
    }

    public void initPosition(ArrayList<ViewAnimotionBean> list) {
        for (ViewAnimotionBean bean : list) {
            bean.initPosition();
        }
    }

    public ViewAnimotionBean getSonViewAniotionBean(View view , float endx , float endY,int direction,int radis ) {
        PointF newPointF = new PointF(endx , endY) ;
        ViewAnimotionBean viewAnimotionBean = new ViewAnimotionBean(view , newPointF , direction ,radis) ;
        return viewAnimotionBean ;
    }

    /**
     * 第一个页面切换动画
     */
    public void pageOneAnimotion() {
        Log.d("JASON", "pageOneAnimotion");
        setPostionOne();
        initPostionTwo();
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(purple1 , mContext.getResources().getDimension(R.dimen.dimen_080dp),mContext.getResources().getDimension(R.dimen.dimen_080dp) ) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(purple , mContext.getResources().getDimension(R.dimen.dimen_050dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp) ) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp) , mContext.getResources().getDimension(R.dimen.dimen_150dp) ) ;

        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_280dp),mContext.getResources().getDimension(R.dimen.dimen_400dp) ) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_300dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp) ) ;
        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_300dp)  , mContext.getResources().getDimension(R.dimen.dimen_380dp) ) ;

        AnimatorSet animatorSet7 = PageAnimotionUtils.getAnimotion(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp)+CircleAnimotionUtils.getTransleX(70,1) , mContext.getResources().getDimension(R.dimen.dimen_0300dp)+CircleAnimotionUtils.getTransleY(70,1) ) ;
        AnimatorSet animatorSet8 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_100dp) + (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_035dp)+ (int)((Math.sqrt(3) / 2.0f)*40) ) ;

        AnimatorSet animatorSet9 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_460dp) -CircleAnimotionUtils.getTransleX(70,1), mContext.getResources().getDimension(R.dimen.dimen_300dp)-CircleAnimotionUtils.getTransleY(70,1) ) ;
        AnimatorSet animatorSet10 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_400dp) -CircleAnimotionUtils.getTransleX(70,1), mContext.getResources().getDimension(R.dimen.dimen_440dp)-CircleAnimotionUtils.getTransleY(70,1) ) ;
        AnimatorSet animatorSet11 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_600dp) -CircleAnimotionUtils.getTransleX(70,1), mContext.getResources().getDimension(R.dimen.dimen_380dp)-CircleAnimotionUtils.getTransleY(70,1)) ;
        AnimatorSet animatorSet12 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp)) ;
        AnimatorSet animatorSet13 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_600dp)-CircleAnimotionUtils.getTransleX(70,1), mContext.getResources().getDimension(R.dimen.dimen_460dp)-CircleAnimotionUtils.getTransleY(70,1)) ;

        AnimatorSet animatorSet14 = PageAnimotionUtils.getAnimotion(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_530dp) + CircleAnimotionUtils.getTransleX(80,0), mContext.getResources().getDimension(R.dimen.dimen_130dp) + CircleAnimotionUtils.getTransleY(80,0)) ;

        AnimatorSet animatorSet15 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) - (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_110dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet16 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) - (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_175dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) - (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_130dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet18 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image4 , 0, 0) ;
        AnimatorSet animatorSet19 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet20 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup , 1f , 1f , false ) ;
        AnimatorSet animatorSet21 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup_image1 , 1f , 1f , false) ;
        AnimatorSet animatorSet22 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup_image2 , 1f , 1f , false) ;
        AnimatorSet animatorSet23 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup_image3 , 1f , 1f , false) ;
        AnimatorSet animatorSet24 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup_layout , 1f , 1f , false) ;
        AnimatorSet animatorSet25 = PageAnimotionUtils.getAnimotionScale(main_one_viewgroup_image4 , 1f , 1f , false) ;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet1, animatorSet2 , animatorSet3 ,
                animatorSet4 , animatorSet5 , animatorSet6,animatorSet14
                ,animatorSet20,animatorSet21,animatorSet22,animatorSet23
                ,animatorSet24,animatorSet25);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.start();

        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                Log.e("test1112" ,"x22:" +CircleAnimotionUtils.getTransleX(70,1) +"  , y22: " +CircleAnimotionUtils.getTransleY(70,1)) ;
//                CircleAnimotionUtils.moveCirclePath(main_one_viewgroup ,70 , 3 , 1 , 10000);
//
//                CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,40 , 3 ,0, 10000 , true);

                CircleAnimotionUtils.moveCirclePath(main_four_viewgroup ,80 , 3 ,0, 8000);

                pageEndAnimotion();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 0 , 55 , true , 0 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),0 , 55) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, 0,0 , 0) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),0 , 55) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_0300dp) ) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 1 , 35 , false , 1 , true) ;
        ViewAnimotionBean viewAnimotionBean10 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_460dp), mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_440dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean12 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_380dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean13 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean14 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_460dp),0 , 0) ;
        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        ValueAnimator valueAnimator12 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean12, 15000 , null) ;
        ValueAnimator valueAnimator13 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean13, 15000 , null) ;
        ValueAnimator valueAnimator14 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean14, 15000 , null) ;
        AnimatorSet animatorSet26 = new AnimatorSet();
        animatorSet26.playTogether(valueAnimator , valueAnimator1 ,valueAnimator2,valueAnimator3,valueAnimator4,valueAnimator5,valueAnimator8,valueAnimator10
                  ,valueAnimator11   ,valueAnimator12 ,valueAnimator13 ,valueAnimator14
        );
        animatorSet26.setDuration(15000) ;
        animatorSet26.setInterpolator(new DecelerateInterpolator());
        animatorSet26.start();
    }

    private void setPostionOne() {
        main_four_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.INVISIBLE);

        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_two_viewgroup_image1 , R.dimen.dimen_0300dp , R.dimen.dimen_330dp) ;
        setViewPosition(main_two_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_400dp) ;
        setViewPosition(main_two_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_355dp) ;
        setViewPosition(main_two_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_450dp) ;
        setViewPosition(main_two_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_two_viewgroup.setChildeVisibilty(View.VISIBLE);
        main_two_viewgroup.setMyScaleX(1.0f);
        main_two_viewgroup.setMyScaleY(1.0f);

    }

    private void initPostionTwo() {
        main_one_viewgroup.putPosition(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp) , mContext.getResources().getDimension(R.dimen.dimen_0300dp));
        main_four_viewgroup.putPosition(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition( mContext.getResources().getDimension(R.dimen.dimen_060dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp));
        orange.putPosition(mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_330dp));
        main_three_viewgroup.putPosition(main_three_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0350dp));

        main_two_viewgroup.putPosition(main_two_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp) , mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_two_viewgroup.putPosition(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;

        main_two_viewgroup.putPosition(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_image4 , 0, 0) ;
        main_two_viewgroup.putPosition(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }


    /**
     * 第一个页面结束的时候调用的方法
     */
    private void pageEndAnimotion() {
        Log.d("JASON", "pageEndAnimotion");
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(blue ,mContext.getResources().getDimension(R.dimen.dimen_080dp) , mContext.getResources().getDimension(R.dimen.dimen_140dp), mContext.getResources().getDimension(R.dimen.dimen_060dp) + CircleAnimotionUtils.getTransleX(35,1) , mContext.getResources().getDimension(R.dimen.dimen_140dp)+ CircleAnimotionUtils.getTransleY(35,1)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_40dp) , mContext.getResources().getDimension(R.dimen.dimen_400dp)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_380dp) ,mContext.getResources().getDimension(R.dimen.dimen_10dp)+ CircleAnimotionUtils.getTransleX(60,1) , mContext.getResources().getDimension(R.dimen.dimen_330dp)+ CircleAnimotionUtils.getTransleY(60,1)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(main_three_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_450dp), mContext.getResources().getDimension(R.dimen.dimen_0425dp) , mContext.getResources().getDimension(R.dimen.dimen_450dp)+ CircleAnimotionUtils.getTransleX(80,1), mContext.getResources().getDimension(R.dimen.dimen_0350dp)+ CircleAnimotionUtils.getTransleY(80,1)) ;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(300);
        animatorSet.playTogether(animatorSet1, animatorSet2 , animatorSet3 ,
                animatorSet4);

        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.d("JASON", "pageEndAnimotion.onAnimationEnd");
                CircleAnimotionUtils.moveCirclePath(blue ,35 , 3 , 1 , 8000);
                CircleAnimotionUtils.moveCirclePath(orange ,60 , 3 , 1 , 8000 );
                CircleAnimotionUtils.moveCirclePath(main_three_viewgroup ,80 , 3 , 1 , 8000 );
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 第二个页面动画
     *
     *
     * 修改说明：移动距离增加一部分为 圆周运动的偏移量 这样就不会造成抖动
     *
     * initPostionThree 为我们初始化没有带偏移量的值的设定
     *
     */
    public void pageTwoToThreeAniomotion() {
        setPostionTwo() ;
        initPostionThree() ;
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0280dp) + CircleAnimotionUtils.getTransleX(70, 1) , mContext.getResources().getDimension(R.dimen.dimen_240dp) + CircleAnimotionUtils.getTransleY(70,1)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp) , mContext.getResources().getDimension(R.dimen.dimen_0240dp)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_image4 , 0,  mContext.getResources().getDimension(R.dimen.dimen_0300dp)) ;
        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp)) ;

        AnimatorSet animatorSet7 = PageAnimotionUtils.getAnimotion(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_100dp)+ (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_035dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet8 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) - (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_110dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet9 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_175dp)+ (int)((Math.sqrt(3) / 2.0f)*40) ) ;
        AnimatorSet animatorSet10 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) - (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_130dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet11 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image4 , 0,0) ;
        AnimatorSet animatorSet12 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_200dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;

        AnimatorSet animatorSet13 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_0150dp),mContext.getResources().getDimension(R.dimen.dimen_100dp)) ;
        AnimatorSet animatorSet14 = PageAnimotionUtils.getAnimotion(green1 , mContext.getResources().getDimension(R.dimen.dimen_0100dp),mContext.getResources().getDimension(R.dimen.dimen_150dp) , mContext.getResources().getDimension(R.dimen.dimen_10dp)+ CircleAnimotionUtils.getTransleX(40,0),mContext.getResources().getDimension(R.dimen.dimen_130dp)+CircleAnimotionUtils.getTransleY(40,0)) ;
        AnimatorSet animatorSet15 = PageAnimotionUtils.getAnimotion(orange1 , mContext.getResources().getDimension(R.dimen.dimen_600dp),mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_540dp) + CircleAnimotionUtils.getTransleX(70,0),mContext.getResources().getDimension(R.dimen.dimen_10dp) + CircleAnimotionUtils.getTransleY(70,0)) ;
        AnimatorSet animatorSet16 = PageAnimotionUtils.getAnimotion(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_0500dp)) ;

//        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;

        AnimatorSet animatorSet18 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_25dp),mContext.getResources().getDimension(R.dimen.dimen_450dp)) ;
        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_0500dp)) ;
        AnimatorSet animatorSet19 = PageAnimotionUtils.getAnimotionScale(main_two_viewgroup , 1f , 1f , false) ;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet13,animatorSet14,animatorSet15
                ,animatorSet16 , animatorSet17,animatorSet18,animatorSet19);
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                CircleAnimotionUtils.moveCirclePath(main_four_viewgroup ,40 , 3 , 1 , 10000 , false);
//                CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,70 , 3 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(orange1 ,70 , 3 , 0 , 10000  );
                CircleAnimotionUtils.moveCirclePath(green1 ,40 , 3 , 0 , 10000  );
                pageTwoEndAnimotion() ;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0280dp),mContext.getResources().getDimension(R.dimen.dimen_240dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup , pointF , 1 , 70 , false , 1 , true) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),1 , 70) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_two_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),1 , 70) ;
        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF , 1 , 40 , true , 0 , true) ;
        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image4 , 0, 0,1 , 0) ;
        ViewAnimotionBean viewAnimotionBean11  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),1 , 40) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        AnimatorSet animatorSet26 = new AnimatorSet();
        animatorSet26.playTogether(valueAnimator , valueAnimator1 ,valueAnimator2,valueAnimator3,valueAnimator4,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9,valueAnimator10
                ,valueAnimator11);
        animatorSet26.setDuration(15000) ;
        animatorSet26.setInterpolator(new DecelerateInterpolator());
        animatorSet26.start();

    }

    private void setPostionTwo() {
        main_four_viewgroup.setChildeVisibilty(View.VISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.INVISIBLE);

//        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
//        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
//        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
//        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
//        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_three_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_three_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_three_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_three_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_three_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_four_viewgroup.setMyScaleX(1f);
        main_four_viewgroup.setMyScaleY(1f);
    }

    private void initPostionThree() {
        main_two_viewgroup.putPosition(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0280dp) , mContext.getResources().getDimension(R.dimen.dimen_240dp) );
        orange1.putPosition( mContext.getResources().getDimension(R.dimen.dimen_540dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));
        green1.putPosition( mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_060dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));
        main_three_viewgroup.putPosition(main_three_viewgroup,mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp));

        main_four_viewgroup.putPosition(main_four_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp) , mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_four_viewgroup.putPosition(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_image4 , 0, 0) ;
        main_four_viewgroup.putPosition(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第二个页面动画结束的动画
     */
    public void pageTwoEndAnimotion() {
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(blue ,mContext.getResources().getDimension(R.dimen.dimen_0120dp),mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_060dp)+CircleAnimotionUtils.getTransleX(80,1),mContext.getResources().getDimension(R.dimen.dimen_10dp)+CircleAnimotionUtils.getTransleY(80,1)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(main_three_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_120dp), mContext.getResources().getDimension(R.dimen.dimen_580dp)+CircleAnimotionUtils.getTransleX(80,0), mContext.getResources().getDimension(R.dimen.dimen_120dp)+CircleAnimotionUtils.getTransleY(80,0)) ;
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet1, animatorSet2);
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                CircleAnimotionUtils.moveCirclePath(main_three_viewgroup ,80 , 3 , 0 , 10000 );
                CircleAnimotionUtils.moveCirclePath(blue ,80 , 3 , 1 , 10000 );
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 第三个页面动画
     */
    public void pageThree() {

        setPostionThree() ;

        initPostionFour();

        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_100dp)+ (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_035dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) - (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_110dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) - (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_175dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) - (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_130dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image4 , 0,  0) ;
        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_200dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;

        AnimatorSet animatorSet7 = PageAnimotionUtils.getAnimotion(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0250dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_280dp) + CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet8 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp) , mContext.getResources().getDimension(R.dimen.dimen_0240dp)) ;
        AnimatorSet animatorSet9 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_280dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        AnimatorSet animatorSet10 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        AnimatorSet animatorSet11 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_image4 , 0,mContext.getResources().getDimension(R.dimen.dimen_300dp) ) ;
        AnimatorSet animatorSet12 = PageAnimotionUtils.getAnimotion(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp)) ;

        AnimatorSet animatorSet13 = PageAnimotionUtils.getAnimotion(main_one_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_650dp), mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_530dp)+CircleAnimotionUtils.getTransleX(55,1), mContext.getResources().getDimension(R.dimen.dimen_130dp)+CircleAnimotionUtils.getTransleY(55,1)) ;

        AnimatorSet animatorSet14 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_0150dp),mContext.getResources().getDimension(R.dimen.dimen_30dp)) ;
        AnimatorSet animatorSet15 = PageAnimotionUtils.getAnimotion(green1 , mContext.getResources().getDimension(R.dimen.dimen_0100dp),mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(orange1 ,  mContext.getResources().getDimension(R.dimen.dimen_500dp),mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        AnimatorSet animatorSet16 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_500dp)) ;
//
        AnimatorSet animatorSet22 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_0340dp) , mContext.getResources().getDimension(R.dimen.dimen_90dp)) ;
        AnimatorSet animatorSet18 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_0340dp) , mContext.getResources().getDimension(R.dimen.dimen_80dp)) ;
        AnimatorSet animatorSet19 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_0200dp) , mContext.getResources().getDimension(R.dimen.dimen_050dp)) ;
        AnimatorSet animatorSet20 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_0300dp)) ;
        AnimatorSet animatorSet21 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_0200dp), mContext.getResources().getDimension(R.dimen.dimen_30dp)) ;

        AnimatorSet animatorSet23 = PageAnimotionUtils.getAnimotionScale(main_four_viewgroup , 1f , 1f , false) ;
////        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_120dp)) ;
//
//        AnimatorSet animatorSet18 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_25dp),mContext.getResources().getDimension(R.dimen.dimen_450dp)) ;
//        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_400dp), mContext.getResources().getDimension(R.dimen.dimen_0500dp)) ;


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet13,animatorSet14,animatorSet15
                ,animatorSet16,animatorSet17,animatorSet18,animatorSet19
                ,animatorSet20,animatorSet21,animatorSet22,animatorSet23);
        animatorSet.setInterpolator(new DecelerateInterpolator());
//        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                CircleAnimotionUtils.moveCirclePath(main_three_viewgroup ,40 , 3 , 1 , 10000 , false);
//
//                CircleAnimotionUtils.moveCirclePath(main_four_viewgroup ,55 , 3 , 0 , 10000 );

                CircleAnimotionUtils.moveCirclePath(main_one_viewgroup ,55 , 3 , 1 , 8000 );

                onThreeEndAnimotion() ;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , pointF , 1 , 40 , true , 0 , true) ;

        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean2  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean4  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image4 , 0, 0,0 , 0) ;
        ViewAnimotionBean viewAnimotionBean5  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),1 , 40) ;

        pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_0250dp),mContext.getResources().getDimension(R.dimen.dimen_280dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup , pointF , 1 , 55 , false , 1 , true) ;

        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),1 , 55) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp),1 , 55) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),1 , 55) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_image4 , 0, mContext.getResources().getDimension(R.dimen.dimen_300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),1 , 55) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        AnimatorSet animatorSet26 = new AnimatorSet();
        animatorSet26.playTogether(valueAnimator , valueAnimator1 ,valueAnimator2,valueAnimator3,valueAnimator4,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9,valueAnimator10
                ,valueAnimator11);
        animatorSet26.setDuration(15000) ;
        animatorSet26.setInterpolator(new DecelerateInterpolator());
        animatorSet26.start();
    }

    private void setPostionThree() {
        main_one_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_two_viewgroup.setChildeVisibilty(View.INVISIBLE);
        main_three_viewgroup.setChildeVisibilty(View.VISIBLE);

//        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_90dp , R.dimen.dimen_110dp) ;
//        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_10dp , R.dimen.dimen_175dp) ;
//        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_200dp , R.dimen.dimen_130dp) ;
//        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_200dp , R.dimen.dimen_200dp) ;
//        setViewPosition(main_one_viewgroup_image4 , 0 ,0 );

        setViewPosition(main_four_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_four_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_four_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_four_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_four_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_three_viewgroup.setMyScaleX(1f);
        main_three_viewgroup.setMyScaleY(1f);
    }

    private void initPostionFour() {
        main_four_viewgroup.putPosition(main_four_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_0250dp), mContext.getResources().getDimension(R.dimen.dimen_280dp) );
        main_one_viewgroup.putPosition(main_one_viewgroup,mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_010dp));
        green1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_550dp),mContext.getResources().getDimension(R.dimen.dimen_10dp));

        main_three_viewgroup.putPosition(main_three_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_three_viewgroup.putPosition(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_image4 , 0, 0) ;
        main_three_viewgroup.putPosition(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第三个页面动画结束的动画
     */
    public void onThreeEndAnimotion () {
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_0200dp),mContext.getResources().getDimension(R.dimen.dimen_10dp)+CircleAnimotionUtils.getTransleX(55,0),mContext.getResources().getDimension(R.dimen.dimen_010dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(green1 ,mContext.getResources().getDimension(R.dimen.dimen_550dp),mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_550dp)+CircleAnimotionUtils.getTransleX(55,1),mContext.getResources().getDimension(R.dimen.dimen_10dp)+CircleAnimotionUtils.getTransleY(55,1)) ;
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet1 , animatorSet2);
        animatorSet.start();

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                CircleAnimotionUtils.moveCirclePath(blue ,55 , 3 , 0 , 10000 );
                CircleAnimotionUtils.moveCirclePath(green1 ,55 , 3 , 1 , 10000 );
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    /**
     * 第四个页面动画
     */
    public void onFourAnimotion() {
        setPostionFour();
        initPositionFive();

        ViewAnimotionBean viewAnimotionBean  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_240dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean1  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp), mContext.getResources().getDimension(R.dimen.dimen_0240dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean2 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean3  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean4 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_image4 , 0,  mContext.getResources().getDimension(R.dimen.dimen_0300dp),0 , 0) ;
        ViewAnimotionBean viewAnimotionBean5 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_four_viewgroup_layout ,mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp),0 , 0) ;


        PointF pointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp),mContext.getResources().getDimension(R.dimen.dimen_035dp)) ;
        ViewAnimotionBean viewAnimotionBean6 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup , pointF , 1 , 40 , true , 0 , true) ;

        ViewAnimotionBean viewAnimotionBean7  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean8  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean9  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp),1 , 40) ;
        ViewAnimotionBean viewAnimotionBean10  = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_image4 , 0, 0,0 , 0) ;
        ViewAnimotionBean viewAnimotionBean11 = ViewAnimotionBeanUtils.getViewAnimotionBean(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp),1 , 40) ;

        ValueAnimator valueAnimator = MyAnimotionUtils.getValueAnimator(viewAnimotionBean, 15000 , null) ;
        ValueAnimator valueAnimator1 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean1, 15000 , null) ;
        ValueAnimator valueAnimator2= MyAnimotionUtils.getValueAnimator(viewAnimotionBean2, 15000 , null) ;
        ValueAnimator valueAnimator3 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean3, 15000 , null) ;
        ValueAnimator valueAnimator4 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean4, 15000 , null) ;
        ValueAnimator valueAnimator5 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean5, 15000 , null) ;
        ValueAnimator valueAnimator6 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean6, 15000 , null) ;
        ValueAnimator valueAnimator7 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean7, 15000 , null) ;
        ValueAnimator valueAnimator8 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean8, 15000 , null) ;
        ValueAnimator valueAnimator9 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean9, 15000 , null) ;
        ValueAnimator valueAnimator10 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean10, 15000 , null) ;
        ValueAnimator valueAnimator11 = MyAnimotionUtils.getValueAnimator(viewAnimotionBean11, 15000 , null) ;
        AnimatorSet animatorSet26 = new AnimatorSet();
        animatorSet26.playTogether(valueAnimator , valueAnimator1 ,valueAnimator2,valueAnimator3,valueAnimator4,valueAnimator5,valueAnimator6,valueAnimator7,valueAnimator8,valueAnimator9,valueAnimator10
                ,valueAnimator11);
        animatorSet26.setDuration(15000) ;
        animatorSet26.setInterpolator(new DecelerateInterpolator());
        animatorSet26.start();

        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(main_three_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0300dp), mContext.getResources().getDimension(R.dimen.dimen_240dp)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_280dp) , mContext.getResources().getDimension(R.dimen.dimen_0240dp)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_240dp) , mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_420dp) , mContext.getResources().getDimension(R.dimen.dimen_0200dp)) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_image4 , 0,  mContext.getResources().getDimension(R.dimen.dimen_0300dp)) ;
        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(main_four_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_420dp), mContext.getResources().getDimension(R.dimen.dimen_0120dp)) ;

        AnimatorSet animatorSet7 = PageAnimotionUtils.getAnimotion(main_one_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_100dp)+ (int)(-(1 / 2.0f) * 40) , mContext.getResources().getDimension(R.dimen.dimen_035dp)- (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet8 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_110dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet9 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_175dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet10 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_130dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;
        AnimatorSet animatorSet11 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_image4 , 0, 0) ;
        AnimatorSet animatorSet12 = PageAnimotionUtils.getAnimotion(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp)- (int)(-(1 / 2.0f) * 40), mContext.getResources().getDimension(R.dimen.dimen_200dp)+ (int)((Math.sqrt(3) / 2.0f)*40)) ;

        AnimatorSet animatorSet13 = PageAnimotionUtils.getAnimotion(blue ,mContext.getResources().getDimension(R.dimen.dimen_10dp),mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;
        AnimatorSet animatorSet14 = PageAnimotionUtils.getAnimotion(green1 , mContext.getResources().getDimension(R.dimen.dimen_550dp),mContext.getResources().getDimension(R.dimen.dimen_0100dp)) ;

        AnimatorSet animatorSet15 = PageAnimotionUtils.getAnimotion(main_four_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_0500dp), mContext.getResources().getDimension(R.dimen.dimen_500dp)) ;
        AnimatorSet animatorSet16 = PageAnimotionUtils.getAnimotionScale(main_three_viewgroup , 1f , 1f , false) ;

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800);
        animatorSet.playTogether(animatorSet13,animatorSet14,animatorSet15
                ,animatorSet16);
        animatorSet.setInterpolator(new DecelerateInterpolator());


        AnimatorSet animatorSet17 = PageAnimotionUtils.getAnimotion(purple , mContext.getResources().getDimension(R.dimen.dimen_0100dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet18 = PageAnimotionUtils.getAnimotion(purple1 , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_50dp) + CircleAnimotionUtils.getTransleX(30,1), mContext.getResources().getDimension(R.dimen.dimen_20dp)+CircleAnimotionUtils.getTransleY(30,1)) ;
        AnimatorSet animatorSet19 = PageAnimotionUtils.getAnimotion(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_530dp) + CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_300dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet20 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        AnimatorSet animatorSet21 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_220dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet22 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_0400dp) , mContext.getResources().getDimension(R.dimen.dimen_500dp) + CircleAnimotionUtils.getTransleX(55,1), mContext.getResources().getDimension(R.dimen.dimen_0250dp)+ CircleAnimotionUtils.getTransleY(55,1)) ;

        AnimatorSet animatorSet23 = new AnimatorSet();
        animatorSet23.setDuration(800);
        animatorSet23.playTogether(animatorSet17,animatorSet18,animatorSet19,animatorSet20,animatorSet21,animatorSet22);

        AnimatorSet animatorSet24 = new AnimatorSet() ;
        animatorSet24.playSequentially(animatorSet , animatorSet23);

//        animatorSet.setInterpolator(new OvershootInterpolator());
        animatorSet24.start();
        animatorSet24.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

//                CircleAnimotionUtils.moveCirclePath(main_one_viewgroup ,55 , 3 , 1 , 10000 , false);

//                onFourEnd() ;

                CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,55 , 3 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple1 ,30 , 2 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple ,55 , 2 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(blue ,55 , 3 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(deep_purple ,20 , 3 , 0 , 10000);
                initAllPosition();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void setPostionFour() {
        main_one_viewgroup.setChildeVisibilty(View.VISIBLE);
        setViewPosition(main_one_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_050dp) ;
        setViewPosition(main_one_viewgroup_image2 , R.dimen.dimen_0400dp , R.dimen.dimen_10dp) ;
        setViewPosition(main_one_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_035dp) ;
        setViewPosition(main_one_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_35dp) ;
        setViewPosition(main_one_viewgroup_image4 , 0 ,R.dimen.dimen_300dp );

        main_one_viewgroup.setMyScaleX(1f);
        main_one_viewgroup.setMyScaleY(1f);
    }

    public void initPositionFive() {
        main_two_viewgroup.putPosition(main_two_viewgroup ,mContext.getResources().getDimension(R.dimen.dimen_500dp) , mContext.getResources().getDimension(R.dimen.dimen_0250dp));
        purple1.putPosition(mContext.getResources().getDimension(R.dimen.dimen_50dp), mContext.getResources().getDimension(R.dimen.dimen_20dp));
        purple.putPosition(mContext.getResources().getDimension(R.dimen.dimen_030dp), mContext.getResources().getDimension(R.dimen.dimen_030dp));
        blue.putPosition(mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_220dp));
        deep_purple.putPosition(mContext.getResources().getDimension(R.dimen.dimen_530dp), mContext.getResources().getDimension(R.dimen.dimen_300dp));

        main_one_viewgroup.putPosition(main_one_viewgroup ,  mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        main_one_viewgroup.putPosition(main_one_viewgroup_image1 , mContext.getResources().getDimension(R.dimen.dimen_90dp) , mContext.getResources().getDimension(R.dimen.dimen_110dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image2 , mContext.getResources().getDimension(R.dimen.dimen_10dp) , mContext.getResources().getDimension(R.dimen.dimen_175dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image3 , mContext.getResources().getDimension(R.dimen.dimen_200dp) , mContext.getResources().getDimension(R.dimen.dimen_130dp)) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_image4 , 0, 0) ;
        main_one_viewgroup.putPosition(main_one_viewgroup_layout , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp)) ;
    }

    /**
     * 第四个页面动画结束的动画
     */
    public void onFourEnd () {
        AnimatorSet animatorSet1 = PageAnimotionUtils.getAnimotion(purple , mContext.getResources().getDimension(R.dimen.dimen_0100dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_030dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet2 = PageAnimotionUtils.getAnimotion(purple1 , mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_0100dp) , mContext.getResources().getDimension(R.dimen.dimen_50dp) + CircleAnimotionUtils.getTransleX(30,1), mContext.getResources().getDimension(R.dimen.dimen_20dp)+CircleAnimotionUtils.getTransleY(30,1)) ;
        AnimatorSet animatorSet3 = PageAnimotionUtils.getAnimotion(deep_purple , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_530dp) + CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_300dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet4 = PageAnimotionUtils.getAnimotion(orange , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp), mContext.getResources().getDimension(R.dimen.dimen_330dp)) ;
        AnimatorSet animatorSet5 = PageAnimotionUtils.getAnimotion(blue , mContext.getResources().getDimension(R.dimen.dimen_900dp), mContext.getResources().getDimension(R.dimen.dimen_600dp) , mContext.getResources().getDimension(R.dimen.dimen_580dp)+CircleAnimotionUtils.getTransleX(55,0), mContext.getResources().getDimension(R.dimen.dimen_220dp)+CircleAnimotionUtils.getTransleY(55,0)) ;
        AnimatorSet animatorSet6 = PageAnimotionUtils.getAnimotion(main_two_viewgroup , mContext.getResources().getDimension(R.dimen.dimen_600dp), mContext.getResources().getDimension(R.dimen.dimen_0400dp) , mContext.getResources().getDimension(R.dimen.dimen_500dp) + CircleAnimotionUtils.getTransleX(55,1), mContext.getResources().getDimension(R.dimen.dimen_0250dp)+ CircleAnimotionUtils.getTransleY(55,1)) ;

        AnimatorSet animatorSet8 = new AnimatorSet();
        animatorSet8.setDuration(800);
        animatorSet8.playTogether(animatorSet1,animatorSet2,animatorSet3,animatorSet4,animatorSet5,animatorSet6);

        AnimatorSet animatorSet7 = new AnimatorSet() ;
        animatorSet7.setDuration(800) ;
        animatorSet7.playTogether(animatorSet4,animatorSet5,animatorSet6);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800) ;
        animatorSet.playTogether(animatorSet7 , animatorSet8);

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.e("SOLON" , ">>>>onAnimationStart") ;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.e("SOLON" , ">>>>onAnimationEnd") ;
                CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,55 , 3 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple1 ,30 , 2 , 1 , 10000);
                CircleAnimotionUtils.moveCirclePath(purple ,55 , 2 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(blue ,55 , 3 , 0 , 10000);
                CircleAnimotionUtils.moveCirclePath(deep_purple ,20 , 3 , 0 , 10000);
                initAllPosition();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                Log.e("SOLON" , ">>>>onAnimationCancel") ;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                Log.e("SOLON" , ">>>>onAnimationRepeat") ;
            }
        });

        animatorSet.start();

    }

    /**
     * 重置一下每一个view的坐标
     */
    public void initAllPosition() {
//        setViewPosition(purple , R.dimen.dimen_030dp , R.dimen.dimen_030dp) ;
//        setViewPosition(purple1 , R.dimen.dimen_50dp , R.dimen.dimen_20dp) ;
//        setViewPosition(blue , R.dimen.dimen_580dp , R.dimen.dimen_220dp) ;
//        setViewPosition(deep_purple , R.dimen.dimen_500dp , R.dimen.dimen_330dp) ;
        setViewPosition(orange , R.dimen.dimen_580dp , R.dimen.dimen_330dp) ;
        setViewPosition(green1 , R.dimen.dimen_0100dp , R.dimen.dimen_0100dp) ;
        setViewPosition(orange1 , R.dimen.dimen_0100dp , R.dimen.dimen_0100dp) ;
        setViewPosition(main_four_viewgroup , R.dimen.dimen_650dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_three_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_50dp) ;
        setViewPosition(main_three_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_30dp) ;
        setViewPosition(main_three_viewgroup_image4 , 0 , R.dimen.dimen_300dp) ;
        setViewPosition(main_three_viewgroup_image1 , R.dimen.dimen_0340dp , R.dimen.dimen_090dp) ;
        setViewPosition(main_three_viewgroup_image2 , R.dimen.dimen_0340dp , R.dimen.dimen_80dp) ;
        setViewPosition(main_three_viewgroup , R.dimen.dimen_0300dp , R.dimen.dimen_240dp) ;
        setViewPosition(main_four_viewgroup_image1 , R.dimen.dimen_280dp , R.dimen.dimen_0240dp) ;
        setViewPosition(main_four_viewgroup_image2 , R.dimen.dimen_240dp , R.dimen.dimen_0100dp) ;
        setViewPosition(main_four_viewgroup_image3 , R.dimen.dimen_420dp , R.dimen.dimen_0200dp) ;
        setViewPosition(main_four_viewgroup_layout , R.dimen.dimen_420dp , R.dimen.dimen_0120dp) ;
        setViewPosition(main_four_viewgroup_image4 , 0 , R.dimen.dimen_0300dp) ;
//        setViewPosition(main_two_viewgroup , R.dimen.dimen_500dp , R.dimen.dimen_0250dp) ;
        setViewPosition(main_two_viewgroup_image3 , R.dimen.dimen_0200dp , R.dimen.dimen_370dp) ;
        setViewPosition(main_two_viewgroup_layout , R.dimen.dimen_0200dp , R.dimen.dimen_450dp) ;
        setViewPosition(main_two_viewgroup_image4 , 0 , R.dimen.dimen_300dp) ;
        setViewPosition(main_two_viewgroup_image1 , R.dimen.dimen_0200dp , R.dimen.dimen_200dp) ;
        setViewPosition(main_two_viewgroup_image2 , R.dimen.dimen_0100dp , R.dimen.dimen_300dp) ;
    }

    /**
     * 设置点的坐标
     * @param view
     * @param demisX
     * @param demisY
     */
    private void setViewPosition(View view , int demisX , int demisY) {
        view.setX(demisX==0?0:mContext.getResources().getDimension(demisX));
        view.setY(demisY==0?0:mContext.getResources().getDimension(demisY));
    }

    private void setViewScale(View view , float scaleX, float scaleY) {
        view.setScaleX(scaleX);
        view.setScaleY(scaleY);
    }

    /**
     * 初始化的第一个页面的抖动
     */
    public void circleView () {
        CircleAnimotionUtils.moveCirclePath(main_one_viewgroup ,70 , 3 , 1 , 10000);

        CircleAnimotionUtils.moveCirclePath(main_two_viewgroup ,40 , 3 ,0, 10000 , true);

        CircleAnimotionUtils.moveCirclePath(main_four_viewgroup ,80 , 3 ,0, 10000);

        CircleAnimotionUtils.moveCirclePath(blue ,80 , 3 , 1 , 9000);
        CircleAnimotionUtils.moveCirclePath(orange ,60 , 3 , 1 , 10000 );
        CircleAnimotionUtils.moveCirclePath(main_three_viewgroup ,80 , 3 , 1 , 9000 );

    }
}
