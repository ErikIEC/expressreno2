package com.nestle.express.frontend_ui.nurun.views;

import android.view.View;

/**
 * Created by jesserci on 16-10-11.
 */
public interface PositionInterface {

    void putPosition(View view, float x, float y);

    void putPosition(float x, float y);

    void setMyX(float addX);

    void setMyY(float addY);
}
