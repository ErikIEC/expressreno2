package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.widget.MyDrinkView;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by xumin on 16/7/8.
 */
public class AnimotionUtils {

    public static final int MAIN_SCREEN = 0 ;
    public static final int ANIMOTION_SHOCONTENT_ONE = 1 ;
    public static final int ANIMOTION_SHOCONTENT_TWO = 2 ;
    public static final int ANIMOTION_SHOCONTENT_THREE = 3 ;
    public static final int ANIMOTION_SHOCONTENT_FOUR = 4 ;
    public static final int ANIMOTION_DRINK_ONE = 5 ;
    public static final int ANIMOTION_DRINK_TWO = 6 ;
    public static final int ANIMOTION_DRINK_THREE = 7 ;
    public static final int ANIMOTION_DRINK_FOUR = 8 ;

    private int currentAnimotinType = MAIN_SCREEN ;

//    private Map<Integer,Float> xMap = new HashMap<Integer, Float>();
    private Map<Integer,Float> yMap = new HashMap<Integer, Float>();

    private Map<Integer,Float> xMap = new HashMap<Integer, Float>();
    private static AnimotionUtils animotionUtils = new AnimotionUtils();

    private boolean isPutValue = false ;

    private long animotionTime = 800 ;
    private long drinkAnimotionTime = 1000 ;

    private AnimotionUtils(){}

    public static AnimotionUtils getAnimotionUtils() {
        return animotionUtils;
    }

    public int getCurrentAnimotinType() {
        return currentAnimotinType;
    }

    public void setCurrentAnimotinType(int currentAnimotinType) {
        this.currentAnimotinType = currentAnimotinType;
    }

    public static AnimotionUtils getInstance() {
        return animotionUtils ;
    }

    public void putViewPosition(View view) {
        if (isPutValue) {
            return ;
        }
        if (view!= null) {
            xMap.put(view.getId() , view.getX()) ;
            yMap.put(view.getId() , view.getY()) ;
        }
    }

    public void setPutValue(boolean flag) {
        this.isPutValue = flag ;
    }

    /**
     * 大图动画
     * @param mContext
     * @param viewGroupLayout
     * @param container
     */
    public void animotionRight (Context mContext , final ViewGroupLayout viewGroupLayout , final AbsoluteLayout container , final ImageView backGroud , final int position) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),3.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),3.0f);
        Log.d("test" , "viewX:" + viewGroupLayout.getX() + "  ,viewY:" + viewGroupLayout.getY()) ;
//        float a = mContext.getResources().getDimension(R.dimen.dimen_020dp) -  viewGroupLayout.getY() + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//        float b = mContext.getResources().getDimension(R.dimen.dimen_100dp) -  viewGroupLayout.getX() + mContext.getResources().getDimension(R.dimen.dimen_135dp);
        float a = mContext.getResources().getDimension(R.dimen.dimen_115dp) - viewGroupLayout.getY() ;
        float b = mContext.getResources().getDimension(R.dimen.dimen_235dp) - viewGroupLayout.getX() ;
        Log.d("test" , "a:" + a + "  ,b:" + b) ;
        Log.d("test" , "x:" + (viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId())) + "  ,y:" + (viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()))) ;
//        float a = mContext.getResources().getDimension(R.dimen.dimen_020dp) -  yMap.get(viewGroupLayout.getId());
//        float b = mContext.getResources().getDimension(R.dimen.dimen_100dp) -  xMap.get(viewGroupLayout.getId());

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()),b );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ,a);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                viewGroupLayout.setChildViewAlp(fraction , true , position);
                backGroud.setAlpha(fraction);

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                viewGroupLayout.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                viewGroupLayout.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(backGroud);
                container.bringChildToFront(viewGroupLayout);
                viewGroupLayout.startTransilation(true , position) ;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("test","x6:" + viewGroupLayout.getX() + ",   " + viewGroupLayout.getY());
                int count = container.getChildCount() ;

                viewGroupLayout.showJieLouView(position);
                if (position == 0) {
                    setCurrentAnimotinType(ANIMOTION_SHOCONTENT_ONE);
                } else if (position == 1) {
                    setCurrentAnimotinType(ANIMOTION_SHOCONTENT_TWO);
                } else if (position == 2) {
                    setCurrentAnimotinType(ANIMOTION_SHOCONTENT_THREE);
                } else {
                    setCurrentAnimotinType(ANIMOTION_SHOCONTENT_FOUR);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

//        if (flag) {
//            animator.setRepeatCount(0);
//            animator.setRepeatMode(Animation.REVERSE);
//        } else {
//            animator.setRepeatCount(0);
//            animator.setRepeatMode(Animation.RESTART);
//        }


        animator.start();
    }

    /**
     * 右上角小图切换
     * @param mContext
     * @param imageView
     * @param container
     */
    public void animotionRightSmall (Context mContext , final ImageView imageView , final AbsoluteLayout container) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", imageView.getScaleX(),0.5f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", imageView.getScaleY(),0.5f);
        float a = mContext.getResources().getDimension(R.dimen.dimen_25dp) -  yMap.get(imageView.getId()) ;
        float b = mContext.getResources().getDimension(R.dimen.dimen_450dp) -  xMap.get(imageView.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",imageView.getX() - xMap.get(imageView.getId()), b );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", imageView.getY() - yMap.get(imageView.getId()) , a);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                float fraction = valueAnimator.getAnimatedFraction() ;
                if (fraction < 0.5f) {
                    imageView.setAlpha(1 - fraction);
                } else {
                    imageView.setImageResource(R.drawable.guanbi);
                    imageView.setAlpha(fraction);
                }

                imageView.setScaleX(animatorValueScaleX);
                imageView.setScaleY(animatorValueScaleY);

                imageView.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                imageView.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(imageView);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("juice","x1:" + imageView.getX() + ",   " + imageView.getY());
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    /**
     * 大图取消动画
     * @param mContext
     * @param viewGroupLayout
     * @param container
     */
    public void animotionRightCancel (Context mContext , final ViewGroupLayout viewGroupLayout , final AbsoluteLayout container , final ImageView backGroud , final int position) {
        Log.d("juice" , "xMap:"+ xMap.get(viewGroupLayout.getId())+",id:" + viewGroupLayout.getId() + ", x:" + viewGroupLayout.getX() + "y:"+viewGroupLayout.getY()) ;

        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.0f);
        float startX , startY  ;

//        if (position == 0) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_100dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_020dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//        } else if (position == 1) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_100dp) - mContext.getResources().getDimension(R.dimen.dimen_160dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_020dp) - mContext.getResources().getDimension(R.dimen.dimen_90dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//        } else if (position == 2) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_100dp) - mContext.getResources().getDimension(R.dimen.dimen_305dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_020dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//        } else {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_100dp) - mContext.getResources().getDimension(R.dimen.dimen_450dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_020dp) - mContext.getResources().getDimension(R.dimen.dimen_90dp) + mContext.getResources().getDimension(R.dimen.dimen_135dp);
//        }

        startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, 0f);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , 0f);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                viewGroupLayout.setChildViewAlp(1 - fraction , false , position);
                backGroud.setAlpha(1 - fraction);

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                viewGroupLayout.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_15dp)
                viewGroupLayout.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_15dp)

                Log.d("juice" , "transtlateX:" + animatorValueTranslationX) ;
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(viewGroupLayout);
                viewGroupLayout.startTransilation(false , position) ;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("juice","x6:" + viewGroupLayout.getX() + ",   " + viewGroupLayout.getY());
//                viewGroupLayout.showJieLouView();
                setCurrentAnimotinType(MAIN_SCREEN);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    /**
     * 小图取消动画
     * @param mContext
     * @param imageView
     * @param container
     */
    public void animotionRightSmallCancel (Context mContext , final ImageView imageView , final AbsoluteLayout container , final int position) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", 0.5f,1.0f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", 0.5f,1.0f);

        float startX , startY ;
//        if (position == 0) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_420dp) - mContext.getResources().getDimension(R.dimen.dimen_75dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_30dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//        } else if (position == 1) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_420dp) - mContext.getResources().getDimension(R.dimen.dimen_210dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_30dp) - mContext.getResources().getDimension(R.dimen.dimen_90dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//        } else if (position == 2) {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_420dp) - mContext.getResources().getDimension(R.dimen.dimen_355dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_30dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//        } else {
//            startX = mContext.getResources().getDimension(R.dimen.dimen_420dp) - mContext.getResources().getDimension(R.dimen.dimen_500dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//            startY = mContext.getResources().getDimension(R.dimen.dimen_30dp) - mContext.getResources().getDimension(R.dimen.dimen_90dp) - mContext.getResources().getDimension(R.dimen.dimen_15dp);
//        }

        startX = mContext.getResources().getDimension(R.dimen.dimen_450dp) - xMap.get(imageView.getId());
        startY = mContext.getResources().getDimension(R.dimen.dimen_25dp) - yMap.get(imageView.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, 0 );
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , 0 );

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int drawbleID = R.drawable.small_title_one ;
                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                float fraction = valueAnimator.getAnimatedFraction() ;
                if (fraction < 0.5f) {
                    imageView.setAlpha(1 - fraction);
                } else {

                    if (position == 0) {
                        drawbleID = R.drawable.small_title_one ;
                    } else if (position == 1) {
                        drawbleID = R.drawable.small_title_three ;
                    } else if (position == 2) {
                        drawbleID = R.drawable.small_title_two ;
                    } else {
                        drawbleID = R.drawable.small_title_four ;
                    }

                    imageView.setImageResource(drawbleID);
                    imageView.setAlpha(fraction);
                }

                imageView.setScaleX(animatorValueScaleX);
                imageView.setScaleY(animatorValueScaleY);

                imageView.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                imageView.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                container.bringChildToFront(imageView);
            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        animator.start();
    }

    public void animotionDrink (final Context mContext , final ViewGroupLayout viewGroupLayout , final MyDrinkView drinkView ,final ImageView image ,  final int position , final boolean isReapt , final View line) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),1.2f ,1.4f,1.2f , 1.4f , 1.2f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),1.2f ,1.4f,1.2f , 1.4f , 1.2f);
        float a , b , startX ,startY;
        startX = 0 ;startY = 0 ;
        if (position == 0) {
            a = mContext.getResources().getDimension(R.dimen.dimen_30dp) -  yMap.get(viewGroupLayout.getId()) ;
            b = mContext.getResources().getDimension(R.dimen.dimen_10dp) -  xMap.get(viewGroupLayout.getId()) ;
        } else if (position == 1) {
            a = mContext.getResources().getDimension(R.dimen.dimen_100dp) -  yMap.get(viewGroupLayout.getId()) ;
            b = mContext.getResources().getDimension(R.dimen.dimen_160dp) -  xMap.get(viewGroupLayout.getId())  ;
        } else if (position == 2) {
            a = mContext.getResources().getDimension(R.dimen.dimen_30dp) -  yMap.get(viewGroupLayout.getId()) ;
            b = mContext.getResources().getDimension(R.dimen.dimen_300dp) -  xMap.get(viewGroupLayout.getId()) ;
        } else {
            a = mContext.getResources().getDimension(R.dimen.dimen_100dp) -  yMap.get(viewGroupLayout.getId());
            b = mContext.getResources().getDimension(R.dimen.dimen_450dp) -  xMap.get(viewGroupLayout.getId()) ;
        }

        Log.d("juice" , "x9:" + viewGroupLayout.getX() + ",y9:" + viewGroupLayout.getY() + ",  " + xMap.get(viewGroupLayout.getId())) ;
        startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, b);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , a);
        ValueAnimator animator ;
        if (!isReapt) {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        } else {
            animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        }
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(drinkAnimotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                if (!isReapt) {
                    float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                    float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                    viewGroupLayout.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                    viewGroupLayout.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                    drinkView.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                    drinkView.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

//                    image.setTranslationX(animatorValueTranslationX);
//                    image.setTranslationY(animatorValueTranslationY);


                    line.setAlpha(fraction);
                    Log.d("juice" , "scaleX:" + animatorValueScaleX + ", x :" + animatorValueTranslationX) ;
                }

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

                drinkView.setScaleX(animatorValueScaleX);
                drinkView.setScaleY(animatorValueScaleY);

//                image.setScaleX(animatorValueScaleX);
//                image.setScaleY(animatorValueScaleY);


//                drinkView.setDrink(fraction , true);

            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                drinkView.setAlpha(1.0f);
                if (!(position == 0 && currentAnimotinType == ANIMOTION_DRINK_ONE
                        || position == 1 && currentAnimotinType == ANIMOTION_DRINK_TWO
                        || position == 2 && currentAnimotinType == ANIMOTION_DRINK_THREE
                        || position == 3 && currentAnimotinType == ANIMOTION_DRINK_FOUR)) {
                    viewGroupLayout.startDrinkTransilation(true , position);
                }
                drinkView.startAnim();

//                if (position == 0) {
//                    setCurrentAnimotinType(ANIMOTION_DRINK_ONE);
//                } else if (position == 1) {
//                    setCurrentAnimotinType(ANIMOTION_DRINK_TWO);
//                } else if (position == 2) {
//                    setCurrentAnimotinType(ANIMOTION_DRINK_THREE);
//                } else {
//                    setCurrentAnimotinType(ANIMOTION_DRINK_FOUR);
//                }

            }

            @Override
            public void onAnimationEnd(Animator animator) {
//                drinkView.setAlpha(0.0f);
                if (position == 0) {
                    setCurrentAnimotinType(ANIMOTION_DRINK_ONE);
                } else if (position == 1) {
                    setCurrentAnimotinType(ANIMOTION_DRINK_TWO);
                } else if (position == 2) {
                    setCurrentAnimotinType(ANIMOTION_DRINK_THREE);
                } else {
                    setCurrentAnimotinType(ANIMOTION_DRINK_FOUR);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    public void animotionDrinkOther (final Context mContext , final ViewGroupLayout viewGroupLayout , final MyDrinkView drinkView , final  ImageView image , final int position , final View line) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", viewGroupLayout.getScaleX(),0.8f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", viewGroupLayout.getScaleY(),0.8f);

        float a , b ;
        if (position == 0) {
            if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }

        } else if (position == 1) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            }
        } else if (position == 2) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }
        } else {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }
        }

        float startX = viewGroupLayout.getX() - xMap.get(viewGroupLayout.getId()) ;
        float startY = viewGroupLayout.getY() - yMap.get(viewGroupLayout.getId()) ;
//        float a = mContext.getResources().getDimension(R.dimen.dimen_10dp) -  viewGroupLayout.getY() ;
//        float b = mContext.getResources().getDimension(R.dimen.dimen_10dp) -  viewGroupLayout.getX() ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, a);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , b);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                viewGroupLayout.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                viewGroupLayout.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                viewGroupLayout.setScaleX(animatorValueScaleX);
                viewGroupLayout.setScaleY(animatorValueScaleY);

//                image.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
//                image.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

//                image.setScaleX(animatorValueScaleX);
//                image.setScaleY(animatorValueScaleY);

                drinkView.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                drinkView.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                drinkView.setScaleX(animatorValueScaleX);
                drinkView.setScaleY(animatorValueScaleY);

                line.setAlpha(1 - fraction);

            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                Log.d("juice" , "onAnimationStart ------>") ;
                if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false , 0);
                } else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false , 1);
                } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                    viewGroupLayout.startDrinkTransilation(false , 2);
                } else {
                    viewGroupLayout.startDrinkTransilation(false , 3);
                }

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }

    public void resetView(final View view) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", view.getScaleX(),1f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", view.getScaleY(),1f);

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",view.getX() - xMap.get(view.getId()), 0);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", view.getY() - yMap.get(view.getId()) , 0);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);

        animator.setDuration(animotionTime / 2) ;

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                view.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                view.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                view.setScaleX(animatorValueScaleX);
                view.setScaleY(animatorValueScaleY);
            }
        });

        animator.start();
    }

    public void animotionDrinkSmall (Context context , final ImageView imageView , int position) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", imageView.getScaleX(),1.0f ,1.2f,1.4f , 1.2f , 0.8f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", imageView.getScaleY(),1.0f ,1.2f,1.4f , 1.2f , 0.8f);

        float a , b , startX ,startY;
        startX = 0 ;startY = 0 ;
        if (position == 0) {
            a = context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = context.getResources().getDimension(R.dimen.dimen_110dp) -  xMap.get(imageView.getId()) ;
        } else if (position == 1) {
            a = context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId()) ;
            b = context.getResources().getDimension(R.dimen.dimen_250dp) -  xMap.get(imageView.getId())  ;
        } else if (position == 2) {
            a = context.getResources().getDimension(R.dimen.dimen_15dp) -  yMap.get(imageView.getId()) ;
            b = context.getResources().getDimension(R.dimen.dimen_400dp) -  xMap.get(imageView.getId()) ;
        } else {
            a = context.getResources().getDimension(R.dimen.dimen_80dp) -  yMap.get(imageView.getId());
            b = context.getResources().getDimension(R.dimen.dimen_550dp) -  xMap.get(imageView.getId()) ;
        }

        startX = imageView.getX() - xMap.get(imageView.getId()) ;
        startY = imageView.getY() - yMap.get(imageView.getId()) ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, b);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , a);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);

        animator.setDuration(animotionTime / 2) ;

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");

                imageView.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                imageView.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                imageView.setScaleX(animatorValueScaleX);
                imageView.setScaleY(animatorValueScaleY);
            }
        });

        animator.start();
    }

    public void animotionDrinkSmallCancel(Context mContext , final ViewGroupLayout viewGroupLayout , final ImageView image , int position) {
        PropertyValuesHolder mPropertyValuesHolderScaleX = PropertyValuesHolder.ofFloat("scaleX", image.getScaleX(),0.8f);
        PropertyValuesHolder mPropertyValuesHolderScaleY = PropertyValuesHolder.ofFloat("scaleY", image.getScaleY(),0.8f);

        float a , b ;
        if (position == 0) {
            if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }

        } else if (position == 1) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else if (viewGroupLayout.getId() == R.id.main_three_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
                b = 0 ;
            }
        } else if (position == 2) {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_20dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_10dp) ;
            }
        } else {
            if (viewGroupLayout.getId() == R.id.main_one_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }else if (viewGroupLayout.getId() == R.id.main_two_viewgroup) {
                a = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
                b = 0 ;
            } else {
                a = mContext.getResources().getDimension(R.dimen.dimen_020dp) ;
                b = mContext.getResources().getDimension(R.dimen.dimen_010dp) ;
            }
        }

        float startX = image.getX() - xMap.get(image.getId()) ;
        float startY = image.getY() - yMap.get(image.getId()) ;
//        float a = mContext.getResources().getDimension(R.dimen.dimen_10dp) -  viewGroupLayout.getY() ;
//        float b = mContext.getResources().getDimension(R.dimen.dimen_10dp) -  viewGroupLayout.getX() ;

        PropertyValuesHolder mPropertyValuesHolderTranslationX = PropertyValuesHolder.ofFloat("translationX",startX, a);
        PropertyValuesHolder mPropertyValuesHolderTranslationY = PropertyValuesHolder.ofFloat("translationY", startY , b);

        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderTranslationX,mPropertyValuesHolderTranslationY,mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
//        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(mPropertyValuesHolderScaleX,mPropertyValuesHolderScaleY);
        animator.setDuration(animotionTime) ;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float fraction = valueAnimator.getAnimatedFraction() ;

                float animatorValueScaleX =  (float) valueAnimator.getAnimatedValue("scaleX");
                float animatorValueScaleY = (float) valueAnimator.getAnimatedValue("scaleY");
                float animatorValueTranslationX =  (float) valueAnimator.getAnimatedValue("translationX");
                float animatorValueTranslationY = (float) valueAnimator.getAnimatedValue("translationY");


                image.setTranslationX(animatorValueTranslationX);  //x最终变成mContext.getResources().getDimension(R.dimen.dimen_100dp)
                image.setTranslationY(animatorValueTranslationY);//Y最终变成mContext.getResources().getDimension(R.dimen.dimen_020dp)

                image.setScaleX(animatorValueScaleX);
                image.setScaleY(animatorValueScaleY);
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        animator.start();
    }
}
