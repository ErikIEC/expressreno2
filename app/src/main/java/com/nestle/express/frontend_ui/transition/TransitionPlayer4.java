package com.nestle.express.frontend_ui.transition;

import android.animation.ValueAnimator;
import android.os.Build;
import android.view.animation.LinearInterpolator;

import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.ChangeBounds;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.Fade;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.Transition;
import com.nestle.express.frontend_ui.transitions.com.transitionseverywhere.TransitionSet;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *Created by xumin on 16/6/7.
 */
public class TransitionPlayer4 extends TransitionSet {
    private LinkedHashMap<Transition, ArrayList<ValueAnimator>> animMap = new LinkedHashMap<>();
    public final ChangeBounds changeBounds = new ChangeBounds();
    public final ChangeAlpha changeAlpha = new ChangeAlpha();
    public final ChangeRotate changeRotate = new ChangeRotate();
    public final ChangeScale changeScale = new ChangeScale();
    public final ChangeTransition changeTransition = new ChangeTransition();
    public final ChangeBackground changeBackground = new ChangeBackground();
    public final ChangeTextColor changeTextColor = new ChangeTextColor();
    public final Fade fadeOut = new Fade(Fade.OUT);
    public final Fade fadeIn = new Fade(Fade.IN);

    public TransitionPlayer4() {

        setOrdering(TransitionSet.ORDERING_TOGETHER);
        setDuration(800);

//        addTransition(new ChangeTransform()) ;

        addTransition(changeBounds);
        addTransition(changeAlpha);
        addTransition(changeRotate);
        addTransition(changeScale);
//        addTransition(changeTransition);
//        addTransition(changeBackground);
//        addTransition(changeTextColor);

//        addTransition(fadeOut);
//        addTransition(fadeIn);

        setInterpolator(new LinearInterpolator());


//        setInterpolator(new BaserInterpolator(0.15f, 0.89f,0.67f, 0.14f));
    }

    public ArrayList<ValueAnimator> getAllAnimator(){
        ArrayList<ValueAnimator> animators = new ArrayList<>();
        for(ArrayList<ValueAnimator> value : animMap.values()){
            animators.addAll(value);
        }
        return animators;
    }

    public void setCurrentFraction(float fraction){
        if(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP){
            for (ValueAnimator valueAnimator : getAllAnimator()) {
//                valueAnimator.setCurrentFraction(fraction);
            }
        }else{
            setCurrentPlayTime((long) (fraction * getDuration()));
        }
    }

    public void setCurrentPlayTime(long playTime){
        for (ValueAnimator valueAnimator : getAllAnimator()) {
            playTime -= valueAnimator.getStartDelay();
            if(playTime<0) playTime=0;
            valueAnimator.setCurrentPlayTime(playTime);
        }
    }

}
