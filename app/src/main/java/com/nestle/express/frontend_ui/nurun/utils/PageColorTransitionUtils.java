package com.nestle.express.frontend_ui.nurun.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;

import com.nestle.express.frontend_ui.R;
import com.nestle.express.frontend_ui.nurun.views.ProductCircleViewGroup;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jesserci on 16-10-06.
 */
public class PageColorTransitionUtils {

    private Context mContext;

    private AnimatorSet mMainAnimatorSet;
    private AnimatorSet mMainTransitionAnimatorSet;
    private AnimatorSet mMainCircleAnimatorSet;
    private boolean mIsSmallCircleAnimationStarted = false;
    private View mMainView;


    private Map<PRODUCT_KEY, ProductViewHolder> mProductViewHolders = new HashMap<PRODUCT_KEY, ProductViewHolder>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableBackgrounds = new HashMap<PRODUCT_KEY, Drawable>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableGlass = new HashMap<PRODUCT_KEY, Drawable>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableFruits = new HashMap<PRODUCT_KEY, Drawable>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableTitles = new HashMap<PRODUCT_KEY, Drawable>();
    private Map<PRODUCT_KEY, Drawable> mProductDrawableSubtitles = new HashMap<PRODUCT_KEY, Drawable>();

    private TransitionDrawable mTransitionDrawable;
    private TransitionDrawable mTransitionJuice;
    private TransitionDrawable mTransitionFruit;
    private TransitionDrawable mTransitionTitle;
    private TransitionDrawable mTransitionSubtitle;


    private enum PRODUCT_KEY {
        PRODUCT_ONE,
        PRODUCT_TWO,
        PRODUCT_THREE,
        PRODUCT_FOUR
    }

    class ProductViewHolder {
        public ProductCircleViewGroup mProductViewGroup;
        public ImageView mProductJuice;
        public ImageView mProductFruit;
        public ImageView mProductTitle;
        public AbsoluteLayout mProductLayout;
        public ImageView mProductSubtitle;
        public AbsoluteLayout mChildLayout;

        ProductViewHolder(ProductCircleViewGroup productCircleViewGroup, ImageView productJuice, ImageView productFruit, ImageView productTitle, AbsoluteLayout productLayout, ImageView productSubtitle) {
            mProductViewGroup = productCircleViewGroup;
            mProductJuice = productJuice;
            mProductFruit = productFruit;
            mProductTitle = productTitle;
            mProductLayout = productLayout;
            mProductSubtitle = productSubtitle;
            mChildLayout = (AbsoluteLayout) mProductViewGroup.getChildAt(0);
        }
    }
    private int transitionTime = 800;
    private PointF mComingUpProductPrositionPointF;
    private PointF mEnteringProductPositionPointF;
    private PointF mCenterProductPositionPointF;
    private PointF mExitingProductPositionPointF;
    private PointF mCenterProductJuicePointF;
    private PointF mCenterProductFruitPointF;
    private PointF mCenterProductTitlePointF;
    private PointF mCenterProductLayoutPointF;
    private PointF mCenterProductSubtitlePointF;

    private enum Corner {TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT}

    public PageColorTransitionUtils(Context context, View view) {
        mContext = context;
        mMainView = view;
        initView(view);
        initCenterProductPointF();
    }

    private void initView(View view) {
        //Product one
        ProductViewHolder productOneViewHolder = new ProductViewHolder(
                (ProductCircleViewGroup) view.findViewById(R.id.product_one_viewgroup),
                (ImageView) view.findViewById(R.id.product_one_juice),
                (ImageView) view.findViewById(R.id.product_one_fruit),
                (ImageView) view.findViewById(R.id.product_one_title),
                (AbsoluteLayout) view.findViewById(R.id.product_one_layout),
                (ImageView) view.findViewById(R.id.product_one_subtitle));
        mProductViewHolders.put(PRODUCT_KEY.PRODUCT_ONE, productOneViewHolder);

//        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, R.drawable.shape_center_blue));
//        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, R.drawable.shape_center_two));
//        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, R.drawable.shape_center_three));
//        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, R.drawable.shape_center_four));
//
        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, android.R.color.transparent));
        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, android.R.color.transparent));
        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, android.R.color.transparent));
        mProductDrawableBackgrounds.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, android.R.color.transparent));

        mProductDrawableGlass.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, R.drawable.juice_beizi_orange));
        mProductDrawableGlass.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, R.drawable.juice_beizi_putao));
        mProductDrawableGlass.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, R.drawable.juice_beizi_lingmeng));
        mProductDrawableGlass.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, R.drawable.juice_beizi_yezi));

        mProductDrawableFruits.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, R.drawable.juice_guo_orange));
        mProductDrawableFruits.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, R.drawable.juice_guo_putao));
        mProductDrawableFruits.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, R.drawable.juice_guo_lingmeng));
        mProductDrawableFruits.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, R.drawable.juice_guo_yezi));

        mProductDrawableTitles.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, R.drawable.title_one));
        mProductDrawableTitles.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, R.drawable.title_two));
        mProductDrawableTitles.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, R.drawable.title_one));
        mProductDrawableTitles.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, R.drawable.title_two));

        mProductDrawableSubtitles.put(PRODUCT_KEY.PRODUCT_ONE, ContextCompat.getDrawable(mContext, R.drawable.wenan));
        mProductDrawableSubtitles.put(PRODUCT_KEY.PRODUCT_TWO, ContextCompat.getDrawable(mContext, R.drawable.wenan04));
        mProductDrawableSubtitles.put(PRODUCT_KEY.PRODUCT_THREE, ContextCompat.getDrawable(mContext, R.drawable.wenan03));
        mProductDrawableSubtitles.put(PRODUCT_KEY.PRODUCT_FOUR, ContextCompat.getDrawable(mContext, R.drawable.wenan05));
    }

    private void initMainAnimatorSet() {
        mMainAnimatorSet = new AnimatorSet();
        //mMainAnimatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        mMainTransitionAnimatorSet = new AnimatorSet();
        mMainTransitionAnimatorSet.setDuration(800);

        mMainCircleAnimatorSet = new AnimatorSet();
        //mMainCircleAnimatorSet.setDuration(7000);
    }



    public void transitionToProduct(final PRODUCT_KEY comingUpProductKey, PRODUCT_KEY enteringProductKey, Corner enteringCorner, final PRODUCT_KEY centeringProductKey,
                                    final PRODUCT_KEY exitingProductKey, Corner exitingCorner) {
        initMainAnimatorSet();

        Drawable[] drawables = new Drawable[2];
        Drawable[] juices = new Drawable[2];
        Drawable[] fruits = new Drawable[2];
        Drawable[] titles = new Drawable[2];
        Drawable[] subtitles = new Drawable[2];
        switch (centeringProductKey){
            case PRODUCT_ONE:

                drawables[0] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_FOUR);
                drawables[1] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_ONE);
                mTransitionDrawable = new TransitionDrawable(drawables);
                mTransitionDrawable.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductViewGroup.setBackground(mTransitionDrawable);
                mTransitionDrawable.startTransition(transitionTime);

                juices[0] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_FOUR);
                juices[1] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_ONE);
                mTransitionJuice = new TransitionDrawable(juices);
                mTransitionJuice.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductJuice.setImageDrawable(mTransitionJuice);
                mTransitionJuice.startTransition(transitionTime);

                fruits[0] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_FOUR);
                fruits[1] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_ONE);
                mTransitionFruit = new TransitionDrawable(fruits);
                mTransitionFruit.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductFruit.setImageDrawable(mTransitionFruit);
                mTransitionFruit.startTransition(transitionTime);

                titles[0] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_FOUR);
                titles[1] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_ONE);
                mTransitionTitle = new TransitionDrawable(titles);
                mTransitionTitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductTitle.setImageDrawable(mTransitionTitle);
                mTransitionTitle.startTransition(transitionTime);

                subtitles[0] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_FOUR);
                subtitles[1] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_ONE);
                mTransitionSubtitle = new TransitionDrawable(subtitles);
                mTransitionSubtitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductSubtitle.setImageDrawable(mTransitionSubtitle);
                mTransitionSubtitle.startTransition(transitionTime);

                AnimationDrawable animationDrawable = (AnimationDrawable)((TransitionDrawable)mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductFruit.getDrawable()).getDrawable(1);
                animationDrawable.start();
                break;
            case PRODUCT_TWO:
                drawables[0] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_ONE);
                drawables[1] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_TWO);
                mTransitionDrawable = new TransitionDrawable(drawables);
                mTransitionDrawable.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductViewGroup.setBackground(mTransitionDrawable);
                mTransitionDrawable.startTransition(transitionTime);

                juices[0] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_ONE);
                juices[1] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_TWO);
                mTransitionJuice = new TransitionDrawable(juices);
                mTransitionJuice.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductJuice.setImageDrawable(mTransitionJuice);
                mTransitionJuice.startTransition(transitionTime);

                fruits[0] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_ONE);
                fruits[1] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_TWO);
                mTransitionFruit = new TransitionDrawable(fruits);
                mTransitionFruit.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductFruit.setImageDrawable(mTransitionFruit);
                mTransitionFruit.startTransition(transitionTime);


                titles[0] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_ONE);
                titles[1] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_TWO);
                mTransitionTitle = new TransitionDrawable(titles);
                mTransitionTitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductTitle.setImageDrawable(mTransitionTitle);
                mTransitionTitle.startTransition(transitionTime);

                subtitles[0] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_ONE);
                subtitles[1] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_TWO);
                mTransitionSubtitle = new TransitionDrawable(subtitles);
                mTransitionSubtitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductSubtitle.setImageDrawable(mTransitionSubtitle);
                mTransitionSubtitle.startTransition(transitionTime);

                break;
            case PRODUCT_THREE:
                drawables[0] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_TWO);
                drawables[1] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_THREE);
                mTransitionDrawable = new TransitionDrawable(drawables);
                mTransitionDrawable.setCrossFadeEnabled(true);

                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductViewGroup.setBackground(mTransitionDrawable);
                mTransitionDrawable.startTransition(transitionTime);

                juices[0] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_TWO);
                juices[1] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_THREE);
                mTransitionJuice = new TransitionDrawable(juices);
                mTransitionJuice.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductJuice.setImageDrawable(mTransitionJuice);
                mTransitionJuice.startTransition(transitionTime);

                fruits[0] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_TWO);
                fruits[1] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_THREE);
                mTransitionFruit = new TransitionDrawable(fruits);
                mTransitionFruit.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductFruit.setImageDrawable(mTransitionFruit);
                mTransitionFruit.startTransition(transitionTime);


                titles[0] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_TWO);
                titles[1] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_THREE);
                mTransitionTitle = new TransitionDrawable(titles);
                mTransitionTitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductTitle.setImageDrawable(mTransitionTitle);
                mTransitionTitle.startTransition(transitionTime);

                subtitles[0] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_TWO);
                subtitles[1] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_THREE);
                mTransitionSubtitle = new TransitionDrawable(subtitles);
                mTransitionSubtitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductSubtitle.setImageDrawable(mTransitionSubtitle);
                mTransitionSubtitle.startTransition(transitionTime);


                break;
            case PRODUCT_FOUR:
                drawables[0] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_THREE);
                drawables[1] = mProductDrawableBackgrounds.get(PRODUCT_KEY.PRODUCT_FOUR);
                mTransitionDrawable = new TransitionDrawable(drawables);
                mTransitionDrawable.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductViewGroup.setBackground(mTransitionDrawable);
                mTransitionDrawable.startTransition(transitionTime);

                juices[0] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_THREE);
                juices[1] = mProductDrawableGlass.get(PRODUCT_KEY.PRODUCT_FOUR);
                mTransitionJuice = new TransitionDrawable(juices);
                mTransitionJuice.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductJuice.setImageDrawable(mTransitionJuice);
                mTransitionJuice.startTransition(transitionTime);

                fruits[0] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_THREE);
                fruits[1] = mProductDrawableFruits.get(PRODUCT_KEY.PRODUCT_FOUR);
                mTransitionFruit = new TransitionDrawable(fruits);
                mTransitionFruit.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductFruit.setImageDrawable(mTransitionFruit);
                mTransitionFruit.startTransition(transitionTime);


                titles[0] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_THREE);
                titles[1] = mProductDrawableTitles.get(PRODUCT_KEY.PRODUCT_FOUR);
                mTransitionTitle = new TransitionDrawable(titles);
                mTransitionTitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductTitle.setImageDrawable(mTransitionTitle);
                mTransitionTitle.startTransition(transitionTime);

                subtitles[0] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_THREE);
                subtitles[1] = mProductDrawableSubtitles.get(PRODUCT_KEY.PRODUCT_FOUR);
                mTransitionSubtitle = new TransitionDrawable(subtitles);
                mTransitionSubtitle.setCrossFadeEnabled(true);
                mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductSubtitle.setImageDrawable(mTransitionSubtitle);
                mTransitionSubtitle.startTransition(transitionTime);

                break;
        }

        ObjectAnimator colorFade = ObjectAnimator.ofObject(mProductViewHolders.get(PRODUCT_KEY.PRODUCT_ONE).mProductViewGroup, "backgroundColor", new ArgbEvaluator(), Color.argb(255, 255, 255, 255), 0xff000000);
        colorFade.setDuration(800);
        //colorFade.start();

//        mMainAnimatorSet.playSequentially(
//                colorFade
////                ,
////                mMainTransitionAnimatorSet
//
//        );

        mMainAnimatorSet.start();

    }

    public void transitionToFirstProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_THREE, PRODUCT_KEY.PRODUCT_TWO, Corner.BOTTOM_RIGHT, PRODUCT_KEY.PRODUCT_ONE, PRODUCT_KEY.PRODUCT_FOUR, Corner.BOTTOM_LEFT);
    }

    public void transitionToSecondProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_FOUR, PRODUCT_KEY.PRODUCT_THREE, Corner.TOP_RIGHT, PRODUCT_KEY.PRODUCT_TWO, PRODUCT_KEY.PRODUCT_ONE, Corner.TOP_LEFT);
    }

    public void transitionToThirdProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_ONE, PRODUCT_KEY.PRODUCT_FOUR, Corner.BOTTOM_RIGHT, PRODUCT_KEY.PRODUCT_THREE, PRODUCT_KEY.PRODUCT_TWO, Corner.BOTTOM_LEFT);
    }

    public void transitionToFourthProduct() {
        transitionToProduct(PRODUCT_KEY.PRODUCT_TWO, PRODUCT_KEY.PRODUCT_ONE, Corner.TOP_RIGHT, PRODUCT_KEY.PRODUCT_FOUR, PRODUCT_KEY.PRODUCT_THREE, Corner.TOP_LEFT);
    }

    private void animateImageView(final View imgView, final boolean startUp, final int duration) {
        final ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imgView, "translationY", startUp ? 0 : mMainView.getMeasuredHeight() - imgView.getMeasuredHeight(), startUp ? mMainView.getMeasuredHeight() - imgView.getMeasuredHeight() : 0);
        //final ObjectAnimator objectAnimatorx = ObjectAnimator.ofFloat(imgView, "translationX", startUp ? 0 : mMainView.getMeasuredWidth() - imgView.getMeasuredWidth(), startUp ? mMainView.getMeasuredWidth() - imgView.getMeasuredWidth() : 0);
        objectAnimator.setDuration(duration);
//        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                imgView.clearAnimation();
                objectAnimator.removeAllListeners();
                animateImageView(imgView, !startUp, duration);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }


    private void initCenterProductPointF() {
        mCenterProductPositionPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_100dp), mContext.getResources().getDimension(R.dimen.dimen_035dp));
        mCenterProductJuicePointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_90dp), mContext.getResources().getDimension(R.dimen.dimen_110dp));
        mCenterProductFruitPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_10dp), mContext.getResources().getDimension(R.dimen.dimen_175dp));
        mCenterProductTitlePointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_130dp));
        mCenterProductLayoutPointF = new PointF(mContext.getResources().getDimension(R.dimen.dimen_200dp), mContext.getResources().getDimension(R.dimen.dimen_200dp));
        mCenterProductSubtitlePointF = new PointF(0, 0);
    }
}
