package com.nestle.express.frontend_ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nestle.express.communications.protocol_layer.events.EventDispense;
import com.nestle.express.frontend_setup.activity.ServiceCenterActivity;
import com.nestle.express.frontend_setup.utils.SharedPreferencesUtil;
import com.nestle.express.frontend_ui.nurun.activities.LocaleActivity;
import com.nestle.express.frontend_ui.nurun.utils.AccessibilityUtils;
import com.nestle.express.frontend_ui.nurun.utils.AnimotionUtils;
import com.nestle.express.frontend_ui.nurun.utils.PreferencesHelper;
import com.nestle.express.frontend_ui.nurun.utils.ViewHelper;
import com.nestle.express.frontend_ui.util.OnPourListener;
import com.nestle.express.iec_backend.ExpressApp;
import com.nestle.express.iec_backend.application.ApplicationManager;
import com.nestle.express.iec_backend.application.structure.RinseRequestCallback;
import com.nestle.express.iec_backend.application.structure.RinseStatus;
import com.nestle.express.iec_backend.machine.MachineManager;
import com.nestle.express.iec_backend.product.ProductManager;
import com.nestle.express.iec_backend.product.structure.NestleProduct;
import com.nestle.express.iec_backend.product.structure.ProductSlot;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by xumin on 16/8/15.
 */
public class MainActivity extends LocaleActivity {

    public static final int DELAY_MILLIS_RINSE = 2200;
    public static final int DELAY_MILLIS_DISPENSE = 1600;
    private int mDelayMillis = DELAY_MILLIS_DISPENSE;
    /**
     * Views
     */
    private ViewGroup mSceneRoot;
    private ViewHelper mViewHelper;

    private TextView mEnglishButton, mFrenchButton, mSpanishButton , mHomeButton, mBackButton;

    private ImageView mDispenseLockedOutImageView, mFakeLoading;

    private ImageView mRinseRequiredImageView;
    private ImageView mRinseCompleteImageView;

    private String mCurrentLang = null;

    private boolean isWentToSettingsScreen = false;

    private boolean isDispenseLockedOut = false;
    private boolean rinseRequired = false;

    /**
     * Handler and transition
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == RETURN_MAIN) {
                mMachineManager.exitDispenseMode();

                changeToMain();
            } else if (msg.what == GO_MAIN) {
                mMachineManager.exitDispenseMode();

                Intent intent = new Intent(MainActivity.this, ActivityUI.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }else if (msg.what == RETURN_DISPENSE) {
                mMachineManager.exitDispenseMode();

                changeToDispense();
                //goToSettings();
            }
        }
    };


    private static final int RETURN_MAIN = 5;
    private static final int GO_MAIN = 6;
    private static final int RETURN_DISPENSE = 7;

    /**
     * Products
     */
    private NestleProduct mProduct1, mProduct2, mProduct3, mProduct4;
    private NestleProduct[] mProducts;
    private MachineManager mMachineManager;
    private ProductManager mProductManager;
    private ApplicationManager mAppManager;
    private ApplicationManager.FRONTEND_STATE mFrontendState;
    private boolean mFirstDown = true;
    private Button mSettingsButton;
    //private String mCurrentVersionGitHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*try
        {
            Intent intent = getIntent();
            mCurrentVersionGitHash =intent.getStringExtra("gitHash");
        }
        catch (Exception e)
        {

        }*/

        initWindowFeature();
        setContentView(R.layout.activity_main);
        mSceneRoot = (ViewGroup) findViewById(R.id.sceneRootView);
        mSceneRoot.setLayerType(View.LAYER_TYPE_NONE, null);
        mSettingsButton = (Button) findViewById(R.id.setting_button);

        initProducts();

        mEnglishButton = (TextView) findViewById(R.id.englishButton);
        mFrenchButton = (TextView) findViewById(R.id.frenchButton);
        mSpanishButton = (TextView) findViewById(R.id.spanishButton);

        mRinseRequiredImageView = (ImageView) findViewById(R.id.rinse_required_sticker);
        mRinseCompleteImageView = (ImageView) findViewById(R.id.rinse_complete_sticker);

        mDispenseLockedOutImageView = (ImageView) findViewById(R.id.dispense_locked_out);

        mFakeLoading = (ImageView) findViewById(R.id.fake_loading);

        mBackButton = (TextView) findViewById(R.id.back_button);
        mBackButton.setBackgroundResource(R.drawable.back_button_behaviour);
        mHomeButton = (TextView) findViewById(R.id.home_button);
        mHomeButton.setBackgroundResource(R.drawable.home_button_behaviour);

        mCurrentLang = PreferencesHelper.getCurrentLocale(this);
        setLocaleSelected(mCurrentLang);
        setCurrentLocale(mCurrentLang);

        //mViewHelper = new ViewHelper(mSceneRoot, MainActivity.this, mOnPourListener, mProduct1, mProduct2, mProduct3, mProduct4);
        mViewHelper = new ViewHelper(mSceneRoot, MainActivity.this, mOnPourListener, mProducts, mProduct1, mProduct2, mProduct3, mProduct4);
        initMachineManager();
        initApplicationManager();
        testFrontendState();
        mSettingsButton.setVisibility(mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT ? View.VISIBLE : View.GONE);

        launchScene();
    }



    @Override
    public void onStop() {
        super.onStop();
        mViewHelper.clearAssets();
        AnimotionUtils.getInstance().clearCurrentAnimotionType();
    }

    private void initMachineManager() {
        mMachineManager = ((ExpressApp) getApplication()).getMachineManager();
        mMachineManager.setOnFaultCallback(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("DISABLE_PRODUCTS", "initMachineManager");
                        initProducts();
                        //mViewHelper.disableProductsIfError(mProducts);
                        mViewHelper.disableProductsIfError(mProducts, mCurrentLang);
                        //mViewHelper.disableProductsIfError();
                    }
                });
            }
        });

        //TODO: force show buttons sold out and fault
        //forceSoldOutFault();

        isDispenseLockedOut = mMachineManager.getDispenseLockout();
    }

    private void forceSoldOutFault() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProduct1.setProductStatus(NestleProduct.ProductStatus.SOLD_OUT);
                mProduct2.setProductStatus(NestleProduct.ProductStatus.FAULT);
                mViewHelper.disableProductsIfError();
            }
        });
    }

    private void initApplicationManager() {
        mAppManager = ((ExpressApp) getApplication()).getApplicationManager();
        mFrontendState = mAppManager.getFrontendState();
        mAppManager.setOnRinseCallback(new RinseRequestCallback() {
            @Override
            public void onRinse(final RinseStatus status) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        testRinseStatus(status);

                    }
                });
                Log.d("RINSE", "== Rinse callback called");
            }
        });
    }

    private void launchScene() {
        android.transition.Scene scene = android.transition.Scene.getSceneForLayout(mSceneRoot, R.layout.mydemo_intro_scene5, this);
        android.transition.TransitionManager.go(scene);
    }

    private void initProducts() {

        mProductManager = ((ExpressApp) getApplication()).getProductManager();
        mProduct1 = mProductManager.getSelectedProduct(ProductSlot.SLOT1, PreferencesHelper.getCurrentLocale(this));
        mProduct2 = mProductManager.getSelectedProduct(ProductSlot.SLOT2, PreferencesHelper.getCurrentLocale(this));
        mProduct3 = mProductManager.getSelectedProduct(ProductSlot.SLOT3, PreferencesHelper.getCurrentLocale(this));
        mProduct4 = mProductManager.getSelectedProduct(ProductSlot.SLOT4, PreferencesHelper.getCurrentLocale(this));

        mProducts = new NestleProduct[]{mProduct1, mProduct2, mProduct3, mProduct4};

    }

    private void initWindowFeature() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void transitionToAttractiveLoopDelayed() {

        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
            if(mRinseRequiredImageView.getVisibility() == View.GONE) {
                mHandler.removeMessages(RETURN_MAIN);
                mHandler.sendEmptyMessageDelayed(RETURN_MAIN, TimeUnit.SECONDS.toMillis(mAppManager.getTimeoutIdle()));
                Log.d("DELAY", "=-=-=-=-=-=-= START DELAY");
            }
        }
    }

    private void transitionToAttractiveLoopCanceled() {
        mHandler.removeMessages(RETURN_MAIN);
        Log.d("DELAY", "=-=-=-=-=-=-= CANCEL DELAY");
    }

    private void transitionToDispenseDelayed() {

        if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mHandler.removeMessages(RETURN_DISPENSE);
                mHandler.sendEmptyMessageDelayed(RETURN_DISPENSE, TimeUnit.SECONDS.toMillis(mAppManager.getTimeoutSettingsMenu()));
                Log.d("DELAY", "=-=-=-=-=-=-= START DELAY");
        }
    }

    private void transitionToDispenseCanceled() {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
            mHandler.removeMessages(RETURN_DISPENSE);
            Log.d("DELAY", "=-=-=-=-=-=-= CANCEL DELAY");
        }
    }

    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();

        final android.transition.Scene scene2 = android.transition.Scene.getSceneForLayout(mSceneRoot, R.layout.dispense_intro_view, this);
        final android.transition.Scene scene3 = android.transition.Scene.getSceneForLayout(mSceneRoot, R.layout.dispense_product_view, this);
        scene2.setEnterAction(new Runnable() {
            @Override
            public void run() {
                Transition toScene3 = TransitionInflater.from(MainActivity.this).inflateTransition(R.transition.arcmotion);
                android.transition.TransitionManager.go(scene3, toScene3);

            }
        });
        scene2.setExitAction(new Runnable() {
            @Override
            public void run() {
                mSceneRoot.removeAllViews();
            }
        });

        scene3.setEnterAction(new Runnable() {
            @Override
            public void run() {
                if (mViewHelper != null) {
                    mViewHelper.clearViewsFocusInOrder();
                    mViewHelper.clearAssets();
                    mViewHelper.addViewFocusInOrder(findViewById(R.id.startFocus));
                    mViewHelper.initViewHelper();
                    mViewHelper.addViewFocusInOrder(mEnglishButton);
                    mViewHelper.addViewFocusInOrder(mFrenchButton);
                    mViewHelper.addViewFocusInOrder(mSpanishButton);
                    mRinseCompleteImageView.setVisibility(View.GONE);
                    if (!isDispenseLockedOut) {
                        mViewHelper.disabebleAllButtons();
                        mDispenseLockedOutImageView.setVisibility(View.VISIBLE);
                        mEnglishButton.setEnabled(false);
                        mFrenchButton.setEnabled(false);
                        mSpanishButton.setEnabled(false);
                    }


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mViewHelper != null) {
                                mViewHelper.initActionButtonsId();
                                testRinseStatus(mAppManager.getRinseStatus());
                                AccessibilityUtils.setFocusOrder(mViewHelper.getViewsFocusOrder());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mViewHelper != null) {
                                            mViewHelper.initAssetsAfterAnimation();
                                            if(mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
                                                mEnglishButton.setVisibility(View.VISIBLE);
                                                mFrenchButton.setVisibility(View.VISIBLE);
                                                mSpanishButton.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                }, 500);
                            }
                        }
                    }, mDelayMillis);
                }
            }
        });

        android.transition.TransitionManager.go(scene2, TransitionInflater.from(MainActivity.this).inflateTransition(R.transition.arcmotion));
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!mFrontendState.equals(mAppManager.getFrontendState())
                || ApplicationManager.FRONTEND_STATE.PRODUCT_CHANGED.equals(mAppManager.getFrontendState())
                || isWentToSettingsScreen) {
            if (ApplicationManager.FRONTEND_STATE.PRODUCT_CHANGED.equals(mAppManager.getFrontendState())) {
                mAppManager.setFrontendState(ApplicationManager.FRONTEND_STATE.DEFAULT);
            }
            Log.d("ATTL","onResume");
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //finish();
        } else {
            mMachineManager.enterDispenseMode();
            transitionToAttractiveLoopDelayed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAUSE", "onPause");
        mMachineManager.exitDispenseMode();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void testFrontendState() {


        switch (mFrontendState) {
            case RINSE:
                mViewHelper.setIsBackofHouse(false);
                mDelayMillis = DELAY_MILLIS_RINSE;
            case SET_PORTIONS:
                mBackButton.setVisibility(View.VISIBLE);
                mHomeButton.setVisibility(View.VISIBLE);
                mEnglishButton.setVisibility(View.GONE);
                mFrenchButton.setVisibility(View.GONE);
                mSpanishButton.setVisibility(View.GONE);

                if (mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                    mViewHelper.setIsBackofHouse(true);
                }

                break;
            default:
                mBackButton.setVisibility(View.GONE);
                mHomeButton.setVisibility(View.GONE);
                mRinseCompleteImageView.setVisibility(View.GONE);
                mDelayMillis = DELAY_MILLIS_DISPENSE;
                break;
        }
    }

    public void onConcentrateButtonPressed(NestleProduct product, Runnable onDispenseFinish) {
        //Cancel transition to AttractiveLoop so that it does not transition while pouring
        transitionToAttractiveLoopCanceled();
        mMachineManager.presizedDispenseStart(product, EventDispense.Mode.MODE_CONCENTRATE,
                MachineManager.ProductSize.CONCENTRATE, onDispenseFinish);
    }

    public void onSmallButtonPressed(NestleProduct product, Runnable onDispenseFinish) {
        //Cancel transition to AttractiveLoop so that it does not transition while pouring
        transitionToAttractiveLoopCanceled();
        mMachineManager.presizedDispenseStart(product, EventDispense.Mode.MODE_NORMAL,
                MachineManager.ProductSize.SMALL, onDispenseFinish);
    }

    public void onMediumButtonPressed(NestleProduct product, Runnable onDispenseFinish) {
        //Cancel transition to AttractiveLoop so that it does not transition while pouring
        transitionToAttractiveLoopCanceled();
        mMachineManager.presizedDispenseStart(product, EventDispense.Mode.MODE_NORMAL,
                MachineManager.ProductSize.MEDIUM, onDispenseFinish);
    }

    public void onLargeButtonPressed(NestleProduct product, Runnable onDispenseFinish) {
        //Cancel transition to AttractiveLoop so that it does not transition while pouring
        transitionToAttractiveLoopCanceled();
        mMachineManager.presizedDispenseStart(product, EventDispense.Mode.MODE_NORMAL,
                MachineManager.ProductSize.LARGE, onDispenseFinish);
    }

    private void testRinseStatus(RinseStatus status) {
        //mViewHelper.disableProductsIfError(mProducts);
        mViewHelper.disableProductsIfError(mProducts, mCurrentLang);
        rinseRequired = false;

        if(isDispenseLockedOut) {
            for (ProductSlot productSlot : ProductSlot.values()) {
                RinseStatus.RINSE_STATUS rinseMode = status.getRinseStatus(productSlot);
                if (rinseMode == RinseStatus.RINSE_STATUS.MANUAL_RINSE)
                    Log.d("RINSE", "-=-= Manual Rinse");
                else if (rinseMode == RinseStatus.RINSE_STATUS.DAILY_RINSE)
                    Log.d("RINSE", "-=-= Daily Rinse");
                else if (rinseMode == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE)
                    Log.d("RINSE", "-=-= Allergen Rinse");
                else if (rinseMode == RinseStatus.RINSE_STATUS.NONE)
                    Log.d("RINSE", "-=-= NONE");


                if (status.getRinseStatus(productSlot) != RinseStatus.RINSE_STATUS.NONE
                        && status.getRinseStatus(productSlot) != RinseStatus.RINSE_STATUS.MANUAL_RINSE) {
                    rinseRequired = true;
                    transitionToDispenseDelayed();
                    //Log.d("RINSE", "== Rinse callback called and RINSE REQUIRED TRUE");
                }
            }
        }

        onUpdateRinseRequired();

        if (rinseRequired) {
            rinseRequired();
            if(mViewHelper != null)
            mViewHelper.disableButtonIfRinseRequired();
        } else {
            rinseDone();
        }

    }

    //You need to integrate this into your pour button press code
    public void onPourButtonPressed(int button) {

        //Cancel transition to AttractiveLoop so that it does not transition while pouring
        transitionToAttractiveLoopCanceled();
        transitionToDispenseCanceled();

        //Start dispense
        if (button == 0) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseStart(mProduct1, EventDispense.Mode.MODE_NORMAL); //left button
            }
        } else if (button == 1) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseStart(mProduct2, EventDispense.Mode.MODE_NORMAL); //left button
            }
        } else if (button == 2) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseStart(mProduct3, EventDispense.Mode.MODE_NORMAL); //left button
            }
        } else if (button == 3) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseStart(mProduct4, EventDispense.Mode.MODE_NORMAL); //left button
            }
        } else if (button == 4) {
            mMachineManager.dispenseWaterNozzleStart();
        }
    }

    //You need to integrate this into your pour button release code
    public void onPourButtonReleased(int button) {
        //Stop dispense
        if (button == 0) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT  || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                Log.d("MachineManager","dispenseEnd");
                mMachineManager.dispenseEnd(mProduct1);
            }
        } else if (button == 1) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT  || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseEnd(mProduct2);
            }
        } else if (button == 2) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseEnd(mProduct3);
            }
        } else if (button == 3) {
            if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT || mFrontendState == ApplicationManager.FRONTEND_STATE.SET_PORTIONS) {
                mMachineManager.dispenseEnd(mProduct4);
            }
        } else if (button == 4) {
            mMachineManager.dispenseWaterNozzleEnd();
        }
    }

    //You need to integrate this into your pour button release code
    public void onPourButtonDispenseEnd(int button) {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
            //Stop dispense
            if (button == 0) {
                    mMachineManager.dispenseEndWithCallback(mProduct1);
            } else if (button == 1) {
                    mMachineManager.dispenseEndWithCallback(mProduct2);
            } else if (button == 2) {
                    mMachineManager.dispenseEndWithCallback(mProduct3);
            } else if (button == 3) {
                    mMachineManager.dispenseEndWithCallback(mProduct4);
            }
        }
    }

    private void onUpdateRinseRequired(){
        RinseStatus status = mAppManager.getRinseStatus();
        boolean enableHome = true;
        for (ProductSlot productSlot : ProductSlot.values()) {
            RinseStatus.RINSE_STATUS rinseMode = status.getRinseStatus(productSlot);

            if (rinseMode == RinseStatus.RINSE_STATUS.ALLERGEN_RINSE || rinseMode == RinseStatus.RINSE_STATUS.DAILY_RINSE) {
                enableHome = false;
            }
        }
        Log.d("RINSE", " onUpdateRinseRequired");
        if(enableHome){
            mRinseRequiredImageView.setVisibility(View.GONE);
            mBackButton.setEnabled(true);
            mHomeButton.setEnabled(true);
            Log.d("RINSE", " home and back button enable true");
        }else{
            mHomeButton.setEnabled(false);
            mBackButton.setEnabled(false);
        }
    }


    private long mStartSettingPortionTime;
    private OnPourListener mOnPourListener = new OnPourListener() {
        @Override
        public void onPress(int position) {
            onPourButtonPressed(position);
        }

        @Override
        public void onPressOut(int position) {
            onPourButtonReleased(position);
        }

        @Override
        public void onDispenseEndPress(int position) {
            onPourButtonDispenseEnd(position);
        }

        @Override
        public void onConcentratePress(NestleProduct product, Runnable onDispenseFinish) {
            onConcentrateButtonPressed(product, onDispenseFinish);
        }

        @Override
        public void onSmallPress(NestleProduct product, Runnable onDispenseFinish) {
            onSmallButtonPressed(product, onDispenseFinish);
        }

        @Override
        public void onMediumPress(NestleProduct product, Runnable onDispenseFinish) {
            onMediumButtonPressed(product, onDispenseFinish);
        }

        @Override
        public void onLargePress(NestleProduct product, Runnable onDispenseFinish) {
            onLargeButtonPressed(product, onDispenseFinish);
        }

        @Override
        public void onTransitionToAttractiveLoopDelayed() {
            transitionToAttractiveLoopDelayed();
        }

        @Override
        public void onRemoveAttractiveLoopDelayed() {
            Log.d("DELAY", "=-=-=-=-=-=-= CANCEL DELAY");
            mHandler.removeMessages(RETURN_MAIN);
        }

        @Override
        public void onTransitionToDispenseDelayed() {
            transitionToDispenseDelayed();
        }

        @Override
        public void onRemoveDispenseDelayed() {
            transitionToDispenseCanceled();
        }

        @Override
        public void onSetPortion() {
            mStartSettingPortionTime = System.currentTimeMillis();
        }

        @Override
        public void onSetPortionOut(int position, MachineManager.ProductSize productSize) {
            int portionTime = (int) (System.currentTimeMillis() - mStartSettingPortionTime);
            int portionMax = mAppManager.getTimeoutPortionSet()*1000;
            if(portionTime > portionMax)
                portionTime = portionMax;

            switch (productSize) {
                case CONCENTRATE:
                    mProducts[position].setTimeConcentrate(portionTime);
                    Log.d("TEST---", "POSITION IS " + position + " size is " + productSize + " and portion time is " + mProducts[position].getTimeSmall() + " available for pour " + mProducts[position].getButtonSetup().isSmall());
                    break;
                case SMALL:
                    mProducts[position].setTimeSmall(portionTime);
                    Log.d("TEST---", "POSITION IS " + position + " size is " + productSize + " and portion time is " + mProducts[position].getTimeSmall() + " available for pour " + mProducts[position].getButtonSetup().isSmall());
                    break;
                case MEDIUM:
                    mProducts[position].setTimeMedium(portionTime);
                    Log.d("TEST---", "POSITION IS " + position + " size is " + productSize + " and portion time is " + mProducts[position].getTimeMedium() + " available for pour " + mProducts[position].getButtonSetup().isMedium());
                    break;
                case LARGE:
                    mProducts[position].setTimeLarge(portionTime);
                    Log.d("TEST---", "POSITION IS " + position + " size is " + productSize + " and portion time is " + mProducts[position].getTimeLarge() + " available for pour " + mProducts[position].getButtonSetup().isLarge());
                    break;
            }

            mProductManager.updateSelectedProductSetting(mProducts[position]);
        }

        @Override
        public void updateRinseRequired() {
            onUpdateRinseRequired();
        }
    };

    private long currentTime = 0;

    public void onBackButton(View view) {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.RINSE) {
            mAppManager.exitRinseMode();
        }
        goToSettings();
    }

    public void onSetting(View view) {
        if (mFrontendState == ApplicationManager.FRONTEND_STATE.DEFAULT) {
            if (System.currentTimeMillis() - currentTime < 1500) {
                mViewHelper.setNowTime(System.currentTimeMillis());
                goToSettings();
            } else {
                currentTime = System.currentTimeMillis();
            }
        }
    }

    private void goToSettings() {
        Log.d("EXPRESS", "Launch Settings");
        if(mBackButton.getVisibility() == View.GONE)
        mFakeLoading.setVisibility(View.VISIBLE);
        transitionToAttractiveLoopCanceled();
        isWentToSettingsScreen = true;
        String defaultLanguageCode = SharedPreferencesUtil.getInstance().getLanguage();
        PreferencesHelper.saveCurrentLocale(this, defaultLanguageCode);
        setCurrentLocale(defaultLanguageCode);
        Log.d("LANGUAGE","Lang default "+defaultLanguageCode);
        mMachineManager.updateDriverSetpoints();
        Intent intent = new Intent(MainActivity.this, ServiceCenterActivity.class);
        //intent.putExtra("gitHash", mCurrentVersionGitHash);
        new Intent(MainActivity.this, ActivityUI.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);
        //finish();
    }

    public void onHome(View view) {
        changeToDispense();
    }

    private void changeToDispense() {
        mAppManager.setFrontendState(ApplicationManager.FRONTEND_STATE.DEFAULT);

        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void changeToMain() {
        mHandler.sendEmptyMessageDelayed(GO_MAIN, 1050);
    }

    @Override
    public void onActivityKeyDown(int keyCode, KeyEvent event) {
        super.onActivityKeyDown(keyCode, event);
        switch (keyCode){
            case KeyEvent.KEYCODE_X:
                Log.d("ADA","KEYCODE_X DOWN");
                mViewHelper.stopPourPressAnimation();
                break;

            case KeyEvent.KEYCODE_ENTER:

                mViewHelper.startFocus();
                Log.d("ADA","KEYCODE_ENTER DOWN");

                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (mFirstDown) {
                Log.d("ADA","KEYCODE_ENTER DOWN");
                mFirstDown = false;
                View currentFocus = getCurrentFocus();
                if(currentFocus != null) {
                    currentFocus.dispatchTouchEvent(MotionEvent.obtain(event.getDownTime(), event.getEventTime(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            Log.d("ADA","KEYCODE_ENTER UP");
            if (!mFirstDown) {
                mFirstDown = true;
                View currentFocus = getCurrentFocus();
                if(currentFocus != null) {
                    currentFocus.dispatchTouchEvent(MotionEvent.obtain(event.getDownTime(), event.getEventTime(), MotionEvent.ACTION_UP, 0, 0, 0));
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {

            Log.d("ADA","KEYCODE_ENTER LONG");

        } else {

        }
        return super.onKeyLongPress(keyCode, event);
    }


    @Override
    protected void onLocaleSelected(Locale locale) {
        super.onLocaleSelected(locale);
        mCurrentLang = locale.getLanguage();
    }

    private void setLocaleSelected(String lang) {
        if (lang.equalsIgnoreCase("en")) {
            mEnglishButton.setTypeface(mEnglishButton.getTypeface(), Typeface.BOLD);
            mEnglishButton.setSelected(true);
            mFrenchButton.setTypeface(mFrenchButton.getTypeface(), Typeface.NORMAL);
            mFrenchButton.setSelected(false);
            mSpanishButton.setTypeface(mSpanishButton.getTypeface(), Typeface.NORMAL);
            mSpanishButton.setSelected(false);
        } else if (lang.equalsIgnoreCase("fr")) {
            mEnglishButton.setTypeface(mEnglishButton.getTypeface(), Typeface.NORMAL);
            mEnglishButton.setSelected(false);
            mFrenchButton.setTypeface(mFrenchButton.getTypeface(), Typeface.BOLD);
            mFrenchButton.setSelected(true);
            mSpanishButton.setTypeface(mSpanishButton.getTypeface(), Typeface.NORMAL);
            mSpanishButton.setSelected(false);
        } else if (lang.equalsIgnoreCase("es")) {
            mEnglishButton.setTypeface(mEnglishButton.getTypeface(), Typeface.NORMAL);
            mEnglishButton.setSelected(false);
            mFrenchButton.setTypeface(mFrenchButton.getTypeface(), Typeface.NORMAL);
            mFrenchButton.setSelected(false);
            mSpanishButton.setTypeface(mSpanishButton.getTypeface(), Typeface.BOLD);
            mSpanishButton.setSelected(true);
        }
    }

    private void setCurrentLocale(String lang) {
        Locale locale = null;
        if (lang.equalsIgnoreCase("en")) {
            locale = new Locale(ENGLISH_ISO_CODE);
        } else if (lang.equalsIgnoreCase("fr")) {
            locale = new Locale(FRENCH_ISO_CODE);
        } else if (lang.equalsIgnoreCase("es")) {
            locale = new Locale(SPANISH_ISO_CODE);
        }

        ExpressApp.setLocale(this, locale);
    }

    private void rinseRequired() {
        mRinseRequiredImageView.setVisibility(View.VISIBLE);
        mMachineManager.exitDispenseMode();
        transitionToAttractiveLoopCanceled();
        Log.d("RINSE", " rinseRequired VISIBLE");
    }

    private void rinseDone() {
        mRinseRequiredImageView.setVisibility(View.GONE);
        if(mBackButton.getVisibility() == View.VISIBLE)
            if( mFrontendState != ApplicationManager.FRONTEND_STATE.SET_PORTIONS){
                //mRinseCompleteImageView.setVisibility(View.VISIBLE);
                changeToDispense();
            }
        else
            mRinseCompleteImageView.setVisibility(View.GONE);
        //Log.d("RINSE", " rinseDone GONE");
    }
}
