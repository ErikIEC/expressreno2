package com.nestle.express.frontend_ui.nurun.views.selections;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jesserci on 20-10-06.
 */
public class ProgressPouringView extends View {
    private Paint mPaint;
    private RectF mRect;
    private int mStokeWidth = 100;
    private int mColor = Color.GREEN;
    private boolean mIsStayDrink = false;

    private float mCurrentAngle;
    private float mStartSweepValue;
    private float mOffset = 1;

    private boolean isAutoDraw = true;

    public ProgressPouringView(Context context) {
        super(context);
        init();
    }

    public ProgressPouringView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressPouringView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mStartSweepValue = -30;
        mCurrentAngle = 0;
    }

    //TODO this onDraw repeat itself every 16ms for no reason (60 fps)
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);

        mPaint.setStrokeWidth(mStokeWidth);

        mPaint.setColor(mColor);
        //圆弧范围
        mRect = new RectF(90, 90, getWidth() - 90, getHeight() - 90);
//        mRect = new RectF(20, 20, 300, 300);
        //绘制圆弧
        canvas.drawArc(mRect, mStartSweepValue, mCurrentAngle, false, mPaint);
//        //判断当前百分比是否小于设置目标的百分比

        if (mIsStayDrink) {
            if (mOffset + 0.01f < 0.6) {
                mOffset = mOffset + 0.01f;
                mStokeWidth = (int) (150 * (1 - mOffset));
                mCurrentAngle = (int) (720 * mOffset);

            } else {
                resetValue();
            }
            postInvalidateDelayed(16);

        } else {
            if (mOffset + 0.01f < 1) {
                mOffset = mOffset + 0.01f;
                mStokeWidth = (int) (150 * (1 - mOffset));
                mCurrentAngle = (int) (720 * mOffset);
            }
            //postInvalidateDelayed(16);//TODO we should remove this, we are drawing the view no stop for no reason
        }


//        if (mCurrentPercent < mTargetPercent) {
//            //当前百分比+1
//            mCurrentPercent += 1;
//            //当前角度+360
//            mCurrentAngle += 1.6;
//
//            if (mStokeWidth > 0) {
//                mStokeWidth -= 1 ;
//            } else {
//                mStokeWidth = 0 ;
//            }
//
////            if (isAutoDraw) {
//                //每100ms重画一次
//                postInvalidateDelayed(10);
////            }
//        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width;
        int height;
        //如果布局里面设置的是固定值,这里取布局里面的固定值;如果设置的是match_parent,则取父布局的大小
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else {

            //如果布局里面没有设置固定值,这里取字体的宽度
            width = widthSize * 1 / 2;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else {
            height = heightSize * 1 / 2;
        }
        setMeasuredDimension(width, height);
    }

    private void resetValue() {
        mOffset = 0;
        mStokeWidth = 150;
        mCurrentAngle = 0;
        mStartSweepValue = -30;
    }

    public void startAnimation() {
        resetValue();
        mIsStayDrink = true;
        postInvalidate();
    }

    public void stopAnimation() {
        mIsStayDrink = false;
    }

    /**
     * The color used with Paint
     *
     * @param color
     */
    public void setColor(int color) {
        this.mColor = color;
    }
}
