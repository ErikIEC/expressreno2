package com.nestle.express.frontend_ui.util;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.animation.DecelerateInterpolator;

import com.nestle.express.frontend_ui.widget.MyView;
import com.nestle.express.frontend_ui.widget.ViewAnimotionBean;
import com.nestle.express.frontend_ui.nurun.widget.ViewGroupLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by xumin on 16/8/29.
 */
public class MyAnimotionUtils {
    private static LinkedList<ValueAnimator> objectAnimators = new LinkedList<>();
    private static Map<ValueAnimator,UpdateListener> updateListeners = new HashMap<>();
    private static Map<ValueAnimator,UpdateListener1> updateListeners1 = new HashMap<>();
    private static Map<ValueAnimator,UpdateListener1> updateListeners2 = new HashMap<>();

    private static Map<ValueAnimator,UpdateListener5> updateListeners5 = new HashMap<>();
    private static LinkedList<ViewAnimotionBean> viewAnimotionBeens = new LinkedList<>();

    public interface OnAnimotionStateListener{
        public void OnAnimotionState() ;
    }

    public static ValueAnimator getValueAnimator (final ViewAnimotionBean viewAnimotionBean , long duraction , OnAnimotionStateListener stateListener) {
//        if (!(viewAnimotionBean.getView() instanceof ViewGroupLayout)) {
//            viewAnimotionBean.getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        }
//        try {
//            ValueAnimator first = objectAnimators.getFirst();
//            first.setDuration(duraction) ;
//            objectAnimators.removeFirst();
//            UpdateListener5 listener = updateListeners5.get(first);
//            listener.setData(viewAnimotionBean , stateListener);
////            first.start();
//            return first;
//        } catch (Exception e) {
//
//        }
////        final ValueAnimator animation = ValueAnimator.ofFloat(0,1f,1.05f,1.08f,1.1f,1.15f,1.18f,1.22f,1.25f,1.28f,1.3f,1.35f,1.38f,1.4f,1.45f,1.48f,1.5f);
//        final ValueAnimator animation = ValueAnimator.ofFloat(0,1f ,1.05f ,1.1f , 1.15f ,1.2f , 1.25f ,1.3f , 1.35f ,1.4f ,1.45f, 1.5f);
//        animation.setDuration(duraction);
//        UpdateListener5 listener = new UpdateListener5();
//        updateListeners5.put(animation,listener);
//        listener.setData(viewAnimotionBean , stateListener);
//        animation.addUpdateListener(listener);
//
//        animation.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                objectAnimators.addLast((ValueAnimator) animation);
//
//                if (!(viewAnimotionBean.getView() instanceof ViewGroupLayout)) {
//                    viewAnimotionBean.getView().setLayerType(View.LAYER_TYPE_NONE, null);
//                            ViewAnimotionBeanUtils.addViewAnimotionBeans(viewAnimotionBean);
//                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
        return TestAnimotion.getValueAnimator(viewAnimotionBean , duraction , stateListener) ;
    }

    /**
     * center circle animotion
     *
     */
    public static void animotionCircle(final ArrayList<ViewAnimotionBean> list , long duraction , OnAnimotionStateListener stateListener) {
//        if (list!=null && list.size() > 0) {
//            for (ViewAnimotionBean sonBean : list) {
//                if (!(sonBean.getView() instanceof ViewGroupLayout)) {
//                    sonBean.getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
//                }
//            }
//        }

        try {

            ValueAnimator first = objectAnimators.getFirst();
            objectAnimators.removeFirst();
            UpdateListener listener = updateListeners.get(first);
            listener.setData(list , stateListener);
            first.start();
            return;
        } catch (Exception e) {

        }
//        ValueAnimator animation = ValueAnimator.ofFloat(0,1f,1.02f,1.05f,1.08f,1.1f,1.12f,1.15f,1.18f,1.2f,1.22f,1.25f,1.28f,1.3f,1.32f,1.35f,1.38f,1.4f,1.42f,1.45f,1.48f,1.5f);
        ValueAnimator animation = ValueAnimator.ofFloat(0,1f,1.5f);
        animation.setDuration(duraction);
        animation.setInterpolator(new DecelerateInterpolator());
        UpdateListener listener = new UpdateListener();
        updateListeners.put(animation,listener);
        listener.setData(list , stateListener);
        animation.addUpdateListener(listener);

        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                objectAnimators.addLast((ValueAnimator) animation);
//                if (list!=null && list.size() > 0) {
//                    for (ViewAnimotionBean sonBean : list) {
//                        if (!(sonBean.getView() instanceof ViewGroupLayout)) {
//                            sonBean.getView().setLayerType(View.LAYER_TYPE_NONE, null);
//                            ViewAnimotionBeanUtils.addViewAnimotionBeans(sonBean);
//                        }
//                    }
//                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animation.start();
    }

    /**
     * 中心圆
     */
    static class UpdateListener implements ValueAnimator.AnimatorUpdateListener{
        private ArrayList<ViewAnimotionBean> list ;
        private OnAnimotionStateListener stateListener ;
        private boolean isPull = false ;

        public void setData(ArrayList<ViewAnimotionBean> list , OnAnimotionStateListener stateListener) {
            this.list = list ;
            this.stateListener = stateListener ;
            isPull = false ;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float valueAnimator = (float) animation.getAnimatedValue();
            if (valueAnimator <= 1.0f) {
                for (ViewAnimotionBean bean : list) {
                    int offsetP = bean.getOffsetP() ;
                    if (offsetP == 0) {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator);
                    } else if (offsetP == 1) {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator*valueAnimator);
                    } else {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator);
                    }
                }
            } else {
//                //1-1.5
                if (!isPull && valueAnimator > 1.05f) {
                    isPull = true ;
                    this.stateListener.OnAnimotionState();
                }
                for (ViewAnimotionBean bean : list) {
                    if (bean.isRotate()) {
                        float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
                        float radius = progress * bean.getRadis();
                        float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                        int x, y;

                        if (bean.getDirection() == 0) {
                            x = (int) (radius * Math.sin(angle));
                            y = (int) (radius * Math.cos(angle));
                        } else {
                            x = (int) (radius * Math.cos(angle));
                            y = (int) (radius * Math.sin(angle));
                        }


                        if (bean.getView() instanceof MyView) {
                            ((MyView)bean.getView()).setMyX(x);
                            ((MyView)bean.getView()).setMyY(y);
                        } else if (bean.getView() instanceof ViewGroupLayout) {
                            ((ViewGroupLayout)bean.getView()).setMyX(x);
                            ((ViewGroupLayout)bean.getView()).setMyY(y);
                        }

                        if (bean.isNeedScale()) {
                            ViewGroupLayout viewGroupLayout = ((ViewGroupLayout) bean.getView()) ;
                            if (valueAnimator < 1.05f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.01f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.001f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.001f);
                                }
                            } else if (valueAnimator > 1.05f && valueAnimator < 1.15f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() > 0.98f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() - 0.001f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() - 0.001f);
                                }
                            } else if (valueAnimator > 1.15f) {
                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.05f) {
                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.0005f);
                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.0005f);
                                }
                            }
                        }

                    }
                }
            }
        }
    }


    /**
     * 测试
     */
    static class UpdateListener5 implements ValueAnimator.AnimatorUpdateListener{
        private ViewAnimotionBean bean ;
        private boolean isPull = false ;
        private OnAnimotionStateListener stateListener ;

        public void setData(ViewAnimotionBean viewAnimotionBean , OnAnimotionStateListener stateListener) {
            this.bean = viewAnimotionBean ;
            isPull = false ;
            this.stateListener = stateListener ;
        }

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {

            if (animation.getAnimatedFraction() > 0.45f) {
                animation.cancel();
            }

            float valueAnimator = (float) animation.getAnimatedValue();
            if (valueAnimator <= 1.0f) {
                    int offsetP = bean.getOffsetP() ;
                    if (offsetP == 0) {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator);
                    } else if (offsetP == 1) {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator*valueAnimator);
                    } else {
                        bean.getView().setX(bean.getOldPointF().x + (bean.getNewAngelPointF().x - bean.getOldPointF().x)*valueAnimator);
                        bean.getView().setY(bean.getOldPointF().y + (bean.getNewAngelPointF().y - bean.getOldPointF().y)*valueAnimator);
                    }
            } else {
//                //1-1.5
                if (!isPull && valueAnimator > 1.05f) {
                    isPull = true ;
                    if (this.stateListener!=null)
                    this.stateListener.OnAnimotionState();
                }
                    if (bean.isRotate()) {
                        float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
                        float radius = progress * bean.getRadis();
                        float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6

                        int x, y;

                        if (bean.getDirection() == 0) {
                            x = (int) (radius * Math.sin(angle));
                            y = (int) (radius * Math.cos(angle));
                        } else {
                            x = (int) (radius * Math.cos(angle));
                            y = (int) (radius * Math.sin(angle));
                        }


                        if (bean.getView() instanceof MyView) {
                            ((MyView)bean.getView()).setMyX(x);
                            ((MyView)bean.getView()).setMyY(y);
                        } else if (bean.getView() instanceof ViewGroupLayout) {
                            ((ViewGroupLayout)bean.getView()).setMyX(x);
                            ((ViewGroupLayout)bean.getView()).setMyY(y);
                        }

//                        if (bean.isNeedScale()) {
//                            ViewGroupLayout viewGroupLayout = ((ViewGroupLayout) bean.getView()) ;
//                            if (valueAnimator < 1.05f) {
//                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.01f) {
//                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.001f);
//                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.001f);
//                                }
//                            } else if (valueAnimator > 1.05f && valueAnimator < 1.15f) {
//                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() > 0.98f) {
//                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() - 0.001f);
//                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() - 0.001f);
//                                }
//                            } else if (valueAnimator > 1.15f) {
//                                if (viewGroupLayout != null && viewGroupLayout.getScaleX() < 1.05f) {
//                                    viewGroupLayout.setMyScaleX(viewGroupLayout.getScaleX() + 0.0005f);
//                                    viewGroupLayout.setMyScaleY(viewGroupLayout.getScaleY() + 0.0005f);
//                                }
//                            }
//                        }

                    }
            }
        }
    }

    /**
     * center circle animotion
     *
     */
//    public static void animotionOther(final ViewAnimotionBean bean) {
////        if (bean.getSonList()!=null && bean.getSonList().size() > 0) {
////            for (ViewAnimotionBean sonBean : bean.getSonList()) {
////                if (!(sonBean.getView() instanceof ViewGroupLayout)) {
////                    sonBean.getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
////                }
////            }
////        }
//
//        try {
//            ValueAnimator first = objectAnimators.getFirst();
//            objectAnimators.removeFirst();
//            UpdateListener1 listener = updateListeners1.get(first);
//            listener.setData(bean);
//            first.start();
//            return;
//        } catch (Exception e) {
//
//        }
//        ValueAnimator animation = ValueAnimator.ofFloat(0,1f,1.05f,1.1f,1.15f,1.2f,1.25f,1.3f,1.35f,1.4f,1.45f,1.5f);
//        animation.setDuration(bean.getDuraction());
//        animation.setInterpolator(new DecelerateInterpolator());
//        UpdateListener listener = new UpdateListener();
//        updateListeners.put(animation,listener);
//        listener.setData(bean);
//        animation.addUpdateListener(listener);
//
//        animation.addListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                objectAnimators.addLast((ValueAnimator) animation);
////                if (bean.getSonList()!=null && bean.getSonList().size() > 0) {
////                    for (ViewAnimotionBean sonBean : bean.getSonList()) {
////                        if (!(sonBean.getView() instanceof ViewGroupLayout)) {
////                            sonBean.getView().setLayerType(View.LAYER_TYPE_NONE, null);
////                        }
////                    }
////                }
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        });
//        animation.start();
//    }


    /**
     * 小圆
     */
    static class UpdateListener1 implements ValueAnimator.AnimatorUpdateListener{
        private ViewAnimotionBean bean ;

        public void setData(ViewAnimotionBean bean) {
            this.bean = bean ;
        }
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float valueAnimator = (float) animation.getAnimatedValue();
            if (valueAnimator <= 1.0f) {
//                if (bean.getSonList()!=null && bean.getSonList().size() > 0) {
//                    for (ViewAnimotionBean sonBean : bean.getSonList()) {
//                        sonBean.getView().setX(sonBean.getOldPointF().x + (sonBean.getNewAngelPointF().x - sonBean.getOldPointF().x)*valueAnimator);
//                        sonBean.getView().setY(sonBean.getOldPointF().y + (sonBean.getNewAngelPointF().y - sonBean.getOldPointF().y)*valueAnimator);
//                    }
//                }
            } else {
//                if (bean.getSonList()!=null && bean.getSonList().size() > 0) {
//                    for (ViewAnimotionBean sonBean : bean.getSonList()) {
//                        View view = ((View) bean.getView()) ;
//                        float progress = (float) Math.pow((float) (1 - Math.abs(1-valueAnimator)*2), 2);  //PI 3/ 4
//                        float radius = progress * bean.getRadis();
//                        float angle = (float) (progress * ((bean.getAngel()) + bean.getEvlotion() * 2.0 * Math.PI));  //2*PI * progress = PI * 8 / 6
//
//                        int x, y;
//
//                        if (bean.getDirection() == 0) {
//                            x = (int) (radius * Math.sin(angle));
//                            y = (int) (radius * Math.cos(angle));
//                        } else {
//                            x = (int) (radius * Math.cos(angle));
//                            y = (int) (radius * Math.sin(angle));
//                        }
//                        if (view instanceof MyView) {
//                            ((MyView)view).setMyX(x);
//                            ((MyView)view).setMyY(y);
//                        } else if (view instanceof ViewGroupLayout) {
//                            ((ViewGroupLayout)view).setMyX(x);
//                            ((ViewGroupLayout)view).setMyY(y);
//                        }
//                    }
//                }

            }
        }
    }

    /**
     * 单纯平移
     */
    static class UpdateListener2 implements ValueAnimator.AnimatorUpdateListener{

        @Override
        public void onAnimationUpdate(ValueAnimator animation) {

        }
    }
}


