package com.nestle.express.frontend_ui.surface;

/**
 * Created by xumin on 2016/8/9.
 */
public abstract class NodeAnimation {
    private long pro;
    private BaseNode node;

    private AnimationListener listener;

    private int duration;

    private boolean isStart = false;

    private int start_time;

    private AnimationInterpolation interpolation;

    public NodeAnimation(){
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void doAnimation(float offset){
//        if (isStart == false){
//            isStart = true;
//            if (listener != null)
//                listener.animationStart(this);
//        }
//        start_time += offset;
//        if (start_time > duration){
//            node.removeAnimation(this);
//            if (listener != null)
//                listener.animationEnd(this);
//            return;
//        }
//        float t = start_time / 1.0f / duration;
//        if (interpolation != null) {
//            t = interpolation.dotInterpolation(t);//(float)(1+(2+3*(-1+t))*(-1+t)*(-1+t));
//        }
////        float v = (end - start) * t;
//        setOffset(t);

    }

    public BaseNode getNode(){
        return node;
    }

    public abstract void setOffset(float offset);

    public void setNode(BaseNode node) {
        this.node = node;
    }

    public AnimationInterpolation getInterpolation() {
        return interpolation;
    }

    public void setInterpolation(AnimationInterpolation interpolation) {
        this.interpolation = interpolation;
    }

    public class AnimationListener{
        public void animationStart(NodeAnimation animation){}
        public void animationEnd(NodeAnimation animation){}
    }

    static public abstract class AnimationInterpolation{
        abstract float dotInterpolation(float t);
    }
}
