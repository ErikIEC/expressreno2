package com.nestle.express.frontend_ui.nurun.views;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

/**
 * Created by jesserci on 16-10-06.
 */
public class PositionView extends View implements PositionInterface {
    private PointF mPointF;

    public PositionView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), " WHERE AM I ?!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public PositionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PositionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PositionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    public void putPosition(View view, float x, float y) {

    }

    @Override
    public void putPosition(float x, float y) {
        mPointF = new PointF(x, y);
    }

    @Override
    public void setMyX(float addX) {
        setX(mPointF.x + addX);
    }

    @Override
    public void setMyY(float addY) {
        setY(mPointF.y + addY);
    }
}
