package com.nestle.express;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class ReceiverAdmin extends DeviceAdminReceiver
{

	final int LENGTH=1000;

	@Override
	public void onEnabled(Context context, Intent intent)
	{
		super.onEnabled(context, intent);

		Toast.makeText(context, "UI: Device Admin Enabled", LENGTH).show();
	}

	@Override
	public CharSequence onDisableRequested(Context context, Intent intent)
	{
		return "UI: Device Admin is going to be disabled.";
	}

	@Override
	public void onDisabled(Context context, Intent intent)
	{
		super.onDisabled(context, intent);

		Toast.makeText(context, "UI: Device Admin Disabled", LENGTH).show();
	}

	@Override
	public void onLockTaskModeEntering(Context context, Intent intent,
									   String pkg)
	{
		super.onLockTaskModeEntering(context, intent, pkg);

		Toast.makeText(context, "UI: Device Admin Lock Task Enabled", LENGTH).show();
	}

	@Override
	public void onLockTaskModeExiting(Context context, Intent intent)
	{
		super.onLockTaskModeExiting(context, intent);

		Toast.makeText(context, "UI: Device Admin Lock Task Disabled", LENGTH).show();
	}

}
