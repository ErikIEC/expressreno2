/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.device_layer;

import com.nestle.express.communications.ByteArray;

/**
 *
 * @author jason
 */
public class DeviceLoopback implements DeviceLayer
{
	ByteArray data=new ByteArray();
		
	@Override
	public void open()
	{
	}

	String arrayToString(byte[] buffer)
	{
		String out="[ ";
		for (byte i : buffer)
			out+=Integer.toHexString(i&0xFF)+" ";
		out+=" ]";

		return out;
	}
	
	@Override
	public byte[] read()
	{
		if (data == null || data.size() < 1)
			return new byte[0];

		byte[] array=data.toArray();
		data.clear();

		System.out.println("Loopback read:\t"+arrayToString(array));

		return array;
	}
	
	@Override
	public void write(byte[] buffer)
	{
		System.out.println("Loopback write:\t"+arrayToString(buffer));
		data.append(buffer);
	}
	
	@Override
	public void close()
	{

	}
}
