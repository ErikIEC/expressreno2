/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.physical_layer;

import com.nestle.express.communications.ByteArray;

/**
 *
 * @author jason
 */
public class PacketSerial
{
	public byte command;
	public byte messageId;
	public byte[] payload;
	public byte crc;

	public PacketSerial()
	{

	}

	public PacketSerial(ByteArray array)
	{
		//write all but last byte
		command=array.getFirst();
		crc=array.getLast();
		messageId=array.get(1);
		//don't copy two bytes and last byte (command, message id, and crc) into payload
		payload=array.toArray(2, 1);
	}
	
	@Override
	public String toString()
	{
		String message="PacketSerial(length="+payload.length;
		message+=", payload=";
		
		if (payload.length==0)
			message+="None";
		else
		{
			for(byte i : payload)
				message+="0x"+Integer.toHexString(i&0xFF)+" ";
		}
		
		message+=", crc=0x"+Integer.toHexString(crc);
		message+=")";		
		
		return message;
	}

	@Override
	public boolean equals(Object obj)
	{
		if ( this == obj )
			return true;

		if ( !(obj instanceof PacketSerial) )
			return false;

		PacketSerial packet = (PacketSerial) obj;

		boolean result=true;

		result&= java.util.Arrays.equals(payload, packet.payload);
		result&= (command==packet.command);
		result&= (crc==packet.crc);

		return result;
	}

	//get packet as it would be received, aka. excluding the CRC
	public byte[] getBytesAsSent()
	{
		ByteArray b=new ByteArray();

		b.append(command);
		b.append(payload);

		return b.toArray();
	}
}
