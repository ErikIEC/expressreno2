/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.physical_layer;
import com.nestle.express.communications.ByteArray;
import com.nestle.express.communications.device_layer.DeviceLayer;


import java.util.ArrayList;

/**
 *
 * @author jason
 */
public class PhysicalNestleSerial implements PhysicalLayer
{
	public PhysicalNestleSerial(DeviceLayer device)
	{
		serial = device;
	}

	@Override
	public void open()
	{
		serial.open();
	}
	
	@Override
	public void close()
	{
		if (serial!=null)
			serial.close();
	}
	
	DeviceLayer serial=null;
	
	ByteArray receive_buffer = new ByteArray();
	ArrayList<PacketSerial> received_packets = new ArrayList<>();
	
	
	final char BEGINNING_OF_FIELD =0xD0;
	final char END_OF_FIELD =0xD1;
	final char CHARACTER_ESCAPE=0x1D;
	final char TRANSPARENCY_MASK =0x80;

	
	final char CRC_INITIAL_VALUE =0xFF;
	char[] crc_table =
	{
		0x00, 0x2f, 0x5e, 0x71, 0xbc, 0x93, 0xe2, 0xcd,
		0x57, 0x78, 0x09, 0x26, 0xeb, 0xc4, 0xb5, 0x9a,
		0xae, 0x81, 0xf0, 0xdf, 0x12, 0x3d, 0x4c, 0x63,
		0xf9, 0xd6, 0xa7, 0x88, 0x45, 0x6a, 0x1b, 0x34,
		0x73, 0x5c, 0x2d, 0x02, 0xcf, 0xe0, 0x91, 0xbe,
		0x24, 0x0b, 0x7a, 0x55, 0x98, 0xb7, 0xc6, 0xe9,
		0xdd, 0xf2, 0x83, 0xac, 0x61, 0x4e, 0x3f, 0x10,
		0x8a, 0xa5, 0xd4, 0xfb, 0x36, 0x19, 0x68, 0x47,
		0xe6, 0xc9, 0xb8, 0x97, 0x5a, 0x75, 0x04, 0x2b,
		0xb1, 0x9e, 0xef, 0xc0, 0x0d, 0x22, 0x53, 0x7c,
		0x48, 0x67, 0x16, 0x39, 0xf4, 0xdb, 0xaa, 0x85,
		0x1f, 0x30, 0x41, 0x6e, 0xa3, 0x8c, 0xfd, 0xd2,
		0x95, 0xba, 0xcb, 0xe4, 0x29, 0x06, 0x77, 0x58,
		0xc2, 0xed, 0x9c, 0xb3, 0x7e, 0x51, 0x20, 0x0f,
		0x3b, 0x14, 0x65, 0x4a, 0x87, 0xa8, 0xd9, 0xf6,
		0x6c, 0x43, 0x32, 0x1d, 0xd0, 0xff, 0x8e, 0xa1,
		0xe3, 0xcc, 0xbd, 0x92, 0x5f, 0x70, 0x01, 0x2e,
		0xb4, 0x9b, 0xea, 0xc5, 0x08, 0x27, 0x56, 0x79,
		0x4d, 0x62, 0x13, 0x3c, 0xf1, 0xde, 0xaf, 0x80,
		0x1a, 0x35, 0x44, 0x6b, 0xa6, 0x89, 0xf8, 0xd7,
		0x90, 0xbf, 0xce, 0xe1, 0x2c, 0x03, 0x72, 0x5d,
		0xc7, 0xe8, 0x99, 0xb6, 0x7b, 0x54, 0x25, 0x0a,
		0x3e, 0x11, 0x60, 0x4f, 0x82, 0xad, 0xdc, 0xf3,
		0x69, 0x46, 0x37, 0x18, 0xd5, 0xfa, 0x8b, 0xa4,
		0x05, 0x2a, 0x5b, 0x74, 0xb9, 0x96, 0xe7, 0xc8,
		0x52, 0x7d, 0x0c, 0x23, 0xee, 0xc1, 0xb0, 0x9f,
		0xab, 0x84, 0xf5, 0xda, 0x17, 0x38, 0x49, 0x66,
		0xfc, 0xd3, 0xa2, 0x8d, 0x40, 0x6f, 0x1e, 0x31,
		0x76, 0x59, 0x28, 0x07, 0xca, 0xe5, 0x94, 0xbb,
		0x21, 0x0e, 0x7f, 0x50, 0x9d, 0xb2, 0xc3, 0xec,
		0xd8, 0xf7, 0x86, 0xa9, 0x64, 0x4b, 0x3a, 0x15,
		0x8f, 0xa0, 0xd1, 0xfe, 0x33, 0x1c, 0x6d, 0x42
	};

	char byteToChar(byte b)
	{
		return (char) (b&0xFF);
	}
	
	int packet_number=0;
	int polls_since_last_packet=0;
	
	boolean receiving_packet =false;
	boolean inside_escape=false;
	int received_length=0;
	public void receive_byte(byte b)
	{
		char data=byteToChar(b);

		received_length++;

		if (data== BEGINNING_OF_FIELD)
			receive_beginning_of_field(data);
		else if (data== END_OF_FIELD)
			receive_end_of_field(data);
		else if (data==CHARACTER_ESCAPE)
			receive_escape_character(data);
		else
			receive_payload(data);
	}
	
	void receive_beginning_of_field(char data)
	{
		/* If we have already started recieving, then
		 * we are not supposed to get another start
		 * of transmission character.
		 *
		 * If we get a BOF, reception is aborted.
		 */
		if (receiving_packet)
		{
			receive_reset();
			return;
		}
		
		//else, enable reception
		packet_number++;
		polls_since_last_packet=0;
		
		receiving_packet =true;
	}

	final int MINIMUM_PACKET_SIZE=2;
	void receive_end_of_field(char data)
	{
		if (receiving_packet)
		{
			if (receive_buffer.size()< MINIMUM_PACKET_SIZE)
				return; //The packet is invalid, must contain at least Command ID byte and CRC byte

			PacketSerial packet=new PacketSerial(receive_buffer);
			receive_buffer.clear();
			
			receive_verify_packet(packet);
		}
		
		//Stop receiving, wait for next packet
		receive_reset();
	}
	
	void receive_escape_character(char data)
	{
		if (!receiving_packet)
		{
			receive_reset();
			return;
		}
		
		//receive_payload will unescape the data
		inside_escape=true;
	}
	
	void receive_payload(char data)
	{
		if (!receiving_packet)
		{
			receive_reset();
			return;
		}
		
		//unmask the data
		if (inside_escape)
		{
			data=mask_character(data);
			inside_escape=false;
		}

		//add to payload buffer
		receive_buffer.append(data);
	}
	
	void receive_reset()
	{
		receiving_packet =false;
		inside_escape=false;
		
		received_length=0;
		polls_since_last_packet=0;
	}	
	
	char calculate_crc(byte[] inputData)
	{
		char running_crc= CRC_INITIAL_VALUE;
		for(byte i: inputData)
			running_crc=crc_table[ ((running_crc & 0xFF) ^ (i&0xFF)) & 0xFF ];
		
		return (char) (running_crc&0xFF);
	}

	public byte calculate_crc(PacketSerial packet)
	{
		char running_crc= CRC_INITIAL_VALUE;

		running_crc=crc_table[ ((running_crc & 0xFF) ^ (packet.command&0xFF)) & 0xFF ];
		running_crc=crc_table[ ((running_crc & 0xFF) ^ (packet.messageId&0xFF)) & 0xFF ];

		for(byte i: packet.payload)
			running_crc=crc_table[ ((running_crc & 0xFF) ^ (i&0xFF)) & 0xFF ];

		return (byte) running_crc;
	}
	
	void receive_verify_packet(PacketSerial packet)
	{
		byte running_crc=calculate_crc(packet);

		if (running_crc != packet.crc)
			return; //reject packet due to bad CRC

		//add it the receive queue
		received_packets.add(packet);
	}
	
	char mask_character(char i)
	{
		char masked=(char) (i^TRANSPARENCY_MASK);

		return masked;
	}
	
	boolean is_special_character(char  i)
	{
		if (i== BEGINNING_OF_FIELD || i==CHARACTER_ESCAPE || i== END_OF_FIELD)
			return true;
		
		return false;
	}
	
	@Override
	synchronized public PacketSerial transmit(byte command, byte messageId, byte[] dataInput)
	{
		ByteArray input=new ByteArray();
		input.append(command);
		input.append(messageId);
		input.append(dataInput);
		char crc=calculate_crc(input.toArray());

		ByteArray dataOutput=new ByteArray();
		dataOutput.append((byte) BEGINNING_OF_FIELD);

		for(byte b: input.toArray())
		{
			char i=byteToChar(b);

			if (is_special_character(i))
			{
				i=mask_character(i);
				dataOutput.append(CHARACTER_ESCAPE);
			}

			dataOutput.append(i);
		}
		
		if (is_special_character(crc))
		{
			dataOutput.append(CHARACTER_ESCAPE);
			crc=mask_character(crc);
		}

		dataOutput.append(crc);
		dataOutput.append(END_OF_FIELD);
		
		internal_transmit(dataOutput.toArray());

		//append crc so that packet can be returned
		input.append(crc);
		PacketSerial packet=new PacketSerial(input);
		return packet;
	}
	
	void internal_transmit(byte[] buffer)
	{		
		//Write to file
		serial.write(buffer);
	}
	
	void internal_receive()
	{
		//Read from file
		byte[] buffer=serial.read();
		
		for(byte i : buffer)
			receive_byte(i);
	}
	

	long last_packet_number=0;
	@Override
	synchronized public void poll()
	{
		internal_receive();
		
		if (receiving_packet)
		{
			polls_since_last_packet++;
			if (last_packet_number==packet_number && polls_since_last_packet>1)
				receive_reset();
			
			last_packet_number=packet_number;
		}
	}

	@Override
	synchronized public PacketSerial[] receive()
	{
		PacketSerial[] packets=received_packets.toArray(new PacketSerial[0]);
		received_packets.clear();
		return packets;
	}
}
