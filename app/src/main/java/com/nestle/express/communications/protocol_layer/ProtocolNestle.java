/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.protocol_layer;

import android.os.Handler;
import android.util.Log;

import com.nestle.express.communications.physical_layer.PacketSerial;
import com.nestle.express.communications.physical_layer.PhysicalLayer;

import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 *
 * @author jason
 */
public class ProtocolNestle implements ProtocolLayer
{
	PhysicalLayer mPhysical=null;

	int messageCounter;
	int numberSentMessages, numberRetries, numberFailures;
	ArrayDeque<PacketSerial> mIncomingPackets = new ArrayDeque<>();
	ArrayDeque<ProtocolTransmission> mOutgoingRetries = new ArrayDeque<>();
	ArrayDeque<ProtocolTransmission> mOutgoingEvents = new ArrayDeque<>();
	ArrayDeque<ProtocolTransmission> mPendingEvents = new ArrayDeque<>();
	ArrayList<CallbackInstance> mCallbacks = new ArrayList<>();

	public ProtocolNestle(PhysicalLayer physical)
	{
		mPhysical=physical;
	}
	
	@Override
	synchronized public void open()
	{
		mPhysical.open();
	}

	@Override
	synchronized public void addCallback(ProtocolEvent event, ProtocolCallback callback)
	{
		CallbackInstance instance = new CallbackInstance(event, callback);
		mCallbacks.add(instance);
	}
	
	@Override
	synchronized public void transmit(ProtocolEvent sentEvent, ProtocolEvent listenerEvent)
	{
		mOutgoingEvents.add(new ProtocolTransmission(sentEvent, listenerEvent));
	}

	void send(ProtocolTransmission transmission, int counter)
	{
		ProtocolEvent event=transmission.outgoing;
		PacketSerial packet=mPhysical.transmit((byte) event.getCommandId(), (byte) counter, event.generatePacket());
		transmission.messageId=packet.messageId;
		numberSentMessages++;
	}

	synchronized public void sendEventFromQueue()
	{
		if (!mOutgoingRetries.isEmpty())
		{
			//Log.d("TX", ""+System.currentTimeMillis());
			ProtocolTransmission tx=mOutgoingRetries.removeFirst();
			Log.d("TX", "Retry "+(int)tx.incoming.getCommandId()+"/"+tx.messageId);
			send(tx, tx.messageId);
			numberRetries++;
			return;
		}

		if (mOutgoingEvents.isEmpty())
			return;

		//Log.d("TX", ""+System.currentTimeMillis());
		ProtocolTransmission transmission=mOutgoingEvents.removeFirst();
		Log.d("TX", "Sent "+(int) transmission.outgoing.getCommandId()+"/"+messageCounter);
		send(transmission, messageCounter);
		mPendingEvents.add(transmission);
		messageCounter++;

		Log.d("RXTXSTAT","Retry "+numberRetries+"/"+numberSentMessages
				+" ("+Math.round(100*numberRetries/((double) numberSentMessages))+"%) \t"
				+"Fail  "+numberFailures+"/"+numberSentMessages
				+" ("+Math.round(100*numberFailures/((double) numberSentMessages))+"%)");
	}
	
	@Override
	synchronized public void receive(Handler handle)
	{
		receiveEventsFromQueue(handle);
	}

	synchronized void receiveEventsFromQueue(Handler handle)
	{
		//Log.d("RX", ""+System.currentTimeMillis());
		//each packet is guaranteed to have both valid command id and crc (although not payload)
		for(PacketSerial packet : mIncomingPackets)
		{
			for(CallbackInstance instance : mCallbacks)
			{
				if (instance.command==packet.command)
					instance.decodeAndCall(packet, handle);
			}

			ArrayDeque<ProtocolTransmission> eventsToRemove=new ArrayDeque<>();
			for(ProtocolTransmission tx : mPendingEvents)
			{
				if (tx.incoming.getCommandId()==packet.command &&
						tx.messageId==packet.messageId)
				{
					eventsToRemove.add(tx);
					Log.d("RX", "got "+(int) tx.incoming.getCommandId()+" "+tx.messageId);
				}
			}

			for(ProtocolTransmission event : eventsToRemove)
				mPendingEvents.remove(event);

			for(ProtocolTransmission r:mPendingEvents)
				Log.d("RX"," pending "+(int)r.incoming.getCommandId()+"/"+r.messageId);
		}

		ArrayDeque<ProtocolTransmission> eventsToRemove=new ArrayDeque<>();
		//update counters on pending
		for(ProtocolTransmission tx : mPendingEvents)
		{
			tx.retryCount++;
			if (tx.retryCount>5)
			{
				eventsToRemove.add(tx);
				runErrorCallback();
				numberFailures++;
			}
			else
				mOutgoingRetries.add(tx);
		}

		for(ProtocolTransmission event : eventsToRemove)
			mPendingEvents.remove(event);

		mIncomingPackets.clear();
	}
	
	@Override
	synchronized public void close()
	{
		mPhysical.close();
	}

	synchronized public void reset()
	{
		close();
		open();
	}

	synchronized public void poll()
	{
		mPhysical.poll();
		PacketSerial[] packets=mPhysical.receive();

		for(PacketSerial serial : packets)
			mIncomingPackets.add(serial);
	}

	Runnable mOnError;
	public void setOnErrorCallback(Runnable run)
	{
		mOnError=run;
	}

	void runErrorCallback()
	{
		if (mOnError!=null)
			mOnError.run();
	}
}
