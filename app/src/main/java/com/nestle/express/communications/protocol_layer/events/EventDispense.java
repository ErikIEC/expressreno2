package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.ByteArray;
import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 7/15/16.
 */
public class EventDispense extends ProtocolEvent
{
	final static char COMMAND_ID=0x10;

	@Override
	public char getCommandId()
	{
		return COMMAND_ID;
	}

	Channel channel = Channel.CHANNEL_NONE;
	double speedFill;
	double speedPump;
	int stepsPump;
	int stepsStart;
	Bank bank = Bank.BANK_LOW;
	Mode mode = Mode.MODE_NORMAL;
	int rinseTime;

	//Packet generation
	ByteArray packet = new ByteArray();
	@Override
	public byte[] generatePacket()
	{
		packet.clear();
		packet.append(channel.ordinal());
		appendRPM(speedFill);
		appendRPM(speedPump);
		packet.appendUint16(stepsPump);
		packet.appendUint16(stepsStart);
		packet.append(bank.ordinal());
		packet.append(mode.ordinal());
		packet.appendUint16(rinseTime);

		return packet.toArray();
	}

	//formulas

	long convertRPMToDriverFormat(double rpm)
	{
		return (long) (rpm * 0.065536+0.5);
	}

	void appendRPM(double rpm)
	{
		if (rpm<100)
		{
			packet.appendUint32(0);
			return;
		}

		long result=convertRPMToDriverFormat(rpm);

		packet.appendUint32(result);
	}


	//Auto generated setters/getters

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}

	public void setSpeedFill(double speedFill)
	{
		this.speedFill = speedFill;
	}

	public void setSpeedPump(double speedPump)
	{
		this.speedPump = speedPump;
	}

	public void setStepsPump(int stepsPump)
	{
		this.stepsPump = stepsPump;
	}

	public void setStepsStart(int stepsStart)
	{
		this.stepsStart = stepsStart;
	}

	public void setBank(Bank bank)
	{
		this.bank = bank;
	}

	public void setMode(Mode mode)
	{
		this.mode = mode;
	}

	public void setRinseTime(int rinseTime)
	{
		this.rinseTime = rinseTime;
	}


	//types

	public enum Channel
	{
		CHANNEL_NONE, //0
		CHANNEL_1, //1
		CHANNEL_2, //2
		CHANNEL_3, //3
		CHANNEL_4,  //4
		CHANNEL_WATER_NOZZLE  //5
	}

	public enum Bank
	{
		BANK_LOW, //0
		BANK_HIGH //1
	}

	public enum Mode
	{
		MODE_NORMAL,			//0
		MODE_CONCENTRATE,		//1
		MODE_WATER,				//2
		MODE_CALIBRATE_WATER,	//3
		MODE_CALIBRATE_CONCENTRATE		//4
	}
}
