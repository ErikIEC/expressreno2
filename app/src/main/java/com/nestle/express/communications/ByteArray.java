package com.nestle.express.communications;

/**
 * Created by jason on 7/14/16.
 *
 */

public class ByteArray
{
	int mPosition=0;
	byte[] mArray = new byte[16];

	public ByteArray() { }
	public ByteArray(byte[] b) { append(b); }
	public ByteArray(ByteArray b) { append(b.toArray()); }


	public void append(boolean input)
	{
		byte value=0;
		if (input)
			value=1;
		mArray[mPosition]=value;

		incrementPosition();
	}

	public void append(int i)
	{
		mArray[mPosition]=(byte) i;

		incrementPosition();
	}

	public void append(char b)
	{
		mArray[mPosition]=(byte) b;

		incrementPosition();
	}

	public void append(byte b)
	{
		mArray[mPosition]=b;

		incrementPosition();
	}

	public void append(byte[] bytes)
	{
		for(byte b : bytes)
			append(b);
	}

	public void appendUint16(int data)
	{
		append(getByte(data, 0));
		append(getByte(data, 1));
	}

	public void appendUint32(long data)
	{
		append(getByte(data, 0));
		append(getByte(data, 1));
		append(getByte(data, 2));
		append(getByte(data, 3));
	}

	public byte get(int i)
	{
		return mArray[i];
	}

	public byte getFirst()
	{
		return mArray[0];
	}

	public byte getLast()
	{
		return mArray[mPosition-1];
	}

	public byte removeLast()
	{
		byte value=getLast();
		decrementPosition();

		return value;
	}

	void incrementPosition()
	{
		mPosition++;
		if (mPosition>=mArray.length)
			expandArray();
	}

	void expandArray()
	{
		byte[] newArray=new byte[mArray.length*2];

		System.arraycopy( mArray, 0, newArray, 0, mArray.length );
		mArray=newArray;
	}

	void decrementPosition()
	{
		if (mPosition<1)
			return; //silently fail

		mPosition--;
	}

	public void clear()
	{
		mPosition=0;
	}

	public byte[] toArray()
	{
		int length=mPosition;
		if (length<0)
			return new byte[0];

		byte[] temporary=new byte[length];

		System.arraycopy(mArray, 0, temporary, 0, length);
		return temporary;
	}

	public byte[] toArray(int skipFront, int skipBack)
	{
		int length=mPosition-(skipFront+skipBack);

		if (length<0)
			return new byte[0];

		byte[] temporary=new byte[length];

		System.arraycopy(mArray, skipFront, temporary, 0, length);
		return temporary;
	}

	public int size()
	{
		return mPosition;
	}

	public boolean isEmpty()
	{
		if (mPosition>0)
			return false;
		return true;
	}


	public static byte getByte(int value, int byteNumber)
	{
		return (byte) (value>>(8*byteNumber));
	}

	public static byte getByte(long value, int byteNumber)
	{
		return (byte) (value>>(8*byteNumber));
	}
}