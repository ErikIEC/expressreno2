package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.ByteArray;
import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 12/19/16.
 */
public class EventIORequest extends ProtocolEvent
{
	final static char COMMAND_ID=0x15;
	public final int CTEST_COMPRESSOR=1, CTEST_WATER=2, CTEST_CONCENTRATE=3;

	public char getCommandId()
	{
		return COMMAND_ID;
	}

	public boolean inTestMode=true, motorEnabled, valveEnabled, testDoorLED, testDispenseLED,
			testEvapFan, testHeater, testCondFan, testWaterRefSol, testConcRefSol;

	public int motorTest; //1-4
	public int valveTest; //1-5
	public int compressorTest; //1-3

	//Packet generation
	ByteArray packet = new ByteArray();
	@Override
	public byte[] generatePacket()
	{
		packet.clear();

		//Is test mode enabled?
		packet.append(inTestMode); //Test mode enabled

		packet.append(generateByte(false, testDoorLED, testDispenseLED, testEvapFan, testHeater,
				testCondFan, testWaterRefSol, testConcRefSol)); //Test IO Struct
		packet.append(0); //Spare
		packet.append(motorTest); //Motor Test number
		packet.append(motorEnabled);
		packet.append(valveTest); //Valve number
		packet.append(valveEnabled);
		packet.append(compressorTest); //compressor test number

		return packet.toArray();
	}
}
