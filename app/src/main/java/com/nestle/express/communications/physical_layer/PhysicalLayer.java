/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.physical_layer;


/**
 * Encodes incoming payloads for transmission
 * via a physical device such as a UART.
 * 
 * Often adds error checking information like
 * CRC checksums and places the payload into
 * a specified packet format.
 * 
 * @author jason
 */
public interface PhysicalLayer
{
	void open();
	void close();

	PacketSerial transmit(byte command, byte messageId, byte[] inputData);
	PacketSerial[] receive();
	void poll();
}
