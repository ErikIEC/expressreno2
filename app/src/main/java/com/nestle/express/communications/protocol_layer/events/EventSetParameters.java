package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.ByteArray;
import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 1/3/17.
 */
public class EventSetParameters extends ProtocolEvent
{
	final static char COMMAND_ID=0x22;
	public char getCommandId()
	{
		return COMMAND_ID;
	}

	public double water1, water2, concentrate;

	int computeAdc(double temperature)
	{
		// in degrees F
		// temp=108.8214d -0.0357143d * adc;

		//return (int) Math.round((temperature - 108.8214d)/(-0.0357143d));

        int[] tempSetting = {2310,2308,2254,2224,2196,2167,2138,2110,2081,2053,2025,1996,1969,
                1941,1913,1886,1858,1831,1804,1777,1751,1724,1698,1672,1646,1620};
        int index;

        /* limit input parameter to array bounds */
        if(temperature < 25.0)
            temperature = 25.0;
        else if(temperature > 50.0)
            temperature = 50.0;

        index = (int) Math.round(temperature-25);
        return (tempSetting[index]);
	}

	ByteArray packet = new ByteArray();
	@Override
	public byte[] generatePacket()
	{
		packet.clear();
		packet.appendUint16(computeAdc(water1));
		packet.appendUint16(computeAdc(water2));
		packet.appendUint16(computeAdc(concentrate));

		return packet.toArray();
	}


}
