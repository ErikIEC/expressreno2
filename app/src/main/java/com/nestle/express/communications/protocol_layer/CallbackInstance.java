package com.nestle.express.communications.protocol_layer;

import android.os.Handler;

import com.nestle.express.communications.physical_layer.PacketSerial;

/**
 * Created by jason on 7/14/16.
 *
 */
public class CallbackInstance
{
	char command;
	public ProtocolCallback callback;
	public ProtocolEvent event;

	public CallbackInstance(ProtocolEvent protocolEvent, ProtocolCallback call)
	{
		event=protocolEvent;
		callback=call;
		command=event.getCommandId();
	}

	public void decodeAndCall(PacketSerial packet, Handler handle)
	{
		event.decodePacket(packet.payload);

		if (callback!=null)
		{
			Runnable runner=new Runnable()
			{
				@Override
				public void run()
				{
					callback.run(event);
				}
			};

			handle.post(runner);
		}
	}
}
