package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 4/24/17.
 */
public class EventClearFaults extends ProtocolEvent
{
	final static char COMMAND_ID=0x17;
	public char getCommandId()
	{
		return COMMAND_ID;
	}
}
