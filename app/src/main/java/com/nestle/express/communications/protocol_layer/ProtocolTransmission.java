package com.nestle.express.communications.protocol_layer;

/**
 * Created by jason on 3/28/17.
 */
public class ProtocolTransmission
{
	public int messageId, retryCount;
	public ProtocolEvent outgoing, incoming;

	public ProtocolTransmission(ProtocolEvent out, ProtocolEvent in)
	{
		outgoing=out;
		incoming=in;
	}
}
