package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 4/24/17.
 */
public class EventClearFaultsResponse extends ProtocolEvent
{
	final static char COMMAND_ID=0x18;
	public char getCommandId()
	{
		return COMMAND_ID;
	}
}
