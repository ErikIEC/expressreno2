/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.device_layer;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * This contains all of the ugly error handling
 * for dealing with the file associated with a
 * serial port
 *
 * @author jason
 */

public class DeviceFile implements DeviceLayer
{
	void print_error(String str)
	{
		System.out.println("Error: "+str+"\n");
	}
	
	String				input_filename="";
	String				output_filename="";
	FileInputStream		input=null;
	FileOutputStream	output=null;
	
	public DeviceFile(String file_name)
	{
		input_filename=file_name;
		output_filename=file_name;
	}
	
	public DeviceFile(String input_name, String output_name)
	{
		input_filename=input_name;
		output_filename=output_name;
	}

	public void stty(String file)
	{
		try {
			java.lang.Process process = Runtime.getRuntime().exec("/system/bin/busybox stty -F "+input_filename+" raw -echo -echoe -echok 19200");
			process.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void open()
	{
		stty(input_filename);
		stty(output_filename);

		openInput();
		openOutput();
	}
	
	void openInput()
	{
		try
		{
			input=new FileInputStream(input_filename);
		}
		catch(FileNotFoundException e)
		{
			print_error(e.toString());
		}
	}
	
	void openOutput()
	{
		try
		{
			output=new FileOutputStream(output_filename ,true);
		}
		catch(IOException e)
		{
			print_error(e.toString());
		}
	}
		
	@Override
	synchronized public byte[] read()
	{
		ByteArrayOutputStream data=new ByteArrayOutputStream();
		
		if (input==null)
		{
			print_error("Cannot read: input device is null");
			return data.toByteArray();
		}
		
		Integer available;
		try
		{
			available=input.available();
		}
		catch(IOException e)
		{
			print_error(e.toString());
			return data.toByteArray();
		}
		
		if (available<1)
			return data.toByteArray();
		
		//We now know data is available
		byte[] buffer=new byte[available];
		
		try
		{
			input.read(buffer);
		}
		catch(IOException e)
		{
			print_error(e.toString());
			return data.toByteArray();
		}
		
		for(int i: buffer)
			data.write(i);
		
		return data.toByteArray();
	}
		
	@Override
	synchronized public void write(byte[] buffer)
	{
		if (output==null)
		{
			print_error("Cannot write: output device is null");
			return;
		}

		try
		{
			for(byte i : buffer)
				output.write(i);
		}
		catch(IOException e)
		{
			print_error(e.toString());
		}
		
		try
		{
			output.flush();
		}
		catch(IOException e)
		{
			print_error(e.toString());
		}
	}
		
	@Override
	public void close()
	{
		if (output!=null)
		{
			try
			{
				output.flush();
				output.close();
			}
			catch(Exception e)
			{
				print_error(e.toString());
			}
		}
		
		if (input!=null)
		{
			try
			{
				input.close();
			}
			catch(IOException e)
			{
				print_error(e.toString());
			}
		}
	}
}
