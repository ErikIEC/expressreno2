package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 7/15/16.
 *
 */
public class EventStatusRequest extends ProtocolEvent
{
	final static char COMMAND_ID=0x13;
	public char getCommandId()
	{
		return COMMAND_ID;
	}
}
