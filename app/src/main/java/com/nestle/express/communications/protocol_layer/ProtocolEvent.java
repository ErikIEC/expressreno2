/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.protocol_layer;

import com.nestle.express.communications.ByteArray;

/**
 *
 * @author jason
 */
public class ProtocolEvent implements Cloneable
{
	final static char COMMAND_ID=0;
	public char getCommandId()
	{
		return COMMAND_ID;
	}

	ByteArray packet = new ByteArray();
	public byte[] generatePacket()
	{
		packet.clear();
		return packet.toArray();
	}
	
	public void decodePacket(byte[] payload)
	{

	}

	public static int getByte(byte[] buffer, int position)
	{
		try
		{
			return (int) buffer[position]&0xFF;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}

	public static boolean getBit(byte[] buffer, int position, int bit)
	{
		try
		{
			return ((buffer[position]>>bit)&1)==1;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public static long getUint32(byte[] buffer, int position)
	{
		int b1, b2, b3, b4;

		b1=getByte(buffer, position);
		b2=getByte(buffer, position+1);
		b3=getByte(buffer, position+2);
		b4=getByte(buffer, position+3);

		long result=(b4<<24)|(b3<<16)|(b2<<8)|(b1);

		return result;
	}

	public static int getUint16(byte[] buffer, int position)
	{
		int b1, b2;

		b1=getByte(buffer, position);
		b2=getByte(buffer, position+1);

		int result=(b2<<8)|(b1);

		return result;
	}

	public byte generateByte(boolean b1, boolean b2, boolean b3, boolean b4,
							 boolean b5, boolean b6, boolean b7, boolean b8)
	{
		int i=0;

		if (b1) i|=0x01;
		if (b2) i|=0x02;
		if (b3) i|=0x04;
		if (b4) i|=0x08;
		if (b5) i|=0x10;
		if (b6) i|=0x20;
		if (b7) i|=0x40;
		if (b8) i|=0x80;

		return (byte) i;
	}
}
