/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nestle.express.communications.protocol_layer;


import android.os.Handler;

/**
 *
 * @author jason
 */
public interface ProtocolLayer
{
	void open();
	void close();
	void addCallback(ProtocolEvent event, ProtocolCallback callback);
	void receive(Handler handle);
	void sendEventFromQueue();
	void reset();
	void transmit(ProtocolEvent sentEvent, ProtocolEvent listenerEvent);
	void poll();
	void setOnErrorCallback(Runnable run);
}
