package com.nestle.express.communications.test_screens;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.nestle.express.communications.R;
import com.nestle.express.communications.device_layer.DeviceFile;
import com.nestle.express.communications.device_layer.DeviceLayer;
import com.nestle.express.communications.physical_layer.PhysicalLayer;
import com.nestle.express.communications.physical_layer.PhysicalNestleSerial;
import com.nestle.express.communications.protocol_layer.ProtocolLayer;
import com.nestle.express.communications.protocol_layer.ProtocolNestle;
import com.nestle.express.communications.protocol_layer.events.EventDispense;
import com.nestle.express.communications.protocol_layer.events.EventDispenseResponse;

public class ActivityTestComms extends Activity
{
	DeviceLayer device;
	PhysicalLayer physical;
	ProtocolLayer protocol;



	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_comms);




		device = new DeviceFile("/dev/ttymxc1");
		physical = new PhysicalNestleSerial(device);
		protocol = new ProtocolNestle(physical);

		protocol.open();
	}

	void dispense(EventDispense.Channel channel)
	{
		EventDispense dispense = new EventDispense();
		dispense.setChannel(channel);
		dispense.setBank(EventDispense.Bank.BANK_LOW);
		dispense.setMode(EventDispense.Mode.MODE_NORMAL);
		dispense.setSpeedFill(1997.8);
		dispense.setSpeedPump(610);
		dispense.setStepsStart(400);
		dispense.setStepsPump(600);

		protocol.transmit(dispense, new EventDispenseResponse());
		protocol.poll();
	}

	public void startDispense1(View v)
	{
		dispense(EventDispense.Channel.CHANNEL_1);
	}

	public void startDispense2(View v)
	{
		dispense(EventDispense.Channel.CHANNEL_2);
	}

	public void startDispense3(View v)
	{
		dispense(EventDispense.Channel.CHANNEL_3);
	}

	public void startDispense4(View v)
	{
		dispense(EventDispense.Channel.CHANNEL_4);
	}

	void stop(EventDispense.Channel channel)
	{
		EventDispense dispense = new EventDispense();
		dispense.setChannel(channel);

		protocol.transmit(dispense, new EventDispenseResponse());
		protocol.poll();
	}

	public void endDispense(View v)
	{
		stop(EventDispense.Channel.CHANNEL_1);
		stop(EventDispense.Channel.CHANNEL_2);
		stop(EventDispense.Channel.CHANNEL_3);
		stop(EventDispense.Channel.CHANNEL_4);
	}

	public void quit(View v)
	{
		finish();
	}
}
