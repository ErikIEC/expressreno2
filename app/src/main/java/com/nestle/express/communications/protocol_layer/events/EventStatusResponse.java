package com.nestle.express.communications.protocol_layer.events;

import com.nestle.express.communications.protocol_layer.ProtocolEvent;

/**
 * Created by jason on 7/15/16.
 */
public class EventStatusResponse extends ProtocolEvent
{
	final static char COMMAND_ID=0x14;

	@Override
	public char getCommandId()
	{
		return COMMAND_ID;
	}

	double water1;
	double water2;
	double concentrate;

	//Bit declaration generated with status packet worksheet
	//status byte 1
	boolean flagDefrost;
	boolean faultShort;
	boolean faultWater1;
	boolean faultWater2;
	boolean faultConcentrate;
	boolean callWater1;
	boolean callWater2;
	boolean callConcentrate;

	//status byte 2
	boolean ioDoor;
	boolean ioCompressor;
	boolean ioCompressorFan;
	boolean ioCabinet;
	boolean ioConcentrate;
	boolean ioWater;
	boolean ioCompressorDisconnect;
	boolean ioDefrost;

	MotorFaults motor1=new MotorFaults();
	MotorFaults motor2=new MotorFaults();
	MotorFaults motor3=new MotorFaults();
	MotorFaults motor4=new MotorFaults();

	int currentW1, currentW2,currentW3, currentW4, currentW5;
	int currentCabFan, currentWSol, currentCSol;
	int versionMajor, versionMinor;
    int refrigState;
    int compressorRunTimer;

	boolean flagWaterSolErr, flagConcSolErr, flagCompErr, flagCompFanErr, flagCoolErr, flagOvertempErr, flagCabFanError;

	public boolean isFlagCompErr()
	{
		return flagCompErr;
	}

	public boolean isFlagCompFanErr()
	{
		return flagCompFanErr;
	}

	public boolean isFlagCoolErr()
	{
		return flagCoolErr;
	}

	public boolean isFlagOvertempErr()
	{
		return flagOvertempErr;
	}

	public boolean isFlagConcSolErr()
	{
		return flagConcSolErr;
	}

	public boolean isFlagWaterSolErr()
	{
		return flagWaterSolErr;
	}

	public boolean isFlagCabFanError()
    {
        return flagCabFanError;
    }

	public int getCurrentW1()
	{
		return currentW1;
	}

	public int getCurrentW2()
	{
		return currentW2;
	}

	public int getCurrentW3()
	{
		return currentW3;
	}

	public int getCurrentW4()
	{
		return currentW4;
	}

	public int getCurrentW5()
	{
		return currentW5;
	}

	public int getCurrentCabFan()
	{
		return currentCabFan;
	}

	public int getCurrentWSol()
	{
		return currentWSol;
	}

	public int getCurrentCSol()
	{
		return currentCSol;
	}
	//Decode

	public int getVersionMajor()
	{
		return versionMajor;
	}

	public int getVersionMinor()
	{
		return versionMinor;
	}

	@Override
	public void decodePacket(byte[] payload)
	{
		int length = payload.length;

		water1=getTemperature(payload, 0);
		water2=getTemperature(payload, 2);
		concentrate=getTemperature(payload, 4);

		//Bit assignment generated with status packet worksheet
		flagDefrost=getBit(payload, 6, 0);
		faultShort=getBit(payload, 6, 1);
		faultWater1=getBit(payload, 6, 2);
		faultWater2=getBit(payload, 6, 3);
		faultConcentrate=getBit(payload, 6, 4);
		callWater1=getBit(payload, 6, 5);
		callWater2=getBit(payload, 6, 6);
		callConcentrate=getBit(payload, 6, 7);

		ioDoor=getBit(payload, 7, 0);
		ioCompressor=getBit(payload, 7, 1);
		ioCompressorFan=getBit(payload, 7, 2);
		ioCabinet=getBit(payload, 7, 3);
		ioConcentrate=getBit(payload, 7, 4);
		ioWater=getBit(payload, 7, 5);
		ioCompressorDisconnect=getBit(payload, 7, 6);
		ioDefrost=getBit(payload, 7, 7);

		motor1=decodeMotor(payload, 8);
		motor2=decodeMotor(payload, 9);
		motor3=decodeMotor(payload, 10);
		motor4=decodeMotor(payload, 11);

		currentW1=getUint16(payload, 12);
		currentW2=getUint16(payload, 14);
		currentW3=getUint16(payload, 16);
		currentW4=getUint16(payload, 18);
		//currentW5=getUint16(payload, 20);
        currentW5 = (getByte(payload, 20)*4);
        refrigState = getByte(payload, 21);
        currentCabFan=getUint16(payload, 22);
		currentWSol=getUint16(payload, 24);
		currentCSol=getUint16(payload, 26);

		flagWaterSolErr=getBit(payload, 28, 0);
		flagConcSolErr=getBit(payload, 28, 1);
		flagCompErr=getBit(payload, 28, 2);
		flagCompFanErr=getBit(payload, 28, 3);
		flagCoolErr=getBit(payload, 28, 4);
		flagOvertempErr=getBit(payload, 28, 5);
        flagCabFanError=getBit(payload, 28, 7);

        if(length > 29) {
            versionMajor = getByte(payload, 29);
            versionMinor = getByte(payload, 30);
            compressorRunTimer = getUint16(payload, 31);
        }
        else{
            versionMajor = 0;
            versionMinor = 0;
            compressorRunTimer = 0;
        }


	}

	MotorFaults decodeMotor(byte[] payload, int byte_number)
	{
		MotorFaults motor=new MotorFaults();

		motor.stepperUVLO=getBit(payload, byte_number, 1);
		motor.stepperTHWRN=getBit(payload, byte_number, 2);
		motor.stepperTHSD=getBit(payload, byte_number, 3);
		motor.stepperOCD=getBit(payload, byte_number, 4);
		motor.homeSenseError=getBit(payload, byte_number, 6);
		motor.waterError=getBit(payload, byte_number, 7);

		return motor;
	}

	//Formulas
	double getTemperature(byte buffer[], int position)
	{
		int adc=getUint16(buffer, position);

		return  108.52d - 0.0362d * adc;//108.8214d -0.0357143d * adc;//adc*(-0.0414f) + 114.0341; //adc*(-0.036153f)+108.27f;
	}

    public String getCompressorRunTimer(){
        int minute = compressorRunTimer/60;
        int second = compressorRunTimer - (minute*60);
        String minutes = String.format("%02d", minute);
        String seconds = String.format("%02d", second);

        return (minutes+":"+seconds);
    }

	//Auto generated getters
    public int getRefrigState() {
        if(refrigState > 9)
            return 9;
        else
            return refrigState;
    }

	public double getWater1()
	{
		return water1;
	}

	public double getWater2()
	{
		return water2;
	}

	public double getConcentrate()
	{
		return concentrate;
	}

	public boolean isFlagDefrost()
	{
		return flagDefrost;
	}

	public boolean isFaultShort()
	{
		return faultShort;
	}

	public boolean isFaultWater1()
	{
		return faultWater1;
	}

	public boolean isFaultWater2()
	{
		return faultWater2;
	}

	public boolean isFaultConcentrate()
	{
		return faultConcentrate;
	}

	public boolean isCallWater1()
	{
		return callWater1;
	}

	public boolean isCallWater2()
	{
		return callWater2;
	}

	public boolean isCallConcentrate()
	{
		return callConcentrate;
	}

	public boolean isIoDoor()
	{
		return ioDoor;
	}

	public boolean isIoCompressor()
	{
		return ioCompressor;
	}

	public boolean isIoCompressorFan()
	{
		return ioCompressorFan;
	}

	public boolean isIoCabinet()
	{
		return ioCabinet;
	}

	public boolean isIoConcentrate()
	{
		return ioConcentrate;
	}

	public boolean isIoWater()
	{
		return ioWater;
	}

	public boolean isIoCompressorDisconnect()
	{
		return ioCompressorDisconnect;
	}

	public boolean isIoDefrost()
	{
		return ioDefrost;
	}

	public MotorFaults getMotor1()
	{
		return motor1;
	}

	public MotorFaults getMotor2()
	{
		return motor2;
	}

	public MotorFaults getMotor3()
	{
		return motor3;
	}

	public MotorFaults getMotor4()
	{
		return motor4;
	}

	public class MotorFaults
	{
		public boolean stepperUVLO;
		public boolean stepperTHWRN;
		public boolean stepperTHSD;
		public boolean stepperOCD;
		public boolean homeSenseError;
		public boolean waterError;

		public boolean isF1()
		{
			return waterError;
		}

		public boolean isF3()
		{
 			return stepperUVLO||stepperTHWRN||stepperTHSD||stepperOCD||homeSenseError;
		}
	}
}
