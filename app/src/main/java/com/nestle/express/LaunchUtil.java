package com.nestle.express;

import android.content.Context;
import android.content.Intent;

/**
 * Created by jason on 1/30/17.
 */
public class LaunchUtil
{
	static public void LaunchMain(Context c)
	{
		Intent i=new Intent(c, com.nestle.express.frontend_ui.MainActivity.class);
		c.startActivity(i);
	}
}
