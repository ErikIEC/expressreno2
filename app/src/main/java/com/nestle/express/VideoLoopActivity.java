package com.nestle.express;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

import com.nestle.express.frontend_ui.MainActivity;


/**
 * Created by jason on 8/14/16.
 */
public class VideoLoopActivity extends ActivityFullScreen implements MediaPlayer.OnCompletionListener
{
	/**
	 * Tag
	 */
	public static final String TAG = "VideoLoopActivity";

	/**
	 * {@link android.widget.VideoView} showing the idle screen video
	 */
	private VideoView mVideoView;

	View.OnTouchListener listener=new View.OnTouchListener()
	{
		@Override
		public boolean onTouch(View v, MotionEvent event)
		{

			Log.i("JASON", "Video 1 clicked, starting playback");

			launch();

			return false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_loop);
		mVideoView = (VideoView) findViewById(R.id.idle_video_view);

		final String path = "android.resource://"+  getPackageName() + "/"+R.raw.odopod_attractl_loop;
		Log.d(TAG, path);


		Uri uri = Uri.parse(path);

		mVideoView.setVideoURI(uri);
		mVideoView.setOnCompletionListener(this);


		mVideoView.setOnTouchListener(listener);

		mVideoView.start();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		mVideoView.start();
	}

	@Override
	public void onCompletion(MediaPlayer mp)
	{
		if (!mp.isLooping())
		{
			mp.setLooping(true);
			mp.start();
		}
	}

	void launch()
	{
		try
		{
			startActivity(new Intent(this, MainActivity.class));
			overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

