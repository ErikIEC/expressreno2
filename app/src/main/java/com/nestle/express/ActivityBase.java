package com.nestle.express;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.nestle.express.communications.test_screens.ActivityTestComms;

import com.nestle.express.test.ActivityTestBackend;
import com.nestle.express.frontend_setup.activity.ServiceCenterActivity;
import com.nestle.express.frontend_ui.ActivityUI;
import com.nestle.express.iec_backend.ExpressApp;



public class ActivityBase extends Activity
{
	TextView startupStatus;
	boolean pinScreen=true;
	DevicePolicyManager mDpm;
	ActivityManager activityManager;
	ExpressApp mApp;

	Runnable runSetFullScreen = new Runnable()
	{
		@Override
		public void run()
		{
			setFullscreen();
			handler.postDelayed(runSetFullScreen, 1000);
		}
	};

	Handler handler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);


		if (BuildConfig.TYPE=="PRODUCTION")
		{
			setContentView(R.layout.activity_blank);
			//startActivity(new Intent(this , ActivityUI.class));
			startActivity(new Intent(this , ActivityStartup.class));
			return;
		}

		mDpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_base);

		//handler.post(runSetFullScreen);
		setFullscreen();

		if (!mDpm.isLockTaskPermitted(this.getPackageName()))
		{
			Context context = getApplicationContext();
			Toast.makeText(context, "Must Be Device Admin, Press Button 2", Toast.LENGTH_LONG);
		}

		String s = PackageUtilities.getPackageVersion(getPackageManager(), getPackageName());

		((TextView) findViewById(R.id.versionText)).setText("Version "+s);
		startupStatus=(TextView) findViewById(R.id.startupCheckText);

		mApp=(ExpressApp) getApplication();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mHandler.post(mUpdateStatus);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_base, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings)
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	void setFullscreen()
	{
		getWindow().getDecorView().setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	void activateScreenPinning()
	{
		Log.d("PINNING", this.getPackageName());


		/*List<ActivityManager.RunningTaskInfo> services = activityManager.getRunningTasks(Integer.MAX_VALUE);

		boolean inForeground = false;

		if (services.get(0).topActivity.getPackageName().toString().equalsIgnoreCase(getPackageName().toString()))
			inForeground=true;

		if (inForeground)
		{*/
		ComponentName mDeviceAdminSample = new ComponentName(this, ReceiverAdmin.class);

		mDpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		try
		{
			if (mDpm.isLockTaskPermitted(this.getPackageName()) && !activityManager.isInLockTaskMode())
			{
				Log.d("PINNING", "ALLOWED");
				startLockTask();
			}
			else
				Log.d("PINNING", "NOT ALLOWED");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		//}
	}

	void deactivateScreenPinning()
	{
		//Causes security exception because the Updater is the device owner, not this app.
		//as a result the only way to unlock is to foribly quit with System.exit(1)
		if (mDpm.isLockTaskPermitted(this.getPackageName()) && activityManager.isInLockTaskMode())
			stopLockTask();
	}

	public void quit(View v)
	{
		pinScreen=false;

		finish();
		System.exit(1);
	}

	public void onWindowFocusChanged(boolean focus)
	{
		super.onWindowFocusChanged(focus);

		if (focus)
		{
			if (pinScreen)
				activateScreenPinning();

			setFullscreen();
		}
	}

	@Override
	public void onBackPressed()
	{

	}

	public void toUI(View v)
	{
		startActivity(new Intent(this , ActivityUI.class));
	}

	public void toVideo(View v)
	{
		startActivity(new Intent(this, VideoLoopActivity.class));
	}

	public void toSettings(View v)
	{
		//startActivity(new Intent(this , ActivityToplevel.class));
		//startActivity(new Intent(this , StaionFaultActivity.class));
		startActivity(new Intent(this, ServiceCenterActivity.class));

	}

	public void toComms(View v)
	{
		startActivity(new Intent(this , ActivityTestComms.class));
	}

	public void toTest(View v)
	{
		startActivity(new Intent(this , ActivityTestBackend.class));
	}

	public void toDateTime(View v)
	{
		startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
	}

	public void toStartup(View v)
	{
		startActivity(new Intent(this , ActivityStartup.class));
	}

	Handler mHandler=new Handler();
	Runnable mUpdateStatus=new Runnable()
	{
		@Override
		public void run()
		{
			ExpressApp.STARTUP_FAILURE_CAUSE cause=mApp.getStartupFailureCase();
			startupStatus.setText("Status: "+cause.toString());

			startupStatus.setBackgroundColor(0);
			if(cause== ExpressApp.STARTUP_FAILURE_CAUSE.ART_DATABASE_ERROR)
			{
				startupStatus.setTextColor(0xFFFFFF00); //Yellow
				startupStatus.setBackgroundColor(0xFFCCCCCC);
				startupStatus.append("\nWarning: the application may crash if launched.");
			}
			else if (cause== ExpressApp.STARTUP_FAILURE_CAUSE.NO_COMMUNICATIONS)
			{
				startupStatus.setTextColor(0xFFFF0000); //Red
				startupStatus.append("\nSome features may not work as intended...");

			}
			else if (cause!= ExpressApp.STARTUP_FAILURE_CAUSE.NO_ERROR)
			{
				startupStatus.setTextColor(0xFFFF0000); //Red
				startupStatus.append("\nWarning: the application may crash if launched.");

			}
			else
				startupStatus.setTextColor(0xFF00AA00); //Green

			mHandler.postDelayed(this, 1000);
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		mHandler.removeCallbacksAndMessages(null);
		handler.removeCallbacksAndMessages(null);
	}
}
