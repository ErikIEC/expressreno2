package com.nestle.express;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;


public class ActivityRestore extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restore);

		//Boilerplate start
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Restore Done")
				.setCancelable(false)
				.setPositiveButton("Go Back to Updater", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
		//boilerplate end
	}

	public void quit()
	{
		finish();
	}

}
